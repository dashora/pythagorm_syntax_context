echo "Evaluating..."
#cd ..

## Here we create a file_list for use with LgEval.
ls output/full_sys_lg/* > temp_a
perl -p -e "s/output\/full_sys_lg/Data\/Expressions\/LG/" temp_a > temp_b
paste temp_a temp_b | column -s $'\t' -t > example_file_list 
rm temp_a temp_b
echo "print1"
echo $LgEvalDir
echo "print2"
evaluate example_file_list t

echo ""
echo "Evaluation results from LgEval are in:"
echo ""
echo "    Results_example_file_list/"
echo ""
echo "Example finished! Have fun."
