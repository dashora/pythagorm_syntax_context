from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
import time

def test_parser():

    segment_config_filename = "configs/symbol_segmentation.conf"
    parsing_config_filename = "configs/symbol_parsing.conf"
    classify_config_filename = "configs/symbol_recognize.conf"

    segment_config,segment_config_name = Configuration.from_file(segment_config_filename),"SegmentConfig"
    parsing_config, parsing_config_name = Configuration.from_file(parsing_config_filename), "ParsingConfig"
    classify_config, classify_config_name = Configuration.from_file(classify_config_filename), "ClassifyConfig"

    #map all the configuration instances in the controller
    control_runner = Controller(segment_config, segment_config_name)
    control_runner.addConfig(parsing_config,parsing_config_name)
    control_runner.addConfig(classify_config, classify_config_name)

    print("[Loading..] \t training and testing Expressions\n")

    testing_set_name= "Testing_Exp_set"
    #load the expressions commonly for all the process
    control_runner.loadExpressions(name=testing_set_name, config_name=segment_config_name, isTraining=False)
    control_runner.preprocessExpressions(testing_set_name, config_name=segment_config_name)
    control_runner.createExpressionGraphs(testing_set_name, segment_config_name)
    control_runner.visualize_expressions(testing_set_name, ".//output/visuals_TraceLevelReport//")


    #create data-set for segmentation
    print("\n[Updating ..] \t merge strokes of same symbol in testing set\n")
    control_runner.merge_strokes_to_symbols(testing_set_name)
    print("\n [Re-Constructing..]\t new layout graph\n")
    control_runner.createExpressionGraphs(testing_set_name, segment_config_name)

    control_runner.visualize_expressions(testing_set_name, ".//output/visuals//")


if __name__ == '__main__':
    test_parser()
