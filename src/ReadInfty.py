import os
import numpy as np
from math import *
from skimage import io
from recognizer.configuration.configuration import Configuration
from recognizer.preprocessing.cc_ops import CCOps

def getBits(num):
    bits = []
    value = num
    for p in range(0,8):
        if value < 2**(7-p):
            bits.append(0)
        else:
            bits.append(1)
            value -= 2**(7-p)
    return bits

def bitmap2img(bits, height, width):
    img = np.zeros([height, width])
    byteWidth = ceil(width/8.0)
    index = 0
    for y in range(0,height):
        scanLine = []
        for b in range(0,byteWidth):
            scanLine += getBits(int(bits[index]))
            index += 1
        img[y,:] = np.array(scanLine)[0:width]
    return img

def loadLabelMap(mapFile):
    labels_file = open(mapFile, "r")
    lines = labels_file.readlines()
    labels_file.close()

    labelsMap = {}
    for line in lines:
        parts = line.strip().split(",")
        if len(parts) == 3:
            labelsMap[parts[0][2:]] = parts[2]
    return labelsMap

def readChar(symbolArgs, labelsMap, id = None):
    parts = symbolArgs.split(",")
    if id is None:
        symId = parts[0]
        if parts[2] in labelsMap:
            symLabel = labelsMap[parts[2]]
        else:
            print("Invalid character code: " + parts[2])
            symLabel = "Invalid"
        img = bitmap2img(parts[15:], int(parts[3]), int(parts[4]))
    else:
        symId = str(id)
        if parts[1] in labelsMap:
            symLabel = labelsMap[parts[1]]
        else:
            print("Invalid character code: " + parts[1])
            symLabel = "Unknown"
        img = bitmap2img(parts[4:], int(parts[2]), int(parts[3]))

    if img.flatten().sum() == 0:
        return None,None,None
    return symId, symLabel, img

def writeLg(symbols, count, sig, config):
    rowHeight = max([sym[2].shape[0] for sym in symbols]) + 1
    rowWidth = (max([sym[2].shape[1] for sym in symbols]) * 10) + 9
    numRows = ceil(len(symbols) / 10.0)
    imageGrid = np.zeros([rowHeight * ceil(len(symbols)/10),rowWidth])
    offSets = []
    insertPosition = np.array([0,0])

    objects = []
    notes = []
    conComps = []
    compIndex = 0

    for r in range(0, numRows):
        if len(symbols) < (r+1) *10:
            rowSyms = symbols[r*10:]
        else:
            rowSyms = symbols[r*10:(r+1)*10]
        for sym in rowSyms:
            note = "#sym, " + sym[0] + ", "
            obj = "O, " + sym[0] + ", " + sym[1] + ", 1.0, "
            heightOffSet = rowHeight - sym[2].shape[0]
            ystart = insertPosition[0]+heightOffSet
            yend = ystart + sym[2].shape[0] - 1
            xstart = insertPosition[1]
            xend = xstart + sym[2].shape[1] - 1
            imageGrid[ystart:yend+1, xstart:xend+1] = sym[2]
            note += str(ystart) + ", " + str(xstart) + ", " + str(yend) + ", " + str(xend)
            insertPosition = [insertPosition[0], xend + 2]

            # find and add connected components
            comps = CCOps.connectedComp(sym[2])
            boxes = CCOps.boundingBoxes(comps)
            for b in boxes:
                cc = "#cc " + str(compIndex) + ", " + str(b[0] + ystart) + ", " + str(b[1] + xstart) + ", " + str(b[2] + ystart) + ", " + str(b[3] + xstart)
                conComps.append(cc)
                obj += str(compIndex) + ", "
                compIndex += 1
            objects.append(obj)
            notes.append(note)

        insertPosition = [rowHeight*(r+1), 0]

    #remove extra columns at ends of image grid
    edge = imageGrid.shape[1]
    for c in range(0,rowWidth-1):
        if imageGrid[:,c:].flatten().sum() == 0:
            edge = c+1
            break
    imageGrid = imageGrid[:, :edge]

    filename = symbols[0][1] + "_" + str(count) + "_" + sig
    lgpath = config.get_str("SYM_FILES_DIR_PATH")
    imgpath = config.get_str("SYM_IMGS_DIR_PATH")
    os.makedirs(lgpath, exist_ok=True)
    os.makedirs(imgpath, exist_ok=True)
    io.imsave(imgpath + filename+".PNG", imageGrid)
    lg = open(lgpath + filename+".lg", "w")
    lg.write("# IUD, " + filename + "\n")
    lg.write("#Exp " + filename + " " + "\n")
    for n in notes:
        lg.write(n + "\n")
    lg.write("# [ OBJECTS ]\n")
    lg.write("# Primitive Nodes (N): " + str(len(conComps)) + "\n")
    lg.write("#    Objects (O): " + str(len(symbols)) + "\n")
    for obj in objects:
        lg.write(obj + "\n")
    lg.write("\n# [PRIMITAVES]\n")
    for cc in conComps:
        lg.write(cc + "\n")
    lg.close()
    return filename

def createExpression(ExpId, symStrings, config):
    indexMap = {}
    objects = []
    rels = []
    notes = []
    imgs = []
    locations = []
    symBoxMap = {}
    idx = 0
    for sym in symStrings:
        symId, obj, rel, n, sym, loc = createSymbol(sym, config)
        indexMap[symId] = idx
        idx += 1
        objects.append(obj)
        rels.append(rel)
        notes.append(n)
        imgs.append(sym)
        locations.append(loc)

    locations = np.array(locations)
    # Create Expression Image
    # buildExpImg adjusts locations
    expImg = buildExpImg(imgs, locations)
    imgpath = config.get_str("EXP_IMG_DIR_PATH")
    os.makedirs(imgpath, exist_ok=True)
    io.imsave(imgpath + ExpId + ".PNG", expImg.astype(np.uint8) * 255)
    for symId, idx in indexMap.items():
        symBoxMap[tuple(locations[idx])] = symId
    # add connected component primitives and symbol bounding boxes
    ccMap = CCOps.matchCC(symBoxMap, expImg)
    #print(ExpId)
    #print(ccMap)
    compIndex = 0
    conComps = []
    for symLoc, symId in symBoxMap.items():
        symIdx = indexMap[symId]
        if symId in ccMap:
            boxes = ccMap[symId]
        else:
            comps = CCOps.connectedComp(imgs[symIdx])
            local = locations[symIdx]
            boxes = [np.array(box) + np.array([local[0], local[1], local[0], local[1]]) for box in CCOps.boundingBoxes(comps)]

        notes[symIdx] += str(locations[symIdx,0]) + ", " + str(locations[symIdx,1]) + ", " + str(locations[symIdx,2]) + ", " + str(locations[symIdx,3])
        for b in boxes:
            cc = "#cc, " + str(compIndex) + ", " + str(b[0]) + ", " + str(b[1]) + ", " + str(b[2]) + ", " + str(b[3])
            conComps.append(cc)
            objects[symIdx] += ", " + str(compIndex)
            compIndex += 1
    # for s in range(len(imgs)):
    #     comps = CCOps.connectedComp(imgs[s])
    #     boxes = CCOps.boundingBoxes(comps)
    #     notes[s] += str(locations[s,0]) + ", " + str(locations[s,2]) + ", " + str(locations[s,1]) + ", " + str(locations[s,3])
    #     for b in boxes:
    #         cc = "#cc, " + str(compIndex) + ", " + str(b[0] + locations[s,0]) + ", " + str(b[1] + locations[s,2]) + ", " + str(b[2] + locations[s,0] + 1) + ", " + str(b[3] + locations[s,2] + 1)
    #         conComps.append(cc)
    #         objects[s] += ", " + str(compIndex)
    #         compIndex += 1
    pn = 0
    numObjs = len(symStrings)
    lgpath = config.get_str("EXPR_LG_DIR_PATH")
    os.makedirs(lgpath, exist_ok=True)
    lg = open(lgpath + ExpId + ".lg", 'w')
    lg.write("# IUD, " + ExpId + "\n")
    lg.write("#Exp " + ExpId + " " + symStrings[0].split(" ")[-1] + "\n")
    for n in notes:
        lg.write(n + "\n")
    lg.write("# [ OBJECTS ]\n")
    lg.write("# Primitive Nodes (N): " + "?" + "\n")
    lg.write("#    Objects (O): " + str(numObjs) + "\n")
    for obj in objects:
        lg.write(obj + "\n")
    reledges = 0
    relcount = 0
    lg.write("\n# [ RELATIONSHIPS ]\n# Primitive Edges (E): ? (? merge, ? relationship \n")
    lg.write("#\tObject Relationships (R): " + str(len(symStrings) - 1) + "\n")
    for rel in rels:
        if len(rel) > 3:
            lg.write(rel + "\n")
    lg.write("\n# [PRIMITAVES]\n")
    for cc in conComps:
        lg.write(cc + "\n")
    lg.close()


def createSymbol(argString, config):
    values = argString.split(" ")
    object = "O, "
    rel = "R, "
    note = "#sym, "
    object += values[0] + ", " + values[1] + ", 1.0"
    if values[7] != "-1":
        rel += values[7] + ", " + values[0] + ", " + values[9] + ", 1.0"
    note += values[0] + ", " #+ values[3] + ", " + values[2]
    path = config.get_str("DATASET_IMG_PATH")
    # sympath = config.get_str("SYM_IMG_DIR_PATH")
    fileImg = io.imread(path+values[-1], "PNG")
    symImg = 1 - (fileImg[int(values[3]):int(values[5]), int(values[2]):int(values[4])]/255)
    # os.makedirs(sympath + values[1], exist_ok=True)
    #io.imsave(sympath + values[1] + "//" + values[0] + ".PNG", img[int(values[3]):int(values[5]), int(values[2]):int(values[4])])
    location = [int(values[3]),int(values[2]),int(values[5]),int(values[4])]
    return values[0],object,rel,note,symImg,np.array(location)


def buildExpImg(symbols, locations):
    ymax = np.max([loc[2] for loc in locations])
    ymin = np.min([loc[0] for loc in locations])
    xmax = np.max([loc[3] for loc in locations])
    xmin = np.min([loc[1] for loc in locations])
    locations -= np.array([ymin-1, xmin-1, ymin-1, xmin-1])
    expImg = np.zeros([ymax-ymin+2, xmax-xmin+2])
    for i in range(len(symbols)):
        sym = symbols[i]
        loc = locations[i]
        expImg[loc[0]:loc[2], loc[1]:loc[3]] = sym
    return expImg


def convertInfty2(inftyFile, config):
    infty = open(config.get_str("EXPR_LIST_PATH") + inftyFile, 'r')
    expressionIds = []
    for line in infty:
        if line[0] == 'E' or line[0] == 'e':
            ExpId = line.split(" ")[1].strip()
            expressionIds.append(ExpId)
            symStrings = []
            symLine = infty.readline().strip()

            while symLine != "" and symLine[0] != "E":
                symStrings.append(symLine)
                symLine = infty.readline().strip()
            createExpression(ExpId, symStrings, config)
    exprListFile = open("..//Data//Expressions//Expression_List.txt", 'w')
    for expr_id in expressionIds:
        exprListFile.write(expr_id + "\n")
    exprListFile.close()



def convertInfty3(inftyFile, listname, config, startId = None):
    syms = []
    symsFileCount = []
    symsIndex = dict([])
    fileList = []
    labelMap = loadLabelMap(config.get_str("CHAR_LABEL_MAP"))

    for line in open(inftyFile):
        if startId is None:
            symbol = readChar(line.strip(), labelMap)
        else:
            symbol = readChar(line.strip(), labelMap, startId)
            startId += 1
        if symbol[0] is None:
            continue
        if symbol[1] not in symsIndex:
            symsIndex[symbol[1]] = len(symsIndex)
            syms.append([symbol])
            symsFileCount.append(0)
        else:
            syms[symsIndex[symbol[1]]].append(symbol)
        if len(syms[symsIndex[symbol[1]]]) == 100:
            fileList.append(writeLg(syms[symsIndex[symbol[1]]], symsFileCount[symsIndex[symbol[1]]], listname.split(".")[0] + "_" + str(np.array(symsFileCount).sum()), config))
            syms[symsIndex[symbol[1]]] = []
            symsFileCount[symsIndex[symbol[1]]] += 1
    for i in range(0,len(syms)):
        sym = syms[i]
        count = symsFileCount[i]
        if len(sym) > 0:
            fileList.append(writeLg(sym, count, listname.split(".")[0] + "_" + str(np.array(symsFileCount).sum()), config))
            syms[i] = []
            symsFileCount[i] += 1

    listpath = config.get_str("SYM_FILE_LIST_PATH")
    listfile = open(listpath + listname, 'w')
    for f in fileList:
        listfile.write(f + "\n")
    listfile.close()
    return fileList

if __name__ == "__main__":
    config = Configuration.from_file("ImageConf.conf")
    # print(convertInfty3("Data//infty//InftyCDB-3//CharInfoDB-3-A.txt", "Symbols_3A.txt", config))
    # print(convertInfty3("Data//infty//InftyCDB-3//InftyCDB-3-B.txt", "Symbols_3B.txt", config, 284885))
    convertInfty2("FixedAllEnglishInftyMath_v3.GT", config)
    # print(convertInfty3("Data//infty//InftyCDB-3//CharInfoTest.txt", "TestList.txt", config))

