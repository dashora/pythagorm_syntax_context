
import sys
import math
import pickle
import time

import numpy as np
from concurrent.futures import ProcessPoolExecutor

from sklearn.ensemble import RandomForestClassifier

from recognizer.configuration.configuration import Configuration
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.visualization.SVG_generator import SVGGenerator
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.features.code_book_feature_extractor import CodeBookFeatureExtractor
from recognizer.training.grid_search_ops import GridSearchOps
from recognizer.training.trainer import ClassifierTrainer
from recognizer.evaluation.eval_ops import EvalOps

def load_raw_data(config):
    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # check if only  a subset of classes will be used ....
    if config.contains("ONLY_ACTIVE_CLASSES"):
        active_classes = config.get("ONLY_ACTIVE_CLASSES")
    else:
        active_classes = None

    workers = config.get_int("PARALLEL_INKML_LOADING_WORKERS", 10)

    # load training data, no features extracted, only symbols pre-processed
    training_path = config.data["TRAINING_DATASET_PATH"]
    data_info = INKMLLoader.load_INKML_directory(training_path, True, None, label_replacement,
                                                 False, None, None, True, workers, active_classes)

    raw_training, valid_files, error_files = data_info

    all_symbols, raw_labels, _ = raw_training

    return all_symbols, raw_labels

def encode_feature_vector(data):
    boxes_data, params = data

    box_regions, box_features = boxes_data
    code_book, cb_feat_extractor = params

    return cb_feat_extractor.getSymbolEncoding(box_regions, box_features, code_book)


def extract_symbol_features(data):
    symbol, cb_feat_extractor, extra_feature_extractor = data

    all_features, all_boxes = cb_feat_extractor.getFeatures(symbol)

    if extra_feature_extractor is not None:
        extra_features = extra_feature_extractor.getFeatures(symbol)
    else:
        extra_features = None

    return all_features, all_boxes, extra_features

def split_data(all_symbols, boxes_regions, boxes_features, mapped_labels, extra_features, num_folds):
    by_class = np.argsort(mapped_labels, axis=0)
    part_symbols, part_features, part_boxes, part_labels, part_indices = [], [], [], [], []
    if extra_features is None:
        part_extra_features = None
    else:
        part_extra_features = []

    for i in range(num_folds):
        current_indices = np.squeeze(by_class[i::num_folds])

        # symbols, their boxes and features ...
        current_partition_symbols = []
        current_partition_boxes = []
        current_partition_features = []
        current_partition_extra = []
        for sym_idx in current_indices:
            sym_idx = int(sym_idx)
            current_partition_symbols.append(all_symbols[sym_idx])
            current_partition_boxes.append(boxes_regions[sym_idx])
            current_partition_features.append(boxes_features[sym_idx])
            if extra_features is not None:
                current_partition_extra.append(extra_features[sym_idx])

        # assign elements per partition ...
        part_symbols.append(current_partition_symbols)
        part_features.append(current_partition_features)
        part_boxes.append(current_partition_boxes)
        part_labels.append(mapped_labels[current_indices])
        part_indices.append(current_indices)
        if extra_features:
            part_extra_features.append(current_partition_extra)

    return part_symbols, part_features, part_boxes, part_labels, part_indices, part_extra_features


def prepare_crossval_clustering_data(num_cross_folds, num_fold, part_features, part_symbols):
    clustering_training_features = []

    num_training_symbols = 0
    if num_cross_folds > 1:
        for other_fold in range(num_cross_folds):
            if num_fold != other_fold:
                for sym_idx, features in enumerate(part_features[other_fold]):
                    clustering_training_features += features

                num_training_symbols += len(part_symbols[other_fold])

        num_testing_symbols = len(part_symbols[num_fold])
    else:
        # no cross-validation, do everything on the entire training set
        for sym_idx, features in enumerate(part_features[0]):
            clustering_training_features += features

        num_training_symbols = len(part_symbols[0])
        num_testing_symbols = 0

    clustering_training_data = np.array(clustering_training_features)

    return clustering_training_data, (num_training_symbols, num_testing_symbols)


def crossvalidate_code_book(all_symbols, label_mapping_info, config, classifier_configs, use_extra_features):
    # Experiment parameters
    num_clusters = config.get_int("CODE_BOOK_SIZE")
    soft_assignment_neighbors = min(config.get_int("CODE_BOOK_SOFT_ASSIGNMENT_NEIGHBORS"), num_clusters)
    batch_size = config.get_int("CODE_BOOK_CLUSTERING_KMEANS_BATCH_SIZE")

    # (regions in percentajes, from 0.0 to 1.0 [w, h, x, y])
    # (w, h, x, y, rows, cols, Soft)
    regions = config.get("CODE_BOOK_ENCODING_REGIONS")

    # box w, box h, sample dist, feat rows, feat cols, feat bins
    box_w_prc = config.get("CODE_BOOK_PATCH_WIDTH")
    box_h_prc = config.get("CODE_BOOK_PATCH_HEIGHT")

    cb_feat_extractor = CodeBookFeatureExtractor(box_w_prc, box_h_prc)
    cb_feat_extractor.set_clustering_parameters(num_clusters, batch_size)

    sampling_type = config.get_int("CODE_BOOK_PATCH_SAMPLING")
    # load parameters based on sampling type
    if sampling_type == CodeBookFeatureExtractor.SamplingUniformPerTrace:
        # Uniform by following traces
        uni_samp_dist = config.get("CODE_BOOK_PATCH_UNIFORM_SAMPLING_DISTANCE")
        cb_feat_extractor.set_uniform_trace_sampling_parameters(uni_samp_dist)

    elif sampling_type == CodeBookFeatureExtractor.SamplingUniformGrid:
        # Uniform by Grid
        grid_samp_rows = config.get("CODE_BOOK_PATCH_GRID_SAMPLING_ROWS")
        grid_samp_cols = config.get("CODE_BOOK_PATCH_GRID_SAMPLING_COLS")
        cb_feat_extractor.set_uniform_grid_sampling_parameters(grid_samp_rows, grid_samp_cols)

    elif sampling_type == CodeBookFeatureExtractor.SamplingSharpPoints:
        # Sampling on Sharp Points ...
        cb_feat_extractor.set_sharp_point_sampling_parameters()

    feature_type = config.get_int("CODE_BOOK_FEATURE_TYPE")
    # Load parameter based on local feature type
    if feature_type == CodeBookFeatureExtractor.LocalFeatureOrientationHistograms:
        feat_rows = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_ROWS")
        feat_cols = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_COLS")
        feat_bins = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_BINS")

        cb_feat_extractor.set_orientation_hist_parameters(feat_rows, feat_cols, feat_bins)

    elif feature_type == CodeBookFeatureExtractor.LocalFeatureLength2DHistograms:
        feat_rows = config.get_int("CODE_BOOK_FEATURE_LENGTH_2D_HIST_ROWS")
        feat_cols = config.get_int("CODE_BOOK_FEATURE_LENGTH_2D_HIST_COLS")

        cb_feat_extractor.set_length_2d_hist_parameters(feat_rows, feat_cols)


    #regions, use_soft_assignment, use_soft_grids, soft_assignment_neighbors
    cb_feat_extractor.set_encoding_parameters(regions, soft_assignment_neighbors)

    num_cross_folds = config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS")

    clear_string = " " * 80

    if use_extra_features:
        config.data["FEATURES_USE_CODE_BOOK_HISTOGRAMS"] = '0'
        extra_feature_extractor = FeatureExtractor(config)
    else:
        extra_feature_extractor = None

    time_preprocessing_start = time.time()
    workers = config.get_int("PARALLEL_FEATURE_EXTRACTION_WORKERS", 10)

    feature_symbol_input = [(symbol, cb_feat_extractor, extra_feature_extractor) for symbol in all_symbols]

    print("Computing training features")
    total_symbols = len(all_symbols)
    boxes_features = []
    boxes_regions = []
    if use_extra_features:
        extra_features = []
    else:
        extra_features = None

    num_boxes = 0
    with ProcessPoolExecutor(max_workers=workers) as executor:
        for i, symbol_data in enumerate(executor.map(extract_symbol_features, feature_symbol_input)):
            print(clear_string, end="\r")
            print("\tprocessing ({0:.3f}%): ".format((i + 1) * 100 / total_symbols), end="\r")

            symbol_features, symbol_boxes, sym_extra_features = symbol_data

            boxes_features.append(symbol_features)
            boxes_regions.append(symbol_boxes)

            num_boxes += len(symbol_boxes)

            if use_extra_features:
                extra_features.append(sym_extra_features)

    # label mapping
    mapped_labels, label_mapping, class_mapping = label_mapping_info

    print("Total features extracted: " + str(num_boxes))
    print("")
    print("Splitting data")
    partitions = split_data(all_symbols, boxes_regions, boxes_features, mapped_labels, extra_features, num_cross_folds)
    part_symbols, part_features, part_boxes, part_labels, part_indices, part_extra_features = partitions

    time_preprocessing_end = time.time()

    if use_extra_features:
        num_extra_features = len(extra_features[0])
    else:
        num_extra_features = 0

    print("")
    print("Using " + str(num_extra_features) + " extra features")

    print("Starting Cross-validation")
    total_time_preparing_data = 0.0
    total_time_clustering = 0.0
    total_time_generating_features = 0.0
    total_time_training = 0.0
    total_time_testing = 0.0

    all_training_accuracies, all_training_per_class_avg, all_training_per_class_std = {}, {}, {}
    all_testing_accuracies, all_testing_per_class_avg, all_testing_per_class_std = {}, {}, {}
    for class_config_filename, class_config, class_output_filename in classifier_configs:
        all_training_accuracies[class_config_filename] = np.zeros(num_cross_folds)
        all_training_per_class_avg[class_config_filename] = np.zeros(num_cross_folds)
        all_training_per_class_std[class_config_filename] = np.zeros(num_cross_folds)

        all_testing_accuracies[class_config_filename] = np.zeros(num_cross_folds)
        all_testing_per_class_avg[class_config_filename] = np.zeros(num_cross_folds)
        all_testing_per_class_std[class_config_filename] = np.zeros(num_cross_folds)

    eval_str = "Global: {0:.3f}%, Per-class AVG: {1:.3f}%, Per-class STDev: {2:.3f}%"
    total_features_used = 0

    for num_fold in range(num_cross_folds):
        time_data_prep_start = time.time()

        print("Fold #" + str(num_fold + 1))
        print("\tPreparing data....")

        clustering_data = prepare_crossval_clustering_data(num_cross_folds, num_fold, part_features, part_symbols)
        clust_train_data, (num_training_symbols, num_testing_symbols) = clustering_data

        print("\tUsing " + str(num_training_symbols) + " symbols for training")

        time_data_prep_end = time.time()
        total_time_preparing_data += time_data_prep_end - time_data_prep_start

        print("\tclustering...")
        time_clustering_start = time.time()

        code_book, mbk_means_labels = cb_feat_extractor.getCodeBook(clust_train_data)

        time_clustering_end = time.time()
        total_time_clustering += time_clustering_end - time_clustering_start

        print("\tpreparing training data")
        time_generating_features_start = time.time()

        # Generating codes for each symbol ...
        # ... prepare for parallel processing ...
        training_labels = []
        testing_labels = []
        all_training_feature_data = []
        all_testing_feature_data = []
        all_training_extra_features = []
        all_testing_extra_features = []
        for other_fold in range(num_cross_folds):
            for sym_idx, features in enumerate(part_features[other_fold]):
                boxes_features = features
                boxes_regions = part_boxes[other_fold][sym_idx]
                boxes_data = boxes_regions, boxes_features

                params = code_book, cb_feat_extractor

                data = boxes_data, params

                if num_fold != other_fold or num_cross_folds == 1:
                    training_labels.append(part_symbols[other_fold][sym_idx].truth)
                    all_training_feature_data.append(data)

                    if use_extra_features:
                        all_training_extra_features.append(part_extra_features[other_fold][sym_idx])
                else:
                    testing_labels.append(part_symbols[other_fold][sym_idx].truth)
                    all_testing_feature_data.append(data)

                    if use_extra_features:
                        all_testing_extra_features.append(part_extra_features[other_fold][sym_idx])

        encoding_length = cb_feat_extractor.current_encoding_length()
        total_features_used = encoding_length + num_extra_features

        # ... encoding training set ...
        classifier_training = np.zeros((num_training_symbols, encoding_length + num_extra_features), np.float32)
        with ProcessPoolExecutor(max_workers=workers) as executor:
            for i, feature_vector in enumerate(executor.map(encode_feature_vector, all_training_feature_data)):
                print(clear_string, end="\r")

                print("\tprocessing ({0:.3f}%): ".format((i + 1) * 100 / num_training_symbols), end="\r")

                classifier_training[i, :len(feature_vector)] = feature_vector
                if use_extra_features:
                    classifier_training[i, len(feature_vector):] = np.array(all_training_extra_features[i])

        training_ds = FeatureDataset.from_INKML_data(classifier_training, training_labels, label_mapping, class_mapping)

        print("")
        print("\tTotal features: " + str(classifier_training.shape[1]))

        # ... encoding testing set ....
        if num_cross_folds > 1:
            print("\tpreparing testing data")

            classifier_testing = np.zeros((num_testing_symbols, encoding_length + num_extra_features), np.float32)
            with ProcessPoolExecutor(max_workers=workers) as executor:
                for i, feature_vector in enumerate(executor.map(encode_feature_vector, all_testing_feature_data)):
                    print(clear_string, end="\r")

                    print("\tprocessing ({0:.3f}%): ".format((i + 1) * 100 / num_testing_symbols), end="\r")

                    classifier_testing[i, :len(feature_vector)] = feature_vector
                    if use_extra_features:
                        classifier_testing[i, len(feature_vector):] = np.array(all_testing_extra_features[i])

            testing_ds = FeatureDataset.from_INKML_data(classifier_testing, testing_labels, label_mapping, class_mapping)

        time_generating_features_end = time.time()
        total_time_generating_features += time_generating_features_end - time_generating_features_start

        # train symbol classifiers ....
        for conf_idx, (class_config_filename, class_config, class_output_filename) in enumerate(classifier_configs):
            print("\ttraining classifier (Configuration #{0})".format(conf_idx + 1))
            print("\tConfiguration: " + class_config_filename)

            time_training_start = time.time()

            classifier = ClassifierTrainer.train_classifier(training_ds, class_config)

            time_training_end = time.time()
            total_time_training += time_training_end - time_training_start

            time_testing_start = time.time()

            # training error
            accuracy, per_class_avg, per_class_stdev, _ = EvalOps.classifier_per_class_accuracy(classifier, training_ds)
            print("\tTraining = " + eval_str.format(accuracy, per_class_avg, per_class_stdev))

            all_training_accuracies[class_config_filename][num_fold] = accuracy
            all_training_per_class_avg[class_config_filename][num_fold] = per_class_avg
            all_training_per_class_std[class_config_filename][num_fold] = per_class_stdev

            # testing error
            if num_cross_folds > 1:
                accuracy, per_class_avg, per_class_stdev, _ = EvalOps.classifier_per_class_accuracy(classifier, testing_ds)

                print("\tTesting = " + eval_str.format(accuracy, per_class_avg, per_class_stdev))
                all_testing_accuracies[class_config_filename][num_fold] = accuracy
                all_testing_per_class_avg[class_config_filename][num_fold] = per_class_avg
                all_testing_per_class_std[class_config_filename][num_fold] = per_class_stdev

            time_testing_end = time.time()
            total_time_testing += time_testing_end - time_testing_start

    final_training_results = {}
    final_testing_results = {}
    for conf_idx, (class_config_filename, class_config, class_output_filename) in enumerate(classifier_configs):
        print("Configuration: " + class_config_filename)

        accuracy = all_training_accuracies[class_config_filename]
        per_class_avg = all_training_per_class_avg[class_config_filename]
        per_class_stdev = all_training_per_class_std[class_config_filename]

        final_training_results[class_config_filename] = (accuracy, per_class_avg, per_class_stdev)

        print("\tAVG Training: " + eval_str.format(accuracy.mean(), per_class_avg.mean(), per_class_stdev.mean()))

        if num_cross_folds > 1:
            accuracy = all_testing_accuracies[class_config_filename]
            per_class_avg = all_testing_per_class_avg[class_config_filename]
            per_class_stdev = all_testing_per_class_std[class_config_filename]

            final_testing_results[class_config_filename] = (accuracy, per_class_avg, per_class_stdev)

            print("\tAVG Testing: " + eval_str.format(accuracy.mean(), per_class_avg.mean(), per_class_stdev.mean()))

    time_completed = time.time()

    feature_extraction_time = time_preprocessing_end - time_preprocessing_start
    total_elapsed_time = time_completed - time_preprocessing_start

    print("Time pre-CV: (I/O + Feat. Ext + Label Mapping + Data Split): " + str(feature_extraction_time))

    print("Time CV preparation: " + str(total_time_preparing_data))
    print("Time CV clustering: " + str(total_time_clustering))
    print("Time CV generating features: " + str(total_time_generating_features))
    print("Time CV training: " + str(total_time_training))
    print("Time CV testing: " + str(total_time_testing))

    print("Total elapsed time: " + str(total_elapsed_time))

    return final_training_results, final_testing_results, total_features_used



def main():
    # usage check
    if len(sys.argv) < 4:
        print("Usage: python train_code_book_grid_search.py main_config search_config [Additional]")
        print("Where")
        print("\tmain_config\t= Path to main configuration file")
        print("\tsearch_config\t= Path to auxiliary configuration defining grid search")
        print("At least one of ")
        print("\t-c [classifier_config] [output_file]\t: Where")
        print("\t\tclassifier_config\t= Configuration file for classifier")
        print("\t\toutput_file\t= Output file for results using this classifier")
        print("Optionally")
        print("\t-b [code_book_config]\t= Additional configuration file for code book")

        print("")
        print("\tBy adding classifier configurations files, the original settings in main file will be ignored")
        return

    print("Loading configuration....")
    # main configuration
    config_filename = sys.argv[1]
    main_config = Configuration.from_file(config_filename)

    search_config_filename = sys.argv[2]
    search_config = Configuration.from_file(search_config_filename)

    classifier_configs = []

    # Handle extra parameters
    extra_params_pos = 3
    while extra_params_pos < len(sys.argv):
        if sys.argv[extra_params_pos] == "-b":
            if extra_params_pos + 1 < len(sys.argv):
                # additional code book configuration
                aux_config_filename = sys.argv[extra_params_pos + 1]
                aux_config = Configuration.from_file(aux_config_filename)

                for parameter in sorted(aux_config.data.keys()):
                    # override ...
                    print("Overriding: <" + parameter + "> from main file")
                    main_config.data[parameter] = aux_config.data[parameter]

                extra_params_pos += 2
            else:
                print("Ignoring -b")
                break
        elif sys.argv[extra_params_pos] == "-c":
            if extra_params_pos + 2 < len(sys.argv):
                # classifier configuration
                class_config_filename = sys.argv[extra_params_pos + 1]
                class_output_filename = sys.argv[extra_params_pos + 2]
                class_config = Configuration.from_file(class_config_filename)

                classifier_configs.append((class_config_filename, class_config, class_output_filename))

                extra_params_pos += 3
            else:
                print("Ignoring -c")
                break
        else:
            # just ignore
            print("Unknown parameter for value <" + sys.argv[extra_params_pos] + ">")
            extra_params_pos += 1

    if len(classifier_configs) == 0:
        # if no classifier configuration was specified, use the one from main file by default
        classifier_configs.append((config_filename, main_config))

    print("A total of " + str(len(classifier_configs)) + " classifier(s) will be used")

    search_params = GridSearchOps.extract_search_parameters(main_config, search_config)
    grid_parameters, grid_parameter_values, grid_combined, grid_desc = search_params

    # compute all parameter combinations to explore
    all_combinations = GridSearchOps.enumerate_combinations(grid_parameters, grid_parameter_values, grid_combined)

    print(grid_desc)

    # Output file header
    full_summary = {}
    for class_config_filename, class_config, class_output_filename in classifier_configs:
        GridSearchOps.write_search_header(class_output_filename, grid_desc, len(grid_parameters))
        full_summary[class_config_filename] = ""

    n_combs = len(all_combinations)

    num_features = np.zeros(n_combs)

    print("Loading data....")

    # dataset features will change, but raw data will not...
    all_training_symbols, all_raw_labels = load_raw_data(main_config)

    label_mapping_info = FeatureDataset.create_update_label_mapping(all_raw_labels)

    print("")
    print("Total symbols found: " + str(len(all_training_symbols)))
    print("Total classes found: " + str(len(label_mapping_info[1])))

    for idx, combination in enumerate(all_combinations):

        # apply configuration change
        for i, parameter in enumerate(grid_parameters):
            main_config.data[parameter] = str(combination[i])

        # check if any feature set (not code-book based) is active
        extra_features_names = main_config.get_active_features()
        if "FEATURES_USE_CODE_BOOK_HISTOGRAMS" in extra_features_names:
            extra_features_names.remove("FEATURES_USE_CODE_BOOK_HISTOGRAMS")

        extra_features = len(extra_features_names) > 0
        if extra_features:
            print("There are additional features active: ")
            for parameter in extra_features_names:
                print(parameter)
        else:
            print("No additional features active")

        train_r, test_r, num_features[idx] = crossvalidate_code_book(all_training_symbols, label_mapping_info,
                                                                     main_config, classifier_configs, extra_features)

        for class_config_filename, class_config, class_output_filename in classifier_configs:
            class_train_r = train_r[class_config_filename]
            class_test_r = test_r[class_config_filename]

            train_acc = class_train_r[0].mean()
            train_avg = class_train_r[1].mean()
            train_std = class_train_r[2].mean()

            eval_acc = class_test_r[0].mean()
            eval_avg = class_test_r[1].mean()
            eval_std = class_test_r[2].mean()

            comb_desc = "\t".join([str(val) for val in combination])
            result_desc = (comb_desc + "\t" +
                  str(round(train_acc, 3)) + "\t" +
                  str(round(train_avg, 3)) + "\t" +
                  str(round(train_std, 3)) + "\t" +
                  str(round(eval_acc, 3)) + "\t" +
                  str(round(eval_avg, 3)) + "\t" +
                  str(round(eval_std, 3)) + "\t" +
                  str(num_features[idx]))

            out_file = open(class_output_filename, "a")
            out_file.write(result_desc + "\n")
            out_file.close()

            full_summary[class_config_filename] += result_desc + "\n"

    print("")
    print("Process finished, Summary")
    print("")
    print(" \t" * (len(grid_parameters)) + "TRAINING \t \t   TESTING")
    print(" \t" * (len(grid_parameters)) + "ACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD\tFeat.")
    for class_config_filename, class_config, class_output_filename in classifier_configs:
        print("Using <" + class_config_filename + "> (Results also in " + class_output_filename + ")")
        print(full_summary[class_config_filename])


if __name__ == '__main__':
    main()