
import sys
import time
import pickle
import numpy as np
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.training.trainer import ClassifierTrainer
from recognizer.configuration.configuration import Configuration
from recognizer.evaluation.eval_ops import EvalOps

from IO.write_output import write_lgfile
from recognizer.util.math_ops import MathOps
import math

def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python train_crossvalidated_classifier.py config_file")
        print("Where")
        print("\tconfig_file\t= Path to configuration file")
        return

    print("Loading configuration....")
    config_filename = sys.argv[1]
    config = Configuration.from_file(config_filename)

    # check how data partitions will be used for cross-validation
    n_partitions = config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
    if n_partitions < 2:
        print("Invalid number of partitions <CLASSIFIER_CROSSVALIDATION_PARTITIONS>")
        return

    save_errors = config.get_bool("EVALUATION_SAVE_ERROR_DATA", False)

    # load training dataset once
    print("Loading data....")
    training = ClassifierTrainer.load_training_set(config)

    # ... run ...!
    print("Training begins....")
    train_r, eval_r, times, best_classifier, eval_errors = ClassifierTrainer.train_crossvalidation(training, config, True, '\n')

    all_training_accuracies, all_training_averages, all_training_stdevs = train_r
    all_validation_accuracies, all_validation_averages, all_validation_stdevs = eval_r
    total_training_time, total_evaluation_time = times

    print("    \t    TRAINING \t \t   TESTING")
    print(" #  \tACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD")

    for i in range(n_partitions):
        print(" " + str(i+1) + "\t" +
              str(round(all_training_accuracies[i], 3)) + "\t" +
              str(round(all_training_averages[i], 3)) + "\t" +
              str(round(all_training_stdevs[i], 3)) + "\t" +
              str(round(all_validation_accuracies[i], 3)) + "\t" +
              str(round(all_validation_averages[i], 3)) + "\t" +
              str(round(all_validation_stdevs[i], 3)))

    print(" Mean\t" + str(round(all_training_accuracies.mean(), 3)) + "\t" +
          str(round(all_training_averages.mean(), 3)) + "\t" +
          str(round(all_training_stdevs.mean(), 3)) + "\t" +
          str(round(all_validation_accuracies.mean(), 3)) + "\t" +
          str(round(all_validation_averages.mean(), 3)) + "\t" +
          str(round(all_validation_stdevs.mean(), 3)))

    print(" STDevs\t" + str(round(all_training_accuracies.std(), 3)) + "\t" +
          str(round(all_training_averages.std(), 3)) + "\t" +
          str(round(all_training_stdevs.std(), 3)) + "\t" +
          str(round(all_validation_accuracies.std(), 3)) + "\t" +
          str(round(all_validation_averages.std(), 3)) + "\t" +
          str(round(all_validation_stdevs.std(), 3)))

    print("")
    print("Training finished!")

    # save to file
    out_filename = config.data["CLASSIFIER_DIR"] + "/" + config.data["CLASSIFIER_NAME"] + ".dat"
    out_file = open(out_filename, 'wb')
    pickle.dump(best_classifier, out_file, pickle.HIGHEST_PROTOCOL)
    out_file.close()

    if save_errors:
        out_filename = config.data["EVALUATION_OUTPUT"] + "/" + config.data["CLASSIFIER_NAME"] + ".dat"
        out_file = open(out_filename, 'wb')
        # 0 - > Errors over training set
        pickle.dump(0, out_file, pickle.HIGHEST_PROTOCOL)
        pickle.dump(eval_errors, out_file, pickle.HIGHEST_PROTOCOL)
        out_file.close()

    print("-> Configuration file: " + config_filename)
    print("-> Training samples: " + str(training.num_samples()))
    print("-> Total features: " + str(training.num_attributes()))
    print("-> Total classes: " + str(training.num_classes()))
    print("-> Total partitions: " + str(n_partitions))
    print("Training Time")
    print(" -> Total: " + str(round(total_training_time, 3)))
    print(" -> Mean: " + str(round(total_training_time / n_partitions, 3)))
    print("Evaluation Time")
    print(" -> Total: " + str(round(total_evaluation_time, 3)))
    print(" -> Mean: " + str(round(total_evaluation_time / n_partitions, 3)))

    print("Finished!")

if __name__ == '__main__':
    main()