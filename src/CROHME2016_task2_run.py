
import sys
import pickle
import xml.etree.ElementTree as ET

from concurrent.futures import ProcessPoolExecutor

from recognizer.data.feature_dataset import FeatureDataset
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.configuration.configuration import Configuration
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.evaluation.eval_ops import EvalOps

def load_classifier(main_config):
    try:
        in_filename = main_config.data["CLASSIFIER_DIR"] + "/" + main_config.data["CLASSIFIER_NAME"] + ".dat"
        in_file = open(in_filename, 'rb')
        classifier = pickle.load(in_file)
        in_file.close()

        return classifier
    except:
        print("Error Loading Classifier")
        return None


def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python CROHME2016_task2_run.py config")
        print("Where")
        print("\tconfig\t= Path to main configuration file")
        return

    print("Loading configuration....")
    # main configuration
    config_filename = sys.argv[1]
    main_config = Configuration.from_file(config_filename)

    # Load training dataset (for normalization parameters, etc... )
    if main_config.get_bool("SCALE_NORMALIZE_DATA"):
        print("Loading training set for normalization parameters")

        training_filename = main_config.data["DATASET_DIR"] + "/" + main_config.data["TRAINING_DATASET_NAME"] + ".dat"
        training_dataset = FeatureDataset.load_from_file(training_filename)
        normalizer = training_dataset.normalizer
    else:
        normalizer = None

    # Load trained classifier
    classifier = load_classifier(main_config)
    if classifier is None:
        return

    # prepare feature extractor
    feature_extractor = FeatureExtractor(main_config)

    # Load test data
    path = main_config.get_str("CROHME_TASK2_DATA_PATH")

    workers = main_config.get_int("PARALLEL_FEATURE_EXTRACTION_WORKERS", 10)

    print("Loading data and computing features")
    dataset, _, _ = INKMLLoader.load_INKML_directory(path, False, feature_extractor, {}, True, None, None, True, workers)

    # Normalize test features
    if main_config.get_bool("SCALE_NORMALIZE_DATA"):
        print("")
        print("Normalizing data")
        dataset.normalize_data(normalizer)

    # classify test data
    print("")
    print("Classifying test data")
    top_k = main_config.get_int("CROHME_TASK2_TOP_K")
    all_confidences = EvalOps.predict_topk_in_chunks(dataset.data, classifier, top_k, 100)

    # produce output file
    print("Saving results")
    out_filename = main_config.get_str("CROHME_TASK2_OUTPUT")
    out_file = open(out_filename, "w")
    n_samples = dataset.data.shape[0]
    prefix_len = len(path + '/')
    suffix_len = len(".inkml")
    for idx in range(n_samples):
        symbol_file, symbol_id = dataset.sources[idx]

        tree = ET.parse(symbol_file)
        root = tree.getroot()

        annotations = INKMLLoader.load_INKML_annotations(root)
        uid = annotations["UI"]

        line = uid
        for class_name, confidence in all_confidences[idx]:
            line += "," + str(class_name)

        out_file.write(line + "\n")

    out_file.close()

    print("")
    print("Finished!")

if __name__ == '__main__':
    main()