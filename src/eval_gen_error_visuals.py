
import os
import sys
import pickle

from recognizer.configuration.configuration import Configuration
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.visualization.HTML_generator import HTMLGenerator
from recognizer.visualization.SVG_generator import SVGGenerator
from recognizer.evaluation.eval_ops import EvalOps


def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python train_crossvalidated_classifier.py config_file")
        print("Where")
        print("\tconfig_file\t= Path to configuration file")
        return

    print("Loading configuration....")
    config_filename = sys.argv[1]
    config = Configuration.from_file(config_filename)

    print("Loading evaluation data....")
    classifier_name = config.data["CLASSIFIER_NAME"]
    error_filename = config.data["EVALUATION_OUTPUT"] + "/" + classifier_name + ".dat"
    try:
        in_file = open(error_filename, 'rb')
        dataset_used = pickle.load(in_file)
        eval_errors = pickle.load(in_file)
        in_file.close()
    except Exception as e:
        print("Error loading evaluation results file")
        print(e)
        return

    n_errors = len(eval_errors)

    if n_errors == 0:
        print("No evaluation errors")
        return

    print("Loading dataset ...")
    if dataset_used == 0:
        # training set
        dataset_file = config.data["DATASET_DIR"] + "/" + config.data["TRAINING_DATASET_NAME"] + ".dat"
        source_path = config.data["TRAINING_DATASET_PATH"]
    elif dataset_used == 1:
        # testing set
        dataset_file = config.data["DATASET_DIR"] + "/" + config.data["TESTING_DATASET_NAME"] + ".dat"
        source_path = config.data["TESTING_DATASET_PATH"]
    else:
        # invalid
        print("Invalid dataset used <" + str(dataset_used) + ">")
        return

    dataset = FeatureDataset.load_from_file(dataset_file)

    if config.contains("SIMILAR_SHAPES"):
        similar_shapes = EvalOps.load_similar_shapes(config.get_str("SIMILAR_SHAPES"))
    else:
        similar_shapes = {}

    print("Total errors: " + str(n_errors))
    print("Generating HTML")

    top_n = config.get_int("EVALUATION_HTML_SHOW_TOP_N", 10)
    class_representation = EvalOps.count_elements_per_class(dataset.labels, dataset.label_mapping)

    # count errors per class
    # errors_per_class = EvalOps.count_errors_from_error_list(eval_errors)

    # general confusion matrix ...
    conf_matrix = EvalOps.create_confusion_matrix(eval_errors)
    error_groups = EvalOps.find_error_clusters(conf_matrix)
    top_gen_errors = EvalOps.confusion_matrix_top_n_entries(conf_matrix, top_n)

    # non-similar classes
    no_sim_matrix = EvalOps.copy_confusion_matrix(conf_matrix)
    EvalOps.delete_confusion_matrix_entries(no_sim_matrix, similar_shapes)
    no_sim_error_groups = EvalOps.find_error_clusters(no_sim_matrix)
    n_no_sim_error = EvalOps.confusion_matrix_sum(no_sim_matrix)
    top_no_sim_errors = EvalOps.confusion_matrix_top_n_entries(no_sim_matrix, top_n)

    # only similar classes
    sim_matrix = EvalOps.copy_confusion_matrix(conf_matrix)
    EvalOps.delete_confusion_matrix_entries(sim_matrix, no_sim_matrix)
    sim_error_groups = EvalOps.find_error_clusters(sim_matrix)
    n_sim_error = EvalOps.confusion_matrix_sum(sim_matrix)
    top_sim_errors = EvalOps.confusion_matrix_top_n_entries(sim_matrix, top_n)

    n_samples = dataset.num_samples()

    html_content = ""
    html_content += "<p>Total Samples in dataset: " + str(n_samples) + "</p>"
    html_content += "<p>Total Attributes in dataset: " + str(dataset.num_attributes()) + "</p>"
    html_content += "<p>Total Classes in dataset: " + str(dataset.num_classes()) + "</p>"
    html_content += "<br />"
    html_content += "<p>Total Errors: " + str(n_errors) + " ({0:.3f}% dataset)".format(n_errors * 100 / n_samples) + "</p>"
    html_content += "<br />"
    html_content += "<h2>Class Representation</h2>"
    html_content += HTMLGenerator.class_representation_HTML(class_representation, dataset.class_mapping, conf_matrix)
    html_content += "<br />"

    html_content += "<h2>Confusions between non-similar shapes</h2>"
    stats = " ({0:.3f}% dataset, {1:.3f}% total error)".format(n_no_sim_error * 100 / n_samples, n_no_sim_error * 100 / n_errors)
    html_content += "<p>Total Errors: " + str(n_no_sim_error) + stats + "</p>"
    html_content += HTMLGenerator.top_errors_HTML(top_no_sim_errors, n_errors, n_samples)
    html_content += "<p>Rows: Expected class<br/> Columns: Predicted class</p>"
    for error_group in no_sim_error_groups:
        table_html = HTMLGenerator.confusion_matrix_HTML(no_sim_matrix, error_group)
        html_content += "<br />" + table_html

    html_content += "<h2>Similar Shapes</h2>"
    html_content += HTMLGenerator.similar_shapes_HTML(similar_shapes)
    html_content += "<br />"

    html_content += "<h2>Confusions between similar shapes</h2>"
    stats = " ({0:.3f}% dataset, {1:.3f}% total error)".format(n_sim_error * 100 / n_samples, n_sim_error * 100 / n_errors)
    html_content += "<p>Total Errors: " + str(n_sim_error) + stats + "</p>"
    html_content += HTMLGenerator.top_errors_HTML(top_sim_errors, n_errors, n_samples)
    html_content += "<p>Rows: Expected class<br/> Columns: Predicted class</p>"
    for error_group in sim_error_groups:
        table_html = HTMLGenerator.confusion_matrix_HTML(sim_matrix, error_group)
        html_content += "<br />" + table_html

    html_content += "<br />"
    html_content += "<h2>Complete Confusion Matrix</h2>"
    html_content += HTMLGenerator.top_errors_HTML(top_gen_errors, n_errors, n_samples)
    html_content += "<p>Rows: Expected class<br/> Columns: Predicted class</p>"
    for error_group in error_groups:
        table_html = HTMLGenerator.confusion_matrix_HTML(conf_matrix, error_group)
        html_content += "<br />" + table_html

    html_filename = config.data["EVALUATION_OUTPUT"] + "/" + classifier_name + ".html"
    HTMLGenerator.save_HTML_page("Evaluation results for " + classifier_name, html_content, html_filename)

    print("Sampling SVG for error cases")

    if dataset.sources is not None:
        svg_sample_size = min(config.get_int("EVALUATION_SVG_SAMPLE_MAX_SIZE", n_errors), n_errors)

        # check if the output path exists
        svg_dir = config.data["EVALUATION_OUTPUT"] + "/" + classifier_name + "_SVG"
        if not os.path.isdir(svg_dir):
            os.mkdir(svg_dir)

        # get sample
        sample_jump = float(n_errors) / svg_sample_size
        sample_indices = [int(i * sample_jump) for i in range(svg_sample_size)]
        sample = {}
        for idx in sample_indices:
            sample_idx, expected, predicted, fold = eval_errors[idx]
            filename, sym_id = dataset.sources[sample_idx]

            sample_data = (sym_id, expected, predicted)

            if filename in sample:
                sample[filename].append(sample_data)
            else:
                sample[filename] = [sample_data]

        # for each file in the sample
        sample_idx = 0
        for filename in sample:
            symbols = INKMLLoader.load_INKML_file(filename, True)

            # for each symbol in the file
            for sym_id, expected, predicted in sample[filename]:
                # check if the output path exists
                sample_dir = svg_dir + "/" + str(dataset.class_mapping[expected])
                if not os.path.isdir(sample_dir):
                    os.mkdir(sample_dir)

                sample_dir += "/" + str(dataset.class_mapping[predicted])
                if not os.path.isdir(sample_dir):
                    os.mkdir(sample_dir)

                for symbol in symbols:
                    if int(symbol.id) == int(sym_id):
                        symbol_path = sample_dir + "/" + str(sample_idx) + ".svg"
                        SVGGenerator.saveSymbolAsSVG(symbol, symbol_path)
                        break

                sample_idx += 1

    print("Finished!")

if __name__ == '__main__':
    main()