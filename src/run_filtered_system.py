from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import DatasetBuilder, TYPES_DATA_SET
from recognizer.recognition.parser import TYPES_PARSING
import time

def test_parser():
    parsing_config_filename = "Default/Parsing_Default.conf"
    #parsing_config_filename = "debugConfig/sample.conf"

    parsing_config, parsing_config_name = Configuration.from_file(parsing_config_filename), "ParsingConfig"

    #map all the configuration instances in the controller
    control_runner = Controller(parsing_config, parsing_config_name)

    print("[Loading..] \t training and testing Expressions\n")
    start_loading_time = time.time()
    training_set_name= "Training_Exp_set"
    testing_set_name= "Testing_Exp_set"

    #load the expressions commonly for all the process
    control_runner.loadExpressions(name=training_set_name,config_name=parsing_config_name, isTraining=True)
    control_runner.loadExpressions(name=testing_set_name, config_name=parsing_config_name, isTraining=False)

    #preprocess the expressions after loading
    control_runner.preprocessExpressions(testing_set_name, config_name=parsing_config_name)
    control_runner.preprocessExpressions(training_set_name, config_name=parsing_config_name)
    end_loading_time = time.time()



    control_runner.construct_expression_graph(parsing_config_name, training_set_name)
    control_runner.construct_expression_graph(parsing_config_name, testing_set_name)
    print("\t\t [Time-Taken..] to Load is", (end_loading_time-start_loading_time))

    print("\n[Updating ..] \t merge strokes of same symbol in testing set\n")
    control_runner.merge_strokes_to_symbols(training_set_name)
    control_runner.merge_strokes_to_symbols(testing_set_name)



    print("\n [Re-Constructing..]\t new layout graph\n")
    control_runner.construct_expression_graph(parsing_config_name, training_set_name)
    control_runner.construct_expression_graph(parsing_config_name, testing_set_name)

    print("\n[Extracting Features..]\t  Filtering features extraction\n")
    filter_expret_name = "FilteredParser"
    training_filtpar_dataset_name = control_runner.createDataset(training_set_name, TYPES_DATA_SET.Relation,
                                                             config_name=parsing_config_name)
    testing_filtpar_dataset_name = control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Relation,
                                                            config_name=parsing_config_name)

    control_runner.createExpert(training_filtpar_dataset_name, expert_name="FilteredParser", config_name=parsing_config_name)
    control_runner.useExpert(testing_set_name, filter_expret_name, testing_filtpar_dataset_name)

    print("\t\t Edges Filtered")

    #create data-set for parsing
    print("\n[Extracting Features..]\t  parsing features extraction\n")
    start_par_ds_time = time.time()
    training_par_dataset_name=control_runner.createDataset(training_set_name, TYPES_DATA_SET.Relation, config_name=parsing_config_name)
    testing_par_dataset_name=control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Relation, config_name=parsing_config_name)
    end_par_ds_time = time.time()
    print("\t\t [Time-Taken..] to create parsing dataset is", (end_par_ds_time - start_par_ds_time))

    name_parser="MY_EXPERT_SYMBOL_LEVEL_PARSER"
    print("\n[Training ..] \t parser\n")
    start_par_time = time.time()
    control_runner.createParser(training_par_dataset_name, expert_name=name_parser, config_name=parsing_config_name, parser_type=TYPES_PARSING.Identify_Layout_Relation)
    end_par_time = time.time()
    print("\t\t [Time-Taken..] to train layout classifier", (end_par_time - start_par_time))

    print("\n [Updating..]\t new layout graph\n")
    start_par_time = time.time()
    control_runner.update_layout_relations(testing_set_name, name_parser, testing_par_dataset_name, recover_edges=False)
    end_par_time = time.time()
    print("\t\t [Time-Taken..] to Parse and update layout graph is", (end_par_time - start_par_time))


    lg_file_directory = parsing_config.data["TESTING_OUTPUT_LG_PATH"]
    control_runner.outputExpressions(testing_set_name, predictedLabels=False, location=lg_file_directory, lg_type="OR")




if __name__ == '__main__':
    test_parser()