from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import DatasetBuilder, TYPES_DATA_SET
from recognizer.recognition.parser import TYPES_PARSING
import time

def test_parser():
    segment_config_filename ="Default/Segmentation_Default.conf"
    parsing_config_filename = "Default/Parsing_Default.conf"
    classify_config_filename = "Default/Classify_Default.conf"

    '''segment_config_filename = "debugConfig/sample.conf"
    parsing_config_filename = "debugConfig/sample.conf"
    classify_config_filename = "debugConfig/sample.conf"'''

    segment_config,segment_config_name = Configuration.from_file(segment_config_filename),"SegmentConfig"
    parsing_config, parsing_config_name = Configuration.from_file(parsing_config_filename), "ParsingConfig"
    classify_config, classify_config_name = Configuration.from_file(classify_config_filename), "ClassifyConfig"

    #map all the configuration instances in the controller
    control_runner = Controller(segment_config, segment_config_name)
    control_runner.addConfig(parsing_config,parsing_config_name)
    control_runner.addConfig(classify_config, classify_config_name)

    print("[Loading..] \t training and testing Expressions\n")
    start_loading_time = time.time()
    training_set_name= "Training_Exp_set"
    testing_set_name= "Testing_Exp_set"

    #load the expressions commonly for all the process
    control_runner.loadExpressions(name=training_set_name,config_name=segment_config_name, isTraining=True)
    control_runner.loadExpressions(name=testing_set_name, config_name=segment_config_name, isTraining=False)

    #preprocess the expressions after loading
    control_runner.preprocessExpressions(testing_set_name, config_name=segment_config_name)
    control_runner.preprocessExpressions(training_set_name, config_name=segment_config_name)
    end_loading_time = time.time()



    control_runner.construct_expression_graph(segment_config_name, training_set_name)
    control_runner.construct_expression_graph(segment_config_name, testing_set_name)
    print("\t\t [Time-Taken..] to Load is", (end_loading_time-start_loading_time))



    #create data-set for segmentation
    segmenter_name = "MY_EXPERT_SEGMENTER"
    start_seg_ds_time = time.time()
    print("\n[Extracting Features..] \t Segmentation feature extraction\n")
    training_seg_dataset_name = control_runner.createDataset(training_set_name, TYPES_DATA_SET.Segmentation,
                                                         config_name=segment_config_name)
    testing_seg_dataset_name = control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Segmentation,
                                                        config_name=segment_config_name)
    end_seg_ds_time = time.time()
    print("\t\t [Time-Taken..] to create segmentation dataset is", (end_seg_ds_time - start_seg_ds_time))

    print("\n[Training ..] \t segmenter\n")
    start_seg_train_time = time.time()
    control_runner.createSegmenter(training_seg_dataset_name, expert_name=segmenter_name, config_name=segment_config_name)
    print("\n[Updating ..] \t merge strokes of same symbol in testing set\n")
    control_runner.merge_strokes_to_symbols(training_set_name)
    end_seg_train_time = time.time()
    print("\t\t [Time-Taken..] to train Segmenter is", (end_seg_train_time - start_seg_train_time))

    start_seg_time = time.time()
    control_runner.segmentExpressions(testing_set_name, segmenter_name, testing_seg_dataset_name, convert=True)
    end_seg_time = time.time()
    print("\t\t [Time-Taken..] to Segment is", (end_seg_time - start_seg_time))


    print("\n [Re-Constructing..]\t new layout graph\n")
    control_runner.construct_expression_graph(segment_config_name, training_set_name)
    control_runner.construct_expression_graph(segment_config_name, testing_set_name)

    #control_runner.visualize_expressions(testing_set_name, ".//output/visuals//")


    #create data-set for parsing
    print("\n[Extracting Features..]\t  parsing features extraction\n")
    start_par_ds_time = time.time()
    training_par_dataset_name=control_runner.createDataset(training_set_name, TYPES_DATA_SET.Relation, config_name=parsing_config_name)
    testing_par_dataset_name=control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Relation, config_name=parsing_config_name)
    end_par_ds_time = time.time()
    print("\t\t [Time-Taken..] to create parsing dataset is", (end_par_ds_time - start_par_ds_time))

    name_parser="MY_EXPERT_SYMBOL_LEVEL_PARSER"
    print("\n[Training ..] \t parser\n")
    start_par_time = time.time()
    control_runner.createParser(training_par_dataset_name, expert_name=name_parser, config_name=parsing_config_name, parser_type=TYPES_PARSING.Identify_Layout_Relation)
    end_par_time = time.time()
    print("\t\t [Time-Taken..] to train layout classifier", (end_par_time - start_par_time))

    print("\n [Updating..]\t new layout graph\n")
    start_par_time = time.time()
    control_runner.update_layout_relations(testing_set_name, name_parser, testing_par_dataset_name, recover_edges=False)
    end_par_time = time.time()
    print("\t\t [Time-Taken..] to Parse and update layout graph is", (end_par_time - start_par_time))


    name_classifier = "MY_EXPERT_SYMBOL_CLASSIFIER"
    print("\n[Normalizing ..] \t Expressions to fixed size box\n")
    control_runner.normalizeExpressions(training_set_name, classify_config_name)
    control_runner.normalizeExpressions(testing_set_name, classify_config_name)
    print("\n[Extracting Features..] \t Classification feature extraction\n")

    start_fea_time = time.time()
    training_class_ds_name = control_runner.createDataset(training_set_name, TYPES_DATA_SET.Symbol,config_name=classify_config_name)
    testing_class_ds_name = control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Symbol,config_name=classify_config_name)
    end_fea_time = time.time()
    print("\t\t [Time-Taken..] to extract parsing features", (end_fea_time - start_fea_time))


    print("\n[Training ..] \t Classifier\n")
    start_symb_train_time = time.time()
    control_runner.createSymbolClassifier(training_class_ds_name, expert_name=name_classifier, config_name=classify_config_name)
    end_symb_train_time = time.time()
    print("\t\t [Time-Taken..] to train symbol classifier is", (end_symb_train_time - start_symb_train_time))

    print("\n [Updating..]\t layout graph node lables\n")
    start_symb_time = time.time()
    control_runner.classifySymbols(testing_set_name, name_classifier, testing_class_ds_name)
    end_symb_time = time.time()
    print("\t\t [Time-Taken..] to classify symbols is", (end_symb_time - start_symb_time))

    lg_file_directory = parsing_config.data["TESTING_OUTPUT_LG_PATH"]
    control_runner.outputExpressions(testing_set_name, location=lg_file_directory, lg_type="OR")




if __name__ == '__main__':
    test_parser()