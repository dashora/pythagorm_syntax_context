import sys
import pickle
import math

def main():
    # usage check
    if len(sys.argv) < 3:
        print(
            "Usage: python test_time_and_consistency.py file1 file2 threshold")
        print("Where")
        print("\tfile1\t= Path to feature set 1 file")
        print("\tfile2\t= Path to feature set 2 file")
        print("\tthreshold\t= Threshold for difference")
        return

    threshold = float(sys.argv[3])

    file1 = open(sys.argv[1], 'rb')
    dataset1 = pickle.load(file1)
    featureList1 = dataset1.data
    feature_1_order = dataset1.sources

    file2 = open(sys.argv[2], 'rb')
    dataset2 = pickle.load(file2)
    featureList2 = dataset2.data
    feature_2_order = dataset2.sources

    # Comparing the feature list
    print(len(featureList1[0]))
    if (len(featureList1) == len(featureList2)):
        count = 0
        for i in range(len(featureList1)):
            fList1 = featureList1[i]
            fList2 = featureList2[i]
            if feature_1_order[i] != feature_2_order[i]:
                print("NOT SAME", feature_1_order[i], feature_2_order[i])
            for j in range(len(fList1)):
                diff = math.fabs(fList1[j] - fList2[j])
                if diff > threshold:
                    count += 1
                    print(fList1[j], fList2[j])
        n = len(featureList1)*(len(featureList1[0]))
        print('Out of ',n,' feature values, ',count,' feature values are different')
        print('Percentage of features differing = ',(count*100/n))
    else:
        print('Feature list do not match check the new implementation')

if __name__ == '__main__':
    main()
