# Original file: Awelemdy Orakwue March 10, 2015
# Modified by Kenny Davila, August 23, 2016

import csv
import sys
import pickle

import socketserver
import http.server
import http.client
import urllib.parse
from xml.dom.minidom import Document, parseString

from recognizer.configuration.configuration import Configuration
from recognizer.recognition.symbol_classifier import SymbolClassifier


class RecognitionServer(http.server.SimpleHTTPRequestHandler):
    CurrentClassifier = None
    CurrentSymbolTable = None

    def do_GET(self):
        # This will process a request which comes into the server.

        unquoted_url = urllib.parse.unquote(self.path)
        unquoted_url = unquoted_url.replace("/?segmentList=", "")
        unquoted_url = unquoted_url.replace("&segment=false", "")

        dom = parseString(unquoted_url)

        Segments = dom.getElementsByTagName("Segment")
        numSegments = Segments.length
        segmentIDS = []
        classifierPoints = []
        for j in range(numSegments):
            segmentIDS.append(Segments[j].getAttribute("instanceID"))
            points = Segments[j].getAttribute("points")
            url_parts = points.split('|')

            translation = Segments[j].getAttribute("translation").split(",")
            tempPoints = []
            for i in range(len(url_parts)):
                pt = url_parts[i].split(",")
                pt2 = (int(pt[0]) + int(translation[0]), int(pt[1]) + int(translation[1]))
                tempPoints.append(pt2)
            classifierPoints.append(tempPoints)

        #print(classifierPoints)
        results = RecognitionServer.CurrentClassifier.classify_points_prob(classifierPoints, 30)
        #print(results)

        doc = Document()
        root = doc.createElement("RecognitionResults")
        doc.appendChild(root)
        root.setAttribute("instanceIDs", ",".join(segmentIDS))

        for k in range(len(results)):
            r = doc.createElement("Result")
            s = str(results[k][0]).replace("\\", "")
            sym = RecognitionServer.CurrentSymbolTable.get(s)
            if sym is None:
                sym = s

            # special case due to CSV file
            if sym.lower() == "comma":
                sym = ","
            v = str(results[k][1])
            c = format(float(v), '.35f')
            r.setAttribute("symbol", sym)
            r.setAttribute("certainty", c)
            root.appendChild(r)

        xml_str = doc.toxml()
        xml_str = xml_str.replace("amp;","")
        self.send_response(http.client.OK, 'OK')
        self.send_header("Content-length", len(xml_str))
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(bytearray(xml_str, encoding="utf-8"))


def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python run_server.py config_file")
        print("Where")
        print("\tconfig_file\t= Path to configuration file")
        return

    config_filename = sys.argv[1]
    config = Configuration.from_file(config_filename)

    HOST = config.get_str("SERVER_HOST")
    PORT = config.get_int("SERVER_PORT_NUMBER", -1)
    symbol_table_filename = config.get_str("SERVER_SYMBOL_TABLE")

    if HOST == "":
        print("Invalid Host name in configuration file")

    if PORT == -1:
        print("Invalid Port in configuration file")

    if symbol_table_filename == "":
        print("Invalid Symbol Table filename in configuration file")

    print("Loading classifier")
    classifier_filename = config.data["CLASSIFIER_DIR"] + "/" + config.data["CLASSIFIER_NAME"] + ".dat"
    in_file = open(classifier_filename, 'rb')
    classifier = pickle.load(in_file)
    in_file.close()
    if not isinstance(classifier, SymbolClassifier):
        print("Invalid classifier file!")
        return

    print("Reading symbol code(s)")
    symbol_table = {}
    with open(symbol_table_filename, 'rt', encoding='utf-8') as input_file:
        csv_reader = csv.reader(input_file, delimiter=',')
        for row in csv_reader:
            symbol_table[row[3]] = row[0]

    # Use static attributes to pass values to Request Handler child class
    RecognitionServer.CurrentClassifier = classifier
    RecognitionServer.CurrentSymbolTable = symbol_table

    print("Starting server")
    try:
        server = socketserver.TCPServer(("", PORT), RecognitionServer)
    except Exception as e:
        print(e)
        return


    print("Serving")
    server.serve_forever()

if __name__ == "__main__":
    main()


