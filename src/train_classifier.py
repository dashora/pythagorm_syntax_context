
import sys
import time
import pickle
import numpy as np

from recognizer.configuration.configuration import Configuration
from recognizer.training.trainer import ClassifierTrainer
from recognizer.evaluation.eval_ops import EvalOps

def training_phase(config):
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python train_classifier.py config_file")
        print("Where")
        print("\tconfig_file\t= Path to configuration file")
        return


    # check how many times to run the classifier
    train_times = config.get_int("CLASSIFIER_TRAINING_TIMES", 1)

    # load training dataset once
    print("Loading data....")
    training = ClassifierTrainer.load_training_set(config)

    # do the training
    all_train_acc, all_train_avg, all_train_std = np.zeros(train_times), np.zeros(train_times), np.zeros(train_times)

    print("Training begins....")
    print(" #  \tACC\tC. AVG\tC. STDev")

    total_training_time = 0
    total_evaluation_time = 0
    best_classifier_ref = None
    best_accuracy = None
    #train for a give number of times and save the find the best classifier
    for i in range(train_times):
        start_training_time = time.time()
        classifier = ClassifierTrainer.train_classifier(training, config)
        end_training_time = time.time()
        total_training_time += end_training_time - start_training_time

        start_training_time = time.time()
        all_train_acc[i], all_train_avg[i], all_train_std[i], _ = EvalOps.classifier_per_class_accuracy(classifier, training)
        end_training_time = time.time()
        total_evaluation_time += end_training_time - start_training_time

        print(" " + str(i+1) + "\t" +
              str(round(all_train_acc[i], 3)) + "\t" +
              str(round(all_train_avg[i], 3)) + "\t" +
              str(round(all_train_std[i], 3)) + "\t")

        if best_classifier_ref is None or best_accuracy < all_train_acc[i]:
            best_classifier_ref = classifier
            best_accuracy = all_train_acc[i]

    print(" Mean\t" + str(round(all_train_acc.mean(), 3)) + "\t" +
          str(round(all_train_avg.mean(), 3)) + "\t" +
          str(round(all_train_std.mean(), 3)))

    print(" STDevs\t" + str(round(all_train_acc.std(), 3)) + "\t" +
          str(round(all_train_avg.std(), 3)) + "\t" +
          str(round(all_train_std.std(), 3)))

    print("")
    print("Training finished!")

    print("-> Configuration File: " + config_filename)
    print("-> Training samples: " + str(training.num_samples()))
    print("-> Total features: " + str(training.num_attributes()))
    print("-> Total classes: " + str(training.num_classes()))
    print("Training Time")
    print(" -> Total: " + str(round(total_training_time, 3)))
    print(" -> Mean: " + str(round(total_training_time / train_times, 3)))
    print("Evaluation Time")
    print(" -> Total: " + str(round(total_evaluation_time,3)))
    print(" -> Mean: " + str(round(total_evaluation_time/ train_times, 3)))

    print("Finished!")
    return best_classifier_ref



def save_to_file(best_classifier_ref, config):
    # save to file
    out_filename = training_filename = config.data["CLASSIFIER_DIR"] + "/" + config.data["CLASSIFIER_NAME"] + ".dat"
    out_file = open(out_filename, 'wb')
    pickle.dump(best_classifier_ref, out_file, pickle.HIGHEST_PROTOCOL)
    out_file.close()



if __name__ == '__main__':
    print("Loading configuration....")
    config_filename = sys.argv[1]
    config = Configuration.from_file(config_filename)
    classifier = training_phase(config)
    save_to_file(classifier, config)