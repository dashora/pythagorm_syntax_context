import numpy as np
import matplotlib.pyplot as plt

class ImageOps:

    @staticmethod
    def rayTrace(pt1, pt2):
        """

        :param pt1: y,x values of starting point in line segment
        :param pt2: y,x values of ending point in line segment
        :return:
        """
        pt1 = np.array([round(i) for i in pt1]).astype(int)
        pt2 = np.array([round(i) for i in pt2]).astype(int)
        if pt1[0] == pt2[0]:
            low = min([pt1[1], pt2[1]])
            high = max([pt1[1], pt2[1]])
            return [[pt1[0], j] for j in range(low,high+1)]
        elif pt1[1] == pt2[1]:
            low = min([pt1[0], pt2[0]])
            high = max([pt1[0], pt2[0]])
            return [[i, pt1[1]] for i in range(low,high+1)]
        else:
            points = []
            xsign = int(np.sign(pt2[1]-pt1[1]))
            ysign = int(np.sign(pt2[0]-pt1[0]))
            deltaErr = float(pt1[0] - pt2[0])/float(pt1[1]-pt2[1])
            dsign = np.sign(deltaErr)
            error = 0.0
            y = pt1[0]
            for x in range(pt1[1], pt2[1]+xsign, xsign):
                points.append([y,x])
                error += deltaErr
                while abs(error) > 1:
                    y += ysign
                    points.append([y,x])
                    error -= dsign
        return points

    @staticmethod
    def pts2img(pts, size, value=1):
        """

        Args:
            pts: array of [y,x] points with integer values

        Returns: greyscale image as 2D array where every point from pts has been set to 0 and all other pixels in the image
        are 255.

        """
        img = np.zeros(np.array(size)+1)
        for p in pts:
            img[p[0],p[1]] = value
        return img

    @staticmethod
    def plotTraces(traces, plot=False, show=True):
        """

        Args:
            traces: List of traces to plot
            plot: Boolean to change between point only and connected points
            show: Boolean to indicate whether to immediately show the ploted image or to wait.

        Returns:

        """
        plt.figure()
        fig = plt.gcf()
        ax = plt.gca()

        xmax = np.amax([np.amax(np.array(s)[:,1]) for s in traces])
        xmin = np.amin([np.amin((np.array(s)[:,1])) for s in traces])
        ymax = np.amax([np.amax(np.array(s)[:,0]) for s in traces])
        ymin = np.amin([np.amin((np.array(s)[:,0])) for s in traces])
        ymid = (ymax + ymin)/2
        yrng = ymax - ymin
        xrng = xmax-xmin
        #ax.set_ylim(-ymid-(xrng/2), -ymid+(xrng/2))
        ax.set_ylim(-ymax - (yrng * .10), -ymin + (yrng * .10))
        ax.set_xlim(xmin - (xrng * .10), xmax + (xrng * .10))
        plt.axes().set_aspect('equal')
        for s in range(len(traces)):
            if plot:
                ax.plot([rowx[1] for rowx in np.array(traces[s])], [-rowy[0] for rowy in np.array(traces[s])])
            ax.scatter([rowx[1] for rowx in np.array(traces[s])], [-rowy[0] for rowy in np.array(traces[s])], s=5, facecolor='red')
        if show:
            plt.show()