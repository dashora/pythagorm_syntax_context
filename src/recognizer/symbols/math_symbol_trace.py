import math
import numpy as np

class MathSymbolTrace:
    def __init__(self, trace_id, trace_points):
        self.id = trace_id
        self.points = trace_points
        self.sharp_points = None
        self.bounding_box = None

        #self.original_points = list(trace_points)

    def getTotalLength(self):
        total_length = 0.0

        for idx in range(len(self.points) - 1):
            total_length += self.distance(idx, idx + 1)

        return total_length

    # ================================================================
    #  swap between original and current points
    #  warning: This leaves the trace on an inconsistent state
    #           and should be used only for debugging purposes...
    # ================================================================
    # def swapPoints(self):
    #     tempo = self.points
    #     self.points = self.original_points
    #     self.original_points = tempo
    #
    #     # out of date...
    #     self.bounding_box = None

    # Gets current boundaries for the trace
    def getBoundaries(self):
        # only compute if it has never been computer or if it has changed...
        if self.bounding_box is None:
            # assume first point to define all boundaries initially..
            self.updateBoundaries()

        return self.bounding_box

    def updateBoundaries(self):
        p = np.array(self.points)
        minX, minY = np.min(p, axis=0)
        maxX, maxY = np.max(p, axis=0)
        self.bounding_box = (minX, maxX, minY, maxY)

    # Checks for duplicated points
    def hasDuplicatedPoints(self):
        for i in range(len(self.points) - 1):
            for j in range(i + 1, len(self.points)):
                if self.points[i] == self.points[j]:
                    return True

        return False

    def distance(self, i, j):
        x1, y1 = self.points[i]
        x2, y2 = self.points[j]

        return math.sqrt(math.pow((x1 - x2), 2) + math.pow((y1 - y2), 2))

    # relocate points in the trace based on two boxes, one to define and clamp current values
    # and the second to relocate values inside of it box in format [ minX, maxX, minY, maxY ]
    def relocatePoints(self, inputBox, outputBox):
        inputWidth = inputBox[1] - inputBox[0]
        inputHeight = inputBox[3] - inputBox[2]
        outputWidth = outputBox[1] - outputBox[0]
        outputHeight = outputBox[3] - outputBox[2]

        if inputWidth == 0:
            inputBox[1] = inputBox[1] + 0.01
            inputBox[0] = inputBox[0] - 0.01
            inputWidth = 0.02

        if inputHeight == 0:
            inputBox[3] = inputBox[3] + 0.01
            inputBox[2] = inputBox[2] - 0.01
            inputHeight = 0.02

        # for all points ...
        for i in range(len(self.points)):
            # clamp (just to keep the function as general as possible)
            # minX
            if self.points[i][0] < inputBox[0]:
                self.points[i] = (inputBox[0], self.points[i][1])
            # maxX
            if self.points[i][0] > inputBox[1]:
                self.points[i] = (inputBox[1], self.points[i][1])

            # minY
            if self.points[i][1] < inputBox[2]:
                self.points[i] = (self.points[i][0], inputBox[2])
            # maxX
            if self.points[i][1] > inputBox[3]:
                self.points[i] = (self.points[i][0], inputBox[3])

            # new Coordinates
            x = ((self.points[i][0] - inputBox[0]) / (inputWidth)) * outputWidth + outputBox[0]
            y = ((self.points[i][1] - inputBox[2]) / (inputHeight)) * outputHeight + outputBox[2]

            # replace...
            self.points[i] = (x, y)

        # update sharp points...
        if self.sharp_points is not None:
            for i in range(len(self.sharp_points)):
                # clamp (just to keep the function as general as possible)
                # minX
                if self.sharp_points[i][1][0] < inputBox[0]:
                    self.sharp_points[i] = (self.sharp_points[i][0], (inputBox[0], self.sharp_points[i][1][1]))
                # maxX
                if self.sharp_points[i][1][0] > inputBox[1]:
                    self.sharp_points[i] = (self.sharp_points[i][0], (inputBox[1], self.sharp_points[i][1][1]))
                # minY
                if self.sharp_points[i][1][1] < inputBox[2]:
                    self.sharp_points[i][1] = (self.sharp_points[i][0], (self.sharp_points[i][1][0], inputBox[2]))
                # maxY
                if self.sharp_points[i][1][1] > inputBox[3]:
                    self.sharp_points[i] = (self.sharp_points[i][0], (self.sharp_points[i][1][0], inputBox[3]))

                # new Coordinates
                x = ((self.sharp_points[i][1][0] - inputBox[0]) / (inputWidth)) * outputWidth + outputBox[0]
                y = ((self.sharp_points[i][1][1] - inputBox[2]) / (inputHeight)) * outputHeight + outputBox[2]

                # replace...
                self.sharp_points[i] = (self.sharp_points[i][0], (x, y))

        # bounding box has changed...
        self.bounding_box = None

    # Convert to string representation
    def __repr__(self):
        return str(self)

    def __str__(self):
        result = ''

        for x, y in self.points:
            result += str(x) + "," + str(y) + "\r\n"

        return result

    def save_to_file(self, filename):
        file = open(filename, 'w')
        file.write(str(self))
        file.close()

    @staticmethod
    def from_INKML_trace(trace_node):
        # points separated by comma
        points_s = trace_node.text.split(",")

        # now get the points coordinates
        points_f = []
        for p_s in points_s:
            # split again...
            coords_s = p_s.split()

            # add...
            points_f.append( (float(coords_s[0]), float(coords_s[1])) )

        trace_id = int(trace_node.attrib['id'])

        # now create the element
        return MathSymbolTrace(trace_id, points_f)

