
from enum import Enum


class Relation:
    def __init__(self, source, parent_id, child_id, relation):
        self.source = source
        self.parent_id = int(parent_id)
        self.child_id = int(child_id)
        self.relation = relation

    def get_parent_id(self):
        return self.parent_id

    def get_child_id(self):
        return self.child_id

    def get_relation(self):
        return self.relation

    def __str__(self):
        return str(self.parent_id)+" to "+str(self.child_id)+" is "+self.relation
