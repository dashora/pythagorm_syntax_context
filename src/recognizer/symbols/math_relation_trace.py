from recognizer.symbols.math_symbol_trace import MathSymbolTrace
import numpy as np

class MathRelationTrace:
    def __init__(self, id, endpoints, weights=None, source=None):
        self.id = id
        self.start,self.end = endpoints # Directionality can be assumed
        self.weights = weights # dictioinary mapping relation name to weight/confidence
        self.parent,self.child,self.expression = source # These should be id strings only.
        self.trace = None


    def setTrace(self):
        points = []
        dist = max(.5, np.linalg.norm(np.array(self.end)-np.array(self.start)))
        ## get interpolation points between the center of the two symbols
        dl = 1/(dist)
        dx = self.end[0] - self.start[0]
        dy = self.end[1] - self.start[1]
        l = 0.0
        points = [(self.start[0]+(dx*l),self.start[1]+(dy*l)) for l in np.arange(0.0, 1.0, dl)] + [self.end]
        #print("\t\tDist: " + str(dist) + " dl: " + str(dl))
        # while(l<1.0):
        #         x = self.start[0] + dx*l
        #         y = self.start[1] + dy*l
        #         l+=dl
        #         points.append((x, y))
        # points.append((self.end[0],self.end[1]))
        self.trace = MathSymbolTrace(self.id, points)


    def getWeight(self, relation):
        if relation not in self.weights:
            return 0
        return self.weights[relation]

