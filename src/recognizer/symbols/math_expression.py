import numpy as np
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_relation_trace import MathRelationTrace
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES
import os
import networkx as nx
from os.path import basename

class MathExpression:

    def __init__(self, expr_id, componentMap, gt_symbolLabels=None, gt_relationLabels=None, gt_segmentations=None, source=None):
        self.id = basename(expr_id)
        self.source = source # File name
        self.componentMap = componentMap # Map trace_set id -> trace_set
        self.expressionGraph = None
        self.filteredGraph = None
        self.edgeTraces = None

        self.normalized = False
        self.unnormalizedComponents = None
        # Ground Truth Dictionaries
        self.symbolLabels = gt_symbolLabels # Map trace id -> symbol label
        self.relationLabels = gt_relationLabels # Map (Symbol id, Symbol id) -> relation label
        self.symbolTraces = gt_segmentations # Map Symbol id -> [Trace id, Trace id, ...]
        self.traceSymbols = {} # Map Trace id -> Symbol id

        for sym,traces in gt_segmentations.items():
            for t in traces:
                self.traceSymbols[t] = sym

        for symId, sym in self.componentMap.items():
            sym.set_expression(self)

        # Build Ground Truth Graphs
        # if gt_symbolLabels and gt_relationLabels :
        #     self.symbolGraphGt = self.buildSymbolGraph(gt_symbolLabels, gt_relationLabels)
        #
        # if gt_symbolLabels and gt_relationLabels and gt_segmentations :
        #     self.symbolTracesGt = gt_segmentations
        #     self.traceSymbolGt = {}
        #     for sym,traces in gt_segmentations:
        #         for t in traces:
        #             self.traceSymbolGt[t] = sym
        #     self.traceGraphGt = self.buildStrokeGraph(gt_symbolLabels, gt_relationLabels, gt_segmentations)

        self.offsets, self.sizes = self.intializeSizesOffsets(componentMap)
        self.layout_graph = None

    def intializeGraph(self, components):
        graph = nx.DiGraph()
        graph.add_nodes_from(components)
        return graph

    def setLayoutGraph(self, layoutgraph):
        self.expressionGraph = layoutgraph

    def getLayoutGraph(self):
        return self.expressionGraph

    def intializeSizesOffsets(self, components):
        #every symbol gets a offset based on it's position in the expression.
        # This is used to calculate the position of the symbols after the points are normalized.
        sizes = {}
        offsets = {}
        for id,comp in components.items():
            comp = components[id]
            box = (comp.minX, comp.maxX, comp.minY, comp.maxY)
            sizes[id] = np.array([box[1]-box[0], box[3]-box[2]])
            offsets[id] = np.array([box[0],box[2]])
        np_offsets = np.array(list(offsets.values()))
        np_sizes=np.array(list(sizes.values()))

        self.minEx, self.minEy=np.min(np_offsets, axis=0)
        self.maxEx, self.maxEy = np.max(np_offsets+np_sizes, axis=0)
        self.boundingBox = [self.minEx, self.minEy, self.maxEx, self.maxEy]
        return offsets,sizes

    def buildSymbolGraph(self, symbolLabels, relationLabels):
        graph = nx.DiGraph()
        graph.add_nodes_from([(symId, {'label': label}) for symId, label in symbolLabels])
        graph.add_edges_from([(u, v, {'label': label}) for (u, v), label in relationLabels])
        return graph

    def buildStrokeGraph(self, symbolLabels, relationLabels, symbolTraceMap):
        graph = nx.DiGraph()
        for sym,label in symbolLabels.items():
            traces = symbolTraceMap[sym]
            graph.add_nodes_from([(tid, {'label': label}) for tid in traces])
            for t in range(len(traces)):
                t1 = traces[t]
                for r in range(t+1, len(traces)):
                    t2 = traces[r]
                    graph.add_edges_from([(t1,t2,{'label': label}), (t2, t1, {'label': label})])

        for (u,v),label in relationLabels:
            parents = symbolTraceMap[u]
            children = symbolTraceMap[v]
            graph.add_edges_from([(p, c, {'label': label}) for p in parents for c in children])

    def convertSymbolLevel(self, fromGroundTruth=False):
        symbols = {}
        if fromGroundTruth:
            cc_graph = nx.Graph([(u, v, d) for u, v, d in self.expressionGraph.edges(data=True) if d[EDGE_ATTRIBUTES.SAME_SYMBOL] == 1.0 or d[EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] == "MERGE"])

        else:
            cc_graph = nx.Graph([(u,v,d) for u,v,d in self.expressionGraph.edges(data=True) if d[EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] == "MERGE" or d[EDGE_ATTRIBUTES.PREDICTED_LAYOUT] == "MERGE"])

        cc_graph.add_nodes_from(self.expressionGraph.nodes())

        connected = nx.connected_components(cc_graph)
        new_trace_symbols={}
        for i,cc in enumerate(connected):
            newId = str(1000+i)
            #for every item in a group - map it to the new symbol id
            for trc_id in cc:
                new_trace_symbols[trc_id]=newId
            symbols[newId] = self.combineTraceSets(cc, newId)
            symbols[newId].trace_list = cc
            symbols[newId].set_expression(self)

        self.componentMap = symbols
        self.expressionGraph = None
        self.offsets, self.sizes = self.intializeSizesOffsets(symbols)
        self.traceSymbols = new_trace_symbols
        self.normalized = False

        #update the trace-id->symbol reference map traceSymbol to point to trace-id-> new_symbol_Id

        # convert symbol class and relation label ground truths to symbol level
        symbolLabels = {}
        relationLabels = {}
        for strokeId, label in self.symbolLabels.items():
            symId = self.traceSymbols[strokeId]
            symbolLabels[symId] = label
        for (pid,cid), label in self.relationLabels.items():
            if self.symbolLabels[pid] != label:
                p = self.traceSymbols[pid]
                c = self.traceSymbols[cid]
                relationLabels[(p,c)] = label
        self.symbolLabels = symbolLabels
        self.relationLabels = relationLabels

        #Now the trace-id doesn't represent any object in expression map.
        #hence convert the trace-id to to point to itself
        self_map={}
        for trace, symbol_id in self.traceSymbols.items():
            self_map[symbol_id] = symbol_id
        self.traceSymbols = self_map


    def getComponentClass(self, comp_id):
        if comp_id in self.symbolLabels:
            return self.symbolLabels[comp_id]
        else:
            return "Unknown"

    def getComponentSegments(self, comp_id):
        if comp_id in self.symbolTraces:
            return self.symbolTraces[comp_id]
        elif comp_id in self.traceSymbols:
            return [comp_id]
        else:
            return self.componentMap[comp_id].trace_list

    def getComponentsRelation(self, pid, cid):
        if (pid, cid) in self.relationLabels:
            return self.relationLabels[(pid, cid)]
        else:
            return "NoRelation"

    def getSizes(self):
        return np.array(list(self.sizes.values()))

    def getOffsets(self):
        return np.array(list(self.offsets.values()))

    def getSymbols(self):
        return self.componentMap.values()

    def getCompositeTrace(self, symId, orig=False):
        if orig:
            return self.getSymbolPoints(symId)
        else:
            return np.concatenate([trace.points for trace in self.componentMap[symId].traces])

    def combineTraceSets(self, setIds, newId):
        reset_traces = []
        for setId in setIds:
            for trace in self.componentMap[setId].traces:
                reset_traces.append(MathSymbolTrace(trace.id, list(self.getTracePoints(trace, setId))))
        newsymbol= MathSymbolTraceSet(newId, reset_traces, "unknown")
        newsymbol.expression=self
        return newsymbol

    def getTracePoints(self, trace, symId):
        if not self.normalized:
            return trace.points
        elif self.unnormalizedComponents:
            return np.concatenate([t.points for t in self.componentMap[symId].traces if t.id == trace.id]).astype(float)

        symPoints = np.concatenate([trace.points for trace in self.componentMap[symId].traces]).astype(float)
        points = np.array(trace.points[:], dtype=float)

        orig_size = self.sizes[symId]
        curr_size = np.amax(symPoints, axis=0) - np.amin(symPoints, axis=0)

        if orig_size[0] == 0 and curr_size[0] == 0:
            orig_size[0] = 1
            curr_size[0] = 1
        elif curr_size[0] == 0:
            curr_size[0] = orig_size[0]

        if orig_size[1] == 0 and curr_size[1] == 0:
            orig_size[1] = 1
            curr_size[1] = 1
        elif curr_size[1] == 0:
            curr_size[1] = orig_size[1]

        points *= (orig_size/curr_size)
        symPoints *= (orig_size/curr_size)

        lower = np.amin(symPoints, axis=0)
        points += (self.offsets[symId] - lower)
        return points

    def getSymbolPoints(self, symId):
        if not self.normalized:
            return np.concatenate([trace.points for trace in self.componentMap[symId].traces]).astype(float)
        elif self.unnormalizedComponents:
            return np.concatenate([trace.points for trace in self.unnormalizedComponents[symId].traces]).astype(float)

        points = np.concatenate([trace.points for trace in self.componentMap[symId].traces]).astype(float)

        orig_size = self.sizes[symId]
        curr_size = np.amax(points, axis=0) - np.amin(points, axis=0).astype(float)

        if orig_size[0] == 0 and curr_size[0] == 0:
            orig_size[0] = 1
            curr_size[0] = 1
        elif curr_size[0] == 0:
            curr_size[0] = orig_size[0]

        if orig_size[1] == 0 and curr_size[1] == 0:
            orig_size[1] = 1
            curr_size[1] = 1
        elif curr_size[1] == 0:
            curr_size[1] = orig_size[1]

        points *= (orig_size/curr_size)

        lower = np.amin(points, axis=0)
        points += (self.offsets[symId] - lower)
        return points

    def getContextPoints_category(self, *args):
        # get any number of symbols and get their context points
        points = np.array([]).reshape([0, 2])
        strokeset_list = args
        id_wise_pts = {}
        # get the ids of all the stroke-sets specified
        ss_ids = []
        for stroke_set in strokeset_list:
            ss_ids.append(stroke_set.getId())

        if not self.normalized:
            for key, sym in self.componentMap.items():
                # if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in sym.traces]).astype(float)
                    id_wise_pts[key] = symPts;
                    points = np.concatenate([points, symPts])
        elif self.unnormalizedComponents:
            for key, traces in self.unnormalizedComponents.items():
                # if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in traces]).astype(float)
                    id_wise_pts[key] = symPts;
                    points = np.concatenate([points, symPts])
        else:
            for key, sym in self.componentMap.items():
                # if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in sym.traces]).astype(float)

                    orig_size = self.sizes[key]
                    curr_size = np.amax(symPts, axis=0) - np.amin(symPts, axis=0).astype(float)

                    if orig_size[0] == 0 and curr_size[0] == 0:
                        orig_size[0] = 1
                        curr_size[0] = 1
                    elif curr_size[0] == 0:
                        curr_size[0] = orig_size[0]

                    if orig_size[1] == 0 and curr_size[1] == 0:
                        orig_size[1] = 1
                        curr_size[1] = 1
                    elif curr_size[1] == 0:
                        curr_size[1] = orig_size[1]
                    symPts *= (orig_size / curr_size)
                    lower = np.amin(symPts, axis=0)
                    symPts += (self.offsets[key] - lower)
                    id_wise_pts[key] = symPts;
                    points = np.concatenate([points, symPts])
        if points.size == 0:
            points = np.concatenate([points, np.array([[0, 0]])])

        #return points
        return id_wise_pts,points
    def getContextPoints(self, *args):
        #get any number of symbols and get their context points
        points = np.array([]).reshape([0,2])
        strokeset_list = args

        #get the ids of all the stroke-sets specified
        ss_ids= []
        for stroke_set in strokeset_list:
            ss_ids.append(stroke_set.getId())

        if not self.normalized:
            for key,sym in self.componentMap.items():
                #if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in sym.traces]).astype(float)
                    points = np.concatenate([points,symPts])
        elif self.unnormalizedComponents:
            for key,traces in self.unnormalizedComponents.items():
                #if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in traces]).astype(float)
                    points = np.concatenate([points,symPts])
        else:
            for key,sym in self.componentMap.items():
                #if the current stroke_Set is not one of the stroke-set mentioned
                if not key in ss_ids:
                    symPts = np.concatenate([trace.points for trace in sym.traces]).astype(float)

                    orig_size = self.sizes[key]
                    curr_size = np.amax(symPts, axis=0) - np.amin(symPts, axis=0).astype(float)

                    if orig_size[0] == 0 and curr_size[0] == 0:
                        orig_size[0] = 1
                        curr_size[0] = 1
                    elif curr_size[0] == 0:
                        curr_size[0] = orig_size[0]

                    if orig_size[1] == 0 and curr_size[1] == 0:
                        orig_size[1] = 1
                        curr_size[1] = 1
                    elif curr_size[1] == 0:
                        curr_size[1] = orig_size[1]
                    symPts *= (orig_size/curr_size)
                    lower = np.amin(symPts, axis=0)
                    symPts += (self.offsets[key] - lower)
                    points = np.concatenate([points,symPts])
        if points.size == 0:
            points = np.concatenate([points, np.array([[0,0]])])
        return points

    def setUnnormalized(self):
        self.unnormalizedComponents = None
        unnormalized = {}
        for symId,sym in self.componentMap.items():
            unnormalized[symId] = []
            for trace in sym.traces:
                tracePoints = self.getTracePoints(trace, symId)
                unnormalized[symId].append(MathSymbolTrace(trace.id, list(tracePoints)))
        self.unnormalizedComponents = unnormalized

    @staticmethod
    def setEdgeTraces(data):
        expression, useGT = data
        edge_traces = []
        for pid, cid, data in expression.expressionGraph.edges(data=True):
            if useGT and data[EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION][0] == "NoRelation":
                continue
            elif not useGT and data[EDGE_ATTRIBUTES.POSSIBLE_CLASSES][0] == "NoRelation":
                continue
            weights = {}
            if useGT:
                classes = [data[EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]]
                probs = [1.0]
            else:
                classes = data[EDGE_ATTRIBUTES.POSSIBLE_CLASSES]
                probs = data[EDGE_ATTRIBUTES.CLASS_CONFIDENCES]
            for i,cls in enumerate(classes):
                if probs[i] > 0:
                    weights[cls] = probs[i]
            start = expression.componentMap[pid].eye
            end = expression.componentMap[cid].eye
            edge_traces.append(MathRelationTrace(pid+cid, [start,end], weights, (expression.id,pid,cid)))
        expression.edgeTraces = edge_traces
        return expression

    def writeORLg(self, graph, predictedLabels=True, location=None):
        if not location:
            location = os.path.dirname(os.getcwd())
        else:
            os.makedirs(location, exist_ok=True)
        if ".inkml" in self.id:
            base_file_name = basename(self.id).replace(".inkml", ".lg")
        else:
            base_file_name = self.id + ".lg"
        outfile = open(location + "//" + base_file_name, "w")
        # Write object lines
        prefix = "O, "
        middle = ", 1.0, "
        for name in graph.nodes():
            prims = ', '.join(str(x) for x in self.componentMap[name].trace_list)
            if predictedLabels:
                try:
                    label = graph.node[name][NODE_ATTRIBUTES.PREDICTED_CLASS]
                except ValueError:
                    print("Error: " + name)
            else:
                label = graph.node[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
            outfile.write(prefix + "N_"+str(name) + ", " + label + middle + prims + "\n")

        # Write relation lines
        outfile.write("\n\n# Relations:\n")
        prefix = "R, "
        suffix = ", 1.0"
        for p,c in graph.edges():
            edge = graph[p][c]
            label = edge[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
            outfile.write(prefix + "N_"+str(p) + ", " + "N_"+str(c) + ", " + label + suffix + "\n")
        outfile.close()

    def writeNELg(self, graph, predictedLabels=True, location=None):
        if not location:
            location = os.path.dirname(os.getcwd())
        else:
            os.makedirs(location, exist_ok=True)

        if ".inkml" in self.id:
            base_file_name = basename(self.id).replace(".inkml", ".lg")
        else:
            base_file_name = self.id + ".lg"

        outfile = open(location + "//" + base_file_name, "w+")
        # Write object lines
        prefix = "N, "
        suffix = ", 1.0"
        for name in graph.nodes():
            node = graph[name]
            if predictedLabels:
                label = graph.node[name][NODE_ATTRIBUTES.PREDICTED_CLASS]
            else:
                label = graph.node[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
            outfile.write(prefix + name + ", " + label + suffix  + "\n")

        # Write relation lines
        prefix = "E, "
        suffix = ", 1.0"
        for p,c in graph.edges():
            edge = graph[p][c]
            if predictedLabels:
                label = edge[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
            else:
                label = edge[EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
                if label == "MERGE":
                    label = graph.node[p][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
            outfile.write(prefix + p + ", " + c + ", " + label + suffix + "\n")

        outfile.close()

    def writePDFORLg(self, graph, predictedLabels=True, location=None):
        if not location:
            location = os.path.dirname(os.getcwd())
        else:
            os.makedirs(location, exist_ok=True)
        if ".inkml" in self.id:
            base_file_name = basename(self.id).replace(".inkml", ".lg")
        else:
            base_file_name = self.id + ".lg"
        outfile = open(location + "//" + base_file_name, "w")
        # Write object lines
        prefix = "O, "
        middle = ", 1.0, "

        # for name in graph.nodes():
        #     prims = ', '.join(str(x) for x in self.componentMap[name].trace_list)
        #     if predictedLabels:
        #         try:
        #             label = graph.node[name][NODE_ATTRIBUTES.PREDICTED_CLASS]
        #         except ValueError:
        #             print("Error: " + name)
        #     else:
        #         label = graph.node[name][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
        #     outfile.write(prefix + "N_"+str(name) + ", " + label + middle + prims + "\n")

        # Write relation lines
        outfile.write("\n\n# Relations:\n")
        prefix = "R, "
        suffix = ", 1.0"
        for p,c in graph.edges():
            edge = graph[p][c]
            label = edge[EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
            outfile.write(prefix + "N_"+str(p) + ", " + "N_"+str(c) + ", " + label + suffix + "\n")
        outfile.close()