
#=====================================================================
#  This class represents a traces, and also performs all the operations
#  relative to it
#
#  Created by:
#      - Kenny Davila (May, 2016)
#   Modified by:
#      - Lakshmi Ravi
#      - Micheal Condon
#=====================================================================
#
# References to papers for the original methods....
# [1] - Preprocessing Techniques for Online Handwriting Recognition,
#      B.Q. Huang and Y.B. Zhang and M-T. Kechadi
#
#
#=====================================================================
import numpy as np
from scipy.spatial.distance import cdist

class MathSymbolTraceSet:
    # constructor
    def __init__(self, sym_id, traces, truth):
        self.id = sym_id
        self.traces = traces
        self.truth = truth
        self.trace_list = [t.id for t in traces]

        if len(traces) == 0:
            raise Exception("Symbol ID: " + sym_id + "\nThe number of traces of a symbol cannot be 0")

        # process limits...
        self.minX, self.maxX, self.minY, self.maxY = (0, 0, 0, 0)
        self.updateTracesBoundingBox()
        self.original_box = (self.minX, self.maxX, self.minY, self.maxY)
        self.eye = np.array([(self.minX+self.maxX)/2, (self.minY + self.maxY)/2])

        self.w_ratio = 1.0
        self.h_ratio = 1.0
        self.expression = None
        self.un_normalized_symbol=None

    # ================================================================
    #  swap between original and current points
    #  warning: This leaves the symbol on an inconsistent state
    #           and should be used only for debugging purposes...
    # ================================================================
    def swapPoints(self):
        for t in self.traces:
            t.swapPoints()

    def setTraceList(self, trace_list):
        self.trace_list = trace_list

    def getPoints(self):
        return self.expression.getSymbolPoints(self.id)

    def getTraceList(self):
        return self.trace_list

    def getId(self):
        return self.id

    def closest_distance(self, other_strokeset):
        points = self.expression.getSymbolPoints(self.id)
        other_points = self.expression.getSymbolPoints(other_strokeset.id)
        return np.min(cdist(points, other_points))

    def setConvexHull(self, points):
        self.convexHull= points

    def get_ground_truth(self):
        return self.truth

    def set_expression(self, expression):
        self.expression = expression

    def get_expression(self):
        return self.expression

    def set_file_name(self, filename):
        self.filename = filename
        
    def get_file_name(self):
        return self.filename

    def updateTracesBoundingBox(self):
        mins_x, maxs_x, mins_y, maxs_y = [], [], [], []
        for i in range(0, len(self.traces)):
            new_min_x, new_max_x, new_min_y, new_max_y = self.traces[i].getBoundaries()

            mins_x.append(new_min_x)
            maxs_x.append(new_max_x)
            mins_y.append(new_min_y)
            maxs_y.append(new_max_y)

        self.minX = min(mins_x)
        self.maxX = max(maxs_x)
        self.minY = min(mins_y)
        self.maxY = max(maxs_y)

        self.eye =np.array([(self.minX + self.maxX) / 2, (self.minY + self.maxY) / 2])

    #manually set size ratio for this symbol
    def setSizeRatio(self, avg_width, avg_height):
        if avg_width > 0.0:
            self.w_ratio = (self.original_box[1] - self.original_box[0]) / avg_width
        else:
            self.w_ratio = 1.0

        if avg_height > 0.0:
            self.h_ratio = (self.original_box[3] - self.original_box[2]) / avg_height
        else:
            self.h_ratio = 1.0

    #Get current size ratio of the symbol
    #This represents the proportional size of this symbol
    #in the complete math expression
    def getSizeRatio(self):
        return (self.w_ratio, self.h_ratio)

    # Get a squared box that contains the symbol centered...
    def getSquaredBoundingBox(self):
        # start with first trace...
        minX, maxX, minY, maxY = self.traces[0].getBoundaries()

        # expand if other traces need it
        for i in range(1, len(self.traces)):
            oMinX, oMaxX, oMinY, oMaxY = self.traces[i].getBoundaries()

            # min X
            if oMinX < minX:
                minX = oMinX
            # max X
            if oMaxX > maxX:
                maxX = oMaxX
            # min Y
            if oMinY < minY:
                minY = oMinY
            # max Y
            if oMaxY > maxY:
                maxY = oMaxY

        # the original proportions must be keep, check longest side...
        width = maxX - minX
        height = maxY - minY
        if width > height:
            # keep minX, maxX, move Y bounds to keep proportions inside a square
            minY = ((maxY + minY) / 2.0) - (width / 2.0)
            maxY = minY + width
        else:
            # keep minY, maxY, move X bounds to keep proportions inside a square
            minX = ((maxX + minX) / 2.0) - (height / 2.0)
            maxX = minX + height

        return [minX, maxX, minY, maxY]

    # normalize the symbol
    def normalize(self):
        # first, get the bounding box of the whole symbol...
        current_box = self.getSquaredBoundingBox()

        # relocate and re-scale each trace, to be in the new box [-1, 1],[-1,1]
        new_box = [-1, 1, -1, 1]

        for trace in self.traces:
            trace.relocatePoints(current_box, new_box)

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return isinstance(other, MathSymbolTraceSet) and self.id == other.id

