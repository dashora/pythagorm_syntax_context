
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from recognizer.util.math_ops import MathOps
from scipy.spatial import distance
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_trace_set import MathSymbolTraceSet


#TODO combine use_length and use_parzen into membershiptype
class HistogramFeatureExtractor:
    @staticmethod
    def trace2DLengthHistogram(trace, rows, cols, min_x, min_y, max_x, max_y):
        # create the bins...
        distribution = np.zeros((rows, cols), np.float64)

        bin_size_x = (max_x - min_x) / (cols - 1)
        bin_size_y = (max_y - min_y) / (rows - 1)

        for i in range(len(trace.points) - 1):
            p0_x, p0_y = trace.points[i]
            p1_x, p1_y = trace.points[i + 1]

            mid_x = (p0_x + p1_x) / 2.0
            mid_y = (p0_y + p1_y) / 2.0

            if min_x <= mid_x <= max_x and min_y <= mid_y <= max_y:
                length = math.sqrt((p1_x - p0_x) ** 2 + (p1_y - p0_y) ** 2)

                h_div = (mid_x - min_x) / bin_size_x
                h_bin0 = int(math.floor(h_div))
                if h_bin0 == cols - 1:
                    h_bin0 = cols - 2
                    h_w1 = 1.0
                else:
                    h_w1 = h_div - h_bin0
                h_bin1 = h_bin0 + 1

                v_div = (mid_y - min_y) / bin_size_y
                v_bin0 = int(math.floor(v_div))
                if v_bin0 == rows - 1:
                    v_bin0 = rows - 2
                    v_w1 = 1.0
                else:
                    v_w1 = v_div - v_bin0
                v_bin1 = v_bin0 + 1

                distribution[v_bin0, h_bin0] += length * (1.0 - h_w1) * (1.0 - v_w1)
                distribution[v_bin0, h_bin1] += length * h_w1 * (1.0 - v_w1)
                distribution[v_bin1, h_bin0] += length * (1.0 - h_w1) * v_w1
                distribution[v_bin1, h_bin1] += length * h_w1 * v_w1

        return distribution

    @staticmethod
    def trace2DPointHistogram(trace, rows, cols, min_x, min_y, max_x, max_y):
        # create the bins...
        distribution = np.zeros((rows, cols), np.float64)

        bin_size_x = (max_x - min_x) / (cols - 1)
        bin_size_y = (max_y - min_y) / (rows - 1)

        for x, y in trace.points:
            if min_x <= x <= max_x and min_y <= y <= max_y:
                h_div = (x - min_x) / bin_size_x
                h_bin0 = int(math.floor(h_div))
                if h_bin0 == cols - 1:
                    h_bin0 = cols - 2
                    h_w1 = 1.0
                else:
                    h_w1 = h_div - h_bin0
                h_bin1 = h_bin0 + 1

                v_div = (y - min_y) / bin_size_y
                v_bin0 = int(math.floor(v_div))
                if v_bin0 == rows - 1:
                    v_bin0 = rows - 2
                    v_w1 = 1.0
                else:
                    v_w1 = v_div - v_bin0
                v_bin1 = v_bin0 + 1

                distribution[v_bin0, h_bin0] += (1.0 - h_w1) * (1.0 - v_w1)
                distribution[v_bin0, h_bin1] += h_w1 * (1.0 - v_w1)
                distribution[v_bin1, h_bin0] += (1.0 - h_w1) * v_w1
                distribution[v_bin1, h_bin1] += h_w1 * v_w1

        return distribution

    @staticmethod
    def trace2DPointHistogramUsingParzen(symbol_points, corners, variance):
        '''

        :param trace: symbol trace
        :param min_x, min_y, max_x, max_y: bounding coordinates of the grids
        :param corners: list of corners where parzen estimation will be measured
        :param variance: variance for the parzen function
        '''
        symbol_points = np.array(symbol_points)
        features = MathOps.get_parzen_density_vector(np.array(corners), symbol_points, variance)

        return features

    @staticmethod
    def getCorners(rows, cols, min_x, min_y, max_x, max_y):
        points = []
        bin_size_x = (max_x - min_x) / (cols - 1)
        bin_size_y = (max_y - min_y) / (rows - 1)
        min_x += bin_size_x/2
        min_y += bin_size_y/2
        for i in range(rows-1):
            for j in range(cols-1):
                x = min_x + (bin_size_x * i)
                y = min_y + (bin_size_y * j)
                points.append((x,y))
        return points

    @staticmethod
    def visualizeCornerFeatures(symbol, rows, cols, min_x, min_y, max_x, max_y, corners, feature, plot_path, variance=None, name=None):
        boundingBox = plt.Rectangle((min_x, -max_y), (max_x - min_x), (max_y - min_y), linestyle="-", facecolor="none")
        plt.gca().add_patch(boundingBox)

        bin_size_x = (max_x - min_x) / (cols - 1)
        bin_size_y = (max_y - min_y) / (rows - 1)

        # Plot the grid lines on the symbol
        xGrids = np.arange(min_x, max_x+.01, bin_size_x)
        yGrids = np.arange(min_y, max_y+.01, bin_size_y) * -1
        plt.xticks(xGrids)
        plt.yticks(yGrids)
        plt.grid(True)

        # Plot the symbol
        for trace in symbol.traces:
            for x,y in trace.points:
                plt.plot(x , -y ,'r.')
                if variance:
                    parzenCircle = plt.Circle((x, -y), math.sqrt(variance), fill=False, color='y')
                    plt.gca().add_artist(parzenCircle)

        plt.axis('scaled')
        # Save the symbol and parzen rings plot
        if name is None:
            name = str(symbol.id)
        plt.savefig(plot_path + name + "_POINTS_AND_CIRCLE.png")
        plt.clf()

        # Set the grids again
        plt.xticks(xGrids)
        plt.yticks(yGrids)
        for x in xGrids:
            plt.axvline(x = x, linestyle='--', linewidth=1, color='black')
        for y in yGrids:
            plt.axhline(y = y, linestyle='--', color='black')
        # Draw bounding box for the symbol
        boundingBox = plt.Rectangle((min_x, -max_y), ((max_x - min_x)), ((max_y - min_y)), linestyle='-', facecolor="none")
        plt.gca().add_patch(boundingBox)
        # Now plot the features at the corner points
        xList = [x for (x,y) in corners]
        yList = [-y for (x,y) in corners]
        # feature = (feature-np.amin(feature))/np.amax(feature)
        # print(feature)

        plt.scatter(xList, yList, c=feature, s=300, marker="o", cmap=cm.Reds)
        #plt.colorbar()
        plt.axis('scaled')
        # Save the symbol and parzen rings plot
        plt.savefig(plot_path + name + "_INTENSITY_PLOT.png")
        plt.clf()

    @staticmethod
    def trace2DPointHistogramUsingCount(trace, rows, cols, min_x, min_y, max_x, max_y):

        x_line = np.linspace(min_x, max_x, num=cols)
        y_line = np.linspace(min_y, max_y, num=rows)

        X = []
        Y = []

        for x,y in trace.points:
            X.append(x)
            Y.append(y)

        distribution, _, _ = np.histogram2d(X, Y, bins=[x_line, y_line])

        return distribution

    @staticmethod
    def visualizeCountFeatures(symbol, rows, cols, min_x, min_y, max_x, max_y, features, plot_path):
        boundingBox = plt.Rectangle((min_x, -max_y), (max_x - min_x), (max_y - min_y), linestyle="-", facecolor="none")
        plt.gca().add_patch(boundingBox)

        bin_size_x = (max_x - min_x) / (cols - 1)
        bin_size_y = (max_y - min_y) / (rows - 1)

        # Plot the grid lines on the symbol
        xGrids = np.linspace(min_x, max_x, num=cols)
        yGrids = np.linspace(min_y, max_y, num=rows)

        plt.xticks(xGrids)
        plt.yticks(yGrids)
        plt.grid(True)

        # Plot the symbol
        for trace in symbol.traces:
            for x, y in trace.points:
                plt.plot(x, -y, 'r.')

        plt.axis('scaled')
        # Save the symbol and parzen rings plot
        plt.savefig(plot_path + str(symbol.id) + "_POINTS_AND_CIRCLE.pdf")
        plt.clf()

        # Set the grids again
        plt.xticks(xGrids)
        plt.yticks(yGrids)
        for x in xGrids:
            plt.axvline(x=x, linestyle='--', linewidth=1, color='black')
        for y in yGrids:
            plt.axhline(y=y, linestyle='--', color='black')
        # Draw bounding box for the symbol
        boundingBox = plt.Rectangle((min_x, -max_y), (max_x - min_x), (max_y - min_y), linestyle='-', facecolor="none")
        plt.gca().add_patch(boundingBox)
        # Now plot the features at the corner points
        max_f = max(features)
        ax = plt.subplot(111)
        xList = []
        yList = []
        id = 0
        for i in range(rows-1):
            for j in range(cols-1):
                feature = features[id]
                id += 1
                rect = plt.Rectangle((xGrids[i], -yGrids[j]-bin_size_y), (bin_size_x), (bin_size_y), linestyle='-', facecolor=cm.Reds(feature/max_f))
                xList.append(xGrids[i])
                yList.append(-yGrids[j]-bin_size_y)
                if feature == 0:
                    rect.set_hatch('xxxxxx')
                plt.gca().add_patch(rect)

        # plt.scatter(xList, yList, c=features, s=0, cmap=cm.Reds)
        # plt.colorbar()

        ax.axis('scaled')
        # Save the feature intensity plot
        plt.savefig(plot_path + str(symbol.id) + "_INTENSITY_PLOT.pdf")
        plt.clf()

    @staticmethod
    def trace2DPointHistogramUsingInverseDistance(trace, rows, cols, min_x, min_y, max_x, max_y):
        # create the bins...
        distribution = np.zeros((rows, cols), np.float64)

        x_line = np.linspace(min_x, max_x, num=cols)
        y_line = np.linspace(min_y, max_y, num=rows)

        for x,y in trace.points:
            if min_x <= x <= max_x and min_y <= y <= max_y:

                for idx_x in range(len(x_line)-1):
                    if x_line[idx_x] <= x < x_line[idx_x+1]:
                        x_Bin = idx_x
                if x == 1.0:
                    x_Bin = len(x_line) - 2

                for idx_y in range(len(y_line)-1):
                    if y_line[idx_y] <= y < y_line[idx_y+1]:
                        y_Bin = idx_y
                if y == 1.0:
                    y_Bin = len(y_line) - 2

                corners = []
                corners.append((x_line[x_Bin],y_line[y_Bin]))
                corners.append((x_line[x_Bin],y_line[y_Bin+1]))
                corners.append((x_line[x_Bin+1],y_line[y_Bin]))
                corners.append((x_line[x_Bin+1],y_line[y_Bin+1]))

                dist = [0.0] * 4
                for idx, corner in enumerate(corners):
                    distCP = distance.cdist([corner],[(x,y)])
                    if distCP[0] == 0:
                        dist = [0.0] * 4
                        dist[idx] = 1.0
                        break
                    else:
                        dist[idx] = (1 / distCP[0])

                distribution[x_Bin][y_Bin] += dist[0]
                distribution[x_Bin][y_Bin+1] += dist[1]
                distribution[x_Bin+1][y_Bin] += dist[2]
                distribution[x_Bin+1][y_Bin+1] += dist[3]

        return distribution

    @staticmethod
    def computeSymbol2DHistogram(symbol, grids, plot_path, variance_factor=None):
        features = []
        for n, m, w_prc, h_prc, off_x, off_y, use_lengths, membership_type, use_parzen, varianceFactor in grids:
            # min_x = -1.0 + off_x * 2.0
            # min_y = -1.0 + off_y * 2.0
            # max_x = min_x + 2.0 * w_prc
            # max_y = min_y + 2.0 * h_prc
            min_x = symbol.minX
            min_y = symbol.minY
            max_x = symbol.maxX
            max_y = symbol.maxY
            centerX = (min_x + max_x) / 2
            centerY = (min_y + max_y) / 2
            length = max(max_x-min_x,max_y-min_y,1)
            min_x = centerX - (length*w_prc)/2
            max_x = centerX + (length*w_prc)/2
            min_y = centerY - (length*h_prc)/2
            max_y = centerY + (length*h_prc)/2
            corners = []
            if use_parzen:
                corners = HistogramFeatureExtractor.getCorners(n+1, m+1, min_x, min_y, max_x, max_y)
                half_diagonal = math.sqrt( ((max_x-min_x)**2) + ((max_y-min_y)**2) ) / 2
                if variance_factor:
                    varianceFactor = variance_factor
                variance = half_diagonal * varianceFactor
                variance = variance ** 2
            # ...for each trace...
            bidimensional_hist = np.zeros((n, m), dtype=np.float64)
            symbol_points = []
            for trace in symbol.traces:
                if use_lengths:
                    hist = HistogramFeatureExtractor.trace2DLengthHistogram(trace, n, m, min_x, min_y, max_x, max_y)
                else:
                    if use_parzen:
                        symbol_points += trace.points
                    else:
                        if membership_type == 1:
                            hist = HistogramFeatureExtractor.trace2DPointHistogram(trace, n, m, min_x, min_y, max_x, max_y)
                        elif membership_type == 2:
                            hist = HistogramFeatureExtractor.trace2DPointHistogramUsingCount(trace, n+1, m+1, min_x, min_y, max_x, max_y)
                        elif membership_type == 3:
                            hist = HistogramFeatureExtractor.trace2DPointHistogramUsingInverseDistance(trace, n, m, min_x, min_y, max_x, max_y)

                if use_parzen:
                    continue

                bidimensional_hist += hist

            if not use_parzen:
                normalization_factor = bidimensional_hist.sum()
                if normalization_factor > 0.0:
                    bidimensional_hist /= normalization_factor

                # add to feature vector (as list of continuous attributes)
                features += bidimensional_hist.reshape((n * m,)).tolist()
                # Visualize Inverse features
                #print(n,m)
                #print(features)
                # corners = HistogramFeatureExtractor.getCorners(n+1, m+1, min_x, min_y, max_x, max_y)
                # print(corners)
                # corners = []
                #
                # for y in np.arange(min_y,max_y+.01,length/(m-1)):
                #     for x in np.arange(min_x, max_x + .01, length / (n - 1)):
                #        corners.append((x,y))
                # print(corners)
                # HistogramFeatureExtractor.visualizeCornerFeatures(symbol, n, m, min_x, min_y, max_x, max_y, corners, features, plot_path)
                # Visualize Count Features
                #HistogramFeatureExtractor.visualizeCountFeatures(symbol, n, m, min_x, min_y, max_x, max_y, list(features), plot_path)
            else:
                features += HistogramFeatureExtractor.trace2DPointHistogramUsingParzen(symbol_points, corners, variance).tolist()

                # corners = []
                #
                # for x in np.arange(min_x, max_x + .01, length / (n - 1)):
                #     for y in np.arange(min_y, max_y + .01, length / (m - 1)):
                #         corners.append((x, y))
                # HistogramFeatureExtractor.visualizeCornerFeatures(symbol, n, m, min_x, min_y, max_x, max_y, corners,features, plot_path)
                #.visualizeCountFeatures(symbol, n+1, m+1, min_x, min_y, max_x, max_y, list(features), plot_path)

        return features


    @staticmethod
    def getTraceOrientationHistogram(trace, rows, cols, min_x, min_y, max_x, max_y, orientation_bins):
        distribution = np.zeros((rows, cols, orientation_bins), np.float64)

        distances = []
        mid_points = []
        orientations = []
        for i in range(len(trace.points) - 1):
            p0_x, p0_y = trace.points[i]
            p1_x, p1_y = trace.points[i + 1]
            mid_x = (p0_x + p1_x) / 2
            mid_y = (p0_y + p1_y) / 2

            # only if the middle point of the segment is inside of the current grid
            if min_x <= mid_x <= max_x and min_y <= mid_y <= max_y:
                mid_points.append((mid_x, mid_y))

                # Length
                length = math.sqrt((p1_x - p0_x) ** 2 + (p1_y - p0_y) ** 2)
                distances.append(length)

                # Orientation
                currentAngle = math.atan2(p1_y - p0_y, p1_x - p0_x)
                orientations.append(currentAngle)

        # Now calculate the angles...
        ang_bin_size = math.pi / orientation_bins
        off = math.pi

        grid_w = max_x - min_x
        grid_h = max_y - min_y

        # use midpoint of the line to distribute values on the corresponding cells of the grid
        for i, (mid_x, mid_y) in enumerate(mid_points):
            # relative weight according to length in relation to total...
            wl = distances[i]

            if rows >= 2:
                #....for Y....
                val_y = ((mid_y - min_y) / grid_h) * (rows - 1)
                c0_y = int(val_y)
                if c0_y == rows - 1:
                    c0_y -= 1
                c1_y = c0_y + 1
                w1_y = val_y - c0_y
                w0_y = 1 - w1_y

            if cols >= 2:
                #....for X....
                val_x = ((mid_x - min_x) / grid_w) * (cols - 1)
                c0_x = int(val_x)
                if c0_x == cols - 1:
                    c0_x -= 1
                c1_x = c0_x + 1
                w1_x = val_x - c0_x
                w0_x = 1 - w1_x

            # angle of current segment of line...
            currentAngle = orientations[i]

            # angle is between -pi and pi, add offset and divide between angular bin size
            p = (currentAngle + off) / ang_bin_size
            # the base orientation
            fp = math.floor(p)

            # the weight of the second orientation
            wp1 = p - fp
            # select bin for current orientation
            p0 = int(fp % orientation_bins)
            # select bin for next orientation (circular)
            p1 = int((p0 + 1) % orientation_bins)

            # the values....
            g0 = wl * (1.0 - wp1)
            g1 = wl * wp1

            # add to corresponding bins...
            if rows >= 2 and cols >= 2:
                # 4 cells affected, 8 bins
                # (0,0)
                distribution[c0_y][c0_x][p0] += g0 * w0_x * w0_y
                distribution[c0_y][c0_x][p1] += g1 * w0_x * w0_y
                # (0,1)
                distribution[c0_y][c1_x][p0] += g0 * w1_x * w0_y
                distribution[c0_y][c1_x][p1] += g1 * w1_x * w0_y
                # (1,0)
                distribution[c1_y][c0_x][p0] += g0 * w0_x * w1_y
                distribution[c1_y][c0_x][p1] += g1 * w0_x * w1_y
                # (1,1)
                distribution[c1_y][c1_x][p0] += g0 * w1_x * w1_y
                distribution[c1_y][c1_x][p1] += g1 * w1_x * w1_y
            elif rows >= 2:
                # 2 cells affected, 4 bins
                # (0,0)
                distribution[c0_y][0][p0] += g0 * w0_y
                distribution[c0_y][0][p1] += g1 * w0_y
                # (1,0)
                distribution[c1_y][0][p0] += g0 * w1_y
                distribution[c1_y][0][p1] += g1 * w1_y
            elif cols >= 2:
                # 2 cells affected, 4 bins
                # (0,0)
                distribution[0][c0_x][p0] += g0 * w0_x
                distribution[0][c0_x][p1] += g1 * w0_x
                # (0,1)
                distribution[0][c1_x][p0] += g0 * w1_x
                distribution[0][c1_x][p1] += g1 * w1_x
            else:
                # 1 cell affected, 2 bins
                distribution[0][0][p0] += g0
                distribution[0][0][p1] += g1

        return distribution

    @staticmethod
    def computeSymbolOrientationHistogram(symbol, grids):
        features = []
        for n, m, w_prc, h_prc, off_x, off_y, orientation_bins in grids:
            min_x = -1.0 + off_x * 2.0
            min_y = -1.0 + off_y * 2.0
            max_x = min_x + 2.0 * w_prc
            max_y = min_y + 2.0 * h_prc

            # ...for each trace...
            bidimensional_hist = np.zeros((n, m, orientation_bins), np.float64)
            for trace in symbol.traces:
                hist = HistogramFeatureExtractor.getTraceOrientationHistogram(trace, n, m, min_x, min_y, max_x, max_y,
                                                                              orientation_bins)

                bidimensional_hist += hist

            # then, normalize!
            normalization_factor = bidimensional_hist.sum()
            if normalization_factor > 0.0:
                bidimensional_hist /= normalization_factor

            # add to feature vector (as list of continuous attributes)
            features += bidimensional_hist.reshape((n * m * orientation_bins,)).tolist()

        return features

    @staticmethod
    def computeRelation2DHistogram(parent, child, expression, grids, plot_path, variance_factor=None):
        features = []
        contextPoints = expression.getContextPoints(parent,child)
        for n, m, w_prc, h_prc, off_x, off_y, use_lengths, membership_type, use_parzen, varianceFactor in grids:
            min_x = min(parent.minX, child.minX)
            min_y = min(parent.minY, child.minY)
            max_x = max(parent.maxX, child.maxX)
            max_y = max(parent.maxY, child.maxY)
            centerX = (min_x + max_x) / 2
            centerY = (min_y + max_y) / 2
            length = max(max_x-min_x,max_y-min_y)
            min_x = centerX - (length*w_prc)/2
            max_x = centerX + (length*w_prc)/2
            min_y = centerY - (length*h_prc)/2
            max_y = centerY + (length*h_prc)/2

            # Remove Context points beyond the diagonal
            diag = np.linalg.norm(np.array([centerX,centerY])-[min_x,min_y])
            inside_indices = distance.cdist(np.array([(centerX,centerY)]), contextPoints) <= diag
            contextPoints = [contextPoints[i] for i in range(contextPoints.shape[0]) if inside_indices[0][i]]
            if contextPoints == []:
                contextPoints = [(centerX,centerY)]

            context_trace_set = MathSymbolTraceSet("666", [MathSymbolTrace("999", contextPoints)], None)

            # create trace to hold other points from the expression
            if use_parzen:
                hists = [np.zeros((n * m), dtype=np.float64), np.zeros((n * m), dtype=np.float64), np.zeros((n * m), dtype=np.float64)]
            else:
                hists = [np.zeros((n, m), dtype=np.float64), np.zeros((n, m), dtype=np.float64), np.zeros((n, m), dtype=np.float64)]
            for i, point_set in enumerate([parent,child,context_trace_set]):
                for t in point_set.traces:
                    trace = MathSymbolTrace("temp", expression.getTracePoints(t, point_set.id)) # Gets unnormalized points for the trace.
                    if use_lengths: # Will give weird results for context points, because it is many traces combined
                        hist = HistogramFeatureExtractor.trace2DLengthHistogram(trace, n, m, min_x, min_y, max_x, max_y)
                    elif use_parzen:
                        #TODO fix corner generation, current does bin centers(?)
                        corners = HistogramFeatureExtractor.getCorners(n+1, m+1, min_x, min_y, max_x, max_y)
                        half_diagonal = np.linalg.norm(np.array([min_x,min_y])-[max_x,max_y])/2
                        if variance_factor:
                            varianceFactor = variance_factor
                        variance = (half_diagonal * varianceFactor) ** 2
                        hist = HistogramFeatureExtractor.trace2DPointHistogramUsingParzen(trace.points, corners, variance)
                    elif membership_type == 1:
                        hist = HistogramFeatureExtractor.trace2DPointHistogram(trace, n, m, min_x, min_y, max_x, max_y)
                    elif membership_type == 2:
                        hist = HistogramFeatureExtractor.trace2DPointHistogramUsingCount(trace, n+1, m+1, min_x, min_y, max_x, max_y)
                    elif membership_type == 3:
                        hist = HistogramFeatureExtractor.trace2DPointHistogramUsingInverseDistance(trace, n, m, min_x, min_y, max_x, max_y)
                    else:
                        raise Exception("Invalid or Missing type of histogram")
                    hists[i] += hist

            for h in hists:
                if not use_parzen:
                    normalization_factor = h.sum()
                    if normalization_factor > 0.0:
                        h /= normalization_factor

                # add to feature vector (as list of continuous attributes)
                features += h.reshape((n * m,)).tolist()

            #if use_parzen:
                # corners = []
                # for x in np.arange(min_x, max_x + .01, length / (n - 1)):
                #     for y in np.arange(min_y, max_y + .01, length / (m - 1)):
                #         corners.append((x, y))
                # prefix = str(parent.id) + "_" + str(child.id) + "_"
                # HistogramFeatureExtractor.visualizeCornerFeatures(parent, n, m, min_x, min_y, max_x, max_y, corners,
                #                                                   features[:(n*m)], plot_path, name=prefix+"parent")
                # HistogramFeatureExtractor.visualizeCornerFeatures(child, n, m, min_x, min_y, max_x, max_y, corners,
                #                                                   features[(n*m):(2*n*m)], plot_path, name=prefix+"child")
                # HistogramFeatureExtractor.visualizeCornerFeatures(context_trace_set, n, m, min_x, min_y, max_x, max_y, corners,
                #                                                   features[(2 * n * m):], plot_path,
                #                                                   name=prefix + "context")

        return features

    @staticmethod
    def computeMSTContext2DHistogram(expression, symbols, grids, relation_types, plot_path, variance_factor=None):
        features = []
        # get set of edge traces
        traces = expression.edgeTraces
        if traces is None:
            raise Exception("No edge traces were set for expression " + expression.id)

        # get features from each grid
        for n, m, w_prc, h_prc, off_x, off_y, use_lengths, membership_type, use_parzen, varianceFactor in grids:
            # get boundries of grid
            min_x = min([sym.minX for sym in symbols])
            min_y = min([sym.minY for sym in symbols])
            max_x = max([sym.maxX for sym in symbols])
            max_y = max([sym.maxY for sym in symbols])
            centerX = (min_x + max_x) / 2
            centerY = (min_y + max_y) / 2
            length = max(max_x-min_x,max_y-min_y)
            min_x = centerX - (length*w_prc)/2
            max_x = centerX + (length*w_prc)/2
            min_y = centerY - (length*h_prc)/2
            max_y = centerY + (length*h_prc)/2

            # Setup histograms for each relatioin group
            histograms = {}
            for group in relation_types.values():
                if group not in histograms:
                    if use_parzen:
                        histograms[group] = np.zeros((n*m), dtype=np.float64)
                    else:
                        histograms[group] = np.zeros((n, m), dtype=np.float64)

            # for relation trace get grid contributions
            for rel_trace in traces:
                if set(rel_trace.weights.keys()) & set(relation_types.keys()) == set():
                    continue
                if use_lengths:
                    hist = HistogramFeatureExtractor.trace2DLengthHistogram(rel_trace.trace, n, m, min_x, min_y, max_x, max_y)
                elif use_parzen:
                    #TODO check if any trace points are within grid.
                    corners = HistogramFeatureExtractor.getCorners(n+1, m+1, min_x, min_y, max_x, max_y)
                    half_diagonal = np.linalg.norm(np.array([min_x,min_y])-[max_x,max_y])/2
                    if variance_factor:
                        varianceFactor = variance_factor
                    variance = (half_diagonal * varianceFactor) ** 2
                    hist = HistogramFeatureExtractor.trace2DPointHistogramUsingParzen(rel_trace.trace.points, corners, variance)
                elif membership_type == 1:
                    hist = HistogramFeatureExtractor.trace2DPointHistogram(rel_trace.trace, n, m, min_x, min_y, max_x, max_y)
                elif membership_type == 2:
                    hist = HistogramFeatureExtractor.trace2DPointHistogramUsingCount(rel_trace.trace, n+1, m+1, min_x, min_y, max_x, max_y)
                elif membership_type == 3:
                    hist = HistogramFeatureExtractor.trace2DPointHistogramUsingInverseDistance(rel_trace.trace, n, m, min_x, min_y, max_x, max_y)
                else:
                    raise Exception("Invalid or Missing type of histogram")

                # for each desired relation type add relation features to it histogram after weighting by confidence
                for rel_type, group in relation_types.items():
                    try:
                        histograms[group] += hist*rel_trace.getWeight(rel_type)
                    except:
                        print(histograms[group].shape)
                        print(hist.shape)
                        print(rel_trace.getWeight(rel_type))
                        raise Exception("Error in adding histograms")

            # normalize all histograms if not parzen
            for h in histograms.values():
                # if not use_parzen:
                #     normalization_factor = h.sum()
                #     if normalization_factor > 0.0:
                #         h /= normalization_factor

                # add to feature vector (as list of continuous attributes)
                features += h.reshape((n * m,)).tolist()
        return features

