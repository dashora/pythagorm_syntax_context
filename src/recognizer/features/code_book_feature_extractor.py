import math
import numpy as np
from sklearn.cluster import MiniBatchKMeans

from recognizer.data.code_book import CodeBook
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.features.histograms_feature_extractor import HistogramFeatureExtractor

class CodeBookFeatureExtractor:
    SamplingUniformPerTrace = 1
    SamplingUniformGrid = 2
    SamplingSharpPoints = 3
    LocalFeatureOrientationHistograms = 1
    LocalFeatureLength2DHistograms = 2

    def __init__(self, box_w_prc, box_h_prc):
        self.box_w_prc = box_w_prc
        self.box_h_prc = box_h_prc

        self.sampling_type = CodeBookFeatureExtractor.SamplingUniformPerTrace

        self.uni_samp_dist = 0.2

        self.grid_samp_cols = 5
        self.grid_samp_rows = 5

        self.local_feature_type = CodeBookFeatureExtractor.LocalFeatureOrientationHistograms

        self.l_hist_rows = 2
        self.l_hist_cols = 2

        self.or_hist_rows = 2
        self.or_hist_cols = 2
        self.or_hist_bins = 4

        self.num_clusters = 8
        self.clust_batch = 1000

        self.encoding_regions = [(1, 1, 0.0, 0.0, 2, 2, True)]
        self.soft_assignment_neighbors = 8

        self.min_soft_asignment_dist = 0.000001

    def set_uniform_trace_sampling_parameters(self, uni_samp_dist):
        # Uniform sampling by trace
        self.sampling_type = CodeBookFeatureExtractor.SamplingUniformPerTrace
        self.uni_samp_dist = uni_samp_dist

    def set_uniform_grid_sampling_parameters(self, grid_samp_rows, grid_samp_cols):
        # Uniform sampling by Grid
        self.sampling_type = CodeBookFeatureExtractor.SamplingUniformGrid
        self.grid_samp_rows = grid_samp_rows
        self.grid_samp_cols = grid_samp_cols

    def set_sharp_point_sampling_parameters(self):
        self.sampling_type = CodeBookFeatureExtractor.SamplingSharpPoints

    def set_orientation_hist_parameters(self, orient_hist_rows, orient_hist_cols, orient_hist_bins):
        # Orientation histograms
        self.local_feature_type = CodeBookFeatureExtractor.LocalFeatureOrientationHistograms

        self.or_hist_rows = orient_hist_rows
        self.or_hist_cols = orient_hist_cols
        self.or_hist_bins = orient_hist_bins

    def set_length_2d_hist_parameters(self, rows, cols):
        self.local_feature_type = CodeBookFeatureExtractor.LocalFeatureLength2DHistograms

        self.l_hist_rows = rows
        self.l_hist_cols = cols

    def set_clustering_parameters(self, num_clusters, batch_size):
        self.num_clusters = num_clusters
        self.clust_batch = batch_size

    def set_encoding_parameters(self, encoding_regions, soft_assignment_neighbors):
        self.encoding_regions = encoding_regions
        self.soft_assignment_neighbors = min(self.num_clusters, soft_assignment_neighbors)

    def getFeatures(self, symbol):
        if self.sampling_type == CodeBookFeatureExtractor.SamplingUniformPerTrace:
            boxes = CodeBookFeatureExtractor.uniform_sample_boxes(symbol, self.box_w_prc, self.box_h_prc,
                                                                  self.uni_samp_dist)
        elif self.sampling_type == CodeBookFeatureExtractor.SamplingUniformGrid:
            boxes = CodeBookFeatureExtractor.grid_sample_boxes(symbol, self.box_w_prc, self.box_h_prc,
                                                               self.grid_samp_rows, self.grid_samp_cols)
        elif self.sampling_type == CodeBookFeatureExtractor.SamplingSharpPoints:
            boxes = CodeBookFeatureExtractor.sharp_points_sample_boxes(symbol, self.box_w_prc, self.box_h_prc)

        features = None
        if self.local_feature_type == CodeBookFeatureExtractor.LocalFeatureOrientationHistograms:
            features = CodeBookFeatureExtractor.get_symbol_orientation_features(symbol, boxes, self.or_hist_rows,
                                                                                self.or_hist_cols, self.or_hist_bins)
        elif self.local_feature_type == CodeBookFeatureExtractor.LocalFeatureLength2DHistograms:
            features = CodeBookFeatureExtractor.get_symbol_2D_length_histogram_features(symbol, boxes, self.l_hist_rows,
                                                                                        self.l_hist_cols)

        return features, boxes

    def getCodeBook(self, train_features):
        return CodeBookFeatureExtractor.create_k_means_code_book(train_features, self.num_clusters, self.clust_batch)

    def getSymbolEncoding(self, box_regions, box_features, code_book):
        return CodeBookFeatureExtractor.get_symbol_encoding(box_regions, box_features, code_book,
                                                            self.soft_assignment_neighbors, self.encoding_regions,
                                                            self.min_soft_asignment_dist)

    def current_encoding_length(self):
        return CodeBookFeatureExtractor.get_encoding_length(self.encoding_regions, self.num_clusters)

    @staticmethod
    def get_encoding_length(encoding_regions, num_clusters):
        total_features = 0
        for w, h, x, y, rows, cols, soft_grid in encoding_regions:
            if soft_grid:
                total_features += (rows + 1) * (cols + 1)
            else:
                total_features += rows * cols

        return total_features * num_clusters

    @staticmethod
    def get_symbol_encoding(box_regions, box_features, code_book, soft_assignment_neighbors, regions, code_threshold):
        # generate encoding
        local_features_weights = []

        num_clusters = len(code_book.codes)

        # check weight for all words in the code book ...
        # first, compute distance to all code book centers
        # then, choose top n
        # compute weight for each of the top n
        for l_idx in range(len(box_regions)):
            # for each cluster ...
            cluster_distances = []
            for cluster_idx in range(num_clusters):
                distance = np.linalg.norm(box_features[l_idx] - code_book.codes[cluster_idx])
                cluster_distances.append((distance, cluster_idx))

            cluster_distances = sorted(cluster_distances)

            # choose top N and assign them weights ...
            if cluster_distances[0][0] <= code_threshold:
                # threshold of distance to cluster center, assume is on the center ...
                weights = [0 for distance, cluster_idx in cluster_distances[:soft_assignment_neighbors]]
                weights[0] = 1.0
            else:
                weights = [1 / distance for distance, cluster_idx in cluster_distances[:soft_assignment_neighbors]]

            total_weight = sum(weights)

            weight_assignments = []
            for k in range(soft_assignment_neighbors):
                weight_assignments.append((cluster_distances[k][1], weights[k] / total_weight))

            local_features_weights.append(weight_assignments)

        total_features = CodeBookFeatureExtractor.get_encoding_length(regions, num_clusters)
        feature_vector = np.zeros(total_features, np.float32)

        g_off_label = 0
        for r_idx, (g_w_prc, g_h_prc, g_off_x, g_off_y, g_rows, g_cols, soft_grid) in enumerate(regions):
            g_min_x = g_off_x * 2.0 - 1.0
            g_min_y = g_off_y * 2.0 - 1.0
            g_max_x = g_min_x + g_w_prc * 2.0
            g_max_y = g_min_y + g_h_prc * 2.0
            g_cell_w = (g_max_x - g_min_x) / g_cols
            g_cell_h = (g_max_y - g_min_y) / g_rows

            # for each box in the symbol ...
            for l_idx, feature_weights in enumerate(local_features_weights):
                # box boundaries ...
                (box_min_x, box_min_y), (box_max_x, box_max_y) = box_regions[l_idx]

                # Use box center for cell assignment
                box_mid_x = (box_min_x + box_max_x) / 2.0
                box_mid_y = (box_min_y + box_max_y) / 2.0

                if (g_min_x <= box_mid_x <= g_max_x) and (g_min_y <= box_mid_y <= g_max_y):
                    # is within the grid, find which cell
                    # ... x ...
                    val_x = (box_mid_x - g_min_x) / g_cell_w
                    cell_x0 = int(val_x)
                    if cell_x0 == g_cols:
                        # might happen if box center is on the border of the grid
                        cell_x0 -= 1

                    # ... y ...
                    val_y = (box_mid_y - g_min_y) / g_cell_h
                    cell_y0 = int(val_y)
                    if cell_y0 == g_rows:
                        # might happen if box center is on the border of the grid
                        cell_y0 -= 1

                    assert (0 <= cell_x0 <= g_cols)
                    assert (0 <= cell_y0 <= g_rows)

                    if soft_grid:
                        # weights and other corner affected
                        # ... x ...
                        cell_x1 = cell_x0 + 1
                        w_x1 = val_x - cell_x0
                        w_x0 = 1.0 - w_x1

                        # ... y ...
                        cell_y1 = cell_y0 + 1
                        w_y1 = val_y - cell_y0
                        w_y0 = 1.0 - w_y1

                        assert (0.0 <= w_x0 <= 1.0) and (0.0 <= w_x1 <= 1.0)
                        assert (0.0 <= w_y0 <= 1.0) and (0.0 <= w_y1 <= 1.0)
                        assert (1 <= cell_x1 <= g_cols + 1)
                        assert (1 <= cell_y1 <= g_rows + 1)

                        affected_corners = [(cell_x0, cell_y0, w_x0, w_y0), (cell_x0, cell_y1, w_x0, w_y1),
                                            (cell_x1, cell_y0, w_x1, w_y0), (cell_x1, cell_y1, w_x1, w_y1)]
                    else:
                        # Hard grid
                        affected_corners = [(cell_x0, cell_y0, 1.0, 1.0)]

                    # add weights to affected descriptors
                    for label, weight in feature_weights:
                        for cell_x, cell_y, w_x, w_y in affected_corners:

                            if soft_grid:
                                corner_offset = (cell_y * (g_cols + 1) + cell_x) * num_clusters
                            else:
                                corner_offset = (cell_y * g_cols + cell_x) * num_clusters

                            feature_vector[g_off_label + corner_offset + label] += weight * w_x * w_y

            # move to position of next grid in feature vector
            if soft_grid:
                g_off_label += (g_rows + 1) * (g_cols + 1) * num_clusters
            else:
                g_off_label += g_rows * g_cols * num_clusters

        return feature_vector


    @staticmethod
    def uniform_sample_boxes(symbol, box_w_prc, box_h_prc, sampling_dist):
        # PARAMETERS
        box_size_w = box_w_prc * 2.0
        box_size_h = box_h_prc * 2.0

        half_w = box_size_w * 0.5
        half_h = box_size_h * 0.5

        all_boxes = []
        for idx, trace in enumerate(symbol.traces):
            last_distance = sampling_dist
            for p_idx in range(len(trace.points) - 1):
                if last_distance >= sampling_dist:
                    # add box
                    p_x, p_y = trace.points[p_idx]
                    all_boxes.append(((p_x - half_w, p_y - half_h), (p_x + half_w, p_y + half_h)))
                    last_distance = 0.0

                last_distance += trace.distance(p_idx, p_idx + 1)

        return all_boxes

    @staticmethod
    def grid_sample_boxes(symbol, box_w_prc, box_h_prc, grid_samp_rows, grid_samp_cols):
        # PARAMETERS
        box_size_w = box_w_prc * 2.0
        box_size_h = box_h_prc * 2.0

        half_w = box_size_w * 0.5
        half_h = box_size_h * 0.5

        grid_start_x = half_w
        grid_start_y = half_h
        grid_w = 2.0 - box_size_w
        grid_h = 2.0 - box_size_h
        grid_cell_w = grid_w / (grid_samp_cols - 1)
        grid_cell_h = grid_h / (grid_samp_rows - 1)

        all_boxes = []
        for row in range(grid_samp_rows):
            for col in range(grid_samp_cols):
                p_x = grid_start_x + col * grid_cell_w
                p_y = grid_start_y + row * grid_cell_h

                box = ((p_x - half_w, p_y - half_h), (p_x + half_w, p_y + half_h))
                segments = CodeBookFeatureExtractor.get_segments_on_box(symbol, box)

                if len(segments) > 0:
                    all_boxes.append(box)

        return all_boxes

    @staticmethod
    def sharp_points_sample_boxes(symbol, box_w_prc, box_h_prc):
        # PARAMETERS
        box_size_w = box_w_prc * 2.0
        box_size_h = box_h_prc * 2.0

        half_w = box_size_w * 0.5
        half_h = box_size_h * 0.5

        all_boxes = []
        for idx, trace in enumerate(symbol.traces):
            for _, (p_x, p_y) in trace.sharp_points:
                box = ((p_x - half_w, p_y - half_h), (p_x + half_w, p_y + half_h))

                all_boxes.append(box)

        return all_boxes

    @staticmethod
    def get_segments_on_box(symbol, box):
        (box_min_x, box_min_y), (box_max_x, box_max_y) = box

        segments = []
        for idx, trace in enumerate(symbol.traces):
            for p_idx in range(len(trace.points) - 1):
                p1_x, p1_y = trace.points[p_idx]
                p2_x, p2_y = trace.points[p_idx + 1]

                if ((box_min_x <= p1_x <= box_max_x) and (box_min_y <= p1_y <= box_max_y) and
                    (box_min_x <= p2_x <= box_max_x) and (box_min_y <= p2_y <= box_max_y)):
                    segments.append(((p1_x, p1_y), (p2_x, p2_y)))

        return segments

    @staticmethod
    def get_traces_on_box(symbol, box):
        (box_min_x, box_min_y), (box_max_x, box_max_y) = box

        traces = []
        for idx, trace in enumerate(symbol.traces):
            current_trace = []
            for p_idx in range(len(trace.points)):
                p_x, p_y = trace.points[p_idx]

                if (box_min_x <= p_x <= box_max_x) and (box_min_y <= p_y <= box_max_y):
                    current_trace.append((p_x, p_y))
                else:
                    if len(current_trace) > 0:
                        # don't add isolated points, only segments inside of the box
                        if len(current_trace) >= 2:
                            traces.append(current_trace)

                        current_trace = []

            if len(current_trace) >= 2:
                traces.append(current_trace)

        trace_objects = []
        for idx, trace in enumerate(traces):
            trace_objects.append(MathSymbolTrace(idx, trace))

        return trace_objects

    @staticmethod
    def get_symbol_orientation_features(symbol, all_boxes, rows, cols, bins):
        all_features = []
        for idx, box in enumerate(all_boxes):
            (min_x, min_y), (max_x, max_y) = box

            local_traces = CodeBookFeatureExtractor.get_traces_on_box(symbol, box)

            hist = np.zeros((rows, cols, bins), np.float64)

            if len(local_traces) > 0:
                for trace in local_traces:
                    hist += HistogramFeatureExtractor.getTraceOrientationHistogram(trace, rows, cols, min_x, min_y,
                                                                                   max_x, max_y, bins)

            normalization_factor = hist.sum()
            if normalization_factor > 0.0:
                hist /= normalization_factor

            # add to feature vector (as list of continuous attributes)
            features = hist.reshape((rows * cols * bins,)).tolist()

            all_features.append(features)

        return all_features


    @staticmethod
    def get_symbol_2D_length_histogram_features(symbol, all_boxes, rows, cols):
        all_features = []
        for idx, box in enumerate(all_boxes):
            (min_x, min_y), (max_x, max_y) = box

            local_traces = CodeBookFeatureExtractor.get_traces_on_box(symbol, box)

            hist = np.zeros((rows, cols), np.float64)
            if len(local_traces) > 0:
                for trace in local_traces:
                    hist += HistogramFeatureExtractor.trace2DLengthHistogram(trace, rows, cols, min_x, min_y, max_x,
                                                                             max_y)

            normalization_factor = hist.sum()
            if normalization_factor > 0.0:
                hist /= normalization_factor

            # add to feature vector (as list of continuous attributes)
            features = hist.reshape((rows * cols,)).tolist()

            all_features.append(features)

        return all_features

    @staticmethod
    def create_k_means_code_book(training_features, num_clusters, batch_size):
        if isinstance(training_features, list):
            training_features = np.array(training_features)

        clustering = MiniBatchKMeans(init='k-means++', n_clusters=num_clusters, batch_size=batch_size, n_init=10,
                                     max_no_improvement=10, verbose=0)

        clustering.fit(training_features)

        return CodeBook(clustering.cluster_centers_), clustering.labels_

    @staticmethod
    def create_from_config(config):
        num_clusters = config.get_int("CODE_BOOK_SIZE")
        soft_assignment_neighbors = min(config.get_int("CODE_BOOK_SOFT_ASSIGNMENT_NEIGHBORS"), num_clusters)
        batch_size = config.get_int("CODE_BOOK_CLUSTERING_KMEANS_BATCH_SIZE")

        regions = config.get("CODE_BOOK_ENCODING_REGIONS")

        # box w, box h, sample dist, feat rows, feat cols, feat bins
        box_w_prc = config.get("CODE_BOOK_PATCH_WIDTH")
        box_h_prc = config.get("CODE_BOOK_PATCH_HEIGHT")

        cb_feat_extractor = CodeBookFeatureExtractor(box_w_prc, box_h_prc)
        cb_feat_extractor.set_clustering_parameters(num_clusters, batch_size)

        sampling_type = config.get_int("CODE_BOOK_PATCH_SAMPLING")
        # load parameters based on sampling type
        if sampling_type == CodeBookFeatureExtractor.SamplingUniformPerTrace:
            # Uniform by following traces
            uni_samp_dist = config.get("CODE_BOOK_PATCH_UNIFORM_SAMPLING_DISTANCE")
            cb_feat_extractor.set_uniform_trace_sampling_parameters(uni_samp_dist)

        elif sampling_type == CodeBookFeatureExtractor.SamplingUniformGrid:
            # Uniform by Grid
            grid_samp_rows = config.get("CODE_BOOK_PATCH_GRID_SAMPLING_ROWS")
            grid_samp_cols = config.get("CODE_BOOK_PATCH_GRID_SAMPLING_COLS")
            cb_feat_extractor.set_uniform_grid_sampling_parameters(grid_samp_rows, grid_samp_cols)

        elif sampling_type == CodeBookFeatureExtractor.SamplingSharpPoints:
            # Sampling on Sharp Points ...
            cb_feat_extractor.set_sharp_point_sampling_parameters()

        feature_type = config.get_int("CODE_BOOK_FEATURE_TYPE")
        # Load parameter based on local feature type
        if feature_type == CodeBookFeatureExtractor.LocalFeatureOrientationHistograms:
            feat_rows = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_ROWS")
            feat_cols = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_COLS")
            feat_bins = config.get_int("CODE_BOOK_FEATURE_ORIENTATION_HIST_BINS")

            cb_feat_extractor.set_orientation_hist_parameters(feat_rows, feat_cols, feat_bins)

        elif feature_type == CodeBookFeatureExtractor.LocalFeatureLength2DHistograms:
            feat_rows = config.get_int("CODE_BOOK_FEATURE_LENGTH_2D_HIST_ROWS")
            feat_cols = config.get_int("CODE_BOOK_FEATURE_LENGTH_2D_HIST_COLS")

            cb_feat_extractor.set_length_2d_hist_parameters(feat_rows, feat_cols)

        # regions, use_soft_assignment, use_soft_grids, soft_assignment_neighbors
        cb_feat_extractor.set_encoding_parameters(regions, soft_assignment_neighbors)

        return cb_feat_extractor
