'''
@author : Lakshmi Ravi
@author : Chinmay Jain

This library to extract shape context features for a given symbol in an expression
'''

import math
import numpy as np
import matplotlib.pyplot as plt
import copy
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d
import sys
plot_path = '/home/lr3179/WorkspaceMerge/DPRL_SYMBOL_RECOGNIZER/src/Debug_Plots/Old/'

class ContextFeatureExtractor:

    def __init__(self, radius_restrict_factor, sectors_count, circle_count,variance_factor, method_type, samples_type, circle_type, global_context,scc_radius_method, intensity_type, Normalization_method, debug_mode = False):
        self.RADIUS_RESTRICTION_FACTOR = radius_restrict_factor
        self.SECTORS_COUNT = sectors_count
        self.VARIANCE_FACTOR = variance_factor
        self.CIRCLE_COUNT = circle_count
        
        self.required_features = global_context
        self.extraction_type = method_type
        self.samples_type =samples_type
        self.circle_type=circle_type
        self.scc_radius_method = scc_radius_method
        self.parzen_normalization_method = Normalization_method
        
        #parameters to turn-on visualization
        self.intensity_type = intensity_type
        #parameters to tune the visualization
        self.debug_mode = debug_mode
        if self.required_features != "CONTEXT_ONLY" and self.required_features != "SYMBOL_ONLY" and self.required_features != "CONTEXT_AND_SYMBOL":
            print("How to collect the features is not well defined, It should be either one of CONTEXT_ONLY, SYMBOL_ONLY,CONTEXT_AND_SYMBOL ")
            sys.exit(0)

    def find_max_distance(self, CenterX, centerY, all_local_points):
        distance = 0
        for point in all_local_points:
            p = math.sqrt(
                math.pow((point[0] - CenterX), 2) + math.pow((point[1] - centerY), 2))
            distance = distance if distance > p else p
        return self.RADIUS_RESTRICTION_FACTOR * distance

    def get_parzen_density_value(self,sample_point, points, radius_for_variance):
        '''
        Given the reference sample_point and find the density around that for the radius considered
        '''
        pd = 0
        points_num = len(points)
        ## proportion controls the proportion between window width and radius length
        if points_num == 0 or radius_for_variance == 0:
                return pd
        else:
            hn = (radius_for_variance)*self.VARIANCE_FACTOR
            x = sample_point[0]
            y = sample_point[1]
            for point_i in points:
                xi = point_i[0]
                yi = point_i[1]
                one_pd_x = (1/(math.sqrt(2*math.pi)*hn))*math.exp(((-1)*((xi-x)**2))/(2*(hn**2)))
                one_pd_y = (1/(math.sqrt(2*math.pi)*hn))*math.exp(((-1)*((yi-y)**2))/(2*(hn**2)))
                one_pd = one_pd_x * one_pd_y
                pd = one_pd + pd
        return pd

    def get_count_features(self,center, entire_radius, points):
        '''
        returns the count vector of the current set of points
        '''
        features = []
        hist = np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        if(entire_radius == 0):
            #when there is only one point in the symbol
            return hist.reshape((self.CIRCLE_COUNT * self.SECTORS_COUNT,)).tolist()
        radial_distance = entire_radius/(self.CIRCLE_COUNT)
        angular_distance = 360.0/(self.SECTORS_COUNT)
        for point in points:
                x,y = point
                dist_from_center = math.sqrt((x-center[0])**2 + (y-center[1])**2)
                #the neighboring points that are out-side the shape context circle don't consider
                if dist_from_center > entire_radius:
                    continue
                angle_at_center = math.atan2((y-center[1]),(x-center[0]))
                angle_at_center = math.degrees(angle_at_center)
                if angle_at_center < 0:
                    angle_at_center += 360
                # take minimum of bins available , the ratio obtained,to consider points on the edge of circle
                radial_bin = min(self.CIRCLE_COUNT-1, math.floor(dist_from_center/radial_distance))
                angular_bin = min(self.SECTORS_COUNT-1, math.floor(angle_at_center/angular_distance))
                # for points on the edge of the circle and the symbol
                hist[angular_bin][radial_bin] += 1.0
        #normalize it to total score
        normalization_factor = hist.sum()
        if(normalization_factor != 0.0):
            hist /= normalization_factor
        features += hist.reshape((self.CIRCLE_COUNT * self.SECTORS_COUNT,)).tolist()
        return features

    def get_shape_context_by_count(self, center, entire_radius, symbol_points, other_points):
        '''
            for the given symbol points and neighboring points computes the shape context features by counting
        '''
        features = []
        if(self.required_features != "CONTEXT_ONLY"):
            features += self.get_count_features(center, entire_radius, symbol_points)
        if(self.required_features != "SYMBOL_ONLY"):
            features += self.get_count_features(center, entire_radius, other_points)
        return features

    def get_shape_context_distance(self, center, bounding_box, symbol_points, other_points):
        '''
           get the distance unit of the shape context features
        '''
        if self.scc_radius_method == "BOUNDING_BOX_DIAGONAL":
            # get the bounding box diagonal of the symbol
            #s_minX, s_maxX, s_minY, s_maxY = current_strokes.original_box
            s_minX, s_maxX, s_minY, s_maxY = bounding_box
            return math.sqrt(math.pow(center[0] - s_maxX, 2) + math.pow(center[1] - s_maxY, 2))
        
        elif self.scc_radius_method == "NEAREST_NEIGHBOR":
            # get the minimum neighboring point distance as radius
            if len(other_points) < 1:
                return 0.0
            #get first point and set initial max distance
            first_point = other_points[0]
            min_distance_square = math.pow(first_point[0]-center[0], 2) + math.pow(first_point[1]-center[1], 2)
            for point in other_points:
                dist_between = math.pow(point[0]-center[0], 2) + math.pow(point[1]-center[1], 2)
                min_distance_square = min_distance_square if min_distance_square < dist_between else dist_between
            return math.sqrt(min_distance_square)
        
        elif self.scc_radius_method == "FARTHEST_NEIGHBOR":
            # get the minimum neighboring point distance as radius
            if len(other_points) < 1:
                return 0.0
            #get first point and set initial max distance
            max_distance_square = 0
            for point in other_points:
                dist_between = math.pow(point[0]-center[0], 2) + math.pow(point[1]-center[1], 2)
                max_distance_square = max_distance_square if max_distance_square > dist_between else dist_between
            return math.sqrt(max_distance_square)
        
        elif self.scc_radius_method == "SYMBOL_FARTHEST_PT":
            # get the farthest point in the symbol as radius
            max_distance_square = 0.0
            for point in symbol_points:
                dist_between = math.pow(point[0]-center[0], 2) + math.pow(point[1]-center[1], 2)
                max_distance_square = max(dist_between, max_distance_square)
            return math.sqrt(max_distance_square)
        else:
            print("Distance unit of shape context circles not defined. It should be BOUNDING_BOX_DIAGONAL , SYMBOL_FARTHEST_PT, NEAREST_NEIGHBOR")
            sys.exit(-1)
            
    def get_shape_context_circle_radius(self, entire_radius, circle_index):
        '''
            For a given circle type compute the radius of the shape context 
        '''
        if(self.circle_type == "UNIFORM"):
            distance_units = entire_radius/self.CIRCLE_COUNT
            return distance_units*(circle_index+1)
        elif(self.circle_type == "LOG_POLAR"):
            return entire_radius/pow(2,self.CIRCLE_COUNT-1-circle_index)
        else:
            print("CIRCLE TYPE NOT WELL DEFINED : UNIFORM / LOG-POLAR")
            sys.exit(0 )

    def get_sample_points_delta(self, entire_radius,circle_index, sector_index):
        #find the angle of the current bin
        angle_unit = (2*math.pi)/self.SECTORS_COUNT
        if(self.samples_type == "BIN_CORNERS"):
            angle = (sector_index*angle_unit)
            return self.get_shape_context_circle_radius(entire_radius, circle_index), angle
        elif(self.samples_type == "BIN_CENTER"):
            prev_circle_radius = self.get_shape_context_circle_radius(entire_radius, circle_index-1)
            current_circle_radius = self.get_shape_context_circle_radius(entire_radius, circle_index)
            angle= angle_unit * sector_index +(angle_unit)/2.0
            return prev_circle_radius+(current_circle_radius-prev_circle_radius)/2.0, angle
        else:
            print("Error in sample points specification : Center or Bin Corner")
            sys.exit(0)

    def find_shape_context_sample_points(self, center, entire_radius):
        # Find the co-ordinates at which the sample points are located for each of the bin
        #these are the references around which parzen features are computed
        sample_points=np.zeros((2,self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        for i in range(self.SECTORS_COUNT):
            for j in range(self.CIRCLE_COUNT):
                #find the radius of the current bin
                sample_radius, angle = self.get_sample_points_delta(entire_radius, j, i)
                #find the sample_points to find parzen shape context features
                delta_x = sample_radius*math.cos(angle)
                delta_y = sample_radius*math.sin(angle)
                x_coordinate = center[0] + delta_x
                y_coordinate = center[1] + delta_y
                sample_points[0][i][j]=(x_coordinate)
                sample_points[1][i][j]=(y_coordinate)
        return sample_points

    def get_parzen_features(self, current_points, sample_points, radius):
        '''
            get the features for the current points, w.r.t sample points
            radius : factor for standard deviation of parzen density
            normalization method :
                How to Normalize the parzen features : Point by point or all points together 
        '''
        shape_context_features = np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        if (self.parzen_normalization_method == "POINT_BY_POINT"):
            # if normalization method is 1 : then normalize such that contribution from each point to every bin will sum to "1"
            contributing_points_count = 0
            for point in current_points:
                single_point = []
                single_point.append(point)
                single_point_SCF = np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
                # For every bin find the density contribution from the current point and normalize the density vector
                for i in range(self.SECTORS_COUNT):
                    for j in range(self.CIRCLE_COUNT):
                        # compute the density at the sample points because of the neighboring points
                        current_sample_point = [self.sample_points[0][i][j], self.sample_points[1][i][j]]
                        self_pd = self.get_parzen_density_value(current_sample_point, single_point,
                                                                                    radius)
                        # update the feature vector
                        single_point_SCF[i][j] = self_pd
                # Normalize the symbol features (if not null)
                single_point_density_normalization = single_point_SCF.sum()
                if single_point_density_normalization != 0:
                    contributing_points_count += 1
                    single_point_SCF /= single_point_density_normalization

                # sum up the contribution from every point
                shape_context_features += single_point_SCF
            # Finally Normalize by the count of contributing points
            if contributing_points_count > 0:
                shape_context_features /= contributing_points_count

        elif (self.parzen_normalization_method == "ENTIRE_POINTS"):
            for i in range(self.SECTORS_COUNT):
                for j in range(self.CIRCLE_COUNT):
                    # compute the density at the sample points because of the neighboring points
                    current_sample_point = [self.sample_points[0][i][j], self.sample_points[1][i][j]]
                    self_pd = self.get_parzen_density_value(current_sample_point, current_points,
                                                                                radius)
                    # update the feature vector
                    shape_context_features[i][j] = self_pd
                    # Normalize the symbol features (if not null)
            symbol_feature_normalization = shape_context_features.sum()
            if symbol_feature_normalization != 0:
                shape_context_features /= symbol_feature_normalization
        else:
            print("Normalization of points in parzen neither: POINT_BY_POINT, ENTIRE_POINTS")
            sys.exit(-1)
        return shape_context_features

    '''def get_shape_context_by_parzen(self, center, radius, current_points, other_points):
        symbol_SCF=np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        neighbor_SCF=np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        features=[]
        self.sample_points = self.find_shape_context_sample_points(center, radius)
        
        #Find the shape-context features of the current Symbol based on configuration
        if(self.required_features != 2):
            for i in range(self.SECTORS_COUNT):
                for j in range(self.CIRCLE_COUNT):
                    # compute the density at the sample points because of the neighboring points
                    current_sample_point=[self.sample_points[0][i][j],self.sample_points[1][i][j]]
                    self_pd = self.get_parzen_density_value(current_sample_point, current_points, radius)
                    # update the feature vector
                    symbol_SCF[i][j]=self_pd
            #Normalize the symbol featues (if not null)
            symbol_feature_normalization = symbol_SCF.sum()
            if symbol_feature_normalization != 0:
                symbol_SCF/=symbol_feature_normalization
       
        #Find the shape-context features of the neighboring points of the current symbol if context is needed
        if(self.required_features != 0):
            for i in range(self.SECTORS_COUNT):
                for j in range(self.CIRCLE_COUNT):
                    # compute the density at the sample points because of the neighboring points
                    current_sample_point=[self.sample_points[0][i][j],self.sample_points[1][i][j]]
                    other_pd = self.get_parzen_density_value(current_sample_point,other_points,radius)
                    neighbor_SCF[i][j]=(other_pd)
            neighbor_feature_normalization = neighbor_SCF.sum()
            if neighbor_feature_normalization != 0:
                neighbor_SCF/=neighbor_feature_normalization
            entire_feature = np.concatenate((symbol_SCF,neighbor_SCF), axis=0)
            features+=entire_feature.reshape((2*self.CIRCLE_COUNT * self.SECTORS_COUNT,)).tolist()
        else:
            entire_feature = symbol_SCF
            features+=entire_feature.reshape((self.CIRCLE_COUNT * self.SECTORS_COUNT,)).tolist()
        return features'''

    def get_shape_context_by_parsen(self, center, radius, current_points, other_points):
        symbol_SCF=np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        neighbor_SCF=np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT), np.float64)
        features=[]
        self.sample_points = self.find_shape_context_sample_points(center, radius)
        if(self.required_features != "CONTEXT_ONLY"):
            #Find the shape-context features of the current Symbol
            symbol_SCF += self.get_parzen_features(current_points, self.sample_points, radius)
            
        #Find the shape-context features of the neighboring points of the current symbol if context is needed
        if(self.required_features != "SYMBOL_ONLY"):
            neighbor_SCF += self.get_parzen_features(other_points, self.sample_points, radius)

        #Concatenate the symbol features and Neighbor features 
        entire_feature = np.concatenate((symbol_SCF,neighbor_SCF), axis=0)
        features+=entire_feature.reshape(entire_feature.size).tolist()
        
        return features
    
    def getDistance(self, pt1, pt2):
        x1, y1 = pt1
        x2, y2 = pt2
        dist = math.sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))
        return dist

    def get_shape_context_by_inverse_distance(self, center, entire_radius, symbol_point):
        features = []
        # CIRCLE_COUNT + 1 -> an extra row to represent the center of the circle
        bidimensional_hist = np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT + 1), np.float64)

        radial_distance = entire_radius / (self.CIRCLE_COUNT)
        angular_distance = 360.0 / (self.SECTORS_COUNT)
        # Vectors for radial distances and angular distances
        radii_vector = []
        for i in range(self.CIRCLE_COUNT+1):
            radii_vector.append(radial_distance*(i))
        angles_vector = []
        for i in range(self.SECTORS_COUNT+1):
            angles_vector.append(angular_distance*(i))
        # For each point calculate the features
        for x,y in symbol_point:
            hist = np.zeros((self.SECTORS_COUNT, self.CIRCLE_COUNT + 1), np.float64)
            #y = -y
            point = (x,y)
            dist_from_center = math.sqrt((x - center[0]) ** 2 + (y - center[1]) ** 2)

            # the neighboring points that are out-side the shape context circle don't consider
            if dist_from_center > entire_radius:
                print('Point outside the circle, ',point)
                continue

            angle_at_center = math.atan2((y - center[1]), (x - center[0]))
            angle_at_center = math.degrees(angle_at_center)
            # normalizing the angle with 360 degrees as atan2 returns angle between -pi and pi
            if angle_at_center < 0:
                angle_at_center += 360
            # get all the bins for the point
            min_r = math.floor(dist_from_center / radial_distance)
            min_a = math.floor(angle_at_center / angular_distance)%self.SECTORS_COUNT
            # If point is on the outermost circle
            if min_r == self.CIRCLE_COUNT:
                min_r -= 1
            max_r = min_r + 1
            max_a = (min_a + 1)%self.SECTORS_COUNT

            # Calculating the coordinates for all the corner points
            cornerPts = []
            cornerPts.append((radii_vector[max_r] * np.cos(np.radians(angles_vector[max_a])), radii_vector[max_r] * np.sin(np.radians(angles_vector[max_a]))))
            cornerPts.append((radii_vector[max_r] * np.cos(np.radians(angles_vector[min_a])), radii_vector[max_r] * np.sin(np.radians(angles_vector[min_a]))))
            # avoid calculating the fourth point, in case of the innermost circle
            if min_r != 0:
                cornerPts.append((radii_vector[min_r] * np.cos(np.radians(angles_vector[max_a])), radii_vector[min_r] * np.sin(np.radians(angles_vector[max_a]))))
                cornerPts.append((radii_vector[min_r] * np.cos(np.radians(angles_vector[min_a])), radii_vector[min_r] * np.sin(np.radians(angles_vector[min_a]))))
            else:
                cornerPts.append((center[0], center[1]))

            # Used to store the inverse of the distance, membership weight for the corner point
            distWeight = []
            for corner in cornerPts:
                dist = self.getDistance(point, corner)
                if dist == 0:
                    distWeight.append(1)
                else:
                    distWeight.append(1 / dist)

            N = sum(distWeight)


            hist[max_a][max_r] = distWeight[0]/N
            hist[min_a][max_r] = distWeight[1]/N
            hist[max_a][min_r] = distWeight[2]/N
            # avoid calculating the fourth point, in case of the innermost circle
            if min_r != 0:
                hist[min_a][min_r] = distWeight[3]/N

            bidimensional_hist += hist

            # Print details of all computations for the feature, for debug only
            if False:
                print('\n=====================================')
                print('Center(C):', center, '\nRadius of the outermost circle:', entire_radius)
                print('Point(P):', point)
                print('Distance between P and C:', dist_from_center)
                print('Angle made by P at C:', angle_at_center)
                print('Radial bin bounds:', min_r, ' and ', max_r)
                print('Angular bin bounds:', min_a, ' and ', max_a)
                print('Corner Points to be used: ')
                for z in cornerPts:
                    print('\t', z)
                print('Distance from all corner points: ')
                for z in distWeight:
                    print('\t', z)
                print('Normalization factor for distances= ', N)
                print('_______________')
                print('Membership values in histogram:')
                print(hist.T)
                print('_______________')
                center_row = hist[:, 0]
                center_count = sum(center_row)
                print('Membership value at the center:\n', center_count)
                print('_______________')
                rest_hist = hist[:, 1:]
                print('Membership value at the corner, without the center:\n', rest_hist.T)
                print('_______________\n\n')
                print(bidimensional_hist.T)
                print('_______________\n\n')
                break # cause print only for one point and stop

        # Normalize the histogram
        normalization_factor = bidimensional_hist.sum()
        if normalization_factor > 0.0:
            bidimensional_hist /= normalization_factor
        # Separate the center from the circles and add it to feature list
        center_row = bidimensional_hist[:, 0]
        center_feature = sum(center_row)
        features.append(center_feature)
        # Add rest of the corner points to the feature list
        rest_hist = bidimensional_hist[:, 1:]
        features += rest_hist.reshape(((self.CIRCLE_COUNT) * self.SECTORS_COUNT, )).tolist()
        # return the features
        return features

    def get_shape_context_features(self, symbol, expression):
        '''
            Runner method to compute the shape context features, based on configurations
            finds the symbol points and other points
        '''
        #define the bounding box
        regular_symbol = symbol.get_un_normalized_form()
        s_minX, s_maxX, s_minY, s_maxY = bounding_box = regular_symbol.minX, regular_symbol.maxX, regular_symbol.minY, regular_symbol.maxY
        #s_minX, s_maxX, s_minY, s_maxY = bounding_box = -1, 1, -1, 1
        #define center of the concentric circles to find bounding box
        s_centerX, s_centerY = s_minX + \
            (s_maxX - s_minX) / 2.0, s_minY + (s_maxY - s_minY) / 2.0
        center = [s_centerX, s_centerY]

        other_points = []
        current_points = []

        regular_symbol= symbol.get_un_normalized_form()
        for trace in regular_symbol.traces:
                current_points += trace.points
        # update the other points from the other symbols in the expression
        for symbol_i in expression:
            if symbol_i != symbol:
                regular_symbol_i= symbol_i.get_un_normalized_form()
                for trace in regular_symbol_i.traces:
                    other_points += trace.points
        # find the radius of outer circle in Shape context feature
        distance = self.get_shape_context_distance(center, bounding_box, current_points, other_points)
        radius = self.RADIUS_RESTRICTION_FACTOR * distance
        # Count the Points or compute by parzen
        if (self.extraction_type == "INVERSE_DISTANCE"):
            f = self.get_shape_context_by_inverse_distance(center, radius, current_points)
        elif (self.extraction_type == "PARZEN"):
            f = self.get_shape_context_by_parsen(center, radius, current_points, other_points)
        elif (self.extraction_type == "COUNT_POINTS"):
            f = self.get_shape_context_by_count(center, radius, current_points, other_points)
        else:
            print("METHOD OF EXTRACTING FEATURES should be either: INVERSE_DISTANCE, PARZEN, COUNT_POINTS")
            sys.exit(1)
        
        #if debugger is on, visualize the features
        if self.debug_mode == True:
            #find sample points on other feature computation methods
            self.sample_points = self.find_shape_context_sample_points(center, radius)
            if(self.extraction_type != "INVERSE_DISTANCE"):
                self.plot_sample_point_density(str(symbol.id), center, radius, current_points,f[:self.CIRCLE_COUNT*self.SECTORS_COUNT])
                self.plot_sample_point_density(str(symbol.id)+"_Neighbor", center, radius, other_points,f[self.CIRCLE_COUNT*self.SECTORS_COUNT:2*self.CIRCLE_COUNT*self.SECTORS_COUNT])
                self.visualize_bin_density(str(symbol.id), center, radius, current_points, f[:self.CIRCLE_COUNT*self.SECTORS_COUNT])
                self.visualize_bin_density(str(symbol.id)+"_Neighbor",center, radius,current_points,f[self.CIRCLE_COUNT*self.SECTORS_COUNT:2*self.CIRCLE_COUNT*self.SECTORS_COUNT])
                self.visualize_bin_density_contributors(str(symbol.id), self.sample_points,current_points, center,radius)
            else:
                self.plot_sample_point_density(str(symbol.id), center, radius, current_points,f)

        return f

    def plotSectors(self, center, radius):
        #draw the sector lines in shape context circles for reference
        angle_unit = (2*math.pi)/self.SECTORS_COUNT
        for i in range(0, self.SECTORS_COUNT):
            angle = angle_unit*i
            x = [center[0], center[0] + radius *
                 math.cos(angle)]
            y = [center[1], center[1] + radius *
                 math.sin(angle)]
            plt.plot(x, y, 'k-')

    def plot_shape_context_density_function(self,equation, filename, shape_context_radius, self_center):
        '''
            Visualize the normal distribution curve used to estimate the parzen density
        '''
        self_diag =shape_context_radius
        point_num_per_row = 40
        distance_unit = (self_diag*2)/point_num_per_row
        X = np.arange(self_center[0]-self_diag, self_center[0]+self_diag, distance_unit)
        Y = np.arange(self_center[1]-self_diag, self_center[1]+self_diag, distance_unit)
        X, Y = np.meshgrid(X, Y)

        Z1 = [[0.0 for x in iter(range(X.shape[1]))] for x in iter(range(X.shape[0]))]
        Z1 = np.array(Z1)

        ## print "self_center", self_center
        test_points=[]
        for i in range(X.shape[0]):
                for j in range(X.shape[1]):
                        current_point = [X[i][j], Y[i][j]]
                        test_points=[]
                        test_points.append(current_point)
                        Z1[i][j] = self.get_parzen_density_value(self_center, test_points, self_diag)

        fig = plt.figure()
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(X, Y, Z1, rstride=1, cstride=1, cmap=cm.coolwarm,linewidth=0, antialiased=False)
        fig.colorbar(surf, shrink=0.5, aspect=5)
        plt.savefig(plot_path+filename+'_ParzenShapeContext.png')
            

    def plot_sample_point_density(self, filename,self_center, shape_context_radius, current_points,current_density):
        '''
            Plot the points in the symbol and the neighboring points
            Scatter the sample points considered colored equivalent to it's density
        '''
        fig = plt.gcf()
        #draw circles
        self.draw_shape_context_circles(self_center, shape_context_radius)
        #draw Sectors
        self.plotSectors(self_center, shape_context_radius)
        #plot current points
        fig = plt.gcf()
        varRadius = shape_context_radius*self.VARIANCE_FACTOR
        for point in current_points:
            plt.plot(point[0], point[1], 'k.')
            #draw the parzen circles around the symbol points
            if (self.extraction_type == "PARZEN"):
                parzenCircle = plt.Circle((point[0], point[1]), varRadius, fill=False, color='y')
                fig.gca().add_artist(parzenCircle)
        plt.gca().invert_yaxis()
        
        #if it's inverse distance plot the center point also
        if(self.extraction_type == "INVERSE_DISTANCE"):
            plt.scatter(self_center[0], self_center[1], c=current_density[:1], s=200, marker="o",cmap=cm.Reds)
            current_density = current_density[1:]
        #plot the sample points
        plt.scatter(self.sample_points[0], self.sample_points[1], c=current_density, s=200, marker="o",cmap=cm.Reds)
        plt.colorbar()
        plt.scatter(self_center[0], self_center[1], c=current_density[:1])
        plt.savefig(plot_path+filename+"_SYMBOLS_N_SCC.png")
        plt.clf()

    def draw_shape_context_circles(self, center, shape_context_radius):
        #plot the shape context circles for reference
        fig = plt.gcf()
        for i in range(self.CIRCLE_COUNT):
            radius = self.get_shape_context_circle_radius(shape_context_radius, i)
            circleFig_i = plt.Circle((center[0], center[1]), radius, fill=False, color='k')
            fig.gca().add_artist(circleFig_i)

    def visualize_bin_density_contributors(self, file_name, sample_points, current_points, center, shape_context_radius):
        '''
         Plot the contribution to every bin from every point in the symbol
        '''
        self.draw_shape_context_circles(center, shape_context_radius)
        for i in range(self.SECTORS_COUNT):
            for j in range(self.CIRCLE_COUNT):
                # BIN -X find how each point contributes
                current_sample_point=[self.sample_points[0][i][j],self.sample_points[1][i][j]]
                #plt.plot(current_sample_point[0], current_sample_point[1], 'ko')
                X_Bins=[]
                Y_Bins=[]
                intensity=[]
                self.draw_shape_context_circles(center, shape_context_radius)
                self.plotSectors(center, shape_context_radius)
                plt.scatter(current_sample_point[0], current_sample_point[1], s=500, marker="o",cmap=cm.Blues)
                for pt_i in current_points:
                    #find intensity pt_i contributes the current sample_point
                    pt_i_list=[]
                    pt_i_list.append(pt_i)
                    X_Bins.append(pt_i[0])
                    Y_Bins.append(pt_i[1])
                    intensity.append(self.get_parzen_density_value(current_sample_point, pt_i_list, shape_context_radius))
                plt.scatter(X_Bins, Y_Bins, c=intensity, s=50, marker="o",cmap=cm.Reds)
                plt.gca().invert_yaxis()
                plt.colorbar()
                plt.savefig(plot_path+file_name+str(i)+str(j)+"_CONTRIBUTORS_BIN.png")
                plt.clf()

    def visualize_bin_density(self, filename,self_center, shape_context_radius,current_points,density, cmap_val=cm.Greys):
        '''
            The visual plot of the shape context feature vector in the circle
            The color of each bin of the
        '''
        theta = np.repeat((np.linspace( 0.0, 2*np.pi,self.SECTORS_COUNT, endpoint=False)), self.CIRCLE_COUNT)
        radii=[]
        for point in current_points:
            plt.plot(point[0], point[1], 'k.')

        for i in range(self.SECTORS_COUNT):
            for j in range(self.CIRCLE_COUNT):
                radii.append(self.get_shape_context_circle_radius(shape_context_radius, self.CIRCLE_COUNT-1-j))
        width = 2*np.pi / self.SECTORS_COUNT
        ax = plt.subplot(111, projection='polar')
        #remove grids and axis
        ax.grid(False)
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        ax.spines['polar'].set_visible(False)
        bars = ax.bar(theta, radii, width=width, bottom=0.0)
        # Use custom colors and opacity
        rotated_density= [0.0 for i in range(self.SECTORS_COUNT*self.CIRCLE_COUNT)]

        for i in range(self.SECTORS_COUNT):
            for j in range(self.CIRCLE_COUNT):
                    sector_index = (self.SECTORS_COUNT-i)%self.SECTORS_COUNT
                    sector_index = (sector_index-1)%self.SECTORS_COUNT
                    rotated_density[sector_index*self.CIRCLE_COUNT+j] = density[i*self.CIRCLE_COUNT+(self.CIRCLE_COUNT-1-j)]
        #normalize value in[0,1]
        maxd=max(rotated_density)
        rotated_density=np.array(rotated_density)/maxd
        for r, bar, color_code in zip(radii, bars,rotated_density):
            bar.set_facecolor(cm.Reds(color_code))
            bar.set_alpha(1.0)
            bar.set_edgecolor('black')
            if (color_code) <= 3e-10:
                bar.set_hatch('xxxxx')

        max_r = max(radii)
        for angle in set(theta):
            plt.annotate(math.ceil(math.degrees(angle)), xy=(angle, max_r), xytext=(angle, max_r+0.2), horizontalalignment='right', verticalalignment='top')
        plt.savefig(plot_path+filename+"COLORED_BINS.png")
        plt.clf()

    def get_hatch(self, intensity):
        n = int(intensity*100)
        pattern = ''
        for i in range(n):
            pattern += '/'
        return pattern

    def plot_symbol_boxes(self, symbol):
        #print('_______________________________________________________________')
        #s_minX, s_maxX, s_minY, s_maxY = symbol.original_box
        s_minX, s_maxX, s_minY, s_maxY = -1,1,-1,1
        s_centerX, s_centerY = s_minX + \
            (s_maxX - s_minX) / 2.0, s_minY + (s_maxY - s_minY) / 2.0
        boundingBox = plt.Rectangle((s_minX, -s_maxY),(s_maxX - s_minX),(s_maxY - s_minY), linestyle="--", facecolor="none")
        plt.gca().add_patch(boundingBox)
        for trace in symbol.traces:
            for x,y in trace.points:
                plt.plot(x, -y, 'b.')
        plt.plot(s_centerX, -s_centerY, 'ro')
        max_r = self.find_max_radius((s_centerX, s_centerY), symbol)
        circle = plt.Circle((s_centerX, -s_centerY), max_r, fill=False, color='g')
        plt.gca().add_patch(circle)
        plt.axis('scaled')
        plt.grid()
        plt.show()

