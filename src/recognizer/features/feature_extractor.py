
#=====================================================================
#  This class extracts features from expression components.
#
#
#  Created by:
#      - Kenny Davila (May, 2016)
#  Modified by:
#      - Michael Condon
#      - Lakshmi Ravi
#=====================================================================
import math
import sys
import numpy as np
import pickle

from concurrent.futures import ProcessPoolExecutor
from recognizer.data.code_book import CodeBook
from recognizer.features.crossings_feature_extractor import CrossingsFeatureExtractor
from recognizer.features.histograms_feature_extractor import HistogramFeatureExtractor
from recognizer.features.trace_feature_extractor import TraceFeatureExtractor
from recognizer.features.code_book_feature_extractor import CodeBookFeatureExtractor
from recognizer.features.temporal_feature_extractor import TemporalFeatureExtractor
from recognizer.features.context_feature_extractor import ContextFeatureExtractor
from recognizer.features.shape_context_feature_extractor import ShapeContextFeatureExtractor
from recognizer.features.geometric_feature_extractor import GeometricFeatureExtractor
import warnings

class FeatureExtractor:

    def __init__(self, configuration):
        self.config = configuration

        if self.config.get_bool("FEATURES_USE_TEMPORAL_FEATURES"):
            self.temporal_feature_extractor = TemporalFeatureExtractor(configuration)

        else:
            self.temporal_feature_extractor = None

        if self.config.get_bool("FEATURES_USE_CODE_BOOK_HISTOGRAMS"):
            # Create Code-book feature extractor
            self.cb_feature_extractor = CodeBookFeatureExtractor.create_from_config(configuration)
            self.code_book = CodeBook.load_from_file(configuration)
        else:
            self.cb_feature_extractor = None
            self.code_book = None

    def getCrossingsFeatures(self, symbol):
        features = []
        n_crossings = self.config.get_int("CROSSINGS_NUMBER", 5)
        n_subcrossings = self.config.get_int("CROSSINGS_SUBCROSSINGS", 5)
        use_overlapping = self.config.get_bool("CROSSINGS_OVERLAPPING", False)
        sub_weights = self.config.get_int("SUBCROSSINGS_WEIGHTS", 0)
        sub_sigma = self.config.get_float("SUBCROSSINGS_GAUSS_SIGMA", 1.0)

        # compute crossings ...
        all_crossings = CrossingsFeatureExtractor.getHorizontalVerticalCrossings(symbol, n_crossings, n_subcrossings,
                                                                                 sub_weights, sub_sigma, use_overlapping)

        if self.config.get_bool("CROSSINGS_USE_COUNT"):
            features += all_crossings["counts"]
        if self.config.get_bool("CROSSINGS_USE_MIN"):
            features += all_crossings["mins"]
        if self.config.get_bool("CROSSINGS_USE_MAX"):
            features += all_crossings["maxs"]

        return features

    def getShapeContextFeatures(self, symbol, formula):
        globalContext = self.config.get_str("CONTEXTUAL_USE_GLOBAL_CONTEXT","SYMBOL_ONLY")
        method_type = self.config.get_str("CONTEXTUAL_METHOD_TYPE","COUNT_POINTS")
        
        radius_restrict_factor = self.config.get_float("CONTEXTUAL_RADIUS_FACTOR", 1.0)
        sectors_count = self.config.get_int("CONTEXTUAL_SECTORS_COUNT")
        circle_count = self.config.get_int("CONTEXTUAL_CIRCLE_COUNT")
        shape_context_standard_deviation_factor = self.config.get_float("CONTEXTUAL_STD_DEV_FACTOR", 10.0)
        
        samples_type = self.config.get_str("CONTEXTUAL_SAMPLE_TYPE", "BIN_CENTER")
        scc_circle_type = self.config.get_str("CONTEXTUAL_CIRCLE_TYPE","UNIFORM")
        scc_radius_method = self.config.get_str("CONTEXTUAL_RADIUS_METHOD","SYMBOL_FARTHEST_PT")
        
        Normalization_method = self.config.get_str("CONTEXTUAL_NORMALIZATION_METHOD", "ENTIRE_POINTS")
        intensity_type = self.config.get_int("CONTEXTUAL_INTENSITY_TYPE")
        debug_mode = self.config.get_bool("CONTEXTUAL_DEBUG_MODE")
        context_features = ContextFeatureExtractor(radius_restrict_factor, sectors_count, circle_count,shape_context_standard_deviation_factor, method_type, samples_type, scc_circle_type, globalContext, scc_radius_method, intensity_type, Normalization_method, debug_mode)

        return context_features.get_shape_context_features(symbol, formula)

    def getDiagonalCrossingsFeatures(self, symbol):
        features = []
        n_crossings = self.config.get_int("DIAGONAL_CROSSINGS_NUMBER", 5)
        n_subcrossings = self.config.get_int(
            "DIAGONAL_CROSSINGS_SUBCROSSINGS", 5)
        use_overlapping = self.config.get_bool(
            "DIAGONAL_CROSSINGS_OVERLAPPING", False)
        sub_weights = self.config.get_int("DIAGONAL_SUBCROSSINGS_WEIGHTS", 0)
        sub_sigma = self.config.get_float(
            "DIAGONAL_SUBCROSSINGS_GAUSS_SIGMA", 1.0)

        all_crossings = CrossingsFeatureExtractor.getDiagonalCrossings(symbol, n_crossings, n_subcrossings,
                                                                       sub_weights, sub_sigma, use_overlapping)

        if self.config.get_bool("DIAGONAL_CROSSINGS_USE_COUNT"):
            features += all_crossings["counts"]
        if self.config.get_bool("DIAGONAL_CROSSINGS_USE_MIN"):
            features += all_crossings["mins"]
        if self.config.get_bool("DIAGONAL_CROSSINGS_USE_MAX"):
            features += all_crossings["maxs"]

        return features

    def getAngularCrossingFeatures(self, symbol):
        n_angular = self.config.get_int("ANGULAR_CROSSINGS_NUMBER", 4)
        n_subcrossings = self.config.get_int("ANGULAR_CROSSINGS_SUBCROSSINGS", 1)
        sub_weights = self.config.get_int("ANGULAR_SUBCROSSINGS_WEIGHTS", 0)
        sub_sigma = self.config.get_float("ANGULAR_SUBCROSSINGS_GAUSS_SIGMA", 1.0)


        all_crossings = CrossingsFeatureExtractor.getAngularCrossings(symbol, n_angular, n_subcrossings,
                                                                      sub_weights, sub_sigma)

        features = []
        if self.config.get_bool("ANGULAR_CROSSINGS_USE_COUNT"):
            features += all_crossings["counts"]
        if self.config.get_bool("ANGULAR_CROSSINGS_USE_MIN"):
            features += all_crossings["mins"]
        if self.config.get_bool("ANGULAR_CROSSINGS_USE_MAX"):
            features += all_crossings["maxs"]

        return features

    def getZoningCrossingsFeatures(self, symbol):
        grids = self.config.get("ZONING_CROSSINGS_GRIDS")

        n_hor_lines = self.config.get_int("ZONING_CROSSINGS_HOR_LINES", -1)
        n_ver_lines = self.config.get_int("ZONING_CROSSINGS_VER_LINES", -1)
        n_dia_lines = self.config.get_int("ZONING_CROSSINGS_DIA_LINES", -1)
        weight_function = self.config.get_int("ZONING_CROSSINGS_WEIGHTS", -1)

        weight_sigma = self.config.get_float("ZONING_CROSSINGS_GAUSS_SIGMA", 1.0)


        if n_hor_lines < 1:
            raise Exception("Invalid value for <ZONING_CROSSINGS_HOR_LINES>")

        if n_ver_lines < 0:
            raise Exception("Invalid value for <ZONING_CROSSINGS_VER_LINES>")
        elif n_ver_lines == 0:
            # use the same number of vertical lines as horizontal lines
            n_ver_lines = n_hor_lines

        if n_dia_lines < 0:
            raise Exception("Invalid value for <ZONING_CROSSINGS_DIA_LINES>")
        elif n_dia_lines == 0:
            n_dia_lines = n_hor_lines

        if weight_function < 0:
            raise Exception("Invalid value for <ZONING_CROSSINGS_WEIGHTS>")

        return CrossingsFeatureExtractor.getZoningCrossings(symbol, n_hor_lines, n_ver_lines, n_dia_lines,
                                                            weight_function, weight_sigma, grids)

    def getSymbol2DHistograms(self, symbol, grid_name="2D_HISTOGRAMS_GRIDS"):
        grids = self.config.get(grid_name)
        plot_path = self.config.get("PLOT_PATH")
        variance_factor = self.config.get("PARZEN_STD_DEV_FACTOR", None)

        return HistogramFeatureExtractor.computeSymbol2DHistogram(symbol, grids, plot_path, variance_factor)

    def getOrientationHistograms(self, symbol):
        grids = self.config.get("ORIENTATION_HISTOGRAMS_GRIDS")

        return HistogramFeatureExtractor.computeSymbolOrientationHistogram(symbol, grids)

    def getRelation2DHistograms(self, parent, child, expression, grid_name="RELATION_2D_HISTOGRAMS_GRIDS"):
        grids = self.config.get(grid_name)
        plot_path = self.config.get("PLOT_PATH")
        variance_factor = self.config.get("PARZEN_STD_DEV_FACTOR", None)

        return HistogramFeatureExtractor.computeRelation2DHistogram(parent, child, expression, grids, plot_path, variance_factor)

    def getMSTContext2DHistograms(self, expression, symbols, grid_name="MST_CONTEXT_2D_HISTOGRAMS_GRIDS"):
        grids = self.config.get(grid_name)
        plot_path = self.config.get("PLOT_PATH")
        variance_factor = self.config.get("PARZEN_STD_DEV_FACTOR", None)
        relation_types = {}
        for line in open(self.config.get_str("MST_RELATION_TYPES_FILE")):
            parts = line.strip().split()
            relation_types[parts[0]] = parts[1]

        return HistogramFeatureExtractor.computeMSTContext2DHistogram(expression, symbols, grids, relation_types, plot_path, variance_factor)

    def getTraceDescriptionFeatures(self, symbol):
        features = []
        if self.config.get_bool("TRACES_DESCRIPTION_USE_COUNT"):
            features += TraceFeatureExtractor.getTracesCount(symbol)

        if self.config.get_bool("TRACES_DESCRIPTION_USE_ASPECT_RATIO"):
            features += TraceFeatureExtractor.getAspectRatio(symbol)

        # Angular Change
        use_angular_change_total = self.config.get_bool("TRACES_DESCRIPTION_USE_TOTAL_ANGULAR_CHANGE")
        use_angular_change_average = self.config.get_bool("TRACES_DESCRIPTION_USE_AVERAGE_ANGULAR_CHANGE")
        if use_angular_change_total or use_angular_change_average:
            ang_change_total, ang_change_avg = TraceFeatureExtractor.getAngularChange(symbol)

            if use_angular_change_total:
                features.append(ang_change_total)

            if use_angular_change_average:
                features.append(ang_change_avg)

        # Total Line Length (After smoothing and normalization)
        use_line_length_total = self.config.get_bool("TRACES_DESCRIPTION_USE_TOTAL_LINE_LENGTH")
        use_line_length_average = self.config.get_bool("TRACES_DESCRIPTION_USE_AVERAGE_LINE_LENGTH")
        if use_line_length_total or use_line_length_average:
            line_length_total, line_length_avg = TraceFeatureExtractor.getLineLength(symbol)

            if use_line_length_total:
                features.append(line_length_total)

            if use_line_length_average:
                features.append(line_length_avg)

        # Count of Sharp Points

        use_sharp_points_total = self.config.get_bool("TRACES_DESCRIPTION_USE_TOTAL_SHARP_POINTS")
        use_sharp_points_average = self.config.get_bool("TRACES_DESCRIPTION_USE_AVERAGE_SHARP_POINTS")
        if use_sharp_points_total or use_sharp_points_average:
            sharp_points_total, sharp_points_avg = TraceFeatureExtractor.getNumberOfSharpPoints(symbol)

            if use_sharp_points_total:
                features.append(sharp_points_total)

            if use_sharp_points_average:
                features.append(sharp_points_avg)

        use_var_x = self.config.get_bool("TRACES_DESCRIPTION_USE_VARIANCE_X")
        use_var_y = self.config.get_bool("TRACES_DESCRIPTION_USE_VARIANCE_Y")

        use_covar_xy = self.config.get_bool("TRACES_DESCRIPTION_USE_COVARIANCE_XY")
        if use_var_x or use_var_y or use_covar_xy:
            var_x, var_y, cov_xy = TraceFeatureExtractor.getVarianceFeatures(symbol)


            if use_var_x:
                features.append(var_x)

            if use_var_y:
                features.append(var_y)

            if use_covar_xy:
                features.append(cov_xy)

        use_mid_point_var_x = self.config.get_bool("TRACES_DESCRIPTION_USE_MID_POINT_VARIANCE_X")
        use_mid_point_var_y = self.config.get_bool("TRACES_DESCRIPTION_USE_MID_POINT_VARIANCE_Y")
        use_mid_point_covar_xy = self.config.get_bool("TRACES_DESCRIPTION_USE_MID_POINT_COVARIANCE_XY")
        if use_mid_point_var_x or use_mid_point_var_y or use_mid_point_covar_xy:
            mp_var_x, mp_var_y, mp_cov_xy = TraceFeatureExtractor.getMidPointVarianceFeatures(symbol)


            if use_mid_point_var_x:
                features.append(mp_var_x)

            if use_mid_point_var_y:
                features.append(mp_var_y)

            if use_mid_point_covar_xy:
                features.append(mp_cov_xy)

        if self.config.get_bool("TRACES_DESCRIPTION_TOTAL_ORIGINAL_POINTS"):
            features.append(TraceFeatureExtractor.getTotalOriginalPoints(symbol))


        return features

    def getCodeBookFeatures(self, symbol):
        all_features, all_boxes = self.cb_feature_extractor.getFeatures(symbol)
        features = self.cb_feature_extractor.getSymbolEncoding(all_boxes, all_features, self.code_book)

        return features.tolist()

    def getGeometricFeatures(self, expression, pid, cid):
        geometricFeatureExtractor = GeometricFeatureExtractor
        features = []
        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", message="divide by zero encountered in true_divide")
            warnings.filterwarnings("ignore", message="divide by zero encountered in double_scalars")
            warnings.filterwarnings("ignore", message="invalid value encountered in true_divide")
            warnings.filterwarnings("ignore", message="invalid value encountered in double_scalars")

            parent_composite_trace = expression.getCompositeTrace(pid, orig=True)
            child_composite_trace = expression.getCompositeTrace(cid, orig=True)

            parent_box = np.array([np.min(parent_composite_trace[:,0]), np.min(parent_composite_trace[:,1]), np.max(parent_composite_trace[:,0]), np.max(parent_composite_trace[:,1])])
            child_box = np.array([np.min(child_composite_trace[:,0]), np.min(child_composite_trace[:,1]), np.max(child_composite_trace[:,0]), np.max(child_composite_trace[:,1])])

            filter = self.config.get_bool("LEI_HU_FILTER")

            if self.config.get_bool("BOUNDING_BOX_DISTANCES"):
                raw_features = geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, ratio=False)
                if filter:
                    features += []
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_DISTANCES_RATIOS"):
                raw_features = geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, ratio=True)
                if filter:
                    features += raw_features[8:11]
                    features += geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, ratio=False)[8:10]
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_DISTANCES_CENTER_NORMALIZATION"):
                raw_features = geometricFeatureExtractor.getBoundingBoxDistances(parent_box, child_box, center_distance_norm=True)
                if filter:
                    features += raw_features[0:2]
                    features += raw_features[3:6]
                    features += raw_features[7:10]
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_OVERLAPS"):
                raw_features = geometricFeatureExtractor.getBoundingBoxOverlaps(parent_box, child_box)
                if filter:
                    features += raw_features[0:3]
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_SIZES"):
                raw_features = geometricFeatureExtractor.getBoundingBoxSizes(parent_box, child_box)
                if filter:
                    features += raw_features[2:3]
                    features += list(np.abs(raw_features[2:3]))
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_RATIOS"):
                raw_features = geometricFeatureExtractor.getBoundingBoxRatios(parent_box, child_box)
                if filter:
                    features += []
                else:
                    features += raw_features

            if self.config.get_bool("BOUNDING_BOX_AREAS"):
                raw_features = geometricFeatureExtractor.getBoundingBoxAreas(parent_box, child_box)
                if filter:
                    features += []
                else:
                    features += raw_features

            if self.config.get_bool("POINT_DISTANCES"):
                raw_features = geometricFeatureExtractor.getPointDistances(parent_composite_trace, child_composite_trace)
                if filter:
                    features += raw_features[0:2]
                else:
                    features += raw_features

            if self.config.get_bool("START_END_POINT_DISTANCES"):
                raw_features = geometricFeatureExtractor.getStartEndPointDistances(parent_composite_trace, child_composite_trace)
                if filter:
                    features += raw_features[0:6]
                    features += raw_features[9:12]
                else:
                    features += raw_features

            if self.config.get_bool("MASS_CENTERS_DISTANCES"):
                raw_features = geometricFeatureExtractor.getMassCenterDistances(parent_composite_trace, child_composite_trace)
                if filter:
                    features += raw_features[0:3]
                    features += list(np.abs(raw_features[0:2]))
                else:
                    features += raw_features

            if self.config.get_bool("TRACE_ANGLES"):
                raw_features = geometricFeatureExtractor.getAngles(parent_composite_trace, child_composite_trace)
                if filter:
                    features += raw_features[0:3]
                else:
                    features += raw_features

            if filter:
                features += geometricFeatureExtractor.getMisc(parent_box, child_box)

        numpy_features = np.array(features)
        if np.isfinite(numpy_features).sum() != numpy_features.size:
            print("ALL FEATURES ARE NOT FINITE", np.isfinite(numpy_features).sum(), numpy_features.size, numpy_features)

        return features

    def getTimeOrderFeatures(self, expression, pid, cid):
        features = []
        tempFeatureExtractor = TemporalFeatureExtractor(self.config)
        # Time Gap Feature
        if self.config.get_bool("TIME_GAP") :
           features += tempFeatureExtractor.getRelationFeatures(expression, expression.componentMap[pid], expression.componentMap[cid])

        return features

    def getSymbolFeatures(self, symbol):
        features = []

        # 1) first, crossings...
        if self.config.get_bool("FEATURES_USE_CROSSINGS"):
            features += self.getCrossingsFeatures(symbol)

        # 1.1) Diagonal crossings...
        if self.config.get_bool("FEATURES_USE_DIAGONAL_CROSSINGS"):
            features += self.getDiagonalCrossingsFeatures(symbol)

        # 1.5) Angular crossings ...
        if self.config.get_bool("FEATURES_USE_ANGULAR_CROSSINGS"):
            features += self.getAngularCrossingFeatures(symbol)

        # 1.9) Zoning Crossings ...
        if self.config.get_bool("FEATURES_USE_ZONING_CROSSINGS"):
            features += self.getZoningCrossingsFeatures(symbol)

        # 2) 2D Histograms ...
        if self.config.get_bool("FEATURES_USE_2D_HISTOGRAMS"):
            features += self.getSymbol2DHistograms(symbol)

        # 3) Orientation Histograms ...
        if self.config.get_bool("FEATURES_USE_ORIENTATION_HISTOGRAMS"):
            features += self.getOrientationHistograms(symbol)

        # 4) General Features
        # 4.1) Number of traces ...
        if self.config.get_bool("FEATURES_USE_TRACES_DESCRIPTION"):
            features += self.getTraceDescriptionFeatures(symbol)

        # 5) Code-Book based features
        if self.config.get_bool("FEATURES_USE_CODE_BOOK_HISTOGRAMS"):
            features += self.getCodeBookFeatures(symbol)

        # 6) features based on temporal info
        if self.config.get_bool("FEATURES_USE_TEMPORAL_FEATURES"):
            features += self.temporal_feature_extractor.getFeatures(symbol)

        # 7) Shape context features for the symbol
        if self.config.get_bool("FEATURES_USE_SYMBOL_SHAPE_CONTEXT_FEATURES"):
            #configuration to get old shape context features
            shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            features += shapeContextFeatureExtractor.get_shape_context_features(symbol, expression=symbol.get_expression(), use_expression=False)

        # 8) Shape context features for the symbol and the rest of the expression
        if self.config.get_bool("FEATURES_USE_EXPRESSION_SHAPE_CONTEXT_FEATURES"):
            shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            features += shapeContextFeatureExtractor.get_shape_context_features(symbol, expression=symbol.get_expression(), use_expression=True)

        if len(features) == 0:
            # for debugging only, will prefer majority class
            features.append(0.0)
        else:
            features = [f if math.isfinite(f) else 0.0 for f in features]

        return features


    def getRelationFeatures(self, expression, pid, cid):
        features =[]
        parent = expression.componentMap[pid]
        child = expression.componentMap[cid]

        if expression.edgeTraces:
            for edge in expression.edgeTraces:
                edge.setTrace()


        if not expression.normalized:
            expression.setUnnormalized()

        if self.config.get_bool("FEATURES_USE_GEOMETRIC_FEATURES"):
            features += self.getGeometricFeatures(expression, pid, cid)

	
        if self.config.get_bool("FEATURES_USE_SYMBOL_SHAPE_CONTEXT_FEATURES"):
            shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            #get the shape context features from the parent and child strokes
            features += shapeContextFeatureExtractor.get_shape_context_features(expression.componentMap[pid],expression.componentMap[cid],expression,use_expression=False)
            if self.config.get_bool("INCLUDE_PARENT_SHAPE_CONTEXT_FEATURES"):
                features += shapeContextFeatureExtractor.get_shape_context_features(expression.componentMap[pid],expression=expression, use_expression=False)
            if self.config.get_bool("INCLUDE_CHILD_SHAPE_CONTEXT_FEATURES"):
                features += shapeContextFeatureExtractor.get_shape_context_features(expression.componentMap[cid], expression=expression, use_expression=False)
	# shape context with syntax context
        if self.config.get_bool("FEATURES_USE_EXPRESSION_SHAPE_CONTEXT_FEATURES"):
            shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            bound = self.config.get_bool("BOUND_SHAPE_CONTEXT_FEATURES", False)
            features += shapeContextFeatureExtractor.get_shape_context_features(expression.componentMap[pid],expression.componentMap[cid],expression,use_expression=True, bound=bound)
            features += shapeContextFeatureExtractor.syntax_context(expression.componentMap[pid],expression.componentMap[cid],expression,use_expression=True, bound=bound)

        if self.config.get_bool("FEATURES_USE_PARENT_2D_HISTOGRAMS"):
            features += self.getSymbol2DHistograms(parent, "PARENT_2D_HISTOGRAMS_GRIDS")

        if self.config.get_bool("FEATURES_USE_CHILD_2D_HISTOGRAMS"):
            features += self.getSymbol2DHistograms(child, "CHILD_2D_HISTOGRAMS_GRIDS")

	#2d hist with syntaxr context
        if self.config.get_bool("FEATURES_USE_RELATION_2D_HISTOGRAMS"):
           # shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            features += self.getRelation2DHistograms(parent, child, expression, "RELATION_2D_HISTOGRAMS_GRIDS")
            
        if self.config.get_bool("SYNTAX_CONTEXT_FEATURES"):
            shapeContextFeatureExtractor = ShapeContextFeatureExtractor(self.config)
            bound = True
            features += shapeContextFeatureExtractor.syntax_context(expression.componentMap[pid],expression.componentMap[cid],expression,use_expression=True, bound=bound)

        if self.config.get_bool("FEATURES_USE_MST_CONTEXT_2D_HISTOGRAMS"):
            features += self.getMSTContext2DHistograms(expression, [parent,child], "MST_CONTEXT_2D_HISTOGRAMS_GRIDS")

        if self.config.get_bool("FEATURES_USE_TIME_ORDER_FEATURES"):
            features += self.getTimeOrderFeatures(expression, pid, cid)

        if self.config.get_bool("FEATURES_USE_ANGULAR_BLOCK_FEATURES"):
            features += GraphFeatureExtractor.get_angular_range_blocked(expression, pid, cid)

        if len(features) == 0:
            # for debugging only, will prefer majority class
            features.append(0.0)

        if expression.edgeTraces:
            for edge in expression.edgeTraces:
                edge.trace = None

        expression.unnormalizedComponents = None

        return features

    def getExpressionSymbolFeatures(self, expression):
        feature_sources = []
        raw_feature_vector = []
        if not expression.normalized:
            expression.setUnnormalized()
        nodes = expression.expressionGraph.nodes()
        for compId in nodes:
            feature_sources.append([(expression.id, compId)])
            comp = expression.componentMap[compId]
            raw_feature_vector.append(self.getSymbolFeatures(comp))
        expression.unnormalizedComponents = None
        return feature_sources, raw_feature_vector

    def getExpressionRelationFeatures(self, expression):
        feature_sources = []
        raw_feature_vector=[]
        labels=[]

        if expression.normalized:
            expression.setUnnormalized()
        if expression.edgeTraces:
            for key,edge in expression.edgeTraces.items():
                edge.setTrace()
                expression.edgeTraces[key] = edge
        for parent,child in expression.expressionGraph.edges():
            feature_sources.append([(expression.id,parent,child)])
            raw_feature_vector.append(self.getRelationFeatures(expression, parent, child))
        expression.unnormalizedComponents = None
        if expression.edgeTraces:
            for key,edge in expression.edgeTraces.items():
                edge.trace = None
                expression.edgeTraces[key] = edge
        return (feature_sources, raw_feature_vector)


    @staticmethod
    def getSymbolsFeatures(symbols, config):
        feature_extractor = FeatureExtractor(config)

        symbol_features = []
        for math_symbol in symbols:
            symbol_features.append(feature_extractor.getSymbolFeatures(math_symbol))


        return symbol_features

    @staticmethod
    def getSymbolsFeaturesParallel(symbols, config, n_workers):
        feature_extractor = FeatureExtractor(config)
        symbol_features = []
        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            for current_features in executor.map(feature_extractor.getSymbolFeatures, symbols):
                symbol_features.append(current_features)

        return symbol_features

    @staticmethod
    def getExpressionsFeatures(expressions, config, isRelationalFeature):
        feature_extractor = FeatureExtractor(config)

        features = {}
        extraction = None
        if isRelationalFeature:
            extraction = feature_extractor.getExpressionRelationFeatures
        else:
            extraction = feature_extractor.getExpressionSymbolFeatures

        for exp in expressions:
            feats = extraction(exp)
            features.update(feats)

        return features

    @staticmethod
    def getExpressionsFeaturesParallel(expressions, config, dataset_type, n_workers=5):
        feature_extractor = FeatureExtractor(config)
        features = {}
        extraction = None
        if dataset_type.lower() == 'relation' or dataset_type.lower() == 'segmentation':
            extraction = feature_extractor.getExpressionRelationFeatures
        elif dataset_type == 'symbol':
            extraction = feature_extractor.getExpressionSymbolFeatures

        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            for feats in executor.map(extraction, expressions):
                features.update(feats)

        return features

