
import math

from recognizer.util.math_ops import MathOps

class TraceFeatureExtractor:
    @staticmethod
    def getTracesCount(symbol):
        return [float(len(symbol.traces))]

    @staticmethod
    def getTotalOriginalPoints(symbol):
        total_points = 0.0

        for trace in symbol.traces:
            total_points += len(trace.original_points)

        return total_points

    @staticmethod
    def getAspectRatio(symbol):
        min_x = 1
        max_x = -1
        min_y = 1
        max_y = -1

        for trace in symbol.traces:
            t_minX, t_maxX, t_minY, t_maxY = trace.getBoundaries()
            min_x = min(t_minX, min_x)
            max_x = max(t_maxX, max_x)
            min_y = min(t_minY, min_y)
            max_y = max(t_maxY, max_y)

        w = (max_x - min_x)
        h = (max_y - min_y)
        if w <= 0.01:
            w = 0.01
        if h <= 0.01:
            h = 0.01

        #raw aspect ratio...
        #features.append( [ (w / h) ] ) #add as a 1-D vector

        #normalized aspect ratio...
        if w > h:
            ratio = (w / h) - 1.0
        else:
            ratio = -((h / w) - 1.0)

        return [ratio]

    @staticmethod
    def getAngularChange(symbol):
        total_angular_change = 0.0

        for trace in symbol.traces:
            previous_angle = None
            for i in range(len(trace.points) - 1):
                p0_x, p0_y = trace.points[i]
                p1_x, p1_y = trace.points[i + 1]

                x = p1_x - p0_x
                y = p1_y - p0_y

                current_angle = math.atan2(y, x)

                if previous_angle is not None:
                    total_angular_change += MathOps.angularDifference(current_angle, previous_angle)

                previous_angle = current_angle

        return total_angular_change, total_angular_change / len(symbol.traces)


    @staticmethod
    def getLineLength(symbol):
        lineLength = 0.0

        for trace in symbol.traces:
            lineLength += trace.getTotalLength()

        return lineLength, lineLength / len(symbol.traces)

    @staticmethod
    def getNumberOfSharpPoints(symbol):
        total_sharp_points = 0.0
        for trace in symbol.traces:
            total_sharp_points += float(len(trace.sharp_points))

        return total_sharp_points, total_sharp_points / len(symbol.traces)

    @staticmethod
    def getVarianceFeatures(symbol):
        #add covariance matrix of all points....
        total_points = 0
        mean_x = 0
        mean_y = 0
        for i in range(len(symbol.traces)):
            points = symbol.traces[i].points

            for x, y in points:
                mean_x += x
                mean_y += y

            total_points += len(points)

        mean_x /= total_points
        mean_y /= total_points

        var_x = 0
        var_y = 0
        cov_xy = 0
        for i in range(len(symbol.traces)):
            points = symbol.traces[i].points

            for x, y in points:
                var_x += (x - mean_x) ** 2
                var_y += (y - mean_y) ** 2
                cov_xy += (x - mean_x) * (y - mean_y)

        var_x /= total_points
        var_y /= total_points
        cov_xy /= total_points

        return var_x, var_y, cov_xy

    @staticmethod
    def getMidPointVarianceFeatures(symbol):
        #add covariance matrix of all points....

        total_weight = 0.0
        mid_points = []
        weights = []
        for i in range(len(symbol.traces)):
            points = symbol.traces[i].points

            for p_idx in range(len(points) - 1):
                p0_x, p0_y = points[p_idx]
                p1_x, p1_y = points[p_idx + 1]

                mid_x = (p0_x + p1_x) / 2.0
                mid_y = (p0_y + p1_y) / 2.0

                length = math.sqrt((p1_x - p0_x) ** 2 + (p1_y - p0_y) ** 2)
                total_weight += length

                mid_points.append((mid_x, mid_y))
                weights.append(length)

        mean_x = 0.0
        mean_y = 0.0
        for idx, (x, y) in enumerate(mid_points):
            mean_x += x * weights[idx]
            mean_y += y * weights[idx]

        if total_weight > 0.0:
            mean_x /= total_weight
            mean_y /= total_weight

        var_x = 0.0
        var_y = 0.0
        cov_xy = 0.0
        for idx, (x, y) in enumerate(mid_points):
            var_x += ((x - mean_x) ** 2) * weights[idx]
            var_y += ((y - mean_y) ** 2) * weights[idx]
            cov_xy += ((x - mean_x) * (y - mean_y)) * weights[idx]

        if total_weight > 0.0:
            var_x /= total_weight
            var_y /= total_weight
            cov_xy /= total_weight

        return var_x, var_y, cov_xy