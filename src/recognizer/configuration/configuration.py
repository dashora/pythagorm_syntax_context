import ast

class Configuration:
    def __init__(self, config_data):
        self.data = config_data

    def get(self, name, default=None):
        if name in self.data:
            # let python guess the right class
            try:
                value = ast.literal_eval(self.data[name])
            except:
                # interpret as string
                value = self.data[name]
            return value
        else:
            return default

    def get_str(self, name, default=""):
        if name in self.data:
            return self.data[name]
        else:
            return default

    # This assumes string is integer 1 or 0 (True, False)
    def get_bool(self, name, default=False):
        if name in self.data:
            return int(self.data[name]) > 0
        else:
            return default

    def get_int(self, name, default=0):
        if name in self.data:
            return int(self.data[name])
        else:
            return default

    def get_float(self, name, default=0.0):
        if name in self.data:
            return float(self.data[name])
        else:
            return default

    def set(self, name, value):
        self.data[name] = value

    def contains(self, name):
        return name in self.data

    def get_active_features(self):
        active_features = []
        for parameter in self.data:
            if parameter[0:13] == "FEATURES_USE_" and self.get_bool(parameter):
                active_features.append(parameter)

        return active_features

    @staticmethod
    def from_file(filename):
        input_file = open(filename, "r")
        lines = input_file.readlines()
        input_file.close()
        last_key_stored = ''

        config_data = {}
        for line in lines:
            # comment character
            if "#" in line:
                line = line.split("#")[0]

            parts = line.strip().split("=")

            if len(parts) != 2:
                parts = line.strip().split("+")
                if len(parts) != 2:
                    last_key_stored = ''
                else:
                    config_data[last_key_stored] += parts[1].strip()
            else:
                key = parts[0].strip().upper()
                last_key_stored = key
                data = parts[1].strip()

                config_data[key] = data

        return Configuration(config_data)
