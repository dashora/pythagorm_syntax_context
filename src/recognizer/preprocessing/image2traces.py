import numpy as np
import math

from recognizer.preprocessing.cc_ops import CCOps
from recognizer.util.image_ops import ImageOps

class Image2Traces:

    def __init__(self, config):
        self.config = config


    def createTraces(self, img):
        trace_type = self.config.get_str("IMAGE_PROCESSING_TRACE_TYPE")
        if trace_type.lower() == "contour":
            contours = CCOps.imageContours(img)
            reduc_factor = self.config.get_int("IMAGE_PROCESSING_CONTOUR_REDUCTION_VALUE", 1)
            smooth_dist = self.config.get_int("IMAGE_PROCESSING_CONTOUR_REDUCTION_SMOOTHING_DISTANCE", 0)
            reduc_type = self.config.get_str("IMAGE_PROCESSING_CONTOUR_REDUCTION_TYPE", "simple")
            traces = Image2Traces.traceReduction(contours, reduc_type, reduc_factor, smooth_dist)
            if len(traces) > 0:
                return traces
            else:
                return [np.array([np.array(img.shape)/2])]
        elif trace_type.lower() == "stroke":
            skel_type = self.config.get_str("IMAGE_PROCESSING_SKELETON_FUNCTION")
            eval_type = self.config.get_str("IMAGE_PROCESSING_SEGMENT_EVALUATION_TYPE")
            if eval_type.lower() != "contourbounds":
                min_ratio = self.config.get_float("IMAGE_PROCESSING_MIN_RATIO_CONSTRAINT")
                dist_fac = self.config.get_float("IMAGE_PROCESSING_DISTANCE_CONSTRAINT_FACTOR")
            else:
                min_ratio = None
                dist_fac = None
            win_fac = self.config.get_int("IMAGE_PROCESSING_WINDOW_SIZE_FACTOR")

            conComps = CCOps.connectedComp(img)
            if len(conComps) == 0:
                return [np.array([[0,0]])]
            max_y = np.amax([np.amax(l[:,0]) for l in conComps])
            max_x = np.amax([np.amax(l[:,1]) for l in conComps])

            compImgs = [ImageOps.pts2img(cc, [max_y,max_x]) for cc in conComps]
            skeletons = [CCOps.getSkeleton(cImg, skel_type) for cImg in compImgs]
            if win_fac == -1:
                winSizes = [1 for i in range(len(skeletons))]
            else:
                winSizes = [max(1, min(skel.shape[1] // win_fac, skel.shape[0] // win_fac)) for skel in skeletons]
            dists = [math.sqrt(winSize**2 + winSize**2) for winSize in winSizes]

            pts = [Image2Traces.skel2pts(skeletons[i], winSizes[i]) for i in range(len(skeletons))]
            traces = []
            for i in range(len(pts)):
                traces += Image2Traces.pts2traces(pts[i], compImgs[i], dists[i]*dist_fac, eval_type, min_ratio)
            return traces
        else:
            raise ValueError

    @staticmethod
    def traceReduction(traces, type="simple", factor=1, dist=0):
        if type.lower() == "simple" or dist == 0:
            traces = [t[::factor] for t in traces]
        elif type.lower() == "smooth":

            traces = [np.concatenate((t[0:dist],[np.array(t[i-dist:i+dist]).mean(axis=0) for i in range(dist, len(t)-dist, factor)],t[-dist:])) for t in traces if len(t) > 2 * dist]
        else:
            raise ValueError
        return traces

    @staticmethod
    def pts2traces(pts,img,maxDistance,eval_type, min_ratio):
        """

        Args:
            pts: list of [y,x] points to be turned into traces (sequence of points)
            img: Binary image as 2D array with values 0 or 1, 1 being symbol pixels. Original source of the points in pts.
            maxDistance: A double value represent the furthest distance allowed when connecting end points of a trace to another trace.
                         Used in the final step of creating the traces.
            config: Image configuration with processing and path parameters.

        Returns: List of traces (sequence of points)

        """
        if len(pts) == 0:
            return []

        if eval_type == "ContourBounds":
            contourImg = CCOps.doubleContourImg(img)
        else:
            contourImg = None
        pool = pts[:]
        traces = []
        curtrace = []
        center = np.array([np.array(pts)[:,0].mean(),np.array(pts)[:,1].mean()])
        #start = np.argmax([np.sqrt(np.power(np.array(center) - p2, 2).sum()) for p2 in pool])
        start = np.argmax([np.linalg.norm(center - np.array(p2)) for p2 in pool])
        curtrace.append(pool.pop(start))
        frontDone = False
        backDone = False
        while len(pool) > 0:
            if not(frontDone):
                unchecked = pool[:]
                distances = [np.linalg.norm(curtrace[0]-pt) for pt in np.array(unchecked)]
                near = np.argmin(distances)
                dist = distances[near]
                while dist <= maxDistance:
                    if Image2Traces.evalSegment(curtrace[0], unchecked[near], img, eval_type, min_ratio, contourImg):
                        curtrace = [pool.pop(near)] + curtrace
                        break
                    else:
                        distances[near] = np.inf
                        near = np.argmin(distances)
                        dist = distances[near]#np.linalg.norm(curtrace[0] - np.array(unchecked[near]))

                if np.isinf(dist) or np.linalg.norm(curtrace[0] - np.array(unchecked[near])) >= maxDistance:
                    frontDone = True
            elif not(backDone):
                unchecked = pool[:]
                distances = [np.linalg.norm(curtrace[-1]-pt) for pt in np.array(unchecked)]
                near = np.argmin(distances)
                dist = distances[near]#np.linalg.norm(curtrace[-1] - np.array(unchecked[near]))
                while dist <= maxDistance:
                    if Image2Traces.evalSegment(curtrace[0], unchecked[near], img, eval_type, min_ratio, contourImg):
                        curtrace.append(pool.pop(near))
                        break
                    else:
                        distances[near] = np.inf
                        near = np.argmin(distances)
                        dist = distances[near]

                if np.isinf(dist) or np.linalg.norm(curtrace[-1] - np.array(unchecked[near])) >= maxDistance:
                    backDone = True
            else:
                traces.append(np.array(curtrace[:]))
                curtrace = []
                start = np.argmax([np.linalg.norm(np.array(center) - np.array(p2)) for p2 in pool])
                curtrace.append(pool.pop(start))
                frontDone = False
                backDone = False
        if len(curtrace) != 0:
            traces.append(np.array(curtrace[:]))

        # Concatinate traces with close ends
        closed = []
        for i in range(len(traces)):
            t1 = traces[i]
            t1start = np.array(t1[0])
            t1end = np.array(t1[-1])
            for j in range(len(traces)):
                if traces[j] == []:
                    continue
                t2 = traces[j]
                t2start = t2[0]
                t2end = t2[-1]
                if np.linalg.norm(t1start - t2start) <= maxDistance and Image2Traces.evalSegment(t1start, t2start, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1start, t2start)):
                    traces[i] = []
                    traces[j] = np.concatenate((t2[::-1],t1))
                elif np.linalg.norm(t1start - t2end) <= maxDistance and Image2Traces.evalSegment(t1start, t2end, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1start, t2end)):
                    if np.array_equal(t1,t2) and len(t2) > 2:
                        traces[j] = []
                        closed.append(np.concatenate((t2,[t1start])))
                    elif not(np.array_equal(t1,t2)):
                        traces[i] = []
                        traces[j] = np.concatenate((t2,t1))
                elif np.linalg.norm(t1end - t2start) <= maxDistance and Image2Traces.evalSegment(t1end, t2start, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1end, t2start)):
                    traces[i] = []
                    traces[j] = np.concatenate((t1,t2))
                elif np.linalg.norm(t1end - t2end) <= maxDistance and Image2Traces.evalSegment(t1end, t2end, img, eval_type, min_ratio, contourImg) and not(np.array_equal(t1end, t2end)):
                    traces[i] = []
                    traces[j] = np.concatenate((t1, t2[::-1]))

        traces = [t for t in traces if len(t) > 0]

        # connect end points to near points in other traces
        for i in range(len(traces)):
            tstart = np.array(traces[i][0])
            tend = np.array(traces[i][-1])
            possibles = [p for p in pts if p not in traces[i]]
            for c in closed:
                possibles += list(c)
            if len(possibles) == 0:
                continue
            nearStart = np.argmin([np.linalg.norm(tstart-pt) for pt in possibles])
            nearEnd = np.argmin([np.linalg.norm(tend-pt) for pt in possibles])
            startDist = np.linalg.norm(tstart - possibles[nearStart])
            endDist = np.linalg.norm(tend - possibles[nearEnd])
            if startDist <= maxDistance and Image2Traces.evalSegment(tstart, possibles[nearStart], img, eval_type, min_ratio, contourImg):
                traces[i] = np.concatenate(([possibles[nearStart]], traces[i]))
            if endDist <= maxDistance and Image2Traces.evalSegment(tend, possibles[nearEnd], img, eval_type, min_ratio, contourImg):
                traces[i] = np.concatenate((traces[i], [possibles[nearEnd]]))

        return closed+traces

    @staticmethod
    def evalSegment(p1, p2, img, eval_type="BoxRatio", min_ratio=.6, con_img = None):
        """

        Args:
            p1: [y,x] point and start of a segment
            p2: [y,x] point and end of a segment
            img: binary image as 2D array with 0 and 1 values.

        Returns: The number of pixels in the image corresponding to a symbol in a bounding box around the two points
                 divided by the area of the bounding box.

        """
        if eval_type == "BoxRatio":
            max_y = math.ceil(max(p1[0], p2[0]))
            max_x = math.ceil(max(p1[1], p2[1]))
            min_y = math.floor(min(p1[0], p2[0]))
            min_x = math.floor(min(p1[1], p2[1]))
            if max_x == min_x:
                max_x += 1
            if max_y == min_y:
                max_y += 1
            count = (img[min_y:max_y,min_x:max_x]).flatten().sum()
            area = (max_y-min_y) * (max_x-min_x)
            return count/area > min_ratio
        elif eval_type == "ContourBounds":
            if con_img is None:
                con_img = CCOps.contourImg(img)
            return CCOps.boundsCheck(p1, p2, con_img)
        elif eval_type == "SegmentRatio":
            ray = ImageOps.rayTrace(p1,p2)
            count = np.array([img[p[0], p[1]] for p in ray if p[0] < img.shape[0] and p[1] < img.shape[1]]).sum()
            return count/len(ray) > min_ratio

    @staticmethod
    def skel2pts(skel, winSize):
        """

        Args:
            skel: Skeleton Image as 2D array with boolean 0 and 1 values

        Returns: List of points representing the skeleton.

        """
        points = []
        for r in range(0, skel.shape[0], winSize):
            for c in range(0, skel.shape[1], winSize):
                pts = np.where(skel[r:r+winSize,c:c+winSize] == 1)
                if len(pts[0]) > 0:
                    points.append((pts[0].mean()+r, pts[1].mean()+c))
        return points

