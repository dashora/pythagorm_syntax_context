import sys
from skimage import io
import numpy as np
from concurrent.futures import ProcessPoolExecutor
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.symbols.math_expression import MathExpression
from recognizer.preprocessing.trace_smoother import TraceSmoother
from recognizer.preprocessing.image2traces import Image2Traces
from recognizer.preprocessing.cc_ops import CCOps


class ImgLoader:

    # @staticmethod
    # def loadExpressionLabels(fileId, config):
    #     """
    #
    #     :param fileId: Matches the name of the Lg and Png files containing a
    #             list of symbols or an expression
    #     :param imgConfig: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Three lists with each symbol's id, img, and class label
    #     """
    #     ids,imgs, comps, offsets, labels = [], [], [], [], []
    #     imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
    #     lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
    #
    #     imageGrid = io.imread(imgsPath + fileId + ".PNG").astype(bool)
    #     lg = open(lgPath + fileId + ".lg")
    #
    #     notes = []
    #     symLabels = dict([])
    #     symComps = dict([])
    #     for line in lg:
    #         parts = [p.strip(",") for p in line.strip().split(" ")]
    #         if len(parts) >= 6 and parts[0].lower() == "#sym":
    #             ids.append(parts[1])
    #             notes.append(parts[1:])
    #         elif len(parts) >= 4 and parts[0] == "O":
    #             symLabels[parts[1]] = parts[2]
    #             symComps[parts[1]] = parts[4:]
    #
    #     for n in notes:
    #         labels.append(symLabels[n[0]])
    #         ystart, xstart, yend, xend = np.array(n[1:5]).astype(int)
    #         imgs.append(imageGrid[ystart:yend+1,xstart:xend+1])
    #         offsets.append([ystart,xstart])
    #         comps.append(symComps[n[0]])
    #
    #     return ids, imgs, comps, offsets, labels

    @staticmethod
    def loadExpressionLabels(fileId, config):
        symLabels, relLabels, comps, boxes = {}, {}, {}, {}
        lgPath = config.get_str("IMAGE_PROCESSING_LG_PATH")
        lg = open(lgPath + fileId + ".lg")

        for line in lg:
            parts = [p.strip(",") for p in line.strip().split(" ")]
            if parts[0].lower() == "o":
                symLabels[parts[1]] = parts[2]
                comps[parts[1]] = parts[4:]
            elif parts[0].lower() == "r":
                relLabels[(parts[1], parts[2])] = parts[3]
            elif parts[0].lower() == "#cc":
                boxes[parts[1]] = np.array(parts[2:]).astype(int)

        return symLabels, relLabels, comps, boxes



    @staticmethod
    def loadExpressionImages(fileId, boxes, config):
        imgs = {}
        imgsPath = config.get_str("IMAGE_PROCESSING_IMG_PATH")
        expImage = io.imread(imgsPath + fileId + ".PNG").astype(bool)
        for id,box in boxes.items():
            imgs[id] = np.array(CCOps.extractComp(expImage, box))

        return imgs

    @staticmethod
    def processImg(img, offset, config):
        traceBuilder = Image2Traces(config)
     #   print(img.shape)
        raw_traces = traceBuilder.createTraces(img)

        traces = {}
        for tid, tp in enumerate(raw_traces):
            trace = MathSymbolTrace(tid, list(map(tuple,[t[::-1] + np.array(offset[::-1]) for t in tp])))
            # TraceSmoother.applyPreprocessing(trace) # Should preprocessing be moved to processing module
            traces[trace.id] = trace
        return traces

    # @staticmethod
    # def process_Img(id, img, comps, offset, label, config):
    #     """
    #     Takes the symbol image and data and creates the processed traces for
    #      the math symbol object from the symbol image. Traces are preprocessed
    #      and added to a math symbol.
    #
    #     :param id: Unique Symbol Id
    #     :param img: binary image of symbol
    #     :param comps: list of connected component ids for symbol
    #     :param offset: symbols offset in the expression image [lowest y, lowest x]
    #     :param label: The ground truth symbol class label
    #     :param config: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Math Symbol Object
    #     """
    #     traceBuilder = Image2Traces(config)
    #     raw_traces = traceBuilder.createTraces(img)
    #
    #     traces = []
    #     for tid, tp in enumerate(raw_traces):
    #         trace = MathSymbolTrace(tid, list(map(tuple,[t[::-1] + np.array(offset[::-1]) for t in tp])))
    #         TraceSmoother.applyPreprocessing(trace)
    #         traces.append(trace)
    #
    #     symbol = MathSymbol(id, traces, label)
    #     symbol.setTraceList(comps)
    #
    #     return symbol

    @staticmethod
    def create_traceSet(id, compIds, components, offsets, label, config):
        if(len(components) == 0) :
            return MathSymbolTraceSet(id, [MathSymbolTrace("-1", [[0,0]])], label)
        img, offset = CCOps.combineComps(components,np.array(offsets))
        traces = ImgLoader.processImg(img, offset, config)

        symbol = MathSymbolTraceSet(id, list(traces.values()), label)
        symbol.setTraceList(compIds)

        return symbol


    # @staticmethod
    # def create_Symbols(fileId, config):
    #     """
    #
    #     :param fileId: Matches the name of the Lg and Png files containing a
    #             list of symbols or math expression
    #     :param imgConfig: Configuration containing parameters and paths
    #             for processing symbol images.
    #     :return: Two lists: list of math symbol objects, list of class labels
    #     """
    #     ids, imgs, comps, offsets, labels = ImgLoader.loadExpressionLabels(fileId, config)
    #     symbols = []
    #     for i in range(0, len(imgs)):
    #         symbols.append(ImgLoader.process_Img(ids[i], imgs[i], comps[i], offsets[i], labels[i], config))
    #
    #     if config.get_bool("SAVE_EXPRESSION_CONTEXT", default=True):
    #         expression = MathExpression(fileId, symbols, fileId)
    #         avg_width = expression.getSizes()[:,0].mean()/len(symbols)
    #         avg_height = expression.getSizes()[:,1].mean()/len(symbols)
    #         for symbol in symbols:
    #             symbol.set_expression(expression)
    #             symbol.set_file_name(fileId)
    #             symbol.setSizeRatio(avg_width, avg_height)
    #             symbol.normalize()
    #     else:
    #         for symbol in symbols:
    #             symbol.normalize()
    #             symbol.set_file_name(fileId)
    #
    #     return symbols, labels

    @staticmethod
    def create_expression(data):
        fileid, config = data
        symLabels, symRels, segs, boxes = ImgLoader.loadExpressionLabels(fileid, config)
        imgs = ImgLoader.loadExpressionImages(fileid, boxes, config)


        level = config.get_str("GRAPH_LEVEL", "symbol").lower()

        expression = None
        components = {}
        if(level == "primitive"):
            for id,img in imgs.items():
                traces = list(ImgLoader.processImg(img, boxes[id][:2], config).values())
                components[id] = MathSymbolTraceSet(id, traces, None)
                components[id].trace_list = [id]

                # Create "Stroke" level ground truth symbol class and relation dictionaries
                strokeLabels = {}
                strokeRels = {}
                for symId, label in symLabels.items():
                    for ccId in segs[symId]:
                        strokeLabels[ccId] = label
                        for otherCC in segs[symId]:
                            if ccId != otherCC:
                                strokeRels[(ccId, otherCC)] = label
                for (pid, cid), label in symRels.items():
                    if pid not in segs or cid not in segs:
                        continue
                    for p in segs[pid]:
                        for c in segs[cid]:
                            strokeRels[(p,c)] = label

            expression = MathExpression(fileid, components, strokeLabels, strokeRels, segs, fileid)

        elif(level == "symbol"):
            for symId,label in symLabels.items():
                components[symId] = ImgLoader.create_traceSet(symId, segs[symId], [imgs[compId] for compId in segs[symId]], [boxes[compId][:2] for compId in segs[symId]], label, config)

            expression = MathExpression(fileid, components, symLabels, symRels, segs,  fileid)

        return expression

    @staticmethod
    def loadExpressions(listFile, config, workers=5):
        expressions = {}
        filesIds = []
        for fileid in open(listFile):
            filesIds.append((fileid.strip(),config))
        print("Number of Expressions:" + str(len(filesIds)))
        with ProcessPoolExecutor(max_workers=workers) as executor:
            mapper = executor.map
            loading_function = ImgLoader.create_expression

            for i, exp in enumerate(mapper(loading_function, filesIds)):
                #print(exp.id)
                expressions[exp.id] = exp
                print(" Loading: " + str(((i+1)*100)//len(filesIds)) + "%", end="\r")
            print()

        return expressions

    # @staticmethod
    # def process_Lg(data):
    #     """
    #
    #     :param data: tuple containing a file Id, feature extractor object, and recognition configuration.
    #     :return: The file Id, feature vectors for all symbols connected to file id,
    #              list of class labels for all symbols connected to file id.
    #     """
    #     fileId, feature_extractor, config = data
    #     symbols, labels = ImgLoader.create_Symbols(fileId, config)
    #
    #     symbol_data, label_data, source_data = [], [], []
    #     for sym in symbols:
    #         if feature_extractor is not None:
    #             symbol_data.append(feature_extractor.getFeatures(sym))
    #         else:
    #             symbol_data.append(sym)
    #
    #         symbol_label = sym.truth
    #         label_data.append(symbol_label)
    #
    #         # the source of current symbol will stored
    #         source_data.append((fileId, sym.id, sym.trace_list))
    #     return fileId, (symbol_data, label_data, source_data)
    #
    # @staticmethod
    # def load_Lg_List(path, feature_extractor, config, workers=5, save_sources=True, label_mapping=None, class_mapping=None):
    #     """
    #
    #     :param path: To text file containing list of file Ids
    #     :param feature_extractor: feature extractor object created with configuration
    #     :param config: symbol recognition configuration
    #     :param workers: number of workers to use in thread pool
    #     :param label_mapping: list of all class labels with index as their Id
    #     :param class_mapping: dictionary mapping class to their index id
    #     :return: feature data set object with feature vectors from symbols found using fileIds from list,
    #              list of valid files, list of files with errors.
    #     """
    #     data, labels, valid, error = [], [], [], []
    #     sources = [] if save_sources else None
    #     dir_data = []
    #     for fileId in open(path):
    #         dir_data.append((fileId.strip(), feature_extractor, config))
    #
    #     with ProcessPoolExecutor(max_workers=workers) as executor:
    #
    #         mapper = executor.map
    #         loading_function = ImgLoader.process_Lg
    #
    #         for i, (fileId, file_data) in enumerate(mapper(loading_function, dir_data)):
    #             symbol_data, label_data, source_data = file_data
    #             data += symbol_data
    #             labels += label_data
    #             valid.append(fileId.strip())
    #
    #             if save_sources:
    #                 sources += source_data
    #
    #     if feature_extractor is not None:
    #         dataset = FeatureDataset.from_Img_data(data, labels, label_mapping=label_mapping, class_mapping=class_mapping, sources=sources, config=config)
    #     else:
    #         dataset = (data, labels, sources)
    #
    #     return dataset, valid, error




