from csv import excel

import numpy as np
from skimage import io
from skimage import measure
from skimage.morphology import medial_axis
from skimage.morphology import skeletonize
#from thinning import guo_hall_thinning

from recognizer.util.image_ops import ImageOps

class CCOps:

    @staticmethod
    def connectedComp(img):
        labelImg, numLabels = measure.label(img, return_num=True, connectivity=2, background=0)
        return [np.argwhere(labelImg == label) for label in range(1,numLabels+1)]

    @staticmethod
    def extractComp(img, box):
        raw_comp = img[box[0]:box[2]+1,box[1]:box[3]+1]
        labeled, numLabels = measure.label(raw_comp, return_num=True, connectivity=2, background=0)
        if numLabels == 1:
            return raw_comp
        else:
            trueLabel = -1
            for i in range(1, numLabels):
                if i in labeled[0,:] and i in labeled[-1,:] and i in labeled[:,0] and i in labeled[:,-1]:
                    trueLabel = i
                    break
            comp = np.array([[1 if labeled[y,x] == trueLabel else 0 for x in range(labeled.shape[1])] for y in range(labeled.shape[0])])
            return comp

    @staticmethod
    def combineComps(components, offsets):
        sizes = []
        for c in components:
            sizes.append(c.shape)
        sizes = np.array(sizes)
        try:
            minx = np.amin(offsets[:,1])
            miny = np.amin(offsets[:,0])
            maxx = np.amax(offsets[:,1]+sizes[:,1])
            maxy = np.amax(offsets[:,0]+sizes[:,0])
        except:
            print(offsets)
            print(len(components))
        img = np.zeros([maxy-miny,maxx-minx])
        for i in range(len(components)):
            startx = offsets[i,1]-minx
            endx = startx + sizes[i,1]
            starty = offsets[i,0]-miny
            endy = starty + sizes[i,0]
            img[starty:endy,startx:endx] += components[i]

        return img, [miny, minx]

    @staticmethod
    def matchCC(symbolboxes, expressionImg):
        """

        Args:
            symbolboxes: dictionary (symbol bounding box -> symId)
            expressionImg:

        Returns: dictionary (conntected component box -> symId)

        """
        pass
        # get connect components
        raw_cc_list = CCOps.connectedComp(expressionImg)
        # get cc bounding boxes
        cc_list = CCOps.boundingBoxes(raw_cc_list)

        # sort symbolboxes
        symbox_list = list(symbolboxes.keys())
        symbox_list.sort(key=lambda box: (box[2]-box[0]) *(box[3]-box[1]))

        # match each cc box to a symbolbox
        ccMap = {}
        for cc_box in cc_list:
            flag = False
            for symbox in symbox_list:
                #print(str(symbox) + ", " + str(cc_box))
                #print(CCOps.isNested(symbox,cc_box))
                if CCOps.isNested(symbox,cc_box):
                    if symbolboxes[tuple(symbox)] in ccMap:
                        ccMap[symbolboxes[tuple(symbox)]].append(tuple(cc_box))
                    else:
                        ccMap[symbolboxes[tuple(symbox)]] = [tuple(cc_box)]
                    flag = True
                    break
            if not flag:
                print(cc_box)
                print(symbox_list)
        return ccMap

    @staticmethod
    def isNested(bigbox, littlebox):
        # image bounding boxes : [min_y, min_x, max_y, max_x]
        # little box has to be inside big box, but an error of 2 pixels in each direction is allowed
        return littlebox[0] - bigbox[0] >= -2 and littlebox[1] - bigbox[1] >= -2 and littlebox[2] - bigbox[2] <= 2 and littlebox[3] - bigbox[3] <= 2

    @staticmethod
    def boundingBoxes(comps):
        """

        Args: comps is a list of connected components. A connected component is a 2D array of points(y,x) for a single
        connected component.

        Returns: 2D array where each row has four points that act as the points of the bounding box around a connected component.
        The order of the bounding boxes matches the order of the connected components.
        bounding box -> [min_y, min_x, max_y, max_x]

        """
        return np.array([[np.min(cc[:,0]), np.min(cc[:,1]), np.max(cc[:,0]), np.max(cc[:,1])] for cc in comps])

    @staticmethod
    def imageContours(img):
#        print(img.shape)

        imb = np.pad(img, 1, 'constant', constant_values=0)
        try:
            contours = measure.find_contours(imb, 0)
        except:
            print(img)
        return [c.astype(int) for c in contours if len(c) > 1]

    @staticmethod
    def contourImg(img):
        contours = CCOps.imageContours(img)
        conImg = ImageOps.pts2img(np.concatenate(contours), np.array(img.shape) + 1)
        return conImg

    @staticmethod
    def doubleContourImg(img):
        first = CCOps.contourImg(img) + np.pad(img, 1, 'constant', constant_values=0)
        contours2 = CCOps.imageContours(first)
        return ImageOps.pts2img(np.concatenate(contours2), np.array(first.shape) + 1)

    @staticmethod
    def boundsCheck(pt1, pt2, conImage):
        """
        allows for starting and ending points to be on contour edge (?)
        :param pt1: y,x values of starting point in line segment
        :param pt2: y,x values of ending point in line segment
        :param conImage: the contour image with points in the contour have value 1
        :return:
        """
        pt1 = np.array([round(i) for i in pt1]).astype(int)
        pt2 = np.array([round(i) for i in pt2]).astype(int)
        if pt1[0] == pt2[0]:
            low = min([pt1[1], pt2[1]])
            high = max([pt1[1], pt2[1]])
            return conImage[pt1[0], low:high+1].sum() == 0
        elif pt1[1] == pt2[1]:
            low = min([pt1[0], pt2[0]])
            high = max([pt1[0], pt2[0]])
            return conImage[low:high+1, pt1[1]].sum() == 0
        else:
            xsign = int(np.sign(pt2[1]-pt1[1]))
            ysign = int(np.sign(pt2[0]-pt1[0]))
            deltaErr = float(pt1[0] - pt2[0])/float(pt1[1]-pt2[1])
            dsign = np.sign(deltaErr)
            error = 0.0
            y = pt1[0]
            for x in range(pt1[1], pt2[1], xsign):
                if not np.array_equal([y,x], pt1) and not np.array_equal([y,x], pt2) and conImage[y,x] == 1:
                    return False
                error += deltaErr
                while abs(error) > 1:
                    y += ysign
                    error -= dsign
                    if not np.array_equal([y,x], pt1) and not np.array_equal([y,x], pt2) and conImage[y,x] == 1:
                        return False
        return True

    @staticmethod
    def getSkeleton(img, type="Zhang"):
        """

        Args:
            img: 2D Binary image array, should contain a single connected component

        Returns: 2D binary image array, the connected component is preserved but reduced to single pixel thickness.

        """
        imb = np.pad(img, 1, mode='constant', constant_values=0)
        if type == "Medial":
            skel = medial_axis(imb)
        elif type == "Zhang":
            skel = skeletonize(imb)
        elif type == "Guo":
            skel = guo_hall_thinning(imb.astype(np.uint8))
        else:
            skel = skeletonize(imb)

        return skel[1:-1,1:-1]