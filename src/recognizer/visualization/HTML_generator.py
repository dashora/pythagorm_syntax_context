import math

class HTMLGenerator:
    COLOR_SCALE_RED_GREEN = [(0.0, 1.0, 0.5, 0.5), (0.5, 1.0, 1.0, 0.5), (1.0, 0.5, 1.0, 0.5)]
    COLOR_SCALE_GREEN_RED = [(0.0, 0.5, 1.0, 0.5), (0.5, 1.0, 1.0, 0.5), (1.0, 1.0, 0.5, 0.5)]

    @staticmethod
    def confusion_matrix_HTML(confusion_matrix, subset=None):
        if subset is None:
            subset = list(confusion_matrix.keys())

        html = "<table>"
        # generate ...
        header = "<tr><th>&nbsp;</th>"
        content = ""
        for class_name in subset:
            header += "<th>" + class_name + "</th>"

            content += "<tr>"
            content += "<th>" + class_name + "</th>"
            for predicted_name in subset:
                if (class_name in confusion_matrix) and (predicted_name in confusion_matrix[class_name]):
                    value = str(confusion_matrix[class_name][predicted_name])
                else:
                    value = "&nbsp;"

                content += "<td>" + value + "</td>"
            content += "</tr>"

        header += "</tr>"

        html = "<table class=\"conf_matrix\" cellpadding=\"0\" cellspacing=\"0\">" + header + content + "</table>"

        return html

    @staticmethod
    def HTML_color_in_scale(value, min_value, max_value, color_scale):
        if min_value == max_value:
            scale_value = 1.0
        else:
            scale_value = (value - min_value) / (max_value - min_value)

        range_idx = 0
        while scale_value > color_scale[range_idx + 1][0]:
            range_idx += 1

        w1 = (scale_value - color_scale[range_idx][0]) / (color_scale[range_idx + 1][0] - color_scale[range_idx][0])
        w0 = 1.0 - w1

        final_r = int((color_scale[range_idx][1] * w0 + color_scale[range_idx + 1][1] * w1) * 255)
        final_g = int((color_scale[range_idx][2] * w0 + color_scale[range_idx + 1][2] * w1) * 255)
        final_b = int((color_scale[range_idx][3] * w0 + color_scale[range_idx + 1][3] * w1) * 255)

        html_color = '#%02x%02x%02x' % (final_r, final_g, final_b)

        return html_color



    @staticmethod
    def class_representation_HTML(counts_per_class, class_mapping, confusion_matrix):
        # order from most represented to less represented
        #sorted_classes = sorted([(counts_per_class[name], name) for name in counts_per_class], reverse=True)

        # order by label
        sorted_classes = sorted([(int(class_mapping[name]), name) for name in counts_per_class])

        # presentation parameters
        v_max_proportion = 20.0
        n_classes = len(counts_per_class)

        all_counts = [counts_per_class[name] for name in counts_per_class]
        n_samples = sum(all_counts)
        min_samples = min(all_counts)
        max_samples = max(all_counts)

        total_error_per_class = {}
        top_errors_per_class = {}
        total_sum = 0
        max_sum = 0
        for class_name in confusion_matrix:
            current_sum = 0
            class_errors = []
            for other in confusion_matrix[class_name]:
                current_sum += confusion_matrix[class_name][other]
                class_errors.append((confusion_matrix[class_name][other], other))

            total_error_per_class[class_name] = current_sum
            top_errors_per_class[class_name] = sorted(class_errors, reverse=True)

        all_errors = [total_error_per_class[name] for name in total_error_per_class]
        n_errors = sum(all_errors)
        min_errors = 0
        max_errors = max(all_errors)

        cols = max(1, int(math.ceil(math.sqrt(n_classes / v_max_proportion))))
        min_rows = int(float(n_classes) / cols)

        main_table_content = ""
        # generate cells containing individual tables per column
        offset_pos = 0
        for col in range(cols):
            rows = min_rows + (1 if col < n_classes % cols else 0)

            col_table = "<table class=\"class_representation\" cellspacing=\"0\" cellpadding=\"0\">"
            col_table += "<tr>"
            col_table += "<th>Label</th><th>Class</th><th>Count</th><th>Errors</th>"
            col_table += "<th>% Dataset</th><th>% Accuracy</th><th>% Total Error</th><th colspan=\"3\">Common Confusions</th>"
            col_table += "</tr>"
            for row in range(rows):
                sort_idx, class_name = sorted_classes[offset_pos + row]
                class_count = counts_per_class[class_name]

                if class_name in total_error_per_class:
                    error_count = total_error_per_class[class_name]
                else:
                    error_count = 0

                col_table += "<tr>"
                col_table += "<td>[" + str(class_mapping[class_name]) + "]</td>"
                col_table += "<td>" + class_name + "</td>"
                col_table += "<td>{0}</td>".format(class_count)
                col_table += "<td>{0}</td>".format(error_count)

                prc_dataset_col = HTMLGenerator.HTML_color_in_scale(class_count, min_samples, max_samples, HTMLGenerator.COLOR_SCALE_RED_GREEN)
                col_table += "<td style=\"background-color: " + prc_dataset_col + "\" >{0:.3f}</td>".format(class_count * 100 / n_samples)

                class_accuracy = (1.0 - (error_count / class_count)) * 100.0
                prc_accuracy_col = HTMLGenerator.HTML_color_in_scale(class_accuracy, 0.0, 100.0, HTMLGenerator.COLOR_SCALE_RED_GREEN)
                col_table += "<td style=\"background-color: " + prc_accuracy_col + "\">{0:.3f}</td>".format(class_accuracy)

                prc_error_col = HTMLGenerator.HTML_color_in_scale(error_count, 0.0, max_errors, HTMLGenerator.COLOR_SCALE_GREEN_RED)
                col_table += "<td style=\"background-color: " + prc_error_col + "\">{0:.3f}</td>".format(error_count * 100 / n_errors)

                show_top = 3
                if class_name in top_errors_per_class:
                    for i in range(min(show_top, len(top_errors_per_class[class_name]))):
                        col_table += "<td>" + top_errors_per_class[class_name][i][1] + "</td>"

                    if len(top_errors_per_class[class_name]) < show_top:
                        col_table += "<td colspan=\"" + str(show_top - len(top_errors_per_class[class_name])) + "\">&nbsp;</td>"
                else:
                    col_table += "<td colspan=\"" + str(show_top) + "\">&nbsp;</td>"



                col_table += "</tr>"
            col_table += "</table>"

            main_table_content += "<td style=\"vertical-align:top\">" + col_table + "</td>"
            offset_pos += rows

        main_table = "<table><tr>" + main_table_content + "</tr></table>"

        return main_table

    @staticmethod
    def top_errors_HTML(top_errors, total_errors, total_samples):
        v_max_proportion = 5.0
        n_errors = len(top_errors)

        cols = max(1, int(math.ceil(math.sqrt(n_errors / v_max_proportion))))
        min_rows = int(float(n_errors) / cols)

        main_table_content = ""
        # generate cells containing individual tables per column
        offset_pos = 0
        total_count = 0
        for col in range(cols):
            rows = min_rows + (1 if col < n_errors % cols else 0)

            col_table = "<table class=\"top_errors\" cellspacing=\"0\" cellpadding=\"0\">"
            col_table += "<tr><th>Pos</th><th>Expected</th><th>Predicted</th><th>Count</th><th>% Dataset</th><th>% Error</th></tr>"
            for row in range(rows):
                count, expected, predicted = top_errors[offset_pos + row]
                total_count += count

                col_table += "<tr>"
                col_table += "<td>" + str(offset_pos + row) + "</td>"
                col_table += "<td>" + expected + "</td>"
                col_table += "<td>" + predicted + "</td>"
                col_table += "<td>" + str(count) + "</td>"
                col_table += "<td>{0:.3f}</td>".format(count * 100.0 / total_samples)
                col_table += "<td>{0:.3f}</td>".format(count * 100.0 / total_errors)
                col_table += "</tr>"
            col_table += "</table>"

            main_table_content += "<td style=\"vertical-align:top\">" + col_table + "</td>"
            offset_pos += rows

        main_table = "<table><tr>" + main_table_content + "</tr></table>"

        header = "<h3>Top " + str(n_errors) + " Confusions</h3>"
        header += "<p>"
        header += "<b>Total errors:<b> {0} ({1:.3f}% dataset, {2:.3f}% total error)<br \>".format(total_count, total_count * 100 / total_samples, total_count * 100 / total_errors)
        header += "</p>"

        return header + main_table

    @staticmethod
    def similar_shapes_HTML(similar_shapes):
        ordered_names = sorted([(len(similar_shapes[name]), name) for name in similar_shapes], reverse=True)

        # presentation parameters
        v_max_proportion = 1.0
        n_classes = len(ordered_names)

        cols = max(1, int(math.ceil(math.sqrt(n_classes / v_max_proportion))))
        min_rows = int(float(n_classes) / cols)

        main_table_content = ""
        # generate cells containing individual tables per column
        offset_pos = 0
        for col in range(cols):
            rows = min_rows + (1 if col < n_classes % cols else 0)

            max_similar = ordered_names[offset_pos][0]

            col_html = "<table class=\"similar_shape\" cellpadding=\"0\" cellspacing=\"0\">"
            col_html += "<tr><th>Shape</th><th colspan=\"" + str(max_similar) + "\">Similar to </th></tr>"
            for row in range(rows):
                class_count, class_name = ordered_names[offset_pos + row]

                row_html = "<tr>"
                row_html += "<th>" + class_name + "</th>"
                for other_shape in similar_shapes[class_name]:
                    row_html += "<td>" + other_shape + "</td>"
                if len(similar_shapes[class_name]) < max_similar:
                    row_html += "<td colspan=\"" + str(max_similar - len(similar_shapes[class_name])) + "\">&nbsp;</td>"
                row_html += "</tr>"

                col_html += row_html
            col_html += "</table>"

            main_table_content += "<td style=\"vertical-align:top\">" + col_html + "</td>"
            offset_pos += rows

        main_table = "<table><tr>" + main_table_content + "</tr></table>"

        return main_table

    @staticmethod
    def eval_page_HTML_header(title):
        header = """
        <!DOCTYPE html>
        <html>
            <head>
                <title>""" + title + """</title>
                <style>
                 .conf_matrix td  { border: 1px solid black; padding: 5px; }
                 .conf_matrix th  { border: 1px solid black; padding: 5px; }
                 .class_representation td  { border: 1px solid black; padding: 5px; }
                 .class_representation th  { border: 1px solid black; padding: 5px; }
                 .similar_shape td  { border: 1px solid black; padding: 5px; }
                 .similar_shape th  { border: 1px solid black; padding: 5px; }
                 .top_errors td  { border: 1px solid black; padding: 5px; }
                 .top_errors th  { border: 1px solid black; padding: 5px; }
                 h1 { text-align: center; }
                </style>
            </head>
            <body>
                <h1>""" + title + """</h1>
        """

        return header

    @staticmethod
    def eval_page_HTML_footer():
        footer = """
            </body>
        </html>
        """

        return footer

    @staticmethod
    def save_HTML_page(title, html_content, filename):
        header = HTMLGenerator.eval_page_HTML_header(title)
        footer = HTMLGenerator.eval_page_HTML_footer()

        out_file = open(filename, "w")
        out_file.write(header)
        out_file.write(html_content)
        out_file.write(footer)
        out_file.close()