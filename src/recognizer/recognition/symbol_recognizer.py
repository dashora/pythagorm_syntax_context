import numpy as np

from recognizer.training.trainer import ClassifierTrainer
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES

class SymbolRecognizer:

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        self.type = type
        self.trained_classifier = self.trainClassifier(dataset, config)
        self.classes_list = dataset.label_mapping
        self.classes_dict = dataset.class_mapping
        self.config = config
        self.scaler = scaler
        self.probabilistic = probabilistic

    def trainClassifier(self, feature_dataset, config):
        return ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=True)

    def get_raw_classes(self):
        return self.trained_classifier.classes_

    def predictClasses(self, feature_dataset):
        predicted = self.trained_classifier.predict(feature_dataset.data)
        return [self.classes_list[p] for p in predicted]

    def predictProbabilities(self, feature_dataset, top_n=None):
        predicted = self.trained_classifier.predict_proba(feature_dataset.data)

        all_confidences = []
        n_samples = predicted.shape[0]
        for idx in range(n_samples):
            scores = sorted([(predicted[idx, k], k) for k in range(predicted.shape[1])], reverse=True)

            tempo_classes = self.trained_classifier.classes_
            n_classes = len(tempo_classes)
            if top_n is None or top_n > n_classes:
                top_n = n_classes

            confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(top_n)]

            all_confidences.append(confidences)

        return all_confidences

    def predictSymbolClass(self, feature_vector):
        predicted = self.trained_classifier.predict(feature_vector)

        return self.classes_list[predicted[0]]

    def predictSymbolProbabilities(self, feature_vector, top_n):
        try:
            predicted = self.trained_classifier.predict_proba(feature_vector)
        except:
            raise Exception("Classifier was not trained as probabilistic classifier")

        scores = sorted([(predicted[0, k], k) for k in range(predicted.shape[1])], reverse=True)

        tempo_classes = self.trained_classifier.classes_
        n_classes = len(tempo_classes)
        if top_n is None or top_n > n_classes:
            top_n = n_classes

        confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(top_n)]

        return confidences

    def classifySymbols(self, expressions, feature_dataset):
        predicted_classes = self.predictClasses(feature_dataset)
        #
        #dont call predicatedClasses
        #
        #

        for i in range(len(predicted_classes)):
            label = predicted_classes[i]
            exprId, symId = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.node[symId][NODE_ATTRIBUTES.PREDICTED_CLASS] = label

    def findSymbolProbabilities(self, expressions, feature_dataset, top_n=None):
        predicted_probs = self.predictProbabilities(feature_dataset, top_n)

        for i in range(len(predicted_probs)):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1]
            exprId, symId = feature_dataset.sources[i]
            graph = expressions[exprId]
            graph.node[symId]['classes'] = classes
            graph.node[symId]['scores'] = probs

    def operate(self, expressions, feature_dataset, *args):
        self.classifySymbols(expressions, feature_dataset)
