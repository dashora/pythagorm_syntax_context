import numpy as np
import networkx as nx
import networkx.algorithms as alg

from recognizer.training.trainer import ClassifierTrainer
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

class SymbolParser:

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        self.type = type
        self.trained_classifier = self.trainClassifier(dataset, config)
        self.classes_list = dataset.label_mapping
        self.classes_dict = dataset.class_mapping
        self.config = config
        self.scaler = scaler
        self.probabilistic = probabilistic

    def trainClassifier(self, feature_dataset, config):
        return ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=True)

    def get_raw_classes(self):
        return self.trained_classifier.classes_

    def predictRelations(self, feature_dataset):
        predicted = self.trained_classifier.predict(feature_dataset.data)
        return [self.classes_list[p] for p in predicted]

    def predictProbabilities(self, feature_dataset):
        predicted = self.trained_classifier.predict_proba(feature_dataset.data)

        all_confidences = []
        n_samples = predicted.shape[0]
        for idx in range(n_samples):
            scores = sorted([(predicted[idx, k], k) for k in range(predicted.shape[1])], reverse=True)

            tempo_classes = self.trained_classifier.classes_
            n_classes = len(tempo_classes)

            confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]

            all_confidences.append(confidences)

        return all_confidences

    def predictRelationClass(self, feature_vector):
        predicted = self.trained_classifier.predict(feature_vector)

        return self.classes_list[predicted[0]]

    def predictRelationProbabilities(self, feature_vector):
        try:
            predicted = self.trained_classifier.predict_proba(feature_vector)
        except:
            raise Exception("Classifier was not trained as probabilistic classifier")

        scores = sorted([(predicted[0, k], k) for k in range(predicted.shape[1])], reverse=True)

        tempo_classes = self.trained_classifier.classes_
        n_classes = len(tempo_classes)

        confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]

        return confidences

    def classifyRelations(self, expressions, feature_dataset):
        predicted_classes = self.predictRelations(feature_dataset)

        for i in range(len(predicted_classes)):
            label = predicted_classes[i]
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.edge[pid][cid][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] = label

    def findRelationProbabilities(self, expressions, feature_dataset):
        predicted_probs = self.predictProbabilities(feature_dataset)

        for i in range(len(predicted_probs)):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1].astype(float)
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.edge[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES] = classes
            graph.edge[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES] = probs

    def setWeights(self, expr, useGram=False):
        cls_index = {}

        classes = [self.classes_list[c] for c in self.trained_classifier.classes_]
        graph = expr.expressionGraph
        for pid,cid,data in graph.edges(data=True):
            cls_index[(pid,cid)] = 0
            if data[EDGE_ATTRIBUTES.POSSIBLE_CLASSES][0] == "NoRelation":
                    cls_index[(pid,cid)] += 1
            if cls_index[(pid,cid)] > len(classes) or data[EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid,cid)]] == 0:
                graph.remove_edge(pid,cid)
                del cls_index[(pid,cid)]
        if useGram:
            for pid in graph.nodes():
                #print("Parent: " + str(pid))
                children = graph.neighbors(pid)
                child_cls = [graph.edge[pid][c][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][cls_index[(pid,c)]] for c in children]
                while len(child_cls) != len(set(child_cls)):
                    #print("Child Classes: " + str(child_cls))
                    for cls in classes:
                        #print("Class: " + str(cls))
                        ele = [i for i in range(len(child_cls)) if i < len(classes) and child_cls[i] == cls]
                        #print("Elements: " + str(ele))
                        if len(ele) < 2:
                            continue
                        max_ele = ele[0]
                        max_score = graph.edge[pid][children[max_ele]][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid,children[max_ele])]]
                        for e in ele[1:]:
                            if graph.edge[pid][children[e]][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid,children[e])]] > max_score:
                                #print("Increment " + str(max_ele))
                                cls_index[(pid,children[max_ele])] += 1
                                max_ele = e
                                max_score = graph.edge[pid][children[e]][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][cls_index[(pid,children[e])]]
                            else:
                                #print("Increment " + str(e))
                                cls_index[(pid,children[e])] += 1
                    for cid in children:
                        index = cls_index[(pid,cid)]
                        if index < len(classes) and graph[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][index] == "NoRelation":
                            cls_index[(pid, cid)] += 1
                            index += 1
                        if index >= len(classes) or graph.edge[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][index] == 0:
                            del cls_index[(pid,cid)]
                            graph.remove_edge(pid,cid)

                    children = graph.neighbors(pid)
                    child_cls = [graph.edge[pid][c][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][cls_index[(pid,c)]] for c in children]
                    #print("Child Classes: " + str(child_cls))
        for (pid,cid),index in cls_index.items():
            graph.edge[pid][cid]["weight"] = graph.edge[pid][cid][EDGE_ATTRIBUTES.CLASS_CONFIDENCES][index]
           # centerP = graph.node[pid][NODE_ATTRIBUTES.TRACE_SET].eye
            #print(type(centerP))
           # centerC = graph.node[cid][NODE_ATTRIBUTES.TRACE_SET].eye
           # dist = ((centerP[0]-centerC[0])**2 + (centerP[1]-centerC[1])**2)**(0.5)
            #graph.edge[pid][cid]["weight"] = dist
            graph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = graph.edge[pid][cid][EDGE_ATTRIBUTES.POSSIBLE_CLASSES][index]
        expr.expressionGraph = graph

    def parse(self, expressions, feature_dataset, extractTree=True, useGram=False):
        # get relation scores for the edges
        self.findRelationProbabilities(expressions, feature_dataset)

        # Make highest score the weight of each edge and record its class
        i = 0
        for exprId, expr in expressions.items():
            self.setWeights(expr, useGram)
            if extractTree:
                # Perform Edmonds' algorithm and convert result to symbol graph
                try:
               #     mst = alg.prim_mst((expr.expressionGraph).to_undirected())
                    mst = alg.maximum_spanning_arborescence(expr.expressionGraph, attr='weight', default=0.0)
                except nx.exception.NetworkXException:
                    print("Exception at prims"+exprId)
                    mst = expr.expressionGraph

                for n, data in expr.expressionGraph.nodes(data=True):
                    mst.add_node(n,data)
                for pid,cid,data in expr.expressionGraph.edges(data=True):
                    if mst.has_edge(pid,cid):
                        mst.add_edge(pid,cid,data)
                expr.expressionGraph = mst
            i += 1
            print(" Parsing: " + str(((i + 1) * 100) // len(expressions)) + "%", end="\r")

    def operate(self, expressions, feature_dataset, args):
        extractTree = args[0]
        if len(args) > 1:
            useGram = args[1]
        else:
            useGram = False
        self.parse(expressions, feature_dataset, extractTree, useGram)






