import numpy as np
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_trace_set import MathSymbolTraceSet
from recognizer.preprocessing.trace_smoother import TraceSmoother

class SymbolClassifier:
    TypeRandomForest = 1
    TypeSVMLIN = 2
    TypeSVMRBF = 3
    TypeKNN = 4

    def __init__(self, type, trained_classifier, classes_list, classes_dict, config, scaler=None, probabilistic=False):
        self.type = type
        self.trained_classifier = trained_classifier
        self.classes_list = classes_list
        self.classes_dict = classes_dict
        self.config = config
        self.scaler = scaler
        self.probabilistic = probabilistic

    def predict(self, dataset):
        return self.trained_classifier.predict(dataset)

    def predict_proba(self, dataset, top_n):
        predicted = self.trained_classifier.predict_proba(dataset)

        all_confidences = []
        n_samples = predicted.shape[0]
        for idx in range(n_samples):
            scores = sorted([(predicted[idx, k], k) for k in range(predicted.shape[1])], reverse=True)

            tempo_classes = self.trained_classifier.classes_
            n_classes = len(tempo_classes)
            if top_n is None or top_n > n_classes:
                top_n = n_classes

            confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(top_n)]

            all_confidences.append(confidences)

        return all_confidences

    def get_raw_classes(self):
        return self.trained_classifier.classes_

    def get_symbol_from_points(self, points_lists):

        traces = []
        for trace_id, point_list in enumerate(points_lists):
            object_trace = MathSymbolTrace(trace_id, point_list)

            traces.append(object_trace)

            # apply general trace pre processing...
            TraceSmoother.applyPreprocessing(object_trace)

        new_symbol = MathSymbolTraceSet(0, traces, '{Unknown}')

        # normalize size and locations
        new_symbol.normalize()

        return new_symbol

    def get_symbol_features(self, symbol):
        # get raw features
        feature_extractor = FeatureExtractor(self.config)
        features = feature_extractor.getFeatures(symbol)

        # put them in numpy format
        mat_features = np.mat(features, dtype=np.float64)

        # automatically transform features
        if self.scaler is not None:
            mat_features = self.scaler.transform(mat_features)

        return mat_features

    def classify_points(self, points_lists):
        symbol = self.get_symbol_from_points(points_lists)

        return self.classify_symbol(symbol)

    def classify_points_prob(self, points_lists, top_n=None):
        symbol = self.get_symbol_from_points(points_lists)

        return self.classify_symbol_prob(symbol, top_n)

    def classify_symbol(self, symbol):
        features = self.get_symbol_features(symbol)

        predicted = self.trained_classifier.predict(features)

        return self.classes_list[predicted[0]]

    def classify_symbol_prob(self, symbol, top_n=None):
        features = self.get_symbol_features(symbol)

        try:
            predicted = self.trained_classifier.predict_proba(features)
        except:
            raise Exception("Classifier was not trained as probabilistic classifier")

        scores = sorted([(predicted[0, k], k) for k in range(predicted.shape[1])], reverse=True)

        tempo_classes = self.trained_classifier.classes_
        n_classes = len(tempo_classes)
        if top_n is None or top_n > n_classes:
            top_n = n_classes

        confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(top_n)]

        return confidences

