
import numpy as np
import networkx as nx
import networkx.algorithms as alg

from recognizer.training.trainer import ClassifierTrainer
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

class Segmenter:

    def __init__(self, type, dataset, config, scaler=None, probabilistic=False):
        self.type = type
        self.trained_classifier = self.trainClassifier(dataset, config)
        self.classes_list = dataset.label_mapping
        self.classes_dict = dataset.class_mapping
        self.config = config
        self.scaler = scaler
        self.probabilistic = probabilistic

    def trainClassifier(self, feature_dataset, config):
        return ClassifierTrainer.train_classifier(feature_dataset, config, get_raw_classifier=True)

    def get_raw_classes(self):
        return self.trained_classifier.classes_

    def predictRelations(self, feature_dataset):
        predicted = self.trained_classifier.predict(feature_dataset.data)
        return [self.classes_list[p] for p in predicted]

    def predictProbabilities(self, feature_dataset):
        predicted = self.trained_classifier.predict_proba(feature_dataset.data)

        all_confidences = []
        n_samples = predicted.shape[0]
        for idx in range(n_samples):
            scores = sorted([(predicted[idx, k], k) for k in range(predicted.shape[1])], reverse=True)
            tempo_classes = self.trained_classifier.classes_
            n_classes = len(tempo_classes)
            confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]
            all_confidences.append(confidences)

        return all_confidences

    def predictRelationClass(self, feature_vector):
        predicted = self.trained_classifier.predict(feature_vector)
        return self.classes_list[predicted[0]]

    def predictRelationProbabilities(self, feature_vector):
        try:
            predicted = self.trained_classifier.predict_proba(feature_vector)
        except:
            raise Exception("Classifier was not trained as probabilistic classifier")

        scores = sorted([(predicted[0, k], k) for k in range(predicted.shape[1])], reverse=True)

        tempo_classes = self.trained_classifier.classes_
        n_classes = len(tempo_classes)

        confidences = [(self.classes_list[tempo_classes[scores[k][1]]], scores[k][0]) for k in range(n_classes)]

        return confidences

    def classifyRelations(self, expressions, feature_dataset):
        predicted_classes = self.predictRelations(feature_dataset)

        for i in range(len(predicted_classes)):
            label = predicted_classes[i]
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.edge[pid][cid][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] = label

    def findRelationProbabilities(self, expressions, feature_dataset):
        predicted_probs = self.predictProbabilities(feature_dataset)

        for i, prob in enumerate(predicted_probs):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1]
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.edge[pid][cid]['classes'] = classes
            graph.edge[pid][cid]['scores'] = probs

    def segment(self, expressions, feature_dataset, convert=True):
        # get relation scores for the edges
        self.findRelationProbabilities(expressions, feature_dataset)

        # Make highest score the weight of each edge and record its class
        for exprId, expr in expressions.items():
            for pid,cid in expr.expressionGraph.edges():
                expr.expressionGraph.edge[pid][cid]["weight"] = expr.expressionGraph.edge[pid][cid]["scores"][0]
                expr.expressionGraph.edge[pid][cid][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] = expr.expressionGraph.edge[pid][cid]['classes'][0]
        # Perform Edmonds' algorithm and convert result to symbol graph
        if convert:
            for exprId, expr in expressions.items():
                expr.convertSymbolLevel()
                expressions[exprId] = expr


    # def merge_strokes_to_symbols(self, expressions):
    #     '''
    #     Merges the strokes in the expression set to symbols based on ground truth information
    #     expressions are converted back to primitive form and existing graph structure is removed
    #     '''
    #     for exprId, expr in expressions.items():
    #         expr.convertSymbolLevel(fromGroundTruth=True)

    def operate(self, expressions, feature_dataset, *args):
        convert = args[0]
        self.segment(expressions, feature_dataset, convert)
