import numpy as np
import networkx as nx
import networkx.algorithms as alg

from recognizer.recognition.expert import Expert
from recognizer.training.trainer import ClassifierTrainer
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

class AddPuncuation(Expert):

    def findRelationProbabilities(self, expressions, feature_dataset):
        predicted_probs = self.predictProbabilities(feature_dataset)

        for i in range(len(predicted_probs)):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1].astype(float)
            exprId, pid, cid = feature_dataset.sources[i]
            graph = expressions[exprId].filteredGraph
            graph.edge[pid][cid]['classes'] = classes
            graph.edge[pid][cid]['scores'] = probs

    def getChildRelation(self, expression, nid):
        if nid not in expression.expressionGraph.nodes():
            return "NoRelation", nid
        pred_list = expression.expressionGraph.predecessors(nid)
        if len(pred_list) > 0:
            pred = pred_list[0]
        else:
            return "NoRelation", nid
        rel = expression.expressionGraph.edge[pred][nid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
        return rel, pred

    def findBestChild(self, expressions, feature_dataset):
        predicted_probs = self.predictProbabilities(feature_dataset)
        bestChild = {}
        for i in range(len(predicted_probs)):
            classes = np.array(predicted_probs[i])[:,0]
            probs = np.array(predicted_probs[i])[:,1].astype(float)
            if classes[0] != "NoRelation":
                score = probs[0]
                target = classes[0]
            else:
                score = probs[1]
                target = classes[1]
            exprId, pid, cid = feature_dataset.sources[i]
            rel, pred = self.getChildRelation(expressions[exprId], cid)
            if rel == target and ((exprId, pid) not in bestChild or score > bestChild[(exprId, pid)][2]):
                bestChild[(exprId, pid)] = (cid, pred, score)
        return bestChild

    def insertPunc(self, expressions, mapToRel):
        mapFromRel = {}
        # reverse mapping to get list of punctuation to insert into each horizontal relation
        for (exprId, pid), (cid, pred, score) in mapToRel.items():
            if (exprId, pred, cid) in mapFromRel:
                mapFromRel[(exprId, pred, cid)].append(pid)
            else:
                mapFromRel[(exprId, pred, cid)] = [pid]

        # sort the punctuation from left to right by location of eye
        for k in mapFromRel.keys():
            mapFromRel[k].sort(key = lambda x: expressions[k[0]].componentMap[x].eye[0])

        # insert punctuation into expression graph
        for (exprId, pred, cid), psyms in mapFromRel.items():
            expr = expressions[exprId]
            graph = expr.expressionGraph
            data = graph[pred][cid]
            graph.remove_edge(pred, cid)
            for nid in psyms:
                graph.add_node(nid,expr.filteredGraph.node[nid])
            graph.add_edge(pred, psyms[0], data)
            for p in range(1, len(psyms)):
                graph.add_edge(psyms[p-1], psyms[p], data)
            graph.add_edge(psyms[-1], cid, data)
            expressions[exprId].expressionGraph = graph


    def addPuncuation(self, expressions, feature_dataset):
        bestChildren = self.findBestChild(expressions, feature_dataset)
        self.insertPunc(expressions, bestChildren)

    def operate(self, expressions, feature_dataset, args):
        self.addPuncuation(expressions, feature_dataset)
