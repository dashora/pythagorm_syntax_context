from enum import Enum
from concurrent.futures import ProcessPoolExecutor
import networkx as nx
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES, GraphConstruction

class TYPES_DATA_SET(Enum):
    Symbol = "symbol"
    Relation = "relation"
    Segmentation = "segment"
    Punctuation = 'punctuation'

class DatasetBuilder:

    def __init__(self, data_set_type, feature_extractor):
        self.dataset_type = data_set_type
        self.feature_Extractor = feature_extractor

    def create_dataset_from_nodes(self, expression):
        '''
        Dataset extraction method for classification of symbols, stroke-labelling, class prediction (letter, digit etc)
        :param layout_graph: the expression represented as layout-graph (this can be either strokes or symbols depending on data-set type)
        :param stroke_set_map: a map that contains
        :return: features, labels and source
        '''
        #print(expression.normalized, end="\r")
        layout_graph = expression.expressionGraph
        stroke_set_map = expression.componentMap
        features = []
        source = []
        label = []
        for stroke_set_id in layout_graph.nodes():
            features.append(self.feature_Extractor.getSymbolFeatures(stroke_set_map[stroke_set_id]))
            source.append([expression.id, stroke_set_id])
            label.append(layout_graph.node[stroke_set_id][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS])
        return (features, label, source)

    def create_dataset_from_edges(self, expression, segRel=False, filtered=False):
        #print(expression.normalized, end="\r")
        if filtered:
            layout_graph = expression.filteredGraph
        else:
            layout_graph = expression.expressionGraph
        features = []
        sources = []
        labels = []

        if expression.normalized:
            expression.setUnnormalized()
        if expression.edgeTraces:
            for edge in expression.edgeTraces:
                edge.setTrace()

        for pid, cid in layout_graph.edges():
            features.append(self.feature_Extractor.getRelationFeatures(expression, pid, cid))
            sources.append([expression.id, pid, cid])
            if segRel and layout_graph.edge[pid][cid][EDGE_ATTRIBUTES.SAME_SYMBOL]:
                labels.append("MERGE")
            elif segRel and not layout_graph.edge[pid][cid][EDGE_ATTRIBUTES.SAME_SYMBOL]:
                labels.append("SPLIT")
            else:
                labels.append(layout_graph.edge[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION])

        expression.unnormalizedComponents = None
        if expression.edgeTraces:
            for edge in expression.edgeTraces:
                edge.trace = None

        return features, labels, sources

    def process_layout_graph(self, expression):
        # process the nodes in the graph or edges in the graph based on dataset type
        if self.dataset_type == TYPES_DATA_SET.Symbol:
            return self.create_dataset_from_nodes(expression)
        elif self.dataset_type == TYPES_DATA_SET.Relation:
            return self.create_dataset_from_edges(expression)
        elif self.dataset_type == TYPES_DATA_SET.Segmentation:
            return self.create_dataset_from_edges(expression, segRel=True)
        elif self.dataset_type == TYPES_DATA_SET.Punctuation:
            return self.create_dataset_from_edges(expression, filtered=True)
        else:
            raise Exception("Invalid feature dataset type.", str(self.dataset_type))


    def create_dataset_for_expressions(self, expression_list, label_mapping, class_mapping, workers=4):
        raw_features = []
        raw_labels = []
        raw_source_info = []
        print("")

        # dataBundles = []
        # for expr in expression_list:
        #     dataBundles.append((expr,self.dataset_type,self.feature_Extractor.config))

        #get the raw_features by processing parallels- expression by expression
        total_number_expressions = len(expression_list)
        with ProcessPoolExecutor(max_workers=workers) as executor:
            mapper = executor.map
            for i,data_set in enumerate(mapper(self.process_layout_graph, expression_list)):
                advance = float(i)/total_number_expressions
                print("Getting Data-set for => {:.2%} => ".format(advance), end="\r")
                features, labels, sources = data_set
                raw_features += features
                raw_labels += labels
                raw_source_info += sources

            #create a dataset from the raw features and info
        dataset = FeatureDataset.from_INKML_data(raw_features, raw_labels, label_mapping=label_mapping,class_mapping=class_mapping, dataset_type=self.dataset_type, sources=raw_source_info, config=None)
        print("\n\nCreated dataset with...", dataset.num_attributes(), "Features")
        return dataset