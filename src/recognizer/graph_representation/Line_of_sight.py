import numpy as np
from scipy.spatial.distance import cdist
import math
from scipy.spatial import ConvexHull
import sys
from recognizer.graph_representation.Graph_construction import GraphConstruction,  NODE_ATTRIBUTES, EDGE_ATTRIBUTES
from scipy.spatial.distance import cdist

class Line_of_sight:

    def get_project_angle_range_360_CH(self, eye, mins, maxs, project_stroke_points):
        other_stroke_points = (project_stroke_points)
        stroke_points = np.array(other_stroke_points)
        convex_hull = ConvexHull(stroke_points)
        convex_hull_vertices = convex_hull.vertices
        convex_hull_points = []
        convex_hull_points_x = []
        convex_hull_points_y = []

        for one_point_index in convex_hull_vertices:
            convex_hull_points.append(list(stroke_points[one_point_index]))
            convex_hull_points_x.append(stroke_points[one_point_index][0])
            convex_hull_points_y.append(stroke_points[one_point_index][1])

        convex_hull_mins = (min(convex_hull_points_x), min(convex_hull_points_y))
        convex_hull_maxs = (max(convex_hull_points_x), max(convex_hull_points_y))

        if mins != convex_hull_mins or maxs != convex_hull_maxs:
            sys.exit("mins, maxs and convex_hull_mins, convex_hull_maxs are not the same")

        convex_hull_points_angle = []
        top_points_angle = []
        bottom_points_angle = []
        for i in range(len(convex_hull_points)):
            one_angle = self.get_angle_360(convex_hull_points[i], eye)
            convex_hull_points_angle.append(one_angle)
            if one_angle < math.pi and one_angle >= 0:
                top_points_angle.append(one_angle)
            elif one_angle >= math.pi and one_angle < 2 * math.pi:
                bottom_points_angle.append(one_angle)

        ## for the stroke whose leftmost point is right to the current stroke and its vertical span contains 0
        ## which means it across the right top and right bottom, so its range has two interval because the
        ## initial range is from [0, 2*math.pi]
        if mins[0] >= eye[0] and mins[1] < eye[1] and maxs[1] > eye[1]:
            top_angle = max(top_points_angle)
            bottom_angle = min(bottom_points_angle)
            return [[0, top_angle], [bottom_angle, 2 * math.pi]]
        elif mins[0] >= eye[0] and mins[1] < eye[1] and maxs[1] == eye[1]:
            bottom_angle = min(bottom_points_angle)
            return [[bottom_angle, 2 * math.pi]]
        else:
            low_bound = min(convex_hull_points_angle)
            high_bound = max(convex_hull_points_angle)

            if low_bound > high_bound:
                sys.exit("low_bound is larger than high_bound, bug?")

            return [[low_bound, high_bound]]

    ## dot product
    def dotproduct(self,v1, v2):
        return sum((a * b) for a, b in zip(v1, v2))

    ## module of vector
    def length(self, v):
        return math.sqrt(self.dotproduct(v, v))

    ## angle between two vectors
    def angle(self, v1, v2):
        if (self.length(v1) * self.length(v2)):

            if self.dotproduct(v1, v2) / (self.length(v1) * self.length(v2)) >= 1.0:
                return math.acos(1.0)
            elif self.dotproduct(v1, v2) / (self.length(v1) * self.length(v2)) <= -1.0:
                return math.acos(-1.0)
            else:
                return math.acos(self.dotproduct(v1, v2) / (self.length(v1) * self.length(v2)))
        else:
            return 0


    def get_angle_360(self, point, eye):
        vector = [point[0] - eye[0], point[1] - eye[1]]
        ##        print "vector", vector
        horizontal_vector = [1, 0]
        absolute_angle = self.angle(vector, horizontal_vector)
        if vector[1] >= 0:
            the_angle = absolute_angle
        else:
            the_angle = 2 * math.pi - absolute_angle

        return the_angle

    def get_overlap(self, a, b):
        return max(0, min(a[1], b[1]) - max(a[0], b[0]))



    def get_project_angle_range_360(self, eye, mins, maxs):
        point_1 = [mins[0], mins[1]]
        point_2 = [maxs[0], mins[1]]
        point_3 = [mins[0], maxs[1]]
        point_4 = [maxs[0], maxs[1]]

        angle_1 = self.get_angle_360(point_1, eye)
        angle_2 = self.get_angle_360(point_2, eye)
        angle_3 = self.get_angle_360(point_3, eye)
        angle_4 = self.get_angle_360(point_4, eye)

        BBC_X = (mins[0] + maxs[0]) / 2
        ## for the stroke whose bounding box is right to the current stroke and its vertical span contains 0
        ## which means it across the right top and right bottom, so its range has two interval because the
        ## initial range is from [0, 2*math.pi]

        ## for the stroke whose leftmost point is right to the current stroke and its vertical span contains 0
        ## which means it across the right top and right bottom, so its range has two interval because the
        ## initial range is from [0, 2*math.pi]
        if mins[0] >= eye[0] and mins[1] < eye[1] and maxs[1] > eye[1]:
            ##                print "two interval"
            top_angle = max([angle_3, angle_4])
            bottom_angle = min([angle_1, angle_2])
            return [[0, top_angle], [bottom_angle, 2 * math.pi]]
        else:
            low_bound = min([angle_1, angle_2, angle_3, angle_4])
            high_bound = max([angle_1, angle_2, angle_3, angle_4])

            if low_bound > high_bound:
                sys.exit("low_bound is larger than high_bound, bug?")

            return [[low_bound, high_bound]]

    ## get the total span of the angle range list
    def get_angle_range_list_span(self, current_angle_range_list):
        angle_span = 0.0
        angle_range_num = len(current_angle_range_list)
        for i in range(angle_range_num):
            one_angle_span = current_angle_range_list[i][1] - current_angle_range_list[i][0]
            if one_angle_span < 0:
                sys.exit("span is negative, bug?")
            angle_span += one_angle_span

    def get_reduce_overlap_list(self, angle_range, project_angle_range, overlap):
        reduce_overlap_list = []
        span_1 = angle_range[1] - angle_range[0]
        span_2 = project_angle_range[1] - project_angle_range[0]
        if overlap == span_1:
            return reduce_overlap_list
        elif overlap == span_2:
            range_1 = [angle_range[0], project_angle_range[0]]
            range_2 = [project_angle_range[1], angle_range[1]]
            reduce_overlap_list.append(range_1)
            reduce_overlap_list.append(range_2)
        elif angle_range[0] > project_angle_range[0]:
            range_1 = [project_angle_range[1], angle_range[1]]
            reduce_overlap_list.append(range_1)
        elif angle_range[0] < project_angle_range[0]:
            range_1 = [angle_range[0], project_angle_range[0]]
            reduce_overlap_list.append(range_1)

        return reduce_overlap_list


    def get_in_the_sight(self, current_angle_range_list, project_angle_range):
        ## this means the project_angle_range is a single value (one light line)
        if project_angle_range[0] == project_angle_range[1]:
            in_the_sight = False
            for angle_range in current_angle_range_list:
                if project_angle_range[0] >= angle_range[0] and project_angle_range[0] <= angle_range[1]:
                    in_the_sight = True
                    return True, current_angle_range_list

            if not in_the_sight:
                return False, current_angle_range_list

        new_list = []
        angle_range_num = len(current_angle_range_list)
        in_the_sight = False
        for i in range(angle_range_num):
            angle_range = current_angle_range_list[i]
            overlap = self.get_overlap(angle_range, project_angle_range)
            if overlap == 0:
                new_list.append(angle_range)
            elif overlap > 0:
                in_the_sight = True
                reduce_overlap_list = self.get_reduce_overlap_list(angle_range, project_angle_range, overlap)
                new_list = new_list + reduce_overlap_list

        return in_the_sight, new_list

    def get_bidirectional_edge_set(self, undirected_edge_set):
        bidirectional_edge_set = set()
        for edge in undirected_edge_set:
            bidirectional_edge_set.add(edge)
            bidirectional_edge_set.add((list(edge)[1], list(edge)[0]))
        return bidirectional_edge_set

    def get_LOS_edges(self, eq):
        LOS_edge_set = set()
        LOS_edge_ids_set= set()
        #initialize the distance between strokes

        List_stroke_set = list(eq.componentMap.values())
        stroke_num = len(List_stroke_set)
        distance_mat = [[0.0 for x in range(stroke_num)] for x in range(stroke_num)]
        for j in range(stroke_num):
            for k in range(stroke_num):
                if k > j:
                    stroke1 = List_stroke_set[j]
                    stroke2 = List_stroke_set[k]
                    distance_mat[j][k] = distance_mat[k][j] = stroke1.closest_distance(stroke2)

        for i in range(stroke_num):
            current_stroke_index = i
            ## s is the distance list for the current stroke
            s = distance_mat[i]
            sorted_index = sorted(range(len(s)), key=lambda k: s[k])

            initial_angle_range = [0, 2 * math.pi]
            eye = List_stroke_set[current_stroke_index].eye
            current_angle_range_list = [initial_angle_range]

            for j in range(stroke_num):
                project_stroke_index = sorted_index[j]
                if current_stroke_index == project_stroke_index:
                    continue
                else:
                    mins = (List_stroke_set[project_stroke_index].minX, List_stroke_set[project_stroke_index].minY)
                    maxs = (List_stroke_set[project_stroke_index].maxX, List_stroke_set[project_stroke_index].maxY)
                    try:
                        project_angle_range_list = self.get_project_angle_range_360_CH(eye, mins, maxs, eq.getSymbolPoints(List_stroke_set[project_stroke_index].getId()))
                    except:
                        print("QhullError or ValueError")
                        project_angle_range_list = self.get_project_angle_range_360(eye, mins, maxs)

                    angle_range_list_span = self.get_angle_range_list_span(current_angle_range_list)
                    if angle_range_list_span == 0:  ## all the line of sight is blocked
                        break
                    else:
                        for project_angle_range in project_angle_range_list:
                            in_the_sight, next_angle_range_list = self.get_in_the_sight(current_angle_range_list,
                                                                                   project_angle_range)
                            if in_the_sight:
                                LOS_edge_set.add((current_stroke_index, project_stroke_index))
                                angle = ""
                                for project_angle in project_angle_range_list:
                                    angle += str(project_angle[0] * 180 / math.pi) + "_" + str(
                                        project_angle[1] * 180 / math.pi) + ","
                                LOS_edge_ids_set.add(
                                    (List_stroke_set[current_stroke_index].id, List_stroke_set[project_stroke_index].id, angle))
                                current_angle_range_list = next_angle_range_list

        bidirectional_edge_set = self.get_bidirectional_edge_set(LOS_edge_set)
        return bidirectional_edge_set, LOS_edge_ids_set

    def get_line_of_sight_graph(self, expression):
        List_stroke_set = list(expression.componentMap.values())
        trace_id_to_symbol_id = expression.traceSymbols
        symbol_id_label = expression.symbolLabels

        expression_graph, node_ids = GraphConstruction.create_nodes(List_stroke_set)

        _, edge_set = self.get_LOS_edges(expression)
        for parent, child, angle in edge_set:
            expression_graph.add_edge(parent,child, overLap=angle)

        expression_graph = GraphConstruction.add_edge_attributes(expression_graph)
        expression_graph = GraphConstruction.add_node_attributes(expression_graph)

        for parent_id,child_id in expression_graph.edges():
            edge_relation = expression.getComponentsRelation(parent_id, child_id)
            #avoid self loops
            if parent_id == child_id:
                continue
            #set the appropriate edge relation in the graph
            expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = edge_relation
            if edge_relation != "NoRelation" and parent_id in trace_id_to_symbol_id and child_id in trace_id_to_symbol_id and trace_id_to_symbol_id[parent_id] == trace_id_to_symbol_id[child_id]:
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.SAME_SYMBOL] = 1.0
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = "MERGE"
            elif edge_relation != "NoRelation":
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.IS_RELATION] = 1.0

        # iterate through the trace_sets and set attributes for every trace_set in layout graph
        for traceID, trace_set_object in expression.componentMap.items():
            expression_graph.node[traceID][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] = symbol_id_label[traceID]
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_SET] = trace_set_object
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_LIST] = expression.getComponentSegments(traceID)


        return expression_graph


    @staticmethod
    def generateExpressionGraphs(expressions):
        los_generation = Line_of_sight()
        for exp in expressions.values():
            los_graph= los_generation.get_line_of_sight_graph(exp)
            exp.expressionGraph = los_graph




