import networkx as nx
import matplotlib.pyplot as plt
from enum import Enum
import itertools
import numpy as np
from scipy.spatial.distance import cdist
from intervaltree import IntervalTree
from scipy.spatial import ConvexHull
from concurrent.futures import ProcessPoolExecutor
import sys

class EDGE_ATTRIBUTES(Enum):
    # ground truth values in graph
    GROUND_TRUTH_RELATION = 1
    IS_RELATION = 2
    SAME_SYMBOL = 3
    # predicted values in graph
    PREDICTED_LAYOUT = 4
    PREDICTED_SEGMENTATION = 5
    PREDICT_IS_LAYOUT = 6
    ANGULAR_RANGE_BLOCKED = 7
    POSSIBLE_CLASSES = 8
    CLASS_CONFIDENCES = 9


class NODE_ATTRIBUTES(Enum):
    # ground truth values in graph
    GROUND_TRUTH_CLASS = 1
    # predicted values in graph
    PREDICTED_CLASS = 2
    #Trace set object the node is pointing to
    TRACE_SET = 3
    # Trace ids for traces in symbol
    TRACE_LIST = 4


class TYPES_RELATION(Enum):
    RIGHT = 1
    SUP = 2
    SUB = 3
    INSIDE = 4
    BELOW = 5



class GraphConstruction:
    '''
    This class is  responsible for generating the symbol layout or stroke layout graph from the list of strokes/symbols and relation between them
    '''

    @staticmethod
    def add_edge_attributes(graph):
        #set edge attributes
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.IS_RELATION, 0.0)
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.SAME_SYMBOL, 0.0)
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.PREDICTED_LAYOUT, "NoRelation")
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION, 0)
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.PREDICT_IS_LAYOUT, 0)
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION, "NoRelation")
        nx.set_edge_attributes(graph, 'weight', 0.0)
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.ANGULAR_RANGE_BLOCKED, [0,0,0,0])
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.POSSIBLE_CLASSES, [])
        nx.set_edge_attributes(graph, EDGE_ATTRIBUTES.CLASS_CONFIDENCES, [])
        return graph

    @staticmethod
    def add_node_attributes(graph):
        # set node attributes
        nx.set_node_attributes(graph, NODE_ATTRIBUTES.GROUND_TRUTH_CLASS, "Unknown")
        nx.set_node_attributes(graph, NODE_ATTRIBUTES.PREDICTED_CLASS, "Unknown")
        nx.set_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET, None)
        nx.set_node_attributes(graph, NODE_ATTRIBUTES.TRACE_LIST, [])
        return graph

    @staticmethod
    def draw_graph(graph):
        '''
        Visualization function
        :param graph:
        :return: plot of graph
        '''
        nodes = graph.nodes()
        edges = graph.edges()

        node_attributes = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET)
        edge_attributes = nx.get_edge_attributes(graph, EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION)
        node_label_dictionary = {}
        for node in nodes:
            node_label_dictionary[node] = "Id: "+str(node)+" \n G_T: "+node_attributes[node].get_ground_truth()

        edge_label_dictionary = {}
        edge_list =[]
        for edge in edges:
            #ignore the edges that have no relation
            if not edge_attributes[edge] == 'NoRelation':
                edge_label_dictionary[edge] = edge_attributes[edge]
                edge_list.append(edge)
        #nx.draw_networkx(graph, pos=nx.shell_layout(graph), labels=node_label_dictionary, font_size=26, edgelist=edge_list, node_size=1200 )
        nx.draw_networkx(graph, pos=nx.shell_layout(graph), labels=node_label_dictionary, font_size=16, edgelist=edge_list, node_size=3000, node_color='c' )
        nx.draw_networkx_edge_labels(graph, pos=nx.shell_layout(graph), edge_labels=edge_label_dictionary)
        plt.show()

    @staticmethod
    def get_Maximum_Spanning_Tree(graph):
        try:
            #returns the maximum spanning tree using Edmonds' algorithm
            combinations= itertools.product([-1], graph.nodes())
            graph.add_edges_from(combinations, weight=sys.float_info.min)
            MaxSpanTree = nx.maximum_spanning_arborescence(graph)
            MaxSpanTree.remove_node(-1)
            graph.remove_node(-1)
        except:
            print("\nMaximum spanning tree could not be generated", graph.edges())
            return graph
        return MaxSpanTree

    @staticmethod
    def set_graph_attributes(graph, sub_graph):
        '''
        A sub-graph is created only with node-ids and edge. The attributes to be set
        :param graph: from which attributes have to be borrowed
        :param sub_graph: to which attributes have to be assigned
        :return:
        '''

        # create attribute space (strokeset and prediced labels) for the sub-graph
        nx.set_node_attributes(sub_graph, NODE_ATTRIBUTES.TRACE_SET, None)
        nx.set_node_attributes(graph, NODE_ATTRIBUTES.TRACE_LIST, [])
        nx.set_node_attributes(sub_graph, NODE_ATTRIBUTES.GROUND_TRUTH_CLASS, "NoSymbol")
        nx.set_node_attributes(sub_graph, NODE_ATTRIBUTES.PREDICTED_CLASS, "NoSymbol")

        #get the astrokesets and predicted labels from the graph
        graph_stroke_sets = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_SET)
        graph_predicted_labels = nx.get_node_attributes(graph, NODE_ATTRIBUTES.PREDICTED_CLASS)
        graph_trace_list = nx.get_node_attributes(graph, NODE_ATTRIBUTES.TRACE_LIST)
        graph_ground_truth = nx.get_node_attributes(graph, NODE_ATTRIBUTES.GROUND_TRUTH_CLASS)

        #for every node set the attribute values in the sub-graph
        sub_graph_nodes = sub_graph.nodes()
        for id in sub_graph_nodes:
            sub_graph.node[id][NODE_ATTRIBUTES.PREDICTED_CLASS]=graph_predicted_labels[id]
            sub_graph.node[id][NODE_ATTRIBUTES.TRACE_SET] = graph_stroke_sets[id]
            sub_graph.node[id][NODE_ATTRIBUTES.TRACE_LIST] = graph_trace_list[id]
            sub_graph.node[id][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]= graph_ground_truth[id]

        #create sub-graph attributes for the sub-graph
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.IS_RELATION, 0.0)
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.SAME_SYMBOL, 0.0)
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.PREDICTED_LAYOUT, "NoRelation")
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION, 0)
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.PREDICT_IS_LAYOUT, 0)
        nx.set_edge_attributes(sub_graph, EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION, "NoRelation")

        sub_graph_edges = sub_graph.edges()
        for edge in sub_graph_edges:
            start, end = edge
            sub_graph[start][end][EDGE_ATTRIBUTES.IS_RELATION] = graph[start][end][EDGE_ATTRIBUTES.IS_RELATION]
            sub_graph[start][end][EDGE_ATTRIBUTES.SAME_SYMBOL] = graph[start][end][EDGE_ATTRIBUTES.IS_RELATION]
            sub_graph[start][end][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] = graph[start][end][EDGE_ATTRIBUTES.PREDICTED_LAYOUT]
            sub_graph[start][end][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION] = graph[start][end][EDGE_ATTRIBUTES.PREDICTED_SEGMENTATION]
            sub_graph[start][end][EDGE_ATTRIBUTES.PREDICT_IS_LAYOUT] = graph[start][end][ EDGE_ATTRIBUTES.PREDICT_IS_LAYOUT]
            sub_graph[start][end][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = graph[start][end][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]


    @staticmethod
    def create_nodes(List_stroke_set):
        expression_graph = nx.DiGraph()

        # add each of the stroke_set as a node in the graph
        stroke_set_id_list = []
        for stroke_set in List_stroke_set:
            expression_graph.add_node(stroke_set.getId())
            stroke_set_id_list.append(stroke_set.getId())

        return expression_graph, stroke_set_id_list

    @staticmethod
    def filterPuncNodes(expressions, useGT=True):
        print("Filtering Nodes: useGT=" +str(useGT))
        filter = ["comma", "period", "ldots"]
        if useGT:
            class_label = NODE_ATTRIBUTES.GROUND_TRUTH_CLASS
        else:
            class_label = NODE_ATTRIBUTES.PREDICTED_CLASS

        for exprId, expr in expressions.items():
            graph = expr.expressionGraph
            filter_graph = nx.DiGraph([(u,v,d) for u,v,d in graph.edges(data=True) if graph.node[u][class_label] in filter])
            filter_graph.add_nodes_from([(n,d) for n,d in graph.nodes(data=True) if filter_graph.has_node(n)])
            expressions[exprId].filteredGraph = filter_graph
            for nid in graph.nodes():
                if graph.node[nid][class_label].lower() not in filter:
                    continue
                parent = [pid for pid in graph.predecessors(nid) if graph.edge[pid][nid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] != "NoRelation"]
                children = [cid for cid in graph.successors(nid) if graph.edge[nid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] != "NoRelation"]
                if parent and children and graph.has_edge(parent[0],children[0]):
                    graph.edge[parent[0]][children[0]][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = graph.edge[parent[0]][nid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
                graph.remove_node(nid)
            expressions[exprId].expressionGraph = graph

        return expressions

    @staticmethod
    def findPuncRelations(expressions, useGT=True):
        print("Filtering Relations: useGT=" +str(useGT))
        filter = ["comma", "period", "ldots"]
        if useGT:
            class_label = NODE_ATTRIBUTES.GROUND_TRUTH_CLASS
        else:
            class_label = NODE_ATTRIBUTES.PREDICTED_CLASS
        for exprId, expr in expressions.items():
            graph = expr.expressionGraph
            for pid,cid in graph.edges():
                if graph.node[pid][class_label] not in filter:
                    graph.remove_edge(pid,cid)
                    continue
                if graph.edge[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] == "NoRelation":
                    continue
                child = cid
                while child and graph.node[child][class_label] in filter:
                    children = [rid for rid in graph.successors(child) if graph.edge[child][rid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] != "NoRelation"]
                    if children:
                        child = children[0]
                    else:
                        child = None
                if child and graph.has_edge(pid,child):
                    rel = graph.edge[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION]
                    graph.edge[pid][cid][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = "NoRelation"
                    graph.edge[pid][child][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = rel

            expressions[exprId].expressionGraph = graph
            expressions[exprId].filteredGraph = graph
        return expressions

    @staticmethod
    def get_complete_graph(packet):
        expression, _, _ = packet
        '''
        Given the list of stroke_sets it process and finds a complete graph
        :param List_stroke_set: stokes that have to be present as nodes in graph
        :param edge_relations: layout or segment relations between the edges
        :return: layout graph
        '''

        List_stroke_set = list(expression.componentMap.values())
        trace_id_to_symbol_id = expression.traceSymbols
        symbol_labels = expression.symbolLabels

        expression_graph, node_ids = GraphConstruction.create_nodes(List_stroke_set)

        #add an edge for every two stroke(set) in the list - way to construct a complete graph
        edges = itertools.permutations(node_ids, 2)
        expression_graph.add_edges_from(edges)
        expression_graph = GraphConstruction.add_edge_attributes(expression_graph)
        expression_graph = GraphConstruction.add_node_attributes(expression_graph)

        #iterate through the relations and set attributes for every edge in layout graph
        for parent_id,child_id in expression_graph.edges():
            edge_relation = expression.getComponentsRelation(parent_id, child_id)
            #avoid self loops
            if parent_id == child_id:
                continue
            #set the appropriate edge relation in the graph
            expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = edge_relation
            if edge_relation != "NoRelation" and parent_id in trace_id_to_symbol_id and child_id in trace_id_to_symbol_id and trace_id_to_symbol_id[parent_id] == trace_id_to_symbol_id[child_id]:
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.SAME_SYMBOL] = 1.0
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = "MERGE"
            elif edge_relation != "NoRelation":
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.IS_RELATION] = 1.0

        # iterate through the trace_sets and set attributes for every trace_set in layout graph
        for traceID, trace_set_object in expression.componentMap.items():
            expression_graph.node[traceID][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] = expression.symbolLabels[traceID]
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_SET] = trace_set_object
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_LIST] = expression.getComponentSegments(traceID)

        return expression.id, expression_graph


    @staticmethod
    def get_line_of_sight_graph(packet):
        expression, find_punc, shrink = packet
        # print("\n")
        List_stroke_set = list(expression.componentMap.values())
        trace_id_to_symbol_id = expression.traceSymbols
        symbol_id_label = expression.symbolLabels

        expression_graph, node_ids = GraphConstruction.create_nodes(List_stroke_set)

        #compute the bounding box center of every stroke_Set to be eye
        stroke_set_eyes =[]
        stroke_set_size = []
        stroke_set_hull = []
        num_stroke_sets = len(List_stroke_set)

        #find stroke_set sizes
        for stroke_set in List_stroke_set:
            #find the bounding box center of the values
            minX, maxX, minY, maxY = stroke_set.original_box
            stroke_set_eyes.append(stroke_set.eye)
            stroke_set_size.append((maxX - minX) * (maxY-minY))
            all_points = np.array(expression.getSymbolPoints(stroke_set.id))
            if all_points.shape[0] > 3:
                try:
                    convexhull_vertices = ConvexHull(all_points, qhull_options='QJ Pp').vertices
                    hull_points = all_points[convexhull_vertices]

                except:
                    hull_points = all_points
            else:
                hull_points = all_points
            stroke_set_hull.append(hull_points)


        #computes the closet point distance from stroke-eye to every other stroke-point
        distance_between_Strokes = np.zeros((num_stroke_sets, num_stroke_sets))
        for i in range(num_stroke_sets):
            i_id = List_stroke_set[i].getId()
            for j in range(num_stroke_sets):
                j_id = List_stroke_set[j].getId()
                closet_pair_dist = np.min(
                    cdist(expression.getSymbolPoints(i_id), expression.getSymbolPoints(j_id)))
                distance_between_Strokes[i][j] = closet_pair_dist


        #the strategy is to process the near-by strokes first
        #set the processing order for every stroke by distance between strokes
        Angular_Range_Blocked= {}
        processing_order=[[] for i in range(num_stroke_sets)]
        for i in range(num_stroke_sets):
            processing_order[i] = np.argsort(distance_between_Strokes[i,:])

        # Angle_Range_noter = {}
        # Angle_Range_After_Noter = {}
        # ch ={}

        #compare stroke_set i with every other stroke set j in the processing order
        for i, current_strokeset in enumerate(List_stroke_set):
            AngleRange = IntervalTree()
            punc_AngleRange = IntervalTree()
            #set the visible angle range for the current stroke
            AngleRange.addi(0,360)
            punc_AngleRange.addi(290,360)
            #get the list of processing order for the current stroke
            #current processing order is the index of strokes ordered in
            current_procesing_order = processing_order[i]
            current_eye = current_strokeset.eye

            for j in range(num_stroke_sets):
                '''
                find angular range between eye of the current stroke_set and points in other stroke_sets
                '''
                other_stroke_index = current_procesing_order[j]
                if other_stroke_index == i:
                    continue
                other_stroke_set = List_stroke_set[other_stroke_index]
                all_points = np.array(expression.getSymbolPoints(other_stroke_set.getId()))
             

                # other_stroke_set = List_stroke_set[other_stroke_index]
                # all_points = np.array(expression.getSymbolPoints(other_stroke_set.getId()))
                # if all_points.shape[0] > 3:
                #     try:
                #         convexhull_vertices = ConvexHull(all_points, qhull_options='QJ Pp').vertices
                #         other_ss_points = all_points[convexhull_vertices]
                #
                #     except:
                #         other_ss_points = all_points
                # else:
                #     other_ss_points = all_points
                # ch[List_stroke_set[other_stroke_index].getId()] = other_ss_points
                #
                # other_stroke_set.setConvexHull(other_ss_points)
                other_ss_points = stroke_set_hull[other_stroke_index]

                #calculate the X and Y difference of every point from the current eye
                V1 = other_ss_points - current_eye
                # segregate the points for which the angular direction is different
                #if Y_other_point <  Y_current_eye, then consider the angle in the other direction
                below_direction_point_indices = V1[:, 1] > 0.0
                above_direction_point_indices = V1[:, 1] <= 0.0


                 # calculate the modulus
                modulus_V1 = np.linalg.norm(V1, axis=1)

                zero_angle_indices=(modulus_V1 ==0)
                angle_between=np.zeros(modulus_V1.shape)
                # calcuate the angle formed using cosine formula cosine_inverse(dot_product / product_of_moduli_two_vectors)
                angle_between[~zero_angle_indices] = np.rad2deg(np.arccos(V1[~zero_angle_indices,0]/modulus_V1[~zero_angle_indices]))


                #below angles range from 180 to 360 and above angles ranges from (0, 180)
                below_angles = 360 - angle_between[below_direction_point_indices]
                above_angles = angle_between[above_direction_point_indices]

                if len(below_angles) !=0:
                    minBelowAR, maxBelowAR = np.min(below_angles), np.max(below_angles)
                else:
                    minBelowAR, maxBelowAR=360,360

                if len(above_angles) != 0:
                    minAboveAR, maxAboveAR = np.min(above_angles), np.max(above_angles)
                else:
                    minAboveAR, maxAboveAR = 0,0

                    #check if there is a overlap in the angular range visible
                new_angular_ranges=[]

                #if the stroke is the right-side of the eye and if the vertical range of values crosses the horizontal axis wrt to current eye, consider overlap in other direction

                if len(below_angles)!=0 and len(above_angles)!=0:
                    if (minBelowAR - maxAboveAR) < (360-maxBelowAR)+minAboveAR:
                        minBelowAR = 180
                        maxAboveAR = 180
                    else:
                        minAboveAR = 0
                        maxBelowAR = 360
                new_angular_ranges.append([minBelowAR,maxBelowAR])
                new_angular_ranges.append([minAboveAR, maxAboveAR])
                belowOverLap = AngleRange.overlaps(minBelowAR, maxBelowAR)
                aboveOverLap = AngleRange.overlaps(minAboveAR, maxAboveAR)


                parent_id = List_stroke_set[i].getId()
                child_id = List_stroke_set[other_stroke_index].getId()
                #track the BAR from the parent's eye to target symbol
                Angular_Range_Blocked[(parent_id, child_id)] = [minBelowAR, maxBelowAR, minAboveAR, maxAboveAR]

                if belowOverLap or aboveOverLap:
                    #add an edge between two stroke-sets i and j
                    #add symmetrical edges - if one current stroke can see other stroke, edges added on both directions
                    expression_graph.add_edge(parent_id, child_id)
                    expression_graph.add_edge(child_id, parent_id)
                    #apply shrink
                    rng2, rng1 = new_angular_ranges[:]
                    mod_size = (1.0-shrink) * (((rng1[1] - rng1[0]) + (rng2[1] - rng2[0])) / 2)
                    if (minAboveAR == 0 and maxAboveAR != 0) or maxAboveAR == 180 or minBelowAR == 180 or (maxBelowAR == 360 and minBelowAR != 360):
                        if rng1[1]<mod_size:
                            #print("op1" + str(rng1) + " " +  str(rng2) + " " + str(mod_size))
                            rng2[1] -= (mod_size-rng1[0])
                            rng2[0] += mod_size
                            new_angular_ranges = [rng2]
                        elif rng2[0] + mod_size > 360:
                            #print("op2" + str(rng1) + " " +  str(rng2) + " " + str(mod_size))
                            rng1[0] += ((rng2[0] + mod_size) - 360)
                            rng1[1] -= mod_size
                            new_angular_ranges = [rng1]
                        elif rng1[0] + mod_size > 180:
                            #print("op3" + str(rng1) + " " +  str(rng2) + " " + str(mod_size))
                            rng2[0] += (rng1[0] + mod_size) - 180
                            rng2[1] -= mod_size
                            new_angular_ranges = [rng2]
                        elif rng2[1] - mod_size < 180:
                            #print("op4" + str(rng1) + " " +  str(rng2) + " " + str(mod_size))
                            rng1[1] -= mod_size - (rng2[1] - 180)
                            rng1[0] += mod_size
                            new_angular_ranges = [rng1]
                        else:
                            #print("op5" + str(rng1) + " " + str(rng2) + " " + str(shrink) + " " + str(mod_size))
                            if minAboveAR == 0:
                                rng1[1] -= mod_size
                                rng2[0] += mod_size
                            else:
                                rng1[0] += mod_size
                                rng2[1] -= mod_size
                            new_angular_ranges = [rng1, rng2]
                    #print(new_angular_ranges)
                    for minA, maxA in new_angular_ranges:
                        try:
                            AngleRange.chop(minA, maxA)
                        except KeyError:
                            print(expression.id + " , " + current_strokeset.id)
                #and len(above_angles) == 0
                elif find_punc and minBelowAR != 360 and punc_AngleRange.begin() <= minBelowAR and punc_AngleRange.end() >= maxBelowAR and stroke_set_size[i]/stroke_set_size[other_stroke_index] >= 3:

                    #print("punctuation found")
                    parent_id = List_stroke_set[i].getId()
                    child_id = List_stroke_set[other_stroke_index].getId()
                    expression_graph.add_edge(parent_id, child_id)

        expression_graph = GraphConstruction.add_edge_attributes(expression_graph)
        expression_graph = GraphConstruction.add_node_attributes(expression_graph)
        #iterate through the relations and set attributes for every edge in layout graph
        for parent_id,child_id in expression_graph.edges():
            expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.ANGULAR_RANGE_BLOCKED] = Angular_Range_Blocked[(parent_id, child_id)]
            edge_relation = expression.getComponentsRelation(parent_id, child_id)
            #avoid self loops
            if parent_id == child_id:
                continue
            #set the appropriate edge relation in the graph
            expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = edge_relation
            if edge_relation != "NoRelation" and parent_id in trace_id_to_symbol_id and child_id in trace_id_to_symbol_id and trace_id_to_symbol_id[parent_id] == trace_id_to_symbol_id[child_id]:
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.SAME_SYMBOL] = 1.0
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.GROUND_TRUTH_RELATION] = "MERGE"
            elif edge_relation != "NoRelation":
                expression_graph[parent_id][child_id][EDGE_ATTRIBUTES.IS_RELATION] = 1.0

        # iterate through the trace_sets and set attributes for every trace_set in layout graph
        for traceID, trace_set_object in expression.componentMap.items():
            expression_graph.node[traceID][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS] = symbol_id_label[traceID]
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_SET] = trace_set_object
            expression_graph.node[traceID][NODE_ATTRIBUTES.TRACE_LIST] = expression.getComponentSegments(traceID)

        return expression.id, expression_graph

    @staticmethod
    def generateExpressionGraphs(expressions, graph_type, find_punc=False, shrink=1, n_workers=4):
        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            mapper = executor.map
            if graph_type == 'sight':
                loading_function = GraphConstruction.get_line_of_sight_graph
            elif graph_type == 'complete':
                loading_function = GraphConstruction.get_complete_graph
            else:
                raise Exception("Graph Type is not recognized: " + graph_type)
            packets = []
            for expr in expressions.values():
                packets.append((expr,find_punc, shrink))
            for i, (exprId,graph) in enumerate(mapper(loading_function, packets)):
                expressions[exprId].expressionGraph = graph
                print(" Building: " + str(((i+1)*100)//len(expressions)) + "%", end="\r")
            print()
        return expressions


