import pickle
import os.path as path
from concurrent.futures import ProcessPoolExecutor
from enum import Enum
import os
import numpy as np

from recognizer.data.feature_dataset import FeatureDataset
from recognizer.evaluation.eval_ops import EvalOps
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.preprocessing.img_loader import ImgLoader
from recognizer.preprocessing.preprocessor import Preprocessor
from recognizer.postprocessing.postprocessor import Postprocessor

from recognizer.graph_representation.Graph_construction import GraphConstruction
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.data.dataset_builder import DatasetBuilder, TYPES_DATA_SET
from recognizer.recognition.segmenter import Segmenter
from recognizer.recognition.symbol_recognizer import SymbolRecognizer
from recognizer.recognition.symbol_parser import SymbolParser
from recognizer.recognition.pseudo_binary_parser import PseudoBinaryParser
from recognizer.recognition.Filtered_parser import FilteredParser
from recognizer.recognition.FilterThresholdParser import FilterThresholdParser
from recognizer.recognition.add_punc import AddPuncuation
from recognizer.recognition.parser import Parser, TYPES_PARSING
from recognizer.visualization.visualize_expression import Visualization
from recognizer.symbols.math_expression import MathExpression
from recognizer.graph_representation.Line_of_sight import Line_of_sight



class EXPERT_TYPES(Enum):
    Segmenter = 0
    Symbol_Classifier = 1
    Parser = 2
    Symbol_Parser = 3
    Pseudo_Binary = 4
    Filter_Stroke_Parser = 5
    Filter_Symbol_Parser = 6
    Add_Punc = 7
    Filter_Stroke_Threshold_Parser = 8
    Filter_Symbol_Threshold_Parser = 9

    @staticmethod
    def datasetType(etype):
        if etype in [EXPERT_TYPES.Segmenter]:
            return TYPES_DATA_SET.Segmentation
        elif etype in [EXPERT_TYPES.Symbol_Classifier]:
            return TYPES_DATA_SET.Symbol
        elif etype in [EXPERT_TYPES.Parser, EXPERT_TYPES.Symbol_Parser, EXPERT_TYPES.Pseudo_Binary, EXPERT_TYPES.Filter_Stroke_Parser, EXPERT_TYPES.Filter_Symbol_Parser, EXPERT_TYPES.Filter_Stroke_Threshold_Parser, EXPERT_TYPES.Filter_Symbol_Threshold_Parser]:
            return TYPES_DATA_SET.Relation
        elif etype in [EXPERT_TYPES.Add_Punc]:
            return TYPES_DATA_SET.Punctuation


class Controller:

    def __init__(self, config, config_name):
        # configs
        self.default_name = config_name
        self.configs = {config_name: config, 'default': config}

        # Storage
        self.experts = {}
        self.datasets = {}
        self.expressionSets = {}

        # General Parameters
        self.n_workers = config.get_int("DEFAULT_NUMBER_OF_WORKERS", 8)

    def addConfig(self, config, config_name):
        self.configs[config_name] = config

    def loadExpressions(self, name=None, config_name="default", isTraining=True):
        config = self.configs[config_name]
        expressions = {}
	# below function will return inkml for handwritten datatype
	# currently returns IMG
        if config.get_str("INPUT_DATA_LOADER_TYPE").lower() == "inkml":
            # set graph extraction type
            INKMLLoader.set_Extraction_Parameters(config)
            if isTraining:
                inkml_path, lg_path = config.get_str("TRAINING_DATASET_PATH"), config.get_str("TRAINING_DATASET_LG_PATH")
            else:
                inkml_path, lg_path = config.get_str("TESTING_DATASET_PATH"), config.get_str("TESTING_DATASET_LG_PATH")

            _, expressions, _, _ = INKMLLoader.get_Expression_Objects(inkml_path, lg_path)
            if config.get_str("GRAPH_LEVEL", "primitive") == "symbol":
                if not name:
                    name = "Expr_" + config.get_str("TRAINING_DATASET_PATH")
                self.expressionSets[name] = expressions
                self.createExpressionGraphs(name, config_name)
                self.convertSymbolLevel(name, True)
                expressions = self.expressionSets[name]
        elif config.get_str("INPUT_DATA_LOADER_TYPE").lower() == "img":
            print("Image data set used")
            if isTraining:
                expressions = ImgLoader.loadExpressions(config.get_str("TRAINING_DATASET_PATH"), config, workers=self.n_workers)
            else:
                expressions = ImgLoader.loadExpressions(config.get_str("TESTING_DATASET_PATH"), config, workers=self.n_workers)

        if not name:
            name = "Expr_" + config.get_str("TRAINING_DATASET_PATH")
        print(name)      
        self.expressionSets[name] = expressions
        return name

    def getExpressions(self, name):
        return self.expressionSets[name]

    def preprocessExpressions(self, name, config_name="default"):
        config = self.configs[config_name]
        self.expressionSets[name] = Preprocessor.preprocessExpressionsParallel(self.expressionSets[name], config, self.n_workers)

    def normalizeExpressions(self, set_name,config_name="default"):
        config = self.configs[config_name]
        self.expressionSets[set_name] = Preprocessor.normalizeExpressions(self.expressionSets[set_name], config)

    def postprocessExpressions(self, name, config_name="default"):
        config = self.configs[config_name]
        self.expressionSets[name] = Postprocessor.postprocessExpressionsParallel(self.expressionSets[name], config, self.n_workers)


    def createExpressionGraphs(self, set_name, config_name='default'):
        if set_name not in self.expressionSets:
            raise Exception("Expression Set is not recognized: " + set_name)
        config = self.configs[config_name]
        expressions = self.expressionSets[set_name]
        find_punc = config.get_bool("FIND_PUNCTUATION", False)
        shrink = config.get_float("LOS_SYMBOL_SHRINK", 1.0)
        graph_type = config.get_str("INITIAL_GRAPH_TYPE").lower()
        self.expressionSets[set_name] = GraphConstruction.generateExpressionGraphs(expressions, graph_type, find_punc, shrink, self.n_workers)
        if config.get_bool("FILTER_PUNCTUATION_SYMBOLS", False):
            self.expressionSets[set_name] = GraphConstruction.filterPuncNodes(self.expressionSets[set_name], config.get_bool("FILTER_WITH_GT", True))
        elif config.get_bool("FILTER_PUNCTUATION_EDGES", False):
            self.expressionSets[set_name] = GraphConstruction.findPuncRelations(self.expressionSets[set_name], config.get_bool("FILTER_WITH_GT", True))
        return set_name

    def filterExpressionGraphs(self, set_name, useGT=False, config_name='default'):
        if set_name not in self.expressionSets:
            raise Exception("Expression Set is not recognized: " + set_name)
        config = self.configs[config_name]
        expressions = self.expressionSets[set_name]
        if config.get_bool("FILTER_PUNCTUATION_SYMBOLS", True):
            self.expressionSets[set_name] = GraphConstruction.filterPuncNodes(expressions, useGT)
        elif config.get_bool("FILTER_PUNCTUATION_EDGES", False):
            self.expressionSets[set_name] = GraphConstruction.findPuncRelations(expressions, useGT)
        else:
            raise Exception("No Filtering was done.")

    def setExpressionEdgeTraceSets(self, set_name, useGT=False, config_name='default'):
        if set_name not in self.expressionSets:
            raise Exception("Expression Set is not recognized: " + set_name)
        config = self.configs[config_name]
        expressions = self.expressionSets[set_name]
        data = []
        for expr in expressions.values():
            data.append((expr, useGT is True))
        count = 0
        with ProcessPoolExecutor(max_workers=self.n_workers) as executor:
            for expr in executor.map(MathExpression.setEdgeTraces, data):
                expressions[expr.id] = expr
                count += 1
                print(" Building Edge Traces: " + str(((count)*100)//len(expressions)) + "%", end="\r")
            print()
        self.expressionSets[set_name] = expressions

    def createDataset(self, set_name, dataset_type, merge_strokes=False,label_source=None, dataset_name=None, config_name='default'):
        if set_name not in self.expressionSets or config_name not in self.configs:
            raise Exception("Invalid Arguments: named expression set or config not found.")
        config = self.configs[config_name]
        dataMaker = DatasetBuilder(dataset_type, FeatureExtractor(config))
        if label_source is not None:
            if label_source in self.datasets:
                class_mapping = self.datasets[label_source].class_mapping
                label_mapping = self.datasets[label_source].label_mapping
            elif label_source in self.experts:
                class_mapping = self.experts[label_source].classes_dict
                label_mapping = self.experts[label_source].classes_list
            else:
                raise Exception("Invalid Label Source: could not find label source \'" + label_source +"\'. Should be known dataset or expert.")
        else:
            class_mapping = None
            label_mapping = None

        if merge_strokes:
            self.merge_strokes_to_symbols(set_name)
            self.construct_expression_graph(config_name=config_name, set_name=set_name)
        dataset = dataMaker.create_dataset_for_expressions(list(self.expressionSets[set_name].values()), label_mapping, class_mapping, workers=self.n_workers)

        if dataset_name is None:
            dataset_name = "Data_" + set_name+dataset_type.name
        self.datasets[dataset_name] = dataset
        return dataset_name

    def construct_expression_graph(self,config_name, set_name):
        if set_name not in self.expressionSets or config_name not in self.configs:
            raise Exception("Dataset/Config is not known")
        config = self.configs[config_name]
        exp_set = self.expressionSets[set_name]
        graph_type=config.get("GRAPH_REPRESENTATION")
        GraphConstruction.generateExpressionGraphs(exp_set, graph_type, find_punc=False, n_workers=self.n_workers)


    def replicate_line_of_sight(self,config_name, set_name):
        """
        This is a command used to replicate the line of sight done by lei and was only used for comparison to the 
        reimplemented line of sight code.
        :param config_name: 
        :param set_name: 
        :return: 
        """
        if set_name not in self.expressionSets or config_name not in self.configs:
            raise Exception("Dataset/Config is not known")
        config = self.configs[config_name]
        exp_set = self.expressionSets[set_name]
        Line_of_sight.generateExpressionGraphs(exp_set)


    def writeEdgesToFile(self, set_name, file_name):
        print("Writing edges of", set_name)
        if set_name not in self.expressionSets:
            raise Exception("Dataset/Config is not known")
        exp_set = self.expressionSets[set_name]
        exp_list = exp_set.values()
        output_file = open(file_name, "a+")
        for exp in exp_list:
            graph = exp.getLayoutGraph()

            for parent, child in graph.edges():
                overlap = graph[parent][child]['overLap']
                output_file.write(exp.id+","+str(parent) + "," + str(child)+"\n")
        output_file.close()


    def createExpert(self, dataset_name, expert_type=None, expert_name=None, config_name='default'):
        if dataset_name not in self.datasets or config_name not in self.configs:
            raise Exception("Invalid Arguements: named dataset or config not found.")
        if expert_type is None and "EXPERT_TYPE" not in self.configs[config_name].data:
            raise Exception("No Expert Type Given: An expert type was not given as parameter or given in config.")
        config = self.configs[config_name]
        if expert_type is None:
            expert_type = EXPERT_TYPES(config.get_int("EXPERT_TYPE"))
        if expert_name is None:
            expert_name = dataset_name + "_" + expert_type.name

        cls_type = config.get_int("CLASSIFIER_TYPE", 0)
        expert = None

        if expert_type == EXPERT_TYPES.Segmenter:
            expert = Segmenter(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Symbol_Classifier:
            expert = SymbolRecognizer(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Parser:
            pass
        elif expert_type == EXPERT_TYPES.Symbol_Parser:
            expert = SymbolParser(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Pseudo_Binary:
            expert = PseudoBinaryParser(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Filter_Stroke_Parser or expert_type == EXPERT_TYPES.Filter_Symbol_Parser:
            expert = FilteredParser(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Add_Punc:
            expert = AddPuncuation(cls_type, self.datasets[dataset_name], config)
        elif expert_type == EXPERT_TYPES.Filter_Symbol_Threshold_Parser or expert_type == EXPERT_TYPES.Filter_Stroke_Threshold_Parser:
            expert = FilterThresholdParser(cls_type, self.datasets[dataset_name], config)
        else:
            raise Exception("Invalid Expert Type: The expert type, " + str(expert_type) + ", does not match any known expert type." )

        self.experts[expert_name] = expert
        return expert_name

    def useExpert(self, set_name, expert_name, dataset_name, *args):
        if set_name not in self.expressionSets or expert_name not in self.experts or dataset_name not in self.datasets:
            raise Exception("Invalid Arguements: named expression set, dataset, or config not found.")
        expressions = self.expressionSets[set_name]
        expert = self.experts[expert_name]
        #dont load data set
        dataset = self.datasets[dataset_name]
        expert.operate(expressions, dataset, args)

    def generateExpert(self, config_name, expert_name):
        config = self.configs[config_name]
        expert_type = EXPERT_TYPES(config.get_int("EXPERT_TYPE"))
        dataset_type = EXPERT_TYPES.datasetType(expert_type)

        self.loadExpressions("temp_training", config_name=config_name, isTraining=True)
        self.preprocessExpressions("temp_training", config_name=config_name)
        self.createExpressionGraphs("temp_training", config_name=config_name)
        if config.get_bool("FEATURES_USE_MST_CONTEXT_2D_HISTOGRAMS", False):
            self.setExpressionEdgeTraceSets("temp_training", useGT=True, config_name=config_name)
        self.createDataset("temp_training", dataset_type, dataset_name="temp_data_training", config_name=config_name)
        del self.expressionSets["temp_training"]
        self.createExpert("temp_data_training", expert_name=expert_name, config_name=config_name)
        del self.datasets["temp_data_training"]
        return expert_name


    def merge_strokes_to_symbols(self, set_name):
        '''
        Merges the strokes in the expressions into a symbol based on ground truth
        :param set_name:
        :return:
        '''
        expressions = self.expressionSets[set_name]
        expression_list = expressions.values()
        for expr in expression_list:
            expr.convertSymbolLevel(fromGroundTruth=True)


    def convertSymbolLevel(self, set_name, GT=False):
        if set_name not in self.expressionSets:
            return False
        for expr in self.expressionSets[set_name].values():
            if expr.expressionGraph is not None:
                expr.convertSymbolLevel(GT)


    def crossValidateExpert(self, set_name, num_folds=5, config_name="default"):
        if set_name not in self.expressionSets:
            raise Exception("Invalid expression set name.")
        config = self.configs[config_name]
        expert_type = EXPERT_TYPES(config.get_int("EXPERT_TYPE"))
        dataset_type = EXPERT_TYPES.datasetType(expert_type)
        # create dataset
        full_dataset_name = self.createDataset(set_name,dataset_type, dataset_name="cross_val_full", config_name=config_name)
        full_dataset = self.datasets["cross_val_full"]
        del self.datasets["cross_val_full"]

        # split dataset
        parts_data, parts_labels, parts_indices, parts_sources = full_dataset.split_data(num_folds)

        # loop for training and testing
        local_training_accuracies, local_training_averages, local_training_stdevs = np.zeros(num_folds), np.zeros(num_folds), np.zeros(num_folds)

        local_evaluation_accuracies, local_evaluation_averages, local_evaluation_stdevs = np.zeros(num_folds), np.zeros(num_folds), np.zeros(num_folds)

        label_mapping = full_dataset.label_mapping
        class_mapping = full_dataset.class_mapping

        best_classifier_ref = None
        best_accuracy = None

        offset_partition = 0
        for i in range(num_folds):
            # prepare datasets ...
            training_data = np.concatenate(parts_data[:i] + parts_data[i + 1:], axis=0)
            training_labels = np.concatenate(parts_labels[:i] + parts_labels[i + 1:], axis=0)

            testing_data, testing_labels, testing_indices, testing_sources = parts_data[i], parts_labels[i], parts_indices[i], parts_sources[i]

            self.datasets["cross_train"] = FeatureDataset(training_data, training_labels, label_mapping, class_mapping, dataset_type)
            self.datasets["cross_test"] = FeatureDataset(testing_data, testing_labels, label_mapping, class_mapping, dataset_type, testing_sources)
            training_dataset = self.datasets["cross_train"]
            testing_dataset = self.datasets["cross_test"]

            #classifier = ClassifierTrainer.train_classifier(training_dataset, config)
            expert_name = self.createExpert("cross_train", expert_type, "cross_expert", config_name)
            expert = self.experts[expert_name]

            # training ...
            training_predicted, eval_training_metrics = EvalOps.classifier_per_class_accuracy(expert, training_dataset, get_output_data=True)
            accuracy, per_class_avg, per_class_stdev, errors = eval_training_metrics

            local_training_accuracies[i] = accuracy
            local_training_averages[i] = per_class_avg
            local_training_stdevs[i] = per_class_stdev

            # testing ...
            testing_predicted, eval_testing_metrics = EvalOps.classifier_per_class_accuracy(expert, testing_dataset,get_output_data=True)
            accuracy, per_class_avg, per_class_stdev, errors = eval_testing_metrics

            local_evaluation_accuracies[i] = accuracy
            local_evaluation_averages[i] = per_class_avg
            local_evaluation_stdevs[i] = per_class_stdev

            if best_classifier_ref is None or best_accuracy < local_training_accuracies[i]:
                best_classifier_ref = expert
                best_accuracy = local_training_accuracies[i]

            offset_partition += testing_data.shape[0]
            del self.datasets["cross_train"]
            del self.datasets["cross_test"]

        training_results = (local_training_accuracies, local_training_averages, local_training_stdevs)
        evaluation_results = (local_evaluation_accuracies, local_evaluation_averages, local_evaluation_stdevs)

        return training_results, evaluation_results


    def outputExpressions(self, set_name, predictedLabels=True, location=None, lg_type="OR"):
        if set_name not in self.expressionSets:
            return False
        print("Writing LG files for ", set_name)
        for expr in self.expressionSets[set_name].values():
            if lg_type.upper() == "OR":
                expr.writeORLg(expr.expressionGraph, predictedLabels, location)
            elif lg_type.upper() == "NE":
                expr.writeNELg(expr.expressionGraph, predictedLabels, location)
            else:
                print("Invalid Lg type")
                return False
        return True

    def visualize_expressions(self, exp_Set_name, base_directory):
        expression_objects = self.expressionSets[exp_Set_name].values()
        for expression in expression_objects:
            visualize = Visualization()
            plotPath = base_directory+"//"+exp_Set_name + "//" + expression.id.replace(".inkml", "")
            exprPath = base_directory + "//" + exp_Set_name + "//VIEW_EXPRES//" + expression.id.replace(".inkml", "")
            if not os.path.exists(plotPath):
                os.makedirs(plotPath)
            if not os.path.exists(exprPath):
                os.makedirs(exprPath)
            visualize.graw_edges_graph(expression, plotPath)
            #visualize.draw_expression(expression, exprPath)

    def saveExpressionSet(self, name, filename=None):
        obj = self.expressionSets[name]
        if filename:
            self.save(obj, filename)
        else:
            self.save(obj, name+".expr")

    def saveDataset(self, name, filename=None):
        obj = self.datasets[name]
        if filename:
            self.save(obj, filename)
        else:
            self.save(obj, name+".dat")

    def saveExpert(self, name, filename=None):
        obj = self.experts[name]
        if filename:
            self.save(obj, filename)
        else:
            self.save(obj, name+".dat")

    def loadExpressionSet(self, filename, name=None):
        expressions = self.load(filename)
        if not name:
            name = path.basename(filename).split(".")[0]
        self.expressionSets[name] = expressions

    def loadDataset(self, filename, name=None):
        dataset = self.load(filename)
        if not name:
            name = path.basename(filename).split(".")[0]
        self.datasets[name] = dataset

    def loadExpert(self, filename, name=None):
        classifier = self.load(filename)
        if name:
            name = path.basename(filename).split(".")[0]
        self.experts[name] = classifier

    def save(self, obj, filename):
        out_file = open(filename, 'wb')
        pickle.dump(obj, out_file, pickle.HIGHEST_PROTOCOL)
        out_file.close()

    def load(self, filename):
        in_file = open(filename, 'rb')
        obj = pickle.load(in_file)
        in_file.close()
        return obj

    def loadBoundingBox(self):
        pass

    def getLabels(self):
        pass

    def outputPDFExpressions(self, set_name, predictedLabels=True, location=None, lg_type="OR"):
        if set_name not in self.expressionSets:
            return False
        print("Writing LG files for ", set_name)
        for expr in self.expressionSets[set_name].values():
            if lg_type.upper() == "OR":
                expr.writePDFORLg(expr.expressionGraph, predictedLabels, location)
            #elif lg_type.upper() == "NE":
            #    expr.writeNELg(expr.expressionGraph, predictedLabels, location)
            else:
                print("Invalid Lg type")
                return False
        return True
