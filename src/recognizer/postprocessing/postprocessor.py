import math
from concurrent.futures import ProcessPoolExecutor
import numpy as np
from recognizer.util.math_ops import MathOps
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

class Postprocessor:

    def __init__(self, configuration):
        self.config = configuration


    def correctPuncRelations(self, expression, useGT=False):
        #TODO change to not be hardcoded values
        punc = ["period", 'comma', 'ldots']
        subs = ["RSUB"]
        replace_rel = "PUNC"
        graph = expression.expressionGraph
        if useGT:
            sym_cls = NODE_ATTRIBUTES.GROUND_TRUTH_CLASS
        else:
            sym_cls = NODE_ATTRIBUTES.PREDICTED_CLASS

        for pid, cid, data in graph.edges(data=True):
            if graph.node[pid][sym_cls] in punc and graph.node[cid][sym_cls] in punc and data[EDGE_ATTRIBUTES.PREDICTED_LAYOUT] != "HORIZONTAL":
                graph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] == "HORIZONTAL"
            elif graph.node[cid][sym_cls] in punc and data[EDGE_ATTRIBUTES.PREDICTED_LAYOUT] in subs:
                graph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] == replace_rel
            elif data[EDGE_ATTRIBUTES.PREDICTED_LAYOUT] == "PUNC" and graph.node[pid][sym_cls] not in punc:
                graph[pid][cid][EDGE_ATTRIBUTES.PREDICTED_LAYOUT] == "RSUB"
        expression.expressionGraph = graph
        return expression


    def applyPostprocessing(self, expression):
        return self.correctPuncRelations(expression, useGT=False)

    @staticmethod
    def postprocessExpressions(expressions, config):
        preprocessor = Postprocessor(config)

        for math_expression in expressions:
            preprocessor.applyPostprocessing(math_expression)

    @staticmethod
    def postprocessExpressionsParallel(expressions, config, n_workers=8):
        postprocessor = Postprocessor(config)
        with ProcessPoolExecutor(max_workers=n_workers) as executor:
            #executor.map(preprocessor.applyPreprocessing, list(expressions.values()))
            mapper = executor.map
            for i, exp in enumerate(mapper(postprocessor.applyPostprocessing, list(expressions.values()))):
                expressions[exp.id] = exp
                print(" Postprocessing: " + str(((i+1)*100)//len(expressions)) + "%", end="\r")
            print()
        return expressions

