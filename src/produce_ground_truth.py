from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import DatasetBuilder, TYPES_DATA_SET

def generate_ground_truth_labels():
    classify_config_filename = "Default/Classify_Default.conf"
    #classify_config_filename = "debugConfig/Parsing_debug.conf"

    classify_config, classify_config_name = Configuration.from_file(classify_config_filename), "ClassifyConfig"
    control_runner = Controller(classify_config, classify_config_name)
    print("Load THE testing Expressions into Expression Map\n")
    testing_set_name = "Testing_Exp_set"

    # load the expressions commonly for all the process
    control_runner.loadExpressions(name=testing_set_name, config_name=classify_config_name)
    control_runner.preprocessExpressions(testing_set_name, classify_config_name)
    control_runner.construct_expression_graph(classify_config_name, testing_set_name)
    control_runner.merge_strokes_to_symbols(testing_set_name)
    control_runner.construct_expression_graph(classify_config_name, testing_set_name)
    lg_file_directory = classify_config.data["TESTING_OUTPUT_LG_PATH"]
    control_runner.outputExpressions(testing_set_name, predictedLabels=False, location=lg_file_directory, lg_type="OR")



def generate_eliminated_graph():
    config_file_name = "Default/General.conf"
    #config_file_name = "debugConfig/sample.conf"

    myConfig, my_config_name = Configuration.from_file(config_file_name), "ClassifyConfig"
    control_runner = Controller(myConfig, my_config_name)
    print("Load THE testing Expressions into Expression Map\n")
    training_set_name ,testing_set_name = "Training_Exp_set", "Testing_Exp_set"

    # load the expressions commonly for all the process
    control_runner.loadExpressions(name=training_set_name, config_name=my_config_name)
    control_runner.loadExpressions(name=testing_set_name, config_name=my_config_name, isTraining=False)

    print("Preprocessing\n")
    control_runner.preprocessExpressions(testing_set_name, my_config_name)
    control_runner.preprocessExpressions(training_set_name, my_config_name)

    print("Graph_Construction\n")
    control_runner.construct_expression_graph(my_config_name, training_set_name)
    control_runner.construct_expression_graph(my_config_name, testing_set_name)

    print("Merge Symbols\n")
    control_runner.merge_strokes_to_symbols(training_set_name)
    control_runner.merge_strokes_to_symbols(testing_set_name)

    print("Graph_Construction\n")
    control_runner.construct_expression_graph(my_config_name, testing_set_name)
    control_runner.construct_expression_graph(my_config_name, training_set_name)

    print("\n[Extracting Features..]\t  Filtering features extraction\n")
    filter_expret_name = "FilteredParser"
    training_filtpar_dataset_name = control_runner.createDataset(training_set_name, TYPES_DATA_SET.Relation,config_name=my_config_name)
    testing_filtpar_dataset_name = control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Relation,config_name=my_config_name)

    control_runner.createExpert(training_filtpar_dataset_name, expert_name="FilteredParser", config_name=my_config_name)
    control_runner.useExpert(testing_set_name, filter_expret_name, testing_filtpar_dataset_name)

    lg_file_directory = myConfig.data["TESTING_OUTPUT_LG_PATH"]
    control_runner.outputExpressions(testing_set_name, predictedLabels=False, location=lg_file_directory, lg_type="OR")


def replicate_Lei():
    config_file_name = "Default/Classify_Default.conf"
    config, config_name = Configuration.from_file(config_file_name), "ConfigName"
    # map all the configuration instances in the controller
    control_runner = Controller(config, config_name)
    print("[Loading..] \t training and testing Expressions\n")

    testing_inkml, testing_lg, testing_set_name = config.data["TESTING_DATASET_PATH"], config.data[
        "TESTING_DATASET_LG_PATH"], "Testing_Exp_set"

    # load the expressions commonly for all the process
    control_runner.loadExpressions(name=testing_set_name, config_name=config_name, isTraining=False)
    # control_runner.preprocessExpressions(testing_set_name, config_name=segment_config_name)

    control_runner.replicate_line_of_sight(config_name, testing_set_name)
    control_runner.merge_strokes_to_symbols(testing_set_name)
    control_runner.replicate_line_of_sight(config_name, testing_set_name)

    lg_file_directory = "CROHME_2014/GT_LEI_REPLICA_test//"
    control_runner.outputExpressions(testing_set_name, predictedLabels=False, location=lg_file_directory, lg_type="OR")

if __name__ == '__main__':
    generate_eliminated_graph()