from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import DatasetBuilder, TYPES_DATA_SET
from recognizer.preprocessing.preprocessor import Preprocessor

def test_classifier():
    classify_config_filename = "Default/Classify_Default.conf"


    '''segment_config_filename = "debugConfig/Parsing_debug.conf"
    parsing_config_filename = "debugConfig/Parsing_debug.conf"
    classify_config_filename = "debugConfig/Parsing_debug.conf"
    '''

    classify_config, classify_config_name = Configuration.from_file(classify_config_filename), "ClassifyConfig"

    control_runner = Controller(classify_config, classify_config_name)
    print("Load THE training and testing Expressions into Expression Map\n")
    training_inkml, training_lg, training_set_name = classify_config.data["TRAINING_DATASET_PATH"], classify_config.data["TRAINING_DATASET_LG_PATH"], "Training_Exp_set"

    testing_inkml, testing_lg, testing_set_name = classify_config.data["TESTING_DATASET_PATH"], classify_config.data["TESTING_DATASET_LG_PATH"], "Testing_Exp_set"

    # load the expressions commonly for all the process
    control_runner.loadExpressions((training_inkml, training_lg), name=training_set_name,config_name=classify_config_name)
    control_runner.loadExpressions((testing_inkml, testing_lg), name=testing_set_name, config_name=classify_config_name)

    control_runner.merge_strokes_to_symbols(training_set_name)
    control_runner.merge_strokes_to_symbols(testing_set_name)



    control_runner.construct_expression_graph(classify_config_name, training_set_name)
    control_runner.construct_expression_graph(classify_config_name, testing_set_name)

    control_runner.normalizeExpressions(training_set_name, classify_config_name)
    control_runner.normalizeExpressions(testing_set_name, classify_config_name)

    training_class_dataset_name = control_runner.createDataset(training_set_name, TYPES_DATA_SET.Symbol,
                                                               config_name=classify_config_name)
    testing_class_dataset_name = control_runner.createDataset(testing_set_name, TYPES_DATA_SET.Symbol,
                                                              config_name=classify_config_name)

    name_classifier = "MY_EXPERT_SYMBOL_CLASSIFIER"
    control_runner.createSymbolClassifier(training_class_dataset_name, expert_name=name_classifier,
                                          config_name=classify_config_name)
    control_runner.classifySymbols(testing_set_name, name_classifier, testing_class_dataset_name)

    lg_file_directory = classify_config.data["TESTING_OUTPUT_LG_PATH"]
    control_runner.outputExpressions(testing_set_name, location=lg_file_directory, lg_type="OR")


if __name__ == '__main__':
    test_classifier()