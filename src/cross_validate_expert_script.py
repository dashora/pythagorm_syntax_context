import sys
import time
import matplotlib.pyplot as plt
import numpy as np

from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

if __name__ == "__main__":
    if len(sys.argv) < 3:
        exit()

    config = Configuration.from_file(sys.argv[1])
    num_folds = int(sys.argv[2])

    control = Controller(config, "default")

    ### Load Training Expressions ###
    print("Loading Training Expressions")
    control.loadExpressions("training", isTraining=True)
    control.preprocessExpressions("training")
    control.createExpressionGraphs("training")

    ### Do Cross Validation ###
    train_r, eval_r = control.crossValidateExpert("training", num_folds)
    all_training_accuracies, all_training_averages, all_training_stdevs = train_r
    all_validation_accuracies, all_validation_averages, all_validation_stdevs = eval_r

    ### Output results ###
    print("Print Cross Validation Results")
    print("    \t    TRAINING \t \t   TESTING")
    print(" #  \tACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD")

    for i in range(num_folds):
        print(" " + str(i+1) + "\t" +
              str(round(all_training_accuracies[i], 3)) + "\t" +
              str(round(all_training_averages[i], 3)) + "\t" +
              str(round(all_training_stdevs[i], 3)) + "\t" +
              str(round(all_validation_accuracies[i], 3)) + "\t" +
              str(round(all_validation_averages[i], 3)) + "\t" +
              str(round(all_validation_stdevs[i], 3)))

    print(" Mean\t" + str(round(all_training_accuracies.mean(), 3)) + "\t" +
          str(round(all_training_averages.mean(), 3)) + "\t" +
          str(round(all_training_stdevs.mean(), 3)) + "\t" +
          str(round(all_validation_accuracies.mean(), 3)) + "\t" +
          str(round(all_validation_averages.mean(), 3)) + "\t" +
          str(round(all_validation_stdevs.mean(), 3)))

    print(" STDevs\t" + str(round(all_training_accuracies.std(), 3)) + "\t" +
          str(round(all_training_averages.std(), 3)) + "\t" +
          str(round(all_training_stdevs.std(), 3)) + "\t" +
          str(round(all_validation_accuracies.std(), 3)) + "\t" +
          str(round(all_validation_averages.std(), 3)) + "\t" +
          str(round(all_validation_stdevs.std(), 3)))

