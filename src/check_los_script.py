import sys
import time
import matplotlib.pyplot as plt
import numpy as np

from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

if __name__ == "__main__":
    if len(sys.argv) < 2:
        exit()

    config = Configuration.from_file(sys.argv[1])

    control = Controller(config, "default")

    ### Load Test Expressions ###
    print("Loading Training Expressions")
    control.loadExpressions(name="training", isTraining=True)
    control.preprocessExpressions("training")
    control.createExpressionGraphs("training")

    ### Output results ###
    print("Outputting Label Graph Files")
    control.outputExpressions("training", location=config.get_str("OUTPUT_DIR"), lg_type="OR", predictedLabels=False)