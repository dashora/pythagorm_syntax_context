import sys
import time

from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES


if __name__ == "__main__":
    if len(sys.argv) < 3:
        exit()

    config = Configuration.from_file(sys.argv[1])
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    load_par = bool(int(sys.argv[2]))

    expert_dir = config.get_str("EXPERT_DIR")
    experts_names = config.get("EXPERT_LIST")

    ### Add All Configs ###
    par_config = Configuration.from_file(subconfig_dir+subconfigs_names[0]+".conf")
    control.addConfig(par_config, "par_config")

    ### Train Any Missing Experts ###
    if not load_par:
        print("Generating Parser")
        control.generateExpert("par_config", experts_names[0])
        control.saveExpert(experts_names[0], expert_dir+experts_names[0]+".dat")
        del control.experts[experts_names[0]]

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions("testing", isTraining=False)
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")


    ### Do Parsing ###
    print("Starting Parsing")

    # Get Parser
    print("Loading Parser")
    control.loadExpert(expert_dir+experts_names[0]+".dat", name=experts_names[0])

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions("testing", isTraining=False)
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")
    # control.saveExpressionSet("testing", exprSet_dir+"test_symbol_los_Punc_v5.expr")
    #control.loadExpressionSet(exprSet_dir + "test_symbol_los_Punc_v5.expr", "testing")

    # Get Test Relation Dataset
    print("Creating Test Parsing Dataset")
    control.createDataset("testing", TYPES_DATA_SET.Relation, label_source=experts_names[0], dataset_name="par_data_testing", config_name="par_config")

    # Parse Expressions
    print("Parsing Expressions")
    control.useExpert("testing", experts_names[0], "par_data_testing", True, False)

    # Clean After Parsing
    del control.datasets["par_data_testing"]

    ### Output  Final results ###
    print("Outputting Label Graph Files")
    expressions = control.getExpressions("testing")
    for exprId in expressions.keys():
        expr = expressions[exprId]
        for symbol in expr.expressionGraph.nodes():
            expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.PREDICTED_CLASS] = expr.expressionGraph.node[symbol][NODE_ATTRIBUTES.GROUND_TRUTH_CLASS]
        expressions[exprId] = expr
    control.expressionSets['testing'] = expressions
    #control.postprocessExpressions("testing")
    control.outputExpressions("testing", location=config.get_str("OUTPUT_DIR"), lg_type="OR")
