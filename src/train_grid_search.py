import sys
import numpy as np
import time

from recognizer.data.feature_dataset import FeatureDataset
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.preprocessing.img_loader import ImgLoader
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.configuration.configuration import Configuration
from recognizer.training.trainer import ClassifierTrainer
from recognizer.evaluation.eval_ops import EvalOps
from recognizer.training.grid_search_ops import GridSearchOps

def load_raw_data(config, generate_testing):
    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # load training data, no features extracted, only symbols pre-processed
    training_path = config.data["TRAINING_DATASET_PATH"]

    loader_type = config.get_str("INPUT_DATA_LOADER_TYPE")
    if loader_type.lower() == "inkml":
        data_info = INKMLLoader.load_INKML_directory(training_path, True, None, label_replacement, True, None, None, True)
    elif loader_type.lower() == "img":
        data_info = ImgLoader.load_Lg_List(training_path, None, config, save_sources=False)
    else:
        print("Invalid Loader Type: " + loader_type + "\n INPUT_DATA_LOADER_TYPE Should be INKML or IMG.")
        raise ValueError

    raw_training, valid_files, error_files = data_info

    if generate_testing and loader_type.lower() == "inkml":
        # re-generate testing set
        testing_path = config.data["TESTING_DATASET_PATH"]
        data_info = INKMLLoader.load_INKML_directory(testing_path, True, None, label_replacement,
                                                     True, None, None, True)
        raw_testing, valid_files, error_files = data_info
    elif generate_testing and loader_type.lower() == "img":
        # re-generate testing set
        testing_path = config.data["TESTING_DATASET_PATH"]
        data_info = ImgLoader.load_Lg_List(testing_path, None, config, save_sources=False)
        raw_testing, valid_files, error_files = data_info
    else:
        raw_testing = None

    return raw_training, raw_testing


def create_datasets(config, raw_training, raw_testing):
    workers = config.get_int("PARALLEL_FEATURE_EXTRACTION_WORKERS", 5)

    # create training dataset
    raw_training_data, raw_training_labels, _ = raw_training
    if workers == 0:
        training_features = FeatureExtractor.getSymbolsFeatures(raw_training_data, config)
    else:
        training_features = FeatureExtractor.getSymbolsFeaturesParallel(raw_training_data, config, workers)

    loader_type = config.get_str("INPUT_DATA_LOADER_TYPE")
    if loader_type.lower() == "inkml":
        full_training = FeatureDataset.from_INKML_data(training_features, raw_training_labels, None, None, None, config)
    elif loader_type.lower() == "img":
        full_training = FeatureDataset.from_Img_data(training_features, raw_training_labels, None, None, None, config)
    else:
        print("Invalid Loader Type: " + loader_type + "\n INPUT_DATA_LOADER_TYPE Should be INKML or IMG.")
        raise ValueError

    label_mapping = full_training.label_mapping
    class_mapping = full_training.class_mapping

    # data normalization
    normalize_data = config.get_bool("SCALE_NORMALIZE_DATA", False)
    if normalize_data:
        # if data normalizer is empty, a new will be created
        full_training.normalize_data(None)
        # keep the data normalizer (if new)
        data_normalizer = full_training.normalizer
    else:
        data_normalizer = None

    if raw_testing is not None:

        # create training dataset
        raw_testing_data, raw_testing_labels, _ = raw_testing
        if workers == 0:
            testing_features = FeatureExtractor.getSymbolsFeatures(raw_testing_data, config)
        else:
            testing_features = FeatureExtractor.getSymbolsFeaturesParallel(raw_testing_data, config, workers)

        if loader_type.lower() == "inkml":
            full_testing = FeatureDataset.from_INKML_data(testing_features, raw_testing_labels, label_mapping,
                                                      class_mapping, None, config)
        elif loader_type.lower() == "img":
            full_testing = FeatureDataset.from_Img_data(testing_features, raw_testing_labels, label_mapping, class_mapping, None, config)

        if normalize_data:
            full_testing.normalize_data(data_normalizer)
    else:
        full_testing = None

    return full_training, full_testing


def main():
    # usage check
    if len(sys.argv) < 4:
        print("Usage: python train_grid_search.py main_config search_config out_file")
        print("Where")
        print("\tmain_config\t= Path to main configuration file")
        print("\tsearch_config\t= Path to auxiliary configuration defining grid search")
        print("\tout_file\t= File where grid search results will be appended")
        return

    print("Loading configuration....")
    main_config_filename = sys.argv[1]
    main_config = Configuration.from_file(main_config_filename)

    aux_config_filename = sys.argv[2]
    aux_config = Configuration.from_file(aux_config_filename)

    out_append_filename = sys.argv[3]

    # define especial parameters
    regenerate_dataset = aux_config.get_bool("REGENERATE_DATASET")
    regenerate_symbols = aux_config.get_bool("REGENERATE_SYMBOLS", 0)

    use_crossvalidation = aux_config.get_bool("USE_CROSSVALIDATION")
    if use_crossvalidation:
        n_partitions = aux_config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
        if n_partitions < 2:
            print("Invalid number of partitions <CLASSIFIER_CROSSVALIDATION_PARTITIONS> (Aux. Config)")
            return

        main_config.data["CLASSIFIER_CROSSVALIDATION_PARTITIONS"] = str(n_partitions)
    else:
        n_partitions = 1

    # prepare grid search ....
    search_params = GridSearchOps.extract_search_parameters(main_config, aux_config)
    grid_parameters, grid_parameter_values, grid_combined, grid_desc = search_params
    # compute all parameter combinations to explore
    all_combinations = GridSearchOps.enumerate_combinations(grid_parameters, grid_parameter_values, grid_combined)
    print(grid_desc)

    # Output file header
    GridSearchOps.write_search_header(out_append_filename, grid_desc, len(grid_parameters))

    n_combs = len(all_combinations)

    all_train_acc, all_train_avg, all_train_std = np.zeros(n_combs), np.zeros(n_combs), np.zeros(n_combs)
    all_eval_acc, all_eval_avg, all_eval_std = np.zeros(n_combs), np.zeros(n_combs), np.zeros(n_combs)

    print("Loading data....")
    if not regenerate_dataset:
        # If dataset will not change (only classifier parameters will affect results)
        full_training = ClassifierTrainer.load_training_set(main_config)

        if not use_crossvalidation:
            full_testing = ClassifierTrainer.load_testing_set(main_config)
        else:
            full_testing = None
    elif not regenerate_symbols:
        # dataset features will change, but raw data will not...
        raw_training, raw_testing = load_raw_data(main_config, not use_crossvalidation)
    else:
        raw_training, raw_testing = None, None

    print(" \t" * (len(grid_parameters)) + "TRAINING \t \t   TESTING")
    print(" \t" * (len(grid_parameters)) + "ACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD\tFeat.")

    start_time = time.time()
    for idx, combination in enumerate(all_combinations):
        # apply configuration change
        comb_desc = "\t".join([str(val) for val in combination])
        for i, parameter in enumerate(grid_parameters):
            main_config.data[parameter] = str(combination[i])

        # generate raw data is loader type is for images
        if regenerate_symbols:
            #print("Loading Raw Image data...", end="\r")
            raw_training, raw_testing = load_raw_data(main_config, not use_crossvalidation)

        # regenerate dataset if required

        if regenerate_dataset:
            #print("Updating datasets....", end="\r" )
            full_training, full_testing = create_datasets(main_config, raw_training, raw_testing)
            #print("done creating datasets", end="\r")

        if use_crossvalidation:
            train_r, eval_r, _, _, _ = ClassifierTrainer.train_crossvalidation(full_training, main_config, True, '\r')

            all_train_acc[idx] = train_r[0].mean()
            all_train_avg[idx] = train_r[1].mean()
            all_train_std[idx] = train_r[2].mean()

            all_eval_acc[idx] = eval_r[0].mean()
            all_eval_avg[idx] = eval_r[1].mean()
            all_eval_std[idx] = eval_r[2].mean()
        else:
            # train classifier using all training data
            classifier = ClassifierTrainer.train_classifier(full_training, main_config)

            # measure training error
            all_train_acc[idx], all_train_avg[idx], all_train_std[idx], _ = EvalOps.classifier_per_class_accuracy(classifier, full_training)

            # measure testing error on full testing set
            all_eval_acc[idx], all_eval_avg[idx], all_eval_std[idx], _ = EvalOps.classifier_per_class_accuracy(classifier, full_testing)

        result_desc = (comb_desc + "\t" +
              str(round(all_train_acc[idx], 3)) + "\t" +
              str(round(all_train_avg[idx], 3)) + "\t" +
              str(round(all_train_std[idx], 3)) + "\t" +
              str(round(all_eval_acc[idx], 3)) + "\t" +
              str(round(all_eval_avg[idx], 3)) + "\t" +
              str(round(all_eval_std[idx], 3)) + "\t" +
              str(full_training.num_attributes()))
        print(result_desc)

        out_file = open(out_append_filename, "a")
        out_file.write(result_desc + "\n")
        out_file.close()

    end_time = time.time()
    print("\nTotal Elapsed Time: " + str(end_time - start_time) + " s")


if __name__ == '__main__':
    main()
