import sys
import time
import matplotlib.pyplot as plt
import numpy as np

from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

if __name__ == "__main__":
    if len(sys.argv) < 5:
        exit()

    config = Configuration.from_file(sys.argv[1])
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    load_seg = bool(int(sys.argv[2]))
    load_cls = bool(int(sys.argv[3]))
    load_par = bool(int(sys.argv[4]))

    expert_dir = config.get_str("EXPERT_DIR")
    experts_names = config.get("EXPERT_LIST")

    exprSet_dir = config.get_str("EXPRESSION_SET_DIR")

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions( name="testing", isTraining=False)
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")
    # control.saveExpressionSet( "testing", exprSet_dir + "test_symbol_los_punc_v5.expr")
    #control.loadExpressionSet(exprSet_dir + "test_symbol_los_punc_v5.expr", "testing")

    ### Do Segmentation ###
    print("Starting Segmentation")
    seg_config = Configuration.from_file(subconfig_dir+subconfigs_names[0]+".conf")
    control.addConfig(seg_config, "seg_config")

    # Get segmenter
    # if load_seg:
    #     print("Loading Segmenter")
    #     control.loadExpert(expert_dir+experts_names[0]+".dat", name=experts_names[0])
    # else:
    #     print("Generating Segmenter")
    #     control.generateExpert("seg_config", experts_names[0])
    #     control.saveExpert(experts_names[0], expert_dir+experts_names[0]+".dat")

# Dont create dataset
    #  Get Test Segmentation Dataset
    #print("Creating Test Segmentation Dataset")
    #control.createDataset("testing", TYPES_DATA_SET.Segmentation, label_source=experts_names[0], dataset_name="seg_data_testing", config_name="seg_config")

    # Do Segmentation
    print("Doing Segmentation")
    control.useExpert("testing", experts_names[0], "seg_data_testing", True)

    # Clean After Segmentation
    del control.datasets["seg_data_testing"]

    ### Do Symbol Classification ###
    print("Starting Symbol Classification")
    symRec_config = Configuration.from_file(subconfig_dir+subconfigs_names[1]+".conf")
    control.addConfig(symRec_config, "sym_config")

    # Create Symbol LOS Graph
    # control.normalizeExpressions("testing")
    control.createExpressionGraphs("testing", "sym_config")


    # Get Symbol Classifier
    if load_cls:
        print("Loading Symbol Classifier")
        control.loadExpert(expert_dir+experts_names[1]+".dat", name=experts_names[1])
    else:
        print("Generating Symbol Classifier")
        control.generateExpert("sym_config", experts_names[1])
        control.saveExpert(experts_names[1], expert_dir+experts_names[1]+".dat")

    # Get Test Symbol Classification Dataset
    print("Creating Test Symbol Classifier Dataset")
    control.createDataset("testing", TYPES_DATA_SET.Symbol, label_source=experts_names[1], dataset_name="sym_data_testing", config_name="sym_config")

    # Classify Symbols
    print("Classifying Symbols")
    control.useExpert("testing", experts_names[1], "sym_data_testing")


    def classifySymbols(self, expressions, feature_dataset):
        predicted_classes = self.predictClasses(feature_dataset)
        #
        # dont call predicatedClasses
        #
        #

        for i in range(len(predicted_classes)):
            label = predicted_classes[i]
            exprId, symId = feature_dataset.sources[i]
            graph = expressions[exprId].expressionGraph
            graph.node[symId][NODE_ATTRIBUTES.PREDICTED_CLASS] = label


    # Clean After Symbol Classification
    del control.datasets["sym_data_testing"]

    ### Do Parsing ###
    print("Starting Parsing")
    par_config = Configuration.from_file(subconfig_dir+subconfigs_names[2]+".conf")
    control.addConfig(par_config, "par_config")

    # Get Parser
    if load_par:
        print("Loading Parser")
        control.loadExpert(expert_dir+experts_names[2]+".dat", name=experts_names[2])
    else:
        print("Generating Parser")
        control.generateExpert("par_config", experts_names[2])
        control.saveExpert(experts_names[2], expert_dir+experts_names[2]+".dat")

    # Get Test Relation Dataset
    print("Creating Test Parsing Dataset")
    control.createDataset("testing", TYPES_DATA_SET.Relation, label_source=experts_names[2], dataset_name="par_data_testing", config_name="par_config")

    # Parse Expressions
    print("Parsing Expressions")
    control.useExpert("testing", experts_names[2], "par_data_testing", True, True)

    # Clean After Parsing
    del control.datasets["par_data_testing"]

    ### Output results ###
    print("Outputting Label Graph Files")
    control.outputExpressions("testing", location=config.get_str("OUTPUT_DIR"), lg_type="OR")