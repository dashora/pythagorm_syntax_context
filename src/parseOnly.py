import sys
import time
import matplotlib.pyplot as plt
import numpy as np
import timeit
from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES, EDGE_ATTRIBUTES

if __name__ == "__main__":
    if len(sys.argv) < 5:
        exit()
    timing ={}
    config = Configuration.from_file(sys.argv[1])
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    load_seg = bool(int(sys.argv[2]))
    load_cls = bool(int(sys.argv[3]))
    load_par = bool(int(sys.argv[4]))

    expert_dir = config.get_str("EXPERT_DIR")
    experts_names = config.get("EXPERT_LIST")

    exprSet_dir = config.get_str("EXPRESSION_SET_DIR")

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions( name="testing", isTraining=False)
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")
  
  
    ### Do Parsing ###
    print("Starting Parsing")
    par_config = Configuration.from_file(subconfig_dir+subconfigs_names[2]+".conf")
    control.addConfig(par_config, "par_config")

    # Get Parser
    if load_par:
        print("Loading Parser")
        control.loadExpert(expert_dir+experts_names[2]+".dat", name=experts_names[2])
    else:
        print("Generating Parser")
        start =	timeit.default_timer()
        control.generateExpert("par_config", experts_names[2])
        stop = timeit.default_timer()
        timing["Parser Generating"] = stop -start
        start =	timeit.default_timer()
        control.saveExpert(experts_names[2], expert_dir+experts_names[2]+".dat")
        stop = timeit.default_timer()
        timing["Parser storage"] = stop -start
    # Get Test Relation Dataset
    print("Creating Test Parsing Dataset")
    start =	timeit.default_timer()	
    control.createDataset("testing", TYPES_DATA_SET.Relation, label_source=experts_names[2], dataset_name="par_data_testing", config_name="par_config")
    stop =	timeit.default_timer()
    timing["Create Test Data"] = stop - start 
    # Parse Expressions
    print("Parsing Expressions")
    start =	timeit.default_timer()	
    control.useExpert("testing", experts_names[2], "par_data_testing", True, True)
    stop =	timeit.default_timer()
    timing["Parsing Test Data"] = stop - start 

    # Clean After Parsing
    del control.datasets["par_data_testing"]

    ### Output results ###
    print("Outputting Label Graph Files")
    control.outputExpressions("testing",predictedLabels=False, location=config.get_str("OUTPUT_DIR"), lg_type="OR")

    for k in timing:
        print(k+' : '+str(timing[k]))
