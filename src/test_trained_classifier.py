
import sys
import time
import pickle
import os

from recognizer.configuration.configuration import Configuration
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.evaluation.eval_ops import EvalOps
from IO.write_output import write_lgfile, write_stroke_tree_lg_file, write_symbol_tree_lg_file
from IO.write_output import write_classified
from recognizer.testing.tester import TestClassification
from recognizer.data.dataset_processor import DatasetProcessor, TYPES_DATA_SET
from recognizer.symbols.math_expression import MathExpression
from recognizer.graph_representation.Graph_construction import GraphConstruction



def get_data_set_from_config(config_filename, isTraining=False):
    config = Configuration.from_file(config_filename)
    if isTraining:
        dataset_filename = config.data["DATASET_DIR"] + "/" + config.data["TRAINING_DATASET_NAME"] + ".dat"
    else:
        dataset_filename = config.data["DATASET_DIR"] + "/" + config.data["TESTING_DATASET_NAME"] + ".dat"

    eval_dataset = FeatureDataset.load_from_file(dataset_filename)
    return eval_dataset


def get_classifier_name(config_filename):
    #read the classifier name from the config file
    config = Configuration.from_file(config_filename)
    classifier_filename = config.data["CLASSIFIER_DIR"] + "/" + config.data["CLASSIFIER_NAME"] + ".dat"
    return classifier_filename


def test_and_construct_graph(classifier_filename, eval_dataset, dataset_type, expression_map, write_Lg=True, LgPath=None):

    predicted_output, confidence, evaluation_metrics = TestClassification.predict_layout_classes_with_confidence_from_file(classifier_filename, eval_dataset)
    accuracy, per_class_avg, per_class_stdev, errors = evaluation_metrics

    #create a dataset processor
    dataset_processor = DatasetProcessor(dataset_type, None)

    #update the layout graph in the expression with the predicted values
    dataset_processor.update_all_expressions(expression_map, eval_dataset, predicted_output, confidence, extractMST=True)

    #for every expression, write the output of the layout tree to lg file
    if write_Lg:
        #if dataset type is to iterate through
        if TYPES_DATA_SET.is_trace_level(dataset_type):
            for file_name, expression in expression_map.items():
                lay_out_tree = expression.getLayoutGraph()
                #get the lg file name and say what type of Lgfile (Primitive level or object level)
                base_name = os.path.basename(file_name).replace(".inkml", ".lg")
                lg_file_name = LgPath+base_name
                write_stroke_tree_lg_file(layout_tree=lay_out_tree, file_name=lg_file_name)
        else:
            for file_name, expression in expression_map.items():
                lay_out_tree = expression.getlLayoutGraph()
                #get the lg file name and say what type of Lgfile (Primitive level or object level)
                base_name = os.path.basename(file_name).replace(".inkml", ".lg")
                lg_file_name = LgPath+base_name
                write_symbol_tree_lg_file(layout_tree=lay_out_tree, file_name=lg_file_name)



def run_test(expression_dictionary=None):
    '''
    The function which gets the configuration from command-line and executes the test on the dataset
    :param expression_dictionary: inkml_file_names mapped to expression object
    '''
    config_filename = sys.argv[1]
    dataset_type = sys.argv[2]

    # from the config file , get the dataset name, inkml directory path
    config = Configuration.from_file(config_filename)
    testing_dataset = get_data_set_from_config(config_filename)
    classifier_name = get_classifier_name(config_filename)
    test_inkml_directory = config.get_str("TESTING_DATASET_PATH");
    test_lg_directory = config.get_str("TESTING_DATASET_LG_PATH");
    test_output_lg_directory = config.get_str("TESTING_OUTPUT_LG_PATH");

    #expression dictionary is passed from previous operations if this is a cascaded operation
    if expression_dictionary == None:
        expression_objects, expression_dictionary, valid_files, error_files = INKMLLoader.get_Expression_Objects(test_inkml_directory, test_lg_directory)

    #writes the output to lg file
    test_and_construct_graph(classifier_name, testing_dataset, dataset_type, expression_dictionary, write_Lg=True, LgPath=test_output_lg_directory)





if __name__ == '__main__':
    run_test()