#!/usr/bin/python 2.7
#Siyu Zhu
#May 31, 2013

import os
import numpy
import scipy
import math
from sys import argv
from scipy.misc import imread, imsave
#from skimage.transform import resize
from sys import argv
from myop import * 

def dataExt(fname, np):
    # Read infty data file and convert them into image
    # fname: filename of infty dataset(e. g. InftyCDB3A.txt)
    # targetdir: directory containing output image files
 

    #if not os.path.exists(targetdir):
    #    os.makedirs(targetdir)
    # define function that transform decimal to binary with fixed length
    getBin = lambda x, n: x >= 0 and str(bin(x))[2:].zfill(n) or "-" + str(bin(x))[3:].zfill(n)
    binlen = 8 # each decimal value present 8 bit of binary number
    f = open(fname)
    gridVec = []
    symbolName = []
    
    for k, line in enumerate(f.readlines()):

        #print 'image No. '+ str(k)
        l = line.split(',')
        docnum = l[0]
        symnum = l[1]
        mapht = int(l[2])
        bitwt = int(l[3])
        mapwt = int(math.floor((bitwt+7)/8)*8)
        bitmap = l[4:-1]
        b = []
        for bit in bitmap:
            decnum = int(bit)
            binnum = getBin(decnum, binlen)
            b.append(binnum)
        
        b = ''.join(b)
        im0 = numpy.zeros((mapht*mapwt, 1))
        for i in range(mapht*mapwt):
            im0[i] = int(b[i])
        im = numpy.reshape(im0, (mapht, mapwt), order = 'C')
        im = im[:, :bitwt]

	# compute grid vectors
	im = zeroPadding(im)
	im = imresize(im, np).flatten()
	#im = resize(im, (np, np), order = 0).flatten()

        gridVec.append(im)
        symbolName.append(symnum)
        
        if k%1000 == 0:
            print k, 'samples'
        #filename = 'im'+ str(k) + '.jpg'
        #fullname = os.path.join(targetdir, filename)
        #imsave(fullname, im)
    return gridVec, symbolName

def saveGrid(gridVec, filename):
	gridVec = numpy.asarray(gridVec)
	numpy.savetxt(filename, gridVec, delimiter = ',')
	return

def saveSym(symbolName, filename):
	f = open(filename, 'wb')
	symbolName = '\r\n'.join(symbolName)
	f.write(symbolName)
	f.close()
	return

def main(arg = '15'):
	n = int(arg) # dimension to generate 1xn
	vec, sym = dataExt('InftyCDB3A.txt', n)
	saveGrid(vec, 'gridVecA1x15.csv')
	saveSym(sym, 'symbolCodeA1x15.csv')

	vec, sym = dataExt('InftyCDB3B.txt', n)
	saveGrid(vec, 'gridVecB1x15.csv')
	saveSym(sym, 'symbolCodeB1x15.csv')

if __name__ == '__main__':
	main(argv[1])
