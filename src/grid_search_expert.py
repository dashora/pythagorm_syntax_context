import sys
import time
import pickle
import numpy as np
from recognizer.control.controller import Controller, EXPERT_TYPES
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.training.trainer import ClassifierTrainer
from recognizer.configuration.configuration import Configuration
from recognizer.evaluation.eval_ops import EvalOps
from recognizer.training.grid_search_ops import GridSearchOps

if __name__ == '__main__':
    print("Loading configuration....")
    main_config_filename = sys.argv[1]
    main_config = Configuration.from_file(main_config_filename)
    control = Controller(main_config, "default")

    aux_config_filename = sys.argv[2]
    aux_config = Configuration.from_file(aux_config_filename)

    out_append_filename = sys.argv[3]

    use_crossvalidation = aux_config.get_bool("USE_CROSSVALIDATION")
    if use_crossvalidation:
        n_partitions = aux_config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
        if n_partitions < 2:
            print("Invalid number of partitions <CLASSIFIER_CROSSVALIDATION_PARTITIONS> (Aux. Config)")
            raise Exception("Invalid number of partitions.")

        main_config.data["CLASSIFIER_CROSSVALIDATION_PARTITIONS"] = str(n_partitions)
    else:
        n_partitions = 1

    # prepare grid search ....
    search_params = GridSearchOps.extract_search_parameters(main_config, aux_config)
    grid_parameters, grid_parameter_values, grid_combined, grid_desc = search_params
    # compute all parameter combinations to explore
    all_combinations = GridSearchOps.enumerate_combinations(grid_parameters, grid_parameter_values, grid_combined)
    print(grid_desc)

    # Output file header
    GridSearchOps.write_search_header(out_append_filename, grid_desc, len(grid_parameters))

    n_combs = len(all_combinations)

    all_train_acc, all_train_avg, all_train_std = np.zeros(n_combs), np.zeros(n_combs), np.zeros(n_combs)
    all_eval_acc, all_eval_avg, all_eval_std = np.zeros(n_combs), np.zeros(n_combs), np.zeros(n_combs)

    # Load Training Expressions
    training_set_name = "training"
    testing_set_name = "testing"
    control.loadExpressions(config_name="default", name=training_set_name)
    control.preprocessExpressions("training")
    control.createExpressionGraphs("training")

    # If not using cross validate load Testing Expressions
    if not use_crossvalidation:
        testing = True
        control.loadExpressions(config_name="default", name="testing", isTraining=False)
        control.preprocessExpressions("testing")
        control.createExpressionGraphs("testing")
    else:
        testing = False




    print(" \t" * (len(grid_parameters)) + "TRAINING \t \t   TESTING")
    print(" \t" * (len(grid_parameters)) + "ACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD\tFeat.")

    for idx, combination in enumerate(all_combinations):
        # apply configuration change
        comb_desc = "\t".join([str(val) for val in combination])
        for i, parameter in enumerate(grid_parameters):
            main_config.data[parameter] = str(combination[i])


        #full_training, full_testing = create_datasets(main_config, raw_training, raw_testing)

        if use_crossvalidation:
            #train_r, eval_r, _, _, _ = ClassifierTrainer.train_crossvalidation(full_training, main_config, True, '\r')
            n_partitions = aux_config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
            train_r, eval_r = control.crossValidateExpert("training", n_partitions)

            all_train_acc[idx] = train_r[0].mean()
            all_train_avg[idx] = train_r[1].mean()
            all_train_std[idx] = train_r[2].mean()

            all_eval_acc[idx] = eval_r[0].mean()
            all_eval_avg[idx] = eval_r[1].mean()
            all_eval_std[idx] = eval_r[2].mean()
        else:
            expert_type = EXPERT_TYPES(main_config.get_int("EXPERT_TYPE"))
            dataset_type = EXPERT_TYPES.datasetType(expert_type)
            full_training = control.datasets[control.createDataset("training", dataset_type=dataset_type, label_source=None, dataset_name="training_data")]
            full_testing = control.datasets[control.createDataset("testing", dataset_type=dataset_type, label_source="training_data", dataset_name="testing_data")]
            # merge_type = EXPERT_TYPES.does_merge_strokes(expert_type)
            # full_training = control.datasets[control.createDataset("training", dataset_type, merge_strokes=merge_type, label_source=None, dataset_name="training_data")]
            # full_testing = control.datasets[control.createDataset("testing", dataset_type, merge_strokes=merge_type, label_source="training_data", dataset_name="testing_data")]
            # train classifier using all training data
            #classifier = ClassifierTrainer.train_classifier(full_training, main_config)
            expert = control.experts[control.createExpert("training_data", expert_type, "grid_expert")]

            # measure training error
            all_train_acc[idx], all_train_avg[idx], all_train_std[idx], _ = EvalOps.classifier_per_class_accuracy(expert, full_training)
            del control.datasets["training_data"]

            # measure testing error on full testing set
            all_eval_acc[idx], all_eval_avg[idx], all_eval_std[idx], _ = EvalOps.classifier_per_class_accuracy(expert, full_testing)
            del control.datasets["testing_data"]

        result_desc = (comb_desc + "\t" +
              str(round(all_train_acc[idx], 3)) + "\t" +
              str(round(all_train_avg[idx], 3)) + "\t" +
              str(round(all_train_std[idx], 3)) + "\t" +
              str(round(all_eval_acc[idx], 3)) + "\t" +
              str(round(all_eval_avg[idx], 3)) + "\t" +
              str(round(all_eval_std[idx], 3)) + "\t" + "")
              #str(full_training.num_attributes()))
        print(result_desc)

        out_file = open(out_append_filename, "a")
        out_file.write(result_desc + "\n")
        out_file.close()
