
import sys
import math
import numpy as np
import random
import os

from sklearn.neighbors import KNeighborsClassifier
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.preprocessing.trace_smoother import TraceSmoother
from recognizer.configuration.configuration import Configuration
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.visualization.SVG_generator import SVGGenerator
from recognizer.visualization.INKML_generator import INKMLGenerator
from recognizer.symbols.math_symbol_trace import MathSymbolTrace
from recognizer.symbols.math_symbol import MathSymbol

def load_raw_data(config):
    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # check if only  a subset of classes will be used ....
    if config.contains("ONLY_ACTIVE_CLASSES"):
        active_classes = config.get("ONLY_ACTIVE_CLASSES")
    else:
        active_classes = None

    workers = config.get_int("PARALLEL_INKML_LOADING_WORKERS", 10)

    feature_extractor = FeatureExtractor(config)

    # load training data, no features extracted, only symbols pre-processed
    training_path = config.data["TRAINING_DATASET_PATH"]
    data_info = INKMLLoader.load_INKML_directory(training_path, True, [feature_extractor], label_replacement,
                                                 False, None, None, True, workers, active_classes)

    raw_training, valid_files, error_files = data_info

    all_symbols_data, raw_labels, _ = raw_training

    # split
    all_symbols = []
    all_features = []
    for symbol_data, symbol_features in all_symbols_data:
        all_symbols.append(symbol_data)
        all_features.append(symbol_features)

    return all_symbols, all_features, raw_labels


def compute_weights(knn, features, labels, chunk_size):
    n_chunks = int(math.ceil(len(features) / float(chunk_size)))

    all_classes = knn.classes_.tolist()

    raw_weights = {class_name: [] for class_name in all_classes}
    for chunk_idx in range(n_chunks):
        print("Processing chunk {0} of {1}".format(chunk_idx + 1, n_chunks))
        chunk_start = chunk_idx * chunk_size
        chunk = np.array(features[chunk_start:chunk_start + chunk_size])

        raw_probs = knn.predict_proba(chunk)

        # Now, for each sample
        for offset in range(chunk.shape[0]):
            idx = chunk_start + offset
            class_idx = all_classes.index(labels[idx])
            class_prob = raw_probs[offset, class_idx]

            raw_weights[labels[idx]].append((class_prob, idx))
            #print(str(labels[idx]) + "\t" + str(class_idx) + "\t" + str(class_prob))

    normalized_weights = {}
    for class_name in raw_weights:
        # total weight in the class
        total_class_weight = sum([class_prob for class_prob, idx in raw_weights[class_name]])

        # normalized weights per sample
        class_weights = [(class_prob / total_class_weight, idx) for class_prob, idx in raw_weights[class_name]]
        normalized_weights[class_name] = sorted(class_weights, reverse=True)

    return normalized_weights


def locally_distort_trace(trace, max_local_prc):
    # using original points
    rnd_vals = (np.random.rand(len(trace.original_points)) * 2.0 - 1.0).tolist()

    # trace.sharp_points
    # (original idx, (x, y))
    # creating distorted copy of array, identify new sharp points
    new_points = []
    for idx, (x, y) in enumerate(trace.original_points):
        if idx == 0:
            # add start point
            new_points.append((x, y))
        else:
            # get vector
            old_prev_x, old_prev_y = trace.original_points[idx - 1]
            new_prev_x, new_prev_y = new_points[idx - 1]

            dir_x = x - old_prev_x
            dir_y = y - old_prev_y

            dist_val = (1.0 + rnd_vals[idx] * max_local_prc)
            new_x = new_prev_x + dir_x * dist_val
            new_y = new_prev_y + dir_y * dist_val

            new_points.append((new_x, new_y))

    # translate trace to minimize distance between old and new sharp points ....
    delta_x = 0.0
    delta_y = 0.0
    for idx, (new_point_x, new_point_y) in enumerate(new_points):
        old_point_x, old_point_y = trace.original_points[idx]

        delta_x += (old_point_x - new_point_x)
        delta_y += (old_point_y - new_point_y)

    delta_x /= len(new_points)
    delta_y /= len(new_points)

    for idx, (x, y) in enumerate(new_points):
        new_points[idx] = (x + delta_x, y + delta_y)

    return MathSymbolTrace(trace.id, new_points)


def distort_symbol(symbol, max_rotation, max_aspect_ratio, max_local_prc):
    # first apply the local distortion
    distorted_traces = []
    for trace in symbol.traces:
        trace_points = locally_distort_trace(trace, max_local_prc)
        distorted_traces.append(trace_points)

    # create symbol
    new_symbol = MathSymbol(0, distorted_traces, symbol.truth)
    # normalize size and location around origin to allow rotation of traces
    new_symbol.normalize()

    # then apply linear transformations
    alpha = max_rotation * (random.random() * 2.0 - 1.0)
    for trace_idx, trace in enumerate(distorted_traces):
        transformed_points = np.array(trace.points)
        # scale ...
        ratio_distortion = max_aspect_ratio * (random.random() * 2.0 - 1.0)

        scale_x = 1.0 - ratio_distortion
        scale_y = 1.0

        transformed_points[:, 0] *= scale_x
        transformed_points[:, 1] *= scale_y

        # rotation...
        rotation_matrix = np.array([[math.cos(alpha), math.sin(alpha)], [-math.sin(alpha), math.cos(alpha)]])
        transformed_points = np.dot(transformed_points, rotation_matrix.T)

        # update points and remove old bounding box
        trace.points = [(point[0], point[1]) for point in transformed_points.tolist()]
        trace.bounding_box = None

    # normalize size and locations after linear transformations
    # note that this symbol has NOT been smoothed
    new_symbol.normalize()

    return new_symbol

def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python create_synthetic_data.py config")
        print("Where")
        print("\tconfig\t= Path to main configuration file")
        return

    print("Loading configuration ....")
    # main configuration
    config_filename = sys.argv[1]
    main_config = Configuration.from_file(config_filename)

    # parameters
    n_neighbors = main_config.get_int("SYNTHETIC_DATA_KNN_K")
    weighting = main_config.get_str("SYNTHETIC_DATA_KNN_WEIGHTING")
    search_workers = main_config.get_int("SYNTHETIC_DATA_KNN_WORKERS")
    knn_chunk_size = main_config.get_int("SYNTHETIC_DATA_KNN_CHUNK_SIZE")

    prc_synthetic = main_config.get_float("SYNTHETIC_DATA_GENERATION_PRC")

    max_rotation = main_config.get_float("SYNTHETIC_DATA_MAX_ROTATION")
    max_aspect_ratio = main_config.get_float("SYNTHETIC_DATA_MAX_ASPECT_RATIO")
    max_local_prc = main_config.get_float("SYNTHETIC_DATA_MAX_LOCAL_DIST_PRC")

    SVG_output_dir = main_config.get_str("SYNTHETIC_DATA_SVG_OUTPUT")
    save_SVG_output = main_config.get_bool("SYNTHETIC_DATA_SVG_SAVE_DIST")
    save_top_samples = main_config.get_int("SYNTHETIC_DATA_SVG_SAVE_TOPN")

    output_dir = main_config.get_str("SYNTHETIC_DATA_OUTPUT_DIR")

    # load data ....
    print("Loading data ....")
    all_symbols, all_features, raw_labels = load_raw_data(main_config)

    print("Total Symbols found: " + str(len(all_symbols)))

    # Train the K-nearest neighbors classifier
    print("Fitting K-NN ....")
    knn = KNeighborsClassifier(n_neighbors, weights=weighting, n_jobs=search_workers)
    knn.fit(all_features, raw_labels)

    # Find probabilities per class
    print("Weighting samples ...")
    sample_weights = compute_weights(knn, all_features, raw_labels, knn_chunk_size)

    # show top samples
    if save_top_samples > 0:
        for class_idx, class_name in enumerate(sample_weights):
            for idx in range(min(save_top_samples, len(sample_weights[class_name]))):
                weight, sample_idx = sample_weights[class_name][idx]

                symbol = all_symbols[sample_idx]
                output_path = SVG_output_dir + "/top_class_" + str(class_idx) + "_idx_" + str(idx) + ".svg"
                SVGGenerator.saveSymbolAsSVG(symbol, output_path)

                print(str((class_idx, class_name, idx, sample_idx, weight)))

    # create distorted copies per class
    for class_idx, class_name in enumerate(sample_weights):
        print("Generating Synthetic samples for class: " + str(class_name))
        # use random sampling with the given weights ...
        class_weights = sample_weights[class_name]
        class_size = len(class_weights)

        # uniform random sample
        to_create = int(round(class_size * prc_synthetic, 0))
        positions = sorted(np.random.rand(to_create).tolist())

        # use weights ....
        weight_pos = 0
        current_weight = 0.0
        sample_indices = []
        for weight in positions:
            # (weight, sample_idx)
            while weight > current_weight + class_weights[weight_pos][0]:
                current_weight += class_weights[weight_pos][0]
                weight_pos += 1

            sample_indices.append(class_weights[weight_pos][1])

        class_path = output_dir + "/" + str(class_idx)
        if not os.path.isdir(class_path):
            os.mkdir(class_path)

        # now, create the distorted copies
        for dist_idx, sample_idx in enumerate(sample_indices):
            symbol = all_symbols[sample_idx]
            new_symbol = distort_symbol(symbol, max_rotation, max_aspect_ratio, max_local_prc)

            symbol_filename = class_path + "/synth_" + str(class_idx) + "_" + str(dist_idx) + ".inkml"

            INKMLGenerator.save_symbols(symbol_filename, [new_symbol])

            if save_SVG_output:
                output_original = SVG_output_dir + "/original_class_{0}_idx_{1}.svg".format(class_idx, sample_idx)
                SVGGenerator.saveSymbolAsSVG(symbol, output_original)
                output_distorted = SVG_output_dir + "/distorted_class_{0}_idx_{1}.svg".format(class_idx, sample_idx)
                SVGGenerator.saveSymbolAsSVG(new_symbol, output_distorted)

    print("Finished!")

if __name__ == '__main__':
    main()