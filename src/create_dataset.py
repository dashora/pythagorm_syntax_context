
#=====================================================================
#  Generates a training set from a directory containing the inkml
#
#  Created by:
#      - Kenny Davila (May 5, 2016)
#
#=====================================================================

import sys
import time
import pickle
from recognizer.configuration.configuration import Configuration
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.preprocessing.inkml_loader import INKMLLoader, TYPES_DATA_SET
from recognizer.preprocessing.img_loader import ImgLoader
from recognizer.features.feature_extractor import FeatureExtractor
import timeit
from optparse import OptionParser


def main():
    # usage check
    entire_start_time = time.time()
    parser = OptionParser()
    parser.add_option("--config", dest="config", help="Configuration file name")
    parser.add_option('--type', dest="type", type='choice', choices=[type.name for type in TYPES_DATA_SET])

    parser.add_option("--do_training", action="store_false", dest="do_training", default=True, help="flag to say training files has to be extracted")
    parser.add_option("--do_testing", action="store_false", dest="do_testing", default=True, help="flag to say testing files has to be extracted")
    parser.add_option("--load_label_map", action="store_false", dest="load_label_map", default=False,help="flag to say label_map has to be laded")

    #read from system arguments
    (cmd_options, args) = parser.parse_args()


    print("\nReading from config file", cmd_options.config, "\n Type of dataset collection:",cmd_options.type,"\nGenerate Training dataset? \t :", cmd_options.do_training,"\nGenerate Testing dataset? \t :", cmd_options.do_testing)


    #validate the command line options
    if(not cmd_options.config):
        parser.error("Configuration file name is not specified")
    if not cmd_options.type:
        parser.error("Type of dataset collection is not specified")


    config_filename = cmd_options.config
    config = Configuration.from_file(config_filename)
    data_set_type = cmd_options.type

    paths = []
    do_training = cmd_options.do_training
    do_testing = cmd_options.do_testing
    load_label_map = cmd_options.load_label_map

    if do_training:
        out_filename = config.data["DATASET_DIR"] + "/" + config.data["TRAINING_DATASET_NAME"] + ".dat"
        if not TYPES_DATA_SET.requires_Lg_Files(data_set_type):
            paths.append((config.data["TRAINING_DATASET_PATH"], True, out_filename))
        else:
            paths.append(((config.data["TRAINING_DATASET_PATH"], config.data["TRAINING_DATASET_LG_PATH"]), True, out_filename))


    if do_testing:
        out_filename = config.data["DATASET_DIR"] + "/" + config.data["TESTING_DATASET_NAME"] + ".dat"
        truth_available = config.get_bool("TESTING_DATASET_TRUTH")
        if not TYPES_DATA_SET.requires_Lg_Files(data_set_type):
            paths.append((config.data["TESTING_DATASET_PATH"], truth_available, out_filename))
        else:
            paths.append(
                ((config.data["TESTING_DATASET_PATH"], config.data["TESTING_DATASET_LG_PATH"]), truth_available, out_filename))


    # feature extractor
    feature_extractor = FeatureExtractor(config)

    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}
    
    # set pre-processing type
    if config.contains("PREPROCESS_SMOOTHEN"):
        INKMLLoader.set_proprocess_type(config.get("PREPROCESS_SMOOTHEN"))
    else:
        INKMLLoader.set_proprocess_type("CATMULL")

    workers = config.get_int("PARALLEL_INKML_LOADING_WORKERS", 10)

    # check if only  a subset of classes will be used ....
    if config.contains("ONLY_ACTIVE_CLASSES"):
        active_classes = config.get("ONLY_ACTIVE_CLASSES")
    else:
        active_classes = None

    # label mapping
    if load_label_map:
        map_filename = config.data["DATASET_DIR"] + "/" + config.data["MAP_DATASET_NAME"] + ".dat"
        map_dataset = FeatureDataset.load_from_file(map_filename)
        label_mapping = map_dataset.label_mapping
        class_mapping = map_dataset.class_mapping
    else:
        label_mapping = None
        class_mapping = None

    # sources
    save_sources = config.get_bool("SAVE_DATASET_SOURCES", True)

    # data normalization
    normalize_data = config.get_bool("SCALE_NORMALIZE_DATA", False)
    data_normalizer = None

    loaderType = config.get_str("INPUT_DATA_LOADER_TYPE")

    for path, ground_truth, out_filename in paths:
        print("")
        #print("Processing Path: " + path)
        start_time = time.time()
        if loaderType.lower() == "inkml":
            #argument dataset_type denotes how to interpret the input inkml file
            data_info = INKMLLoader.load_INKML_directory(path, ground_truth, feature_extractor, label_replacement,
                                                     save_sources, label_mapping, class_mapping, True, workers,
                                                     active_classes, data_set_type)
        elif loaderType.lower() == "img":
            data_info = ImgLoader.load_Lg_List(path, feature_extractor, config, save_sources=save_sources, workers=workers, label_mapping=label_mapping, class_mapping=class_mapping)
        else:
            print("Invalid Loader Type: " + loaderType + "\n INPUT_DATA_LOADER_TYPE Should be INKML or IMG.")
            raise ValueError

        dataset, valid_files, error_files = data_info
        dataset.config = config

        # keep label/class mapping (new if training, same if testing)
        label_mapping = dataset.label_mapping
        class_mapping = dataset.class_mapping

        # data normalization
        if normalize_data:
            # if data normalizer is empty, a new will be created
            dataset.normalize_data(data_normalizer)
            # keep the data normalizer (if new)
            data_normalizer = dataset.normalizer

        end_time = time.time()

        print("")
        print("Total input files: " + str(len(valid_files) + len(error_files)))
        print("\tTotal valid files: " + str(len(valid_files)))
        print("\tFiles with errors: ")
        for filename in error_files:
            print("\t\t- " + ''.join(filename))

        print("\tTotal files with error: " + str(len(error_files)))
        print("Dataset Properties")
        print("\tTotal Classes Found: " + str(len(dataset.label_mapping)))
        print("\tTotal Valid Symbols: " + str(dataset.num_samples()))
        print("\tTotal Features: " + str(dataset.num_attributes()))
        print("Total Elapsed Time: " + str(end_time - start_time) + " s")

        print("Saving .... ")
        # now that all the samples have been collected, write them all in the
        # output file
        try:
            dataset.save_to_file(out_filename)
        except:
            print("File <" + sys.argv[2] + "> could not be created")
            return

        print("")

    print("Done!\n\n")
    entire_end_time = time.time()
    print(entire_end_time - entire_start_time)
    print("Time taken to create dataset")



if __name__ == '__main__':
    main()
