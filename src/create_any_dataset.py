 # =====================================================================
#  Generates a training set from a directory containing inkml and lg files
#  The dataset can be for classification, segmentation or parsing
#
#  Derived from create dataset script by Kenny Devilla:
#      - Lakshmi Ravi (Jan 20, 2016)
#
# =====================================================================

import sys
import time
import pickle
from recognizer.configuration.configuration import Configuration
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.preprocessing.img_loader import ImgLoader
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.data.dataset_processor import DatasetProcessor, TYPES_DATA_SET
from optparse import OptionParser


def main():
    # usage check
    start_time = time.time()
    parser = OptionParser()
    #get the config file name
    parser.add_option("--config", dest="config", help="Configuration file name")
    #get the type of data_set to be extracted
    parser.add_option('--type', dest="type", type='choice', choices=[type.name for type in TYPES_DATA_SET], help="type of dataset to be extracted")

    parser.add_option("--do_training", action="store_false", dest="do_training", default=True,
                      help="flag to say training files has to be extracted")
    parser.add_option("--do_testing", action="store_false", dest="do_testing", default=True,
                      help="flag to say testing files has to be extracted")
    parser.add_option("--load_label_map", action="store_false", dest="load_label_map", default=False,
                      help="flag to say label_map has to be laded")

    # read from system arguments
    (cmd_options, args) = parser.parse_args()

    print("\nReading from config file", cmd_options.config, "\n Type of dataset collection:", cmd_options.type,
          "\nGenerate Training dataset? \t :", cmd_options.do_training, "\nGenerate Testing dataset? \t :",
          cmd_options.do_testing)

    # validate the command line options
    if (not cmd_options.config):
        parser.error("Configuration file name is not specified")
    if not cmd_options.type:
        parser.error("Type of dataset collection is not specified")

    config_filename = cmd_options.config
    config = Configuration.from_file(config_filename)
    data_set_type = cmd_options.type

    paths = []
    do_training = cmd_options.do_training
    do_testing = cmd_options.do_testing
    load_label_map = cmd_options.load_label_map

    #get file names from config
    training_directory= (config.data["TRAINING_DATASET_PATH"], config.data["TRAINING_DATASET_LG_PATH"])
    training_output_data_set = config.data["DATASET_DIR"] + "/" + config.data["TRAINING_DATASET_NAME"] + ".dat"

    testing_directory = (config.data["TESTING_DATASET_PATH"], config.data["TESTING_DATASET_LG_PATH"])
    testing_output_data_set = config.data["DATASET_DIR"] + "/" + config.data["TESTING_DATASET_NAME"] + ".dat"

    if do_training:
        paths.append(( training_directory, True, training_output_data_set))

    if do_testing:
        truth_available = config.get_bool("TESTING_DATASET_TRUTH")
        paths.append((testing_directory, truth_available, testing_output_data_set))


    # feature extractor
    feature_extractor = FeatureExtractor(config)

    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # set pre-processing type
    if config.contains("PREPROCESS_SMOOTHEN"):
        INKMLLoader.set_proprocess_type(config.get("PREPROCESS_SMOOTHEN"))
    else:
        INKMLLoader.set_proprocess_type("CATMULL")

    #set graph extraction type
    if config.contains("GRAPH_REPRESENTATION"):
        INKMLLoader.set_graph_representation_type(config.get("GRAPH_REPRESENTATION"))
    else:
        INKMLLoader.set_graph_representation_type("LINE_OF_SIGHT")

    #set visualization parameters - generate the image of a graph after loading an inkml file
    visualize_expression = config.get_bool("VISUALIZE_EXPRESSION", False)
    visualization_plot_path = config.get_str("PLOT_PATH")
    INKMLLoader.set_visualization_parameters(visualize_expression, visualization_plot_path)

    workers = config.get_int("PARALLEL_INKML_LOADING_WORKERS", 10)

    # check if only  a subset of classes will be used ....
    if config.contains("ONLY_ACTIVE_CLASSES"):
        active_classes = config.get("ONLY_ACTIVE_CLASSES")
    else:
        active_classes = None

    # label mapping
    if load_label_map:
        map_filename = config.data["DATASET_DIR"] + "/" + config.data["MAP_DATASET_NAME"] + ".dat"
        map_dataset = FeatureDataset.load_from_file(map_filename)
        label_mapping = map_dataset.label_mapping
        class_mapping = map_dataset.class_mapping
    else:
        label_mapping = None
        class_mapping = None

    # sources
    save_sources = config.get_bool("SAVE_DATASET_SOURCES", True)

    # data normalization
    normalize_data = config.get_bool("SCALE_NORMALIZE_DATA", False)
    data_normalizer = None

    loaderType = config.get_str("INPUT_DATA_LOADER_TYPE")
    dataset_creator = DatasetProcessor(data_set_type, feature_extractor)


    for path, ground_truth, out_filename in paths:
        print("")
        print("Processing Path: " + path[0])
        inkml_path, lg_path = path
        list_expressions, expression_map,valid_files, error_files = INKMLLoader.get_Expression_Objects(inkml_path, lg_path)
        dataset = dataset_creator.create_dataset_for_expressions(list_expressions, label_mapping, class_mapping)
        dataset.config = config

        # keep label/class mapping (new if training, same if testing)
        label_mapping = dataset.label_mapping
        class_mapping = dataset.class_mapping

        # data normalization
        if normalize_data:
            # if data normalizer is empty, a new will be created
            dataset.normalize_data(data_normalizer)
            # keep the data normalizer (if new)
            data_normalizer = dataset.normalizer

        end_time = time.time()


        #USER MESSAGES
        print("")
        print("Total input files: " + str(len(valid_files) + len(error_files)))
        print("\tTotal valid files: " + str(len(valid_files)))
        print("\tFiles with errors: ")
        for filename in error_files:
            print("\t\t- " + ''.join(filename))

        print("\tTotal files with error: " + str(len(error_files)))
        print("Total Elapsed Time: " + str(end_time - start_time) + " s")
        print("\tTotal Valid Symbols: " + str(dataset.num_samples()))
        print("\tTotal Features: " + str(dataset.num_attributes()))
        print("\tTotal classes: " + str(dataset.num_classes()))


        print("Saving the data in "+ out_filename)
        # now that all the samples have been collected, write them all in the
        # output file
        try:
            dataset.save_to_file(out_filename)
        except:
            print("File <" + out_filename + "> could not be created")
            return

        print("Dataset creation successful")


if __name__ == '__main__':
    main()

