
import sys
import time
import pickle
import numpy as np

from concurrent.futures import ProcessPoolExecutor

from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.configuration.configuration import Configuration
from recognizer.features.code_book_feature_extractor import CodeBookFeatureExtractor

def load_raw_data(config):
    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # check if only  a subset of classes will be used ....
    if config.contains("ONLY_ACTIVE_CLASSES"):
        active_classes = config.get("ONLY_ACTIVE_CLASSES")
    else:
        active_classes = None

    workers = config.get_int("PARALLEL_INKML_LOADING_WORKERS", 10)

    # load training data, no features extracted, only symbols pre-processed
    training_path = config.data["TRAINING_DATASET_PATH"]
    data_info = INKMLLoader.load_INKML_directory(training_path, True, None, label_replacement,
                                                 False, None, None, True, workers, active_classes)

    raw_training, valid_files, error_files = data_info

    all_symbols, raw_labels, _ = raw_training

    return all_symbols, raw_labels


def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python create_code_book.py config")
        print("Where")
        print("\tconfig\t= Path to main configuration file")
        return

    print("Loading configuration....")
    # main configuration
    config_filename = sys.argv[1]
    main_config = Configuration.from_file(config_filename)

    # Experiment parameters
    cb_feature_extractor = CodeBookFeatureExtractor.create_from_config(main_config)
    
    print("Loading data....")
    # dataset features will change, but raw data will not...
    time_preprocessing_start = time.time()
    all_symbols, all_raw_labels = load_raw_data(main_config)
    total_symbols = len(all_symbols)

    print("")
    print("Total symbols found: " + str(total_symbols))

    workers = main_config.get_int("PARALLEL_FEATURE_EXTRACTION_WORKERS", 10)

    print("Computing training features")

    boxes_features = []

    num_boxes = 0
    clear_string = " " * 80
    with ProcessPoolExecutor(max_workers=workers) as executor:
        for i, (symbol_features, symbol_boxes) in enumerate(executor.map(cb_feature_extractor.getFeatures, all_symbols)):
            print(clear_string, end="\r")
            print("\tprocessing ({0:.3f}%): ".format((i + 1) * 100 / total_symbols), end="\r")

            boxes_features += symbol_features

            num_boxes += len(symbol_boxes)

    print("Total features extracted: " + str(num_boxes))

    time_preprocessing_end = time.time()

    clust_train_data = np.array(boxes_features)

    print("\tclustering...")
    time_clustering_start = time.time()

    code_book, mbk_means_labels = cb_feature_extractor.getCodeBook(clust_train_data)

    time_clustering_end = time.time()
    total_time_clustering = time_clustering_end - time_clustering_start

    # Save code book
    code_book.save_to_file(main_config)

    time_completed = time.time()
    total_elapsed_time = time_completed - time_preprocessing_start

    feature_extraction_time = time_preprocessing_end - time_preprocessing_start
    print("Time pre-CV: (I/O + Feat. Ext + Label Mapping + Data Split): " + str(feature_extraction_time))
    print("Time CV clustering: " + str(total_time_clustering))
    print("Total elapsed time: " + str(total_elapsed_time))

if __name__ == '__main__':
    main()