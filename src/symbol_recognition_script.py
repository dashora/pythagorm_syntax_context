import sys
import time

from recognizer.control.controller import Controller
from recognizer.configuration.configuration import Configuration
from recognizer.data.dataset_builder import TYPES_DATA_SET
from recognizer.graph_representation.Graph_construction import NODE_ATTRIBUTES


if __name__ == "__main__":
    if len(sys.argv) < 3:
        exit()

    config = Configuration.from_file(sys.argv[1])
    subconfig_dir = config.get_str("SUB_CONFIGS_DIR")
    subconfigs_names = config.get("SUB_CONFIGS")

    control = Controller(config, "default")

    load_sym = bool(int(sys.argv[2]))

    expert_dir = config.get_str("EXPERT_DIR")
    experts_names = config.get("EXPERT_LIST")



    ### Do classification ###
    print("Starting classification")
    sym_config = Configuration.from_file(subconfig_dir+subconfigs_names[0]+".conf")
    control.addConfig(sym_config, "sym_config")

    # Get classifier
    if load_sym:
        print("Loading classifier")
        control.loadExpert(expert_dir+experts_names[0]+".dat", name=experts_names[0])
    else:
        print("Generating classifier")
        control.generateExpert("sym_config", experts_names[0])
        control.saveExpert(experts_names[0], expert_dir+experts_names[0]+".dat")

    ### Load Test Expressions ###
    print("Loading Test Expressions")
    control.loadExpressions("testing", isTraining=False)
    control.preprocessExpressions("testing")
    control.createExpressionGraphs("testing")

    # Get Test Relation Dataset
    print("Creating Test classification Dataset")
    control.createDataset("testing", TYPES_DATA_SET.Symbol, label_source=experts_names[0], dataset_name="sym_data_testing", config_name="sym_config")

    # Parse Expressions
    print("classification Expressions")
    control.useExpert("testing", experts_names[0], "sym_data_testing")

    # Clean After classification
    del control.datasets["sym_data_testing"]

    ### Output  Final results ###
    print("Outputting Label Graph Files")
    control.outputExpressions("testing", location=config.get_str("OUTPUT_DIR"), lg_type="OR")
