import os
from shutil import copyfile
import numpy as np
from skimage import io
import matplotlib.pyplot as plt
import Utility
import ConnectedComp
from recognizer.configuration.configuration import Configuration

def createExpression(ExpId, symStrings, config):
    objects = []
    rels = []
    notes = []
    for sym in symStrings:
        obj, rel, n = createSymbol(sym, config)
        objects.append(obj)
        rels.append(rel)
        notes.append(n)
    pn = 0
    numObjs = len(symStrings)
    exprpath = config.get_str("EXPR_DIR_PATH")
    os.makedirs(exprpath, exist_ok=True)
    lg = open(exprpath + ExpId + ".lg", 'w')
    lg.write("# IUD, " + ExpId + "\n")
    lg.write("#Exp " + ExpId + " " + symStrings[0].split(" ")[-1] + "\n")
    for n in notes:
        lg.write(n + "\n")
    lg.write("# [ OBJECTS ]\n")
    lg.write("# Primitive Nodes (N): " + "?" + "\n")
    lg.write("#    Objects (O): " + str(numObjs) + "\n")
    for obj in objects:
        lg.write(obj + "\n")
    reledges = 0
    relcount = 0
    lg.write("\n# [ RELATIONSHIPS ]\n# Primitive Edges (E): ? (? merge, ? relationship \n")
    lg.write("#\tObject Relationships (R): " + str(len(symStrings) - 1) + "\n")
    for rel in rels:
        if len(rel) > 3:
            lg.write(rel + "\n")
    lg.close()


def createSymbol(argString, config):
    values = argString.split(" ")
    object = "O, "
    rel = "R, "
    note = "#Sym "
    object += values[0] + ", " + values[1] + ", 1.0, " + values[0]
    if values[7] != "-1":
        rel += values[7] + ", " + values[0] + ", " + values[9] + ", 1.0"
    note += values[0] + ", " + values[3] + ", " + values[2]
    path = config.get_str("DATASET_IMG_PATH")
    sympath = config.get_str("SYM_IMG_DIR_PATH")
    img = io.imread(path+values[-1], "PNG")
    os.makedirs(sympath + values[1], exist_ok=True)
    io.imsave(sympath + values[1] + "//" + values[0] + ".PNG", img[int(values[3]):int(values[5]), int(values[2]):int(values[4])])
    return object,rel,note

def readInfty(dataFile, config):
    infty = open(config.get_str("EXPR_LIST_PATH") + dataFile, 'r')
    for line in infty:
        if line[0] == 'E' or line[0] == 'e':
            ExpId = line.split(" ")[1].strip()
            symStrings = []
            symLine = infty.readline().strip()

            while symLine != "" and symLine[0] != "E":
                symStrings.append(symLine)
                symLine = infty.readline().strip()
            createExpression(ExpId, symStrings, config)

def readExpression():
    pass

def readExprSymbols(exprId, config):
    expr_path = config.get_str("EXPR_LG_DIR_PATH")
    try:
        expr = open(expr_path + exprId + ".lg", "r")
    except:
        return [], []
    symbols = []
    classifications = []
    for line in expr:
        if line != "" and line[0] == "O":
            parts = line.strip().split(", ")
            symbols.append(parts[1])
            classifications.append(parts[2])
    return symbols,classifications

def symbolsFromExprList(exprList, config):
    exprPath = config.get_str("EXPR_LIST_PATH")
    symbols = []
    classifications = []
    for line in open(exprPath + exprList):
        syms,cls = readExprSymbols(line.strip(), config)
        symbols += syms
        classifications += cls
    return symbols,classifications

def createExprList(name, config):
    pass


def copyToDir(listfile, path, newDir):
    #os.makedirs(newDir, exist_ok=True)
    for line in open(listfile):
        copyfile(path+line.strip()+".lg", newDir+line.strip()+".lg")

def split(config, trainingfile="Training.txt", testingfile="Testing.txt"):
    training_file = open(trainingfile, 'w')
    testing_file = open(testingfile, 'w')
    train = np.array([])
    test = np.array([])
    syms = dict([])
    exprList = open(config.get_str("EXPR_LIST_PATH") + config.get_str("EXPR_FILE"), 'r')

    numExpr = 0
    for line in exprList:
        symbols,classifications = readExprSymbols(line.strip(), config)
        if symbols == []:
            continue
        numExpr += 1
        sample = np.array([0.0 for x in range(len(train))])
        for sym in classifications:
            if sym not in syms:
                syms[sym] = len(syms)
                train = np.append(train, 0.0)
                test = np.append(test, 0.0)
                sample = np.append(sample, 0.0)
            sample[syms.get(sym)] += 1
        trainError = np.sqrt(np.power(((train+sample)/(train+sample+test))-.666, 2).sum() + np.power((test/(train+sample+test))-.333, 2).sum())
        testError = np.sqrt(np.power(((test+sample)/(train+sample+test))-.333, 2).sum() + np.power((train/(train+sample+test))-.666, 2).sum())
        if trainError <= testError:
            training_file.write(line.strip() + "\n")
            train += sample
        else:
            testing_file.write(line.strip() + "\n")
            test += sample
    training_file.close()
    testing_file.close()
    #print np.sqrt(np.power(((test)/(train+test))-.333, 2).sum() + np.power((train/(train+test))-.666, 2).sum())
    total = (test+train).astype(np.int32)
    trainTotal = train.sum()
    testTotal = test.sum()
    print(numExpr)
    print("Average Deviation from expected Symbol split: " + str((abs((train/total)-.666).sum() + abs((test/total)-.333).sum())/(len(train)+len(test))))
    print("Average Deviation from class distribution: " + str(abs((train/trainTotal)-(test/testTotal)).mean()))


if __name__ == "__main__":
    #os.makedirs("Data//Symbols//A", exist_ok=True)
    # stringArgs = "15229 ast 871 1643 894 1667 ( 15228 , RSUP ) 28000191 00_005.png"

    config = Configuration.from_file("ImageConf.conf")
    # sym = createSymbol(stringArgs, config)
    # for s in sym:
    #     print(s)
    #readInfty(config.get_str("EXPR_FILE"), config)
    #split(config=config)
    copyToDir("..//Data//infty//InftyCDB-2//Training.txt", "..//Data//Expressions//LG//", "..//Data//Expressions//LG//Training//")
    copyToDir("..//Data//infty//InftyCDB-2//Testing.txt", "..//Data//Expressions//LG//", "..//Data//Expressions//LG//Testing//")
    # img = io.imread("Data/infty/InftyCDB-2/English/Images_eng/00_002.png", "PNG")
    # plt.figure()
    # plt.imshow(img)
    # plt.figure()
    # plt.imshow(img[920:978,108:176])
    # plt.show()
