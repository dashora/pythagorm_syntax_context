
import sys
import time
import math
import numpy as np

from pyemd import emd

from sklearn.neighbors import KNeighborsClassifier

from recognizer.configuration.configuration import Configuration
from recognizer.preprocessing.inkml_loader import INKMLLoader
from recognizer.features.feature_extractor import FeatureExtractor
from recognizer.data.feature_dataset import FeatureDataset
from recognizer.recognition.symbol_classifier import SymbolClassifier
from recognizer.evaluation.eval_ops import EvalOps

def emd_distance(x, y, distance_matrix=None):
    return emd(x, y, distance_matrix)

def train_knn(training_dataset, config, use_EMD, distance_matrix):
    assert isinstance(training_dataset, FeatureDataset)

    k = config.get_int("KNN_K")
    workers = config.get_int("KNN_WORKERS", -1)

    #NearestNeighbors(n_neighbors=k, algorithm='ball_tree', metric='pyfunc', func=mydist)
    #mydist = lambda x, y: emd(x, y, distance_matrix)

    if use_EMD:
        #knn = KNeighborsClassifier(n_neighbors=k, algorithm='ball_tree', metric='pyfunc', func=mydist, n_jobs=workers)
        knn = KNeighborsClassifier(n_neighbors=k, algorithm='ball_tree', metric='pyfunc', func=emd_distance, n_jobs=workers, distance_matrix=distance_matrix)
    else:
        knn = KNeighborsClassifier(n_neighbors=k, algorithm='ball_tree', n_jobs=workers)

    knn.fit(training_dataset.data, np.ravel(training_dataset.labels))

    classifier = SymbolClassifier(SymbolClassifier.TypeKNN, knn, training_dataset.label_mapping,
                                  training_dataset.class_mapping, config)

    return classifier


def train_crossvalidation(training_dataset, config, use_EMD, distance_matrix):
    # ... get partitions
    n_partitions = config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
    parts_data, parts_labels, parts_indices = training_dataset.split_data(n_partitions)

    local_training_accuracies = np.zeros(n_partitions)
    local_training_averages = np.zeros(n_partitions)
    local_training_stdevs = np.zeros(n_partitions)

    local_evaluation_accuracies = np.zeros(n_partitions)
    local_evaluation_averages = np.zeros(n_partitions)
    local_evaluation_stdevs = np.zeros(n_partitions)

    label_mapping = training_dataset.label_mapping
    class_mapping = training_dataset.class_mapping

    total_training_time = 0
    total_evaluation_time = 0

    best_classifier_ref = None
    best_accuracy = None

    offset_partition = 0
    all_errors = []
    for i in range(n_partitions):
        print("CROSS-VALIDATION: [" + ("#" * i) + ("-" * (n_partitions - i)) + "]",end="\r")
        # prepare datasets ...
        training_data = np.concatenate(parts_data[:i] + parts_data[i + 1:], axis=0)
        training_labels = np.concatenate(parts_labels[:i] + parts_labels[i + 1:], axis=0)

        testing_data = parts_data[i]
        testing_labels = parts_labels[i]
        testing_indices = parts_indices[i]

        training_dataset = FeatureDataset(training_data, training_labels, label_mapping, class_mapping)
        testing_dataset = FeatureDataset(testing_data, testing_labels, label_mapping, class_mapping)

        start_training_time = time.time()
        #NearestNeighbors(n_neighbors=4, algorithm='ball_tree', metric='pyfunc', func=mydist)

        classifier = train_knn(training_dataset, config, use_EMD, distance_matrix)

        print("CROSS-VALIDATION: [" + ("#" * i) + "+" + ("-" * (n_partitions - i - 1)) + "]",end="\r")

        end_training_time = time.time()
        total_training_time += end_training_time - start_training_time

        start_training_time = time.time()

        # testing ...
        accuracy, per_class_avg, per_class_stdev, errors = EvalOps.classifier_per_class_accuracy(classifier, testing_dataset, chunk_size=10, workers=6)

        print(accuracy)

        local_evaluation_accuracies[i] = accuracy
        local_evaluation_averages[i] = per_class_avg
        local_evaluation_stdevs[i] = per_class_stdev

        # save to complete list of errors
        for idx, expected, predicted, zero in errors:
            expected_class = training_dataset.label_mapping[expected]
            predicted_class = training_dataset.label_mapping[predicted]

            all_errors.append((int(testing_indices[idx]), expected_class, predicted_class, i))

        # training ...
        accuracy, per_class_avg, per_class_stdev, errors = EvalOps.classifier_per_class_accuracy(classifier, training_dataset, chunk_size=10, workers=6)

        local_training_accuracies[i] = accuracy
        local_training_averages[i] = per_class_avg
        local_training_stdevs[i] = per_class_stdev

        end_training_time = time.time()
        total_evaluation_time += end_training_time - start_training_time

        if best_classifier_ref is None or best_accuracy < local_training_accuracies[i]:
            best_classifier_ref = classifier
            best_accuracy = local_training_accuracies[i]

        offset_partition += testing_data.shape[0]

    print("CROSS-VALIDATION: [" + ("#" * n_partitions) + "]")

    training_results = (local_training_accuracies, local_training_averages, local_training_stdevs)
    evaluation_results = (local_evaluation_accuracies, local_evaluation_averages, local_evaluation_stdevs)
    times = (total_training_time, total_evaluation_time)

    return training_results, evaluation_results, times, best_classifier_ref, all_errors


def main():
    # usage check
    if len(sys.argv) < 2:
        print("Usage: python test_knn_emd.py config_file")
        print("Where")
        print("\tconfig_file\t= Path to configuration file")
        return

    print("Loading configuration....")
    config_filename = sys.argv[1]
    config = Configuration.from_file(config_filename)

    # replacement of labels
    if config.contains("REPLACE_LABELS"):
        label_replacement = INKMLLoader.load_label_replacement(config.get("REPLACE_LABELS"))
    else:
        label_replacement = {}

    # check how data partitions will be used for cross-validation
    n_partitions = config.get_int("CLASSIFIER_CROSSVALIDATION_PARTITIONS", -1)
    if n_partitions < 2:
        print("Invalid number of partitions <CLASSIFIER_CROSSVALIDATION_PARTITIONS>")
        return

    # custom parameters for feature extraction
    custom_config = Configuration({})
    custom_config.data["FEATURES_USE_CROSSINGS"] = "0"
    custom_config.data["FEATURES_USE_DIAGONAL_CROSSINGS"] = "0"
    custom_config.data["FEATURES_USE_ANGULAR_CROSSINGS"] = "0"
    custom_config.data["FEATURES_USE_ZONING_CROSSINGS"] = "0"

    custom_config.data["FEATURES_USE_2D_HISTOGRAMS"] = "1"
    custom_config.data["2D_HISTOGRAMS_GRIDS"] = "[[5, 5, 1.0, 1.0, 0.0, 0.0, True]]"
    using_EMD = True

    points = []
    n_rows = 5
    n_cols = 5
    for row in range(n_rows):
        y = -1.0 + (row + 0.5) * (2.0 / n_rows)
        for col in range(n_cols):
            x = -1.0 + (col + 0.5) * (2.0 / n_cols)
            points.append((x, y))

    n_cells = n_cols * n_rows
    distance_matrix = np.zeros((n_cells, n_cells), np.float64)
    for dim1 in range(n_cells):
        x1, y1 = points[dim1]
        for dim2 in range(dim1 + 1, n_cells):
            x2, y2 = points[dim2]

            dist = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

            distance_matrix[dim1, dim2] = dist
            distance_matrix[dim2, dim1] = dist

    # BE SURE TO LIST FEATURES IN THE ORDER IN WHICH THEY APPEAR IN THE FEATURE VECTOR!!!

    custom_config.data["FEATURES_USE_ORIENTATION_HISTOGRAMS"] = "0"
    custom_config.data["FEATURES_USE_CODE_BOOK_HISTOGRAMS"] = "0"
    custom_config.data["FEATURES_USE_TEMPORAL_FEATURES"] = "0"
    custom_config.data["FEATURES_USE_TRACES_DESCRIPTION"] = "0"

    custom_config.data["KNN_K"] = "1"
    custom_config.data["KNN_WORKERS"] = "8"

    # DON'T NORMALIZE FEATURE SETS DATA WHEN USING EMD
    custom_config.data["SCALE_NORMALIZE_DATA"] = "0"
    custom_config.data["CLASSIFIER_CROSSVALIDATION_PARTITIONS"] = "5"
    config.data["SCALE_NORMALIZE_DATA"] = "0"

    # generate dataset
    feature_extractor = FeatureExtractor(custom_config)

    source_path = config.get("TRAINING_DATASET_PATH")
    training, valid_files, error_files = INKMLLoader.load_INKML_directory(source_path, True, feature_extractor,
                                                                          label_replacement, False, None, None, True, 10,
                                                                          None)
    # do cross validation
    # ... run ...!
    print("Training begins....")
    train_r, eval_r, times, best_classifier, eval_errors = train_crossvalidation(training, custom_config, using_EMD, distance_matrix)

    all_training_accuracies, all_training_averages, all_training_stdevs = train_r
    all_validation_accuracies, all_validation_averages, all_validation_stdevs = eval_r
    total_training_time, total_evaluation_time = times

    print("    \t    TRAINING \t \t   TESTING")
    print(" #  \tACC\tC. AVG\tC. STD\tACC\tC. AVG\tC. STD")

    for i in range(n_partitions):
        print(" " + str(i+1) + "\t" +
              str(round(all_training_accuracies[i], 3)) + "\t" +
              str(round(all_training_averages[i], 3)) + "\t" +
              str(round(all_training_stdevs[i], 3)) + "\t" +
              str(round(all_validation_accuracies[i], 3)) + "\t" +
              str(round(all_validation_averages[i], 3)) + "\t" +
              str(round(all_validation_stdevs[i], 3)))

    print(" Mean\t" + str(round(all_training_accuracies.mean(), 3)) + "\t" +
          str(round(all_training_averages.mean(), 3)) + "\t" +
          str(round(all_training_stdevs.mean(), 3)) + "\t" +
          str(round(all_validation_accuracies.mean(), 3)) + "\t" +
          str(round(all_validation_averages.mean(), 3)) + "\t" +
          str(round(all_validation_stdevs.mean(), 3)))

    print(" STDevs\t" + str(round(all_training_accuracies.std(), 3)) + "\t" +
          str(round(all_training_averages.std(), 3)) + "\t" +
          str(round(all_training_stdevs.std(), 3)) + "\t" +
          str(round(all_validation_accuracies.std(), 3)) + "\t" +
          str(round(all_validation_averages.std(), 3)) + "\t" +
          str(round(all_validation_stdevs.std(), 3)))

    print("")
    print("Training finished!")

    print("-> Configuration file: " + config_filename)
    print("-> Training samples: " + str(training.num_samples()))
    print("-> Total features: " + str(training.num_attributes()))
    print("-> Total classes: " + str(training.num_classes()))
    print("-> Total partitions: " + str(n_partitions))
    print("Training Time")
    print(" -> Total: " + str(round(total_training_time, 3)))
    print(" -> Mean: " + str(round(total_training_time / n_partitions, 3)))
    print("Evaluation Time")
    print(" -> Total: " + str(round(total_evaluation_time, 3)))
    print(" -> Mean: " + str(round(total_evaluation_time / n_partitions, 3)))

    print("Finished!")

if __name__ == '__main__':
    main()