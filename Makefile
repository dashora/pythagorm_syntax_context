default:
	@echo "Edit Makefile to define a default action for make."
	@echo ""
	@echo "Try: make example - to run a simple example, or"
	@echo "     make clean - to clean up the example data."

example:
	./myExample

clean:
	rm -f example_file_list
	rm -f -r Results_example_file_list

ex_clean:
	rm -f output/classifiers/*.dat
	rm -f example_file_list
	rm -f -r Results_example_file_list
