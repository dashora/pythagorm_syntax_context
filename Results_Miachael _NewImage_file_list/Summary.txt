LgEval Evaluation Summary
Tue Jan 30 22:48:14 2018

List File: example_file_list

****  PRIMITIVES   **************************************************************

  Directed   Rate(%)     Total   Correct    Errors    SegErr     ClErr    RelErr
---------------------------------------------------------------------------------
     Nodes    100.00     52703     52703         0
     Edges     99.86    945274    943918      1356         0         0      1356

     Total     99.86    997977    996621      1356


Undirected   Rate(%)     Total   Correct    Errors    SegErr     ClErr    RelErr
---------------------------------------------------------------------------------
     Nodes    100.00     52703     52703         0
Node Pairs     99.72    472637    471302      1335         0         0      1335

     Total     99.75    525340    524005      1335

     SegErr: merge/split   ClErr: valid merge class error   RelErr: relation error


****  OBJECTS   **************************************************************************

           Recall(%)   Prec(%) 2RP/(R+P)   Targets   Correct  FalseNeg *Detected *FalsePos
------------------------------------------------------------------------------------------
   Objects    100.00    100.00    100.00     47480     47480         0     47480         0
 + Classes    100.00    100.00    100.00     47480     47480         0     47480         0
 Class/Det    100.00                         47480     47480

 Relations     98.91     98.52     98.72     40442     40002       440     40603       601
 + Classes     98.75     98.35     98.55     40442     39935       507     40603       668
 Class/Det     99.83                         40002     39935

     2RP/(R+P): harmonic mean (f-measure) for (R)ecall and (P)recision
     Class/Det: (correct detection and classification) / correct detection


****  FILES  ***************************************

             Rate(%)     Total   Correct    Errors
---------------------------------------------------
   Objects    100.00      6885      6885         0
 + Classes    100.00      6885      6885         0
 Class/Det    100.00      6885      6885          

 Relations     94.95      6885      6537       348
 + Classes     94.67      6885      6518       367
 Class/Det     99.71      6537      6518          

 Structure     94.95      6885      6537       348
 + Classes     94.67      6885      6518       367    *Final
 Class/Det     99.71      6537      6518          


****  LABEL ERROR HISTOGRAM (Dir. Edges, D_B)  ****

                   0         1         2         3         4         5        >5
----------------------------------------------------------------------------------
Num. Files      6518        27       160        37        55        22        66
Cum. Files      6518      6545      6705      6742      6797      6819      6885

