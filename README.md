# Pythagr^m <br><BR>  Python and Graph-Based Math Recognition System

*Copyright (c) 2014-2017 Kenny Davila, Michael Condon, Lakshmi Ravi, 
	Chinmay Jain, and Richard Zanibbi*  
*Document and Pattern Recognition Lab, Rochester Institute of Technology, USA*  
<https://www.cs.rit.edu/~dprl>

*LgEval and CROHMELib: Copyright (c) 2013-2017 Richard Zanibbi and Harold Mouchere*

## Dependencies (incomplete, as noted):

Python3

python packages (use 'pip' to install):
  - intervaltree
  - cython (version 0.25.2 )  to install, use:
        'pip install cython==0.25.2'
  - scikit-learn (v 0.17.1 or later ... maybe).

doxygen  (for documentation - not a python package!)

## Code Modifications

  - The file 'src/recognizer/recognition/pseudo_binary_parser.py' was
    a missing dependency. Added in from Michael's home directory version.
  
  - Config files: replaced '//' by '/' in file paths (perhaps this was
    a windows requirement?)

## infty Data Set

The 'version 5' ground truth created by Michael Condon for the infty data set is located in the Data/ directory. The set of .lg (label graph) ground truth files are provided, along with a .png image for each expression.

## Getting Started

Issue:

	make

in the main directory, and follow the instructions. See the script file 'example' for details of how these example results are produced. This simple examples recognized just 10 infty images.

## Evaluation Scripts

LgEval and CROHMELib are provided with this package. To install these, go into their subdirectories under the 'evaluation' directory, and follow the installation instructions provided there. **Note that LgEval and CROHMELib use Python 2, while Pythagor^m uses Python 3.**


## Notes on getting started, and the story so far

1. Go into Data/infty/InftyCDB-2 and unzip the zip file to make data infty data
   available for use.

2. Michael has a lot of additional configuration files, python
   programs/scripts and results in his ~/DPRL_SYMBOL_RECOGNIZER directory
   on saskatoon.

   Some of those programs may have been merged and cleaned up; I have
   not added any of that for now, but mention this for future reference.

   I created a configs/ directory to hold the configuration files.


3. As indicated in the src/00_README file, to run the pre-trained system for
   the infty data set, use:

    python full_recognition_script.py configs/full_system_infty.conf 1 1 1

    The '1 1 1' indicates to load pre-trained classifiers, segmenters,
    and parsers.

4. Running the CROHME dataset...TBD. I'm confident that it can be done,
   we will need to figure out how to configure and/or retrain the appropriate
   systems. Lakshmi is still in touch, so hopefully we can figure this out
   on our own, or with her and Micahel's help.

5. Creating documentation:

    Use:

        cd src/doc/
        make

    To generate html and LaTeX documentation in the src/doc directory, using
    doxygen. This automatically extracts the call structure and function
    signatures in the source files.

    This requires the 'doxygen' package to be installed and working, which is
    widely available.

Michael's Notes on Usage and Parameters
-------------------------------------------

These comments pertain to the "src/" directory.

SCRIPTS:

    full_recognition_script:

    Usage: python3 full_recognition_script.py <System Config File> <Load Segmenter> <Load Symbol Classifier> <Load Parser>

	Example: python3 full_recognition_script.py configs/full_recognition_script.conf 1 1 1    # Run *without* training

        System Config File: Location of the recognition system configuration file. This configuration will include basic
            parameters for the entire system, including the location of sub configuration files for the segmenter,
            symbol classifier, and parser.

        Load Parameters: These are boolean values (1 or 0) that indicate if the given expert should be loaded from a
            saved expert file. If the expert is not loaded from a saved source it will be retrained and saved.

CONFIG PARAMETERS:

    Recognition system parameters:

        EXPERT_DIR : Directory location for experts to be saved and loaded.

        EXPERT_LIST : List of expert names used by the system. This is the name of the file in the expert directory.
            Used for both saving and loading experts. (don't include extension of file)

        SUB_CONFIGS_DIR : Directory where config files of the experts for the system are located

        SUB_CONFIGS : Name of the sub configs for the experts are stored. the order should match the order of the expert
            list (ExPERT_LIST).

        OUTPUT_DIR : Directory location where label graph files of recognized expressions will be saved.

    data parameters:
        Used by both the system config, for loading test data, and the expert configs, for loading training datasets.

        TRAINING_DATASET_NAME : Name for the training dataset, mostly if saving dataset.

        TRAINING_DATASET_PATH : Path to the training dataset. For infty this will be a list file of all sample files in
            dataset. If CROHME dataset it will be a directory with all the sample files.

        TESTING_DATASET_NAME = Name for the testing dataset, mostly is saving dataset.

        TESTING_DATASET_PATH = Path to the testing dataset. For infty this will be a list file of all sample files in
            dataset. If CROHME dataset it will be a directory with all the sample files.

        DATASET_DIR = Used if saving or loading an already created dataset. Not really used in full system. Carry over from
            orginal system for symbol recognition.

        INPUT_DATA_LOADER_TYPE : The type of data being recognized. IMG for image data recognition, and INKML for the
            handwritten data recognition.

        GRAPH_LEVEL : The type of primitives used in the recognition. 'primitive' setting uses connected components with
            the image data and strokes with the handwritten data. 'symbol' setting gives symbol level recognition for both,
            these means ground truth segmentation.

        IMAGE_PROCESSING_LG_PATH : Used with image data to point to lg sample file location for both training and testing.
        IMAGE_PROCESSING_IMG_PATH : Used with image data to point to image sample file location for both training and testing.

        IMAGE_PROCESSING_TRACE_TYPE : Used for image processing into strokes (contour or skeleton): you should just set to
            'Contour', this works best.

        IMAGE_PROCESSING_CONTOUR_REDUCTION_TYPE : preprocessing for the traces generated from images. set to 'Smooth'
        IMAGE_PROCESSING_CONTOUR_REDUCTION_VALUE : amount of points to keep from processed image traces. set to '1'
        IMAGE_PROCESSING_CONTOUR_REDUCTION_SMOOTHING_DISTANCE : set to '2'

        PREPROCESS_SMOOTHEN : CATMULL

        NORMALIZE_TRACE_SETS : this should be set to '0' to turn off normalization of traces, not needed for most features,
            which are scale and translation invariant.

        INITIAL_GRAPH_TYPE : The type of graph to start with when segmenting and parsing. 'sight' for line of sight graph,
            and 'full' for a fully connected graph.

    Misc. Parameters:

        DEFAULT_NUMBER_OF_WORKERS : Number of parallel workers for portions of parallel processing.

    Feature Parameters:
        ...

    Classifier Parameters:
        ...


