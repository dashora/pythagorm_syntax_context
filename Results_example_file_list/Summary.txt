LgEval Evaluation Summary
Mon Feb 19 02:14:18 2018

List File: example_file_list

****  PRIMITIVES   **************************************************************

  Directed   Rate(%)     Total   Correct    Errors    SegErr     ClErr    RelErr
---------------------------------------------------------------------------------
     Nodes    100.00     52004     52004         0
     Edges     99.80    911036    909238      1798         0         0      1798

     Total     99.81    963040    961242      1798


Undirected   Rate(%)     Total   Correct    Errors    SegErr     ClErr    RelErr
---------------------------------------------------------------------------------
     Nodes    100.00     52004     52004         0
Node Pairs     99.61    455518    453743      1775         0         0      1775

     Total     99.65    507522    505747      1775

     SegErr: merge/split   ClErr: valid merge class error   RelErr: relation error


****  OBJECTS   **************************************************************************

           Recall(%)   Prec(%) 2RP/(R+P)   Targets   Correct  FalseNeg *Detected *FalsePos
------------------------------------------------------------------------------------------
   Objects    100.00    100.00    100.00     46839     46839         0     46839         0
 + Classes    100.00    100.00    100.00     46839     46839         0     46839         0
 Class/Det    100.00                         46839     46839

 Relations     98.57     98.29     98.43     39915     39343       572     40027       684
 + Classes     98.25     97.97     98.11     39915     39216       699     40027       811
 Class/Det     99.68                         39343     39216

     2RP/(R+P): harmonic mean (f-measure) for (R)ecall and (P)recision
     Class/Det: (correct detection and classification) / correct detection


****  FILES  ***************************************

             Rate(%)     Total   Correct    Errors
---------------------------------------------------
   Objects    100.00      6806      6806         0
 + Classes    100.00      6806      6806         0
 Class/Det    100.00      6806      6806          

 Relations     93.61      6806      6371       435
 + Classes     93.11      6806      6337       469
 Class/Det     99.47      6371      6337          

 Structure     93.61      6806      6371       435
 + Classes     93.11      6806      6337       469    *Final
 Class/Det     99.47      6371      6337          


****  LABEL ERROR HISTOGRAM (Dir. Edges, D_B)  ****

                   0         1         2         3         4         5        >5
----------------------------------------------------------------------------------
Num. Files      6337        39       185        62        71        34        78
Cum. Files      6337      6376      6561      6623      6694      6728      6806

